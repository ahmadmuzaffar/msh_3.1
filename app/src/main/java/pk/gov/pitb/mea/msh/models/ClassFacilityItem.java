package pk.gov.pitb.mea.msh.models;

public class ClassFacilityItem {

	public String fitem_id, fitem_name;

	public ClassFacilityItem() {
		reset();
	}

	public void reset() {
		fitem_id = "";
		fitem_name = "";
	}
}