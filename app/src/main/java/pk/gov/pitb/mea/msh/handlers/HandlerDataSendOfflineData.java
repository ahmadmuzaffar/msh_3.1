package pk.gov.pitb.mea.msh.handlers;


public abstract class HandlerDataSendOfflineData {
	public abstract void onSendComplete(HandlerAction handlerAction);
}
