package pk.gov.pitb.mea.msh.views;

import android.view.View;
import android.widget.CompoundButton;
import android.widget.TableRow;
import android.widget.ToggleButton;

import pk.gov.pitb.mea.msh.R;
import pk.gov.pitb.mea.msh.helpers.MainContainer;

public class CustomToggleButtonTwoState extends ToggleButton implements CompoundButton.OnCheckedChangeListener, View.OnLongClickListener {

	private boolean bIsOdd;
	private boolean bIsCheckChanged = false;
	private int colorBackgroundEmpty = 0;
	private int colorBackgroundChecked = 0;
	private int colorBackgroundUnChecked = 0;

	public CustomToggleButtonTwoState(boolean bIsOdd) {
		super(MainContainer.mContext);
		this.bIsOdd = bIsOdd;
		this.colorBackgroundEmpty = bIsOdd ? MainContainer.mContext.getResources().getColor(R.color.row_body_background_odd) : MainContainer.mContext.getResources().getColor(
				R.color.row_body_background_even);
		changeDrawable(0);
	}
	public void changeDrawable(int type) {
		switch (type) {
			case 3:
				this.colorBackgroundChecked = bIsOdd ? R.drawable.toggle_tick_odd_3 : R.drawable.toggle_tick_even_3;
				this.colorBackgroundUnChecked = bIsOdd ? R.drawable.toggle_cross_odd_3 : R.drawable.toggle_cross_even_3;
			break;
			case 2:
				this.colorBackgroundChecked = bIsOdd ? R.drawable.toggle_tick_odd_2 : R.drawable.toggle_tick_even_2;
				this.colorBackgroundUnChecked = bIsOdd ? R.drawable.toggle_cross_odd : R.drawable.toggle_cross_even;
			break;
			case 1:
			case 0:
			default:
				this.colorBackgroundChecked = bIsOdd ? R.drawable.toggle_tick_odd : R.drawable.toggle_tick_even;
				this.colorBackgroundUnChecked = bIsOdd ? R.drawable.toggle_cross_odd : R.drawable.toggle_cross_even;
			break;
		}
	}

	public void customiseToggleButton(TableRow.LayoutParams paramsToggleButton) {
		try {
			setLayoutParams(paramsToggleButton);
			setBackgroundColor(colorBackgroundEmpty);
			setTextOn("");
			setTextOff("");
			setText("");
			setOnCheckedChangeListener(this);
			setOnLongClickListener(this);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public boolean isCheckChanged() {
		return bIsCheckChanged;
	}
	public void setbIsCheckChanged(boolean bIsCheckChanged) {
		this.bIsCheckChanged = bIsCheckChanged;
	}
	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		try {
			bIsCheckChanged = true;
			if (isChecked) {
				setBackgroundResource(colorBackgroundChecked);
			} else {
				setBackgroundResource(colorBackgroundUnChecked);
			}
			MainContainer.mActivity.getCurrentFocus().clearFocus();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public boolean onLongClick(View v) {
		try {
			setChecked(false);
			setBackgroundResource(0);
			setBackgroundColor(colorBackgroundEmpty);
			bIsCheckChanged = false;
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public void setEnabled(boolean enabled) {
		try {
			super.setEnabled(enabled);
			if (!enabled) {
				setChecked(false);
				setBackgroundResource(0);
				setBackgroundColor(colorBackgroundEmpty);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}