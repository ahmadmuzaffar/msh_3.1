package pk.gov.pitb.mea.msh.handlers;

public interface HandlerFragmentCallBackHepatitis {

	public void onFragmentShown();

	public boolean isFormValid();

	public String onFragmentChanged(int previousPosition);

	public void parseObject();
}