package pk.gov.pitb.mea.msh.fragmentactivities;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import pk.gov.pitb.mea.msh.R;
import pk.gov.pitb.mea.msh.adapters.ArrayAdapterSpinnerItem;
import pk.gov.pitb.mea.msh.dialogs.DialogStaffDetails;
import pk.gov.pitb.mea.msh.dialogs.DialogStaffDetails.HandlerDialogCallBack;
import pk.gov.pitb.mea.msh.handlers.InterfaceFragmentCallBack;
import pk.gov.pitb.mea.msh.helpers.Constants;
import pk.gov.pitb.mea.msh.helpers.MainContainer;
import pk.gov.pitb.mea.msh.helpers.UIHelper;
import pk.gov.pitb.mea.msh.models.ClassScreenData;
import pk.gov.pitb.mea.msh.models.ClassScreenItem;
import pk.gov.pitb.mea.msh.models.ClassStaff;
import pk.gov.pitb.mea.msh.views.CustomEditText;
import pk.gov.pitb.mea.msh.views.CustomSpinnerSingleSelect;

public class FragmentDataonAbsences extends Fragment implements InterfaceFragmentCallBack {

	private View parentView;
	private Button buttonAddNewRow;
	private TableRow.LayoutParams[] paramsArrayTableRowColumns;
	private ArrayList<ClassStaff> arrayListStaff;
	private ArrayList<ClassScreenItem> arrayListDesignations;
	private ArrayList<ClassScreenItem> arrayListTypesOfAbsence;
	private ArrayAdapterSpinnerItem arrayAdapterDesignations;

	private LinearLayout linearLayoutMain;
	@SuppressWarnings("unused")
	private TextView textViewAbsentStaff;
	private TableLayout tableLayoutAbsentStaff;
	private String[] arraySelectedDesignations = {"Gynaecologist", "Anaesthetist" ,"Pediatrician","Lab Technician"};
	List<String> arrayListSelectedDesignations ;
	@Override
	public void parseObject() {
		try {
			JSONArray jsonArrayStaff = new JSONArray();
			for (int i = 1; i < tableLayoutAbsentStaff.getChildCount(); i++) {
				View child = tableLayoutAbsentStaff.getChildAt(i);
				ClassStaff object = arrayListStaff.get(i - 1);
				JSONObject jsonObjectChild = new JSONObject();

				TableRow tableRow = (TableRow) child;
				Spinner designation = (Spinner) tableRow.getChildAt(2);
				Spinner typeOfAbsence = (Spinner) tableRow.getChildAt(3);

				// jsonObjectChild.put("staff_designation_id", object.staff_designation_id);
				// jsonObjectChild.put("staff_role", object.staff_role);
				jsonObjectChild.put("staff_name", object.staff_name);
				jsonObjectChild.put("staff_mobile", object.staff_mobile);
				jsonObjectChild.put("staff_cnic", object.staff_cnic);
				jsonObjectChild.put("comments", object.comments);
				jsonObjectChild.put("as_id", ((ClassScreenItem) designation.getSelectedItem()).item_id);
				jsonObjectChild.put("atoa_id", ((ClassScreenItem) typeOfAbsence.getSelectedItem()).item_id);

				jsonArrayStaff.put(jsonObjectChild);
			}

			JSONObject jsonObject = new JSONObject();
			jsonObject.put("absentees", jsonArrayStaff);
			MainContainer.mJsonObjectFormData.remove(Constants.DB_SCREEN_NAME_DATA_ON_ABSENCES);
			MainContainer.mJsonObjectFormData.put(Constants.DB_SCREEN_NAME_DATA_ON_ABSENCES, jsonObject);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	private void loadSavedData(){
		try {
			if (MainContainer.mJsonObjectFormData.has(Constants.DB_SCREEN_NAME_DATA_ON_ABSENCES)){
				JSONObject jsonObject = MainContainer.mJsonObjectFormData.getJSONObject(Constants.DB_SCREEN_NAME_DATA_ON_ABSENCES);
				JSONArray jsonArray = jsonObject.getJSONArray("absentees");
				for (int i = 0 ; i < jsonArray.length(); i ++){
					JSONObject object = jsonArray.getJSONObject(i);
					ClassStaff classStaff = new ClassStaff();
					classStaff.staff_name = object.getString("staff_name");
					classStaff.staff_mobile = object.getString("staff_mobile");
					classStaff.staff_cnic = object.getString("staff_cnic");
					classStaff.comments = object.getString("comments");
					addTableAbsentStaffType1(classStaff,true);


					View child = tableLayoutAbsentStaff.getChildAt(i+1);

					TableRow tableRow = (TableRow) child;
					EditText name = (EditText) tableRow.getChildAt(1);
					Spinner designation = (Spinner) tableRow.getChildAt(2);
					Spinner typeOfAbsence = (Spinner) tableRow.getChildAt(3);

					name.setText(classStaff.staff_name);
					String designationId = object.getString("as_id");
					String typesOfAbsenceId = object.getString("atoa_id");

					for (int j = 0 ; j < arrayListDesignations.size(); j++){
						if (arrayListDesignations.get(j).item_id.equals(designationId)){
							designation.setSelection(j);
							if (arrayListSelectedDesignations.contains(arrayListDesignations.get(j).item_name.trim())
									&& arrayListStaff.size() <= arrayListSelectedDesignations.size()){
								designation.setEnabled(false);
								name.setOnLongClickListener(new DialogClickListener(null));
							}
							break;
						}
					}

					for (int j = 0 ; j < arrayListTypesOfAbsence.size(); j++){
						if (arrayListTypesOfAbsence.get(j).item_id.equals(typesOfAbsenceId)){
							typeOfAbsence.setSelection(j);
							break;
						}
					}


				}
			}else{

			}
		}
		catch (Exception e){

		}
	}



	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		if (parentView == null) {
			parentView = inflater.inflate(R.layout.fragment_attendance, container, false);
			initializeData();
			generateBody();
			onFragmentShown();
			loadSavedData();
		}
		return parentView;
	}

	private void initializeData() {
		try {
			ClassScreenData objectScreenData = new ClassScreenData();
			objectScreenData.setDefaultValues();
			objectScreenData.screen_id = Constants.DB_SCREEN_ID_DATA_ON_ABSENCES;
			objectScreenData.screen_name = Constants.DB_SCREEN_NAME_DATA_ON_ABSENCES;

			objectScreenData.section_id = "1";
			objectScreenData.section_name = "section1";
			objectScreenData.section_value = "designation_dropdown";
			objectScreenData = MainContainer.mDbAdapter.selectScreenData(objectScreenData);
			arrayListDesignations = MainContainer.mDbAdapter.selectScreenItems(objectScreenData);

			objectScreenData.section_value = "type_of_absence_dropdown";
			objectScreenData = MainContainer.mDbAdapter.selectScreenData(objectScreenData);
			arrayListTypesOfAbsence = MainContainer.mDbAdapter.selectScreenItems(objectScreenData);
			// arrayListStaff = MainContainer.mObjectFacilityData.getListStaff();
			arrayListStaff = new ArrayList<ClassStaff>();

			arrayAdapterDesignations = new ArrayAdapterSpinnerItem(MainContainer.mContext, android.R.layout.simple_list_item_1, arrayListDesignations, true);
			arrayAdapterDesignations.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void generateBody() {
		try {
			linearLayoutMain = (LinearLayout) parentView.findViewById(R.id.linearlayout_main);
			RelativeLayout relativeLayoutAttendance = (RelativeLayout) parentView.findViewById(R.id.relativelayout_attendance);
			buttonAddNewRow = (Button) parentView.findViewById(R.id.button_add_new);
			textViewAbsentStaff = new TextView(MainContainer.mContext);
			tableLayoutAbsentStaff = new TableLayout(MainContainer.mContext);

			UIHelper uiContainer = new UIHelper();
			linearLayoutMain.addView(tableLayoutAbsentStaff);

			RelativeLayout.LayoutParams paramsButtonAddNew = new RelativeLayout.LayoutParams((int) (MainContainer.mScreenWidth * 0.2), (int) (MainContainer.mScreenWidth * 0.2));
			paramsButtonAddNew.setMargins(0, 0, 0, 0);
			paramsButtonAddNew.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
			buttonAddNewRow.setLayoutParams(paramsButtonAddNew);

			paramsArrayTableRowColumns = new TableRow.LayoutParams[4];
			paramsArrayTableRowColumns[0] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.1), TableRow.LayoutParams.MATCH_PARENT);
			paramsArrayTableRowColumns[1] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.30), TableRow.LayoutParams.MATCH_PARENT);
			paramsArrayTableRowColumns[2] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.30), (int) (MainContainer.mScreenWidth * 0.08));
			paramsArrayTableRowColumns[3] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.25), (int) (MainContainer.mScreenWidth * 0.08));
			for (int i = 1; i < paramsArrayTableRowColumns.length; i++) {
				paramsArrayTableRowColumns[i].setMargins(2, 0, 0, 0);
			}

			LinearLayout.LayoutParams paramsRelativeLayout = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
			paramsRelativeLayout.setMargins((int) (MainContainer.mScreenWidth * 0.02), (int) (MainContainer.mScreenHeight * 0.02), (int) (MainContainer.mScreenWidth * 0.02), 0);
			relativeLayoutAttendance.setLayoutParams(paramsRelativeLayout);

			uiContainer.generateTableHeader(tableLayoutAbsentStaff, paramsArrayTableRowColumns, new String[] { "No.", "Name", "Designation", "Doctor Presence" });
			generateTableAbsentStaffType1();

			buttonAddNewRow.setOnClickListener(new DialogStaffDetails(null, new HandlerDialogCallBack() {

				@Override
				public void onButtonPositive() {
					addTableAbsentStaffType1((ClassStaff) buttonAddNewRow.getTag(),true);
					buttonAddNewRow.setTag(null);
				}

				@Override
				public void onButtonNeutral() {
				}

				@Override
				public void onButtonNegative() {
				}
			}));

			arrayListSelectedDesignations = Arrays.asList(arraySelectedDesignations);

			if(!MainContainer.mJsonObjectFormData.has(Constants.DB_SCREEN_NAME_DATA_ON_ABSENCES)){
			for (int i = 0; i < arrayListSelectedDesignations.size(); i++) {
				ClassStaff classStaff = new ClassStaff();
				addTableNewSelectedStaff(classStaff,arrayListSelectedDesignations.get(i));
			}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	private void addTableNewSelectedStaff(ClassStaff object,String selectedDesignation) {
		try {
			arrayListStaff.add(object);
			addTableAbsentStaffRowType1(arrayListStaff.size() - 1,true);
			addTableSelectedStaff(arrayListStaff.size() - 1,selectedDesignation);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	private void addTableSelectedStaff(int index,String selectedDesignation) {
		try {
			View child = tableLayoutAbsentStaff.getChildAt(index + 1);

			TableRow tableRow = (TableRow) child;
			EditText name = (EditText) tableRow.getChildAt(1);
			Spinner designation = (Spinner) tableRow.getChildAt(2);

			for (int j = 0 ; j < arrayListDesignations.size(); j++){
				if (arrayListDesignations.get(j).item_name.trim().equals(selectedDesignation)){
					designation.setSelection(j);
					designation.setEnabled(false);
					name.setOnLongClickListener(new DialogClickListener(null));
					break;
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void generateTableAbsentStaffType1() {
		for (int index = 0; index < arrayListStaff.size(); index++) {
			addTableAbsentStaffRowType1(index,true);
		}
	}

	private void addTableAbsentStaffType1(ClassStaff object,boolean isDesignationEnable) {
		try {
			arrayListStaff.add(object);
			addTableAbsentStaffRowType1(arrayListStaff.size() - 1,isDesignationEnable);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void addTableAbsentStaffRowType1(int index,boolean isDesignationEnable) {
		try {
			int colorBgTableRow = MainContainer.mContext.getResources().getColor(R.color.row_background);
			boolean bisOdd = index % 2 == 0;

			TableRow tableRow = new TableRow(MainContainer.mContext);
			CustomEditText editTextSerialNum = new CustomEditText(bisOdd, "" + (index + 1));
			CustomEditText editTextName = new CustomEditText(bisOdd, arrayListStaff.get(index).staff_name);
			CustomSpinnerSingleSelect spinnerDesignation = new CustomSpinnerSingleSelect(bisOdd, 5);
			CustomSpinnerSingleSelect spinnerTypeOfAbsence = new CustomSpinnerSingleSelect(bisOdd, 4);

			editTextName.setHint("Not Available");
			editTextSerialNum.customiseEditText(paramsArrayTableRowColumns[0]);
			editTextName.customiseEditText(paramsArrayTableRowColumns[1]);
			spinnerDesignation.customiseSpinner(paramsArrayTableRowColumns[2]);
			spinnerTypeOfAbsence.customiseSpinner(paramsArrayTableRowColumns[3]);
			editTextName.setCursorVisible(false);
			editTextName.setFocusableInTouchMode(false);
			tableRow.setBackgroundColor(colorBgTableRow);
			tableLayoutAbsentStaff.addView(tableRow);

			spinnerDesignation.setAdapterItem(arrayListDesignations, arrayAdapterDesignations);
			spinnerTypeOfAbsence.setAdapterItem(arrayListTypesOfAbsence);

			tableRow.addView(editTextSerialNum);
			tableRow.addView(editTextName);
			tableRow.addView(spinnerDesignation);
			tableRow.addView(spinnerTypeOfAbsence);

			editTextName.setTag(arrayListStaff.get(index));
			spinnerTypeOfAbsence.setOnItemSelectedListener(onAbsenceOptionSelected);
			editTextName.setOnClickListener(new DialogStaffDetails(spinnerTypeOfAbsence, null));
			editTextName.setOnLongClickListener(new DialogClickListener(editTextName));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private synchronized void updateTable() {
		for (int i = 1; i < tableLayoutAbsentStaff.getChildCount(); i++) {
			boolean bIsOdd = i % 2 != 0;
			View child = tableLayoutAbsentStaff.getChildAt(i);
			TableRow tableRow = (TableRow) child;
			((EditText) tableRow.getChildAt(0)).setText("" + i);
			((CustomEditText) tableRow.getChildAt(0)).changeBackground(bIsOdd);
			((CustomEditText) tableRow.getChildAt(1)).changeBackground(bIsOdd);
			((CustomSpinnerSingleSelect) tableRow.getChildAt(2)).changeBackground(bIsOdd, 5);
			((CustomSpinnerSingleSelect) tableRow.getChildAt(3)).changeBackground(bIsOdd, 4);
		}
	}

	private AdapterView.OnItemSelectedListener onAbsenceOptionSelected = new AdapterView.OnItemSelectedListener() {

		@Override
		public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
			EditText editText = (EditText) ((TableRow) parent.getParent()).getChildAt(1);
			ClassStaff objectStaff = (ClassStaff) editText.getTag();
			if (objectStaff != null) {
				objectStaff.bIsCommentsReqd = MainContainer.isCommentsRequired((ClassScreenItem) parent.getSelectedItem());
				if (!objectStaff.bIsCommentsReqd) {
					objectStaff.comments = "";
					editText.setTag(objectStaff);
				} else if (objectStaff.comments.length() == 0) {
					(new DialogStaffDetails((Spinner) parent, null)).onClick(editText);
				}
			}
		}

		@Override
		public void onNothingSelected(AdapterView<?> arg0) {
		}
	};

	private class DialogClickListener implements DialogInterface.OnClickListener, View.OnLongClickListener {

		private View view;

		public DialogClickListener(View view) {
			this.view = view;
		}

		@Override
		public void onClick(DialogInterface dialog, int id) {
			try {
				ClassStaff objectStaff = (ClassStaff) view.getTag();
				tableLayoutAbsentStaff.removeView((TableRow) view.getParent());
				arrayListStaff.remove(objectStaff);
				updateTable();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		@Override
		public boolean onLongClick(View view) {
			try {
				AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
				builder.setTitle("Delete " + ((ClassStaff) view.getTag()).staff_name);
				builder.setMessage("Are you sure you want to delete?");
				builder.setPositiveButton("Yes", this);
				builder.setNegativeButton("No", null);
				builder.create().show();
			} catch (Exception e) {
				e.printStackTrace();
			}
			return true;
		}

	}

	@Override
	public void onFragmentShown() {
		try {
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private String errorMessage;

	@Override
	public String onFragmentChanged(int previousPosition) {
		errorMessage = null;
		isFormValid();
		return errorMessage;
	}

	@Override
	public boolean isFormValid() {
		try {
			boolean bIsValid = true;
			for (int i = 0; i < arrayListStaff.size(); i++) {
				ClassStaff object = arrayListStaff.get(i);
				if (object.staff_name.length() == 0) {
					bIsValid = false;
				} else if (object.bIsCommentsReqd && object.comments.length() == 0) {
					bIsValid = false;
				}
			}
			if (!bIsValid) {
				errorMessage = "Please fill all Staff details to continue";
			}
			return bIsValid;
		} catch (Exception e) {
			e.printStackTrace();
			MainContainer.showMessage("Error Processing Your Request");
			return false;
		}
	}
}