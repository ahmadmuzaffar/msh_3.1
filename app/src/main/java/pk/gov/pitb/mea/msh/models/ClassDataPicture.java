package pk.gov.pitb.mea.msh.models;

import pk.gov.pitb.mea.msh.helpers.Constants;

public class ClassDataPicture {

	public int pk_id, fk_id, id_enable, id_disable;
	public String picture_title, picture_name, picture_parameter, picture_status;
	public byte[] picture_data;

	public ClassDataPicture() {
		reset();
	}

	public void reset() {
		pk_id = fk_id = -1;
		picture_title = picture_name = picture_parameter = null;
		picture_status = Constants.PICTURE_STATUS_UNSENT;
		picture_data = null;
	}
}