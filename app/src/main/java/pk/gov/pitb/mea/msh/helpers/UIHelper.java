package pk.gov.pitb.mea.msh.helpers;

import pk.gov.pitb.mea.msh.R;
import pk.gov.pitb.mea.msh.views.CustomEditText;
import android.view.Gravity;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class UIHelper {

	private int marginLeft, marginRight, marginTop, marginBottom;

	public UIHelper() {
		marginLeft = marginRight = (int) (MainContainer.mScreenWidth * 0.02);
		marginTop = marginBottom = (int) (MainContainer.mScreenHeight * 0.02);
	}

	public void generateTableLabel(TextView textViewLabel, String heading) {
		generateTableLabel(textViewLabel, heading, true);
	}

	public void generateTableLabel(TextView textViewLabel, String heading, boolean isFirst) {
		try {
			LinearLayout.LayoutParams paramsTextView = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
			if (isFirst) {
				paramsTextView.setMargins(marginLeft, marginTop, marginRight, 0);
			} else {
				paramsTextView.setMargins(marginLeft, 0, marginRight, 0);
			}
			textViewLabel.setLayoutParams(paramsTextView);
			textViewLabel.setText(heading);
			textViewLabel.setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void generateTableLabelNoMargin(TextView textViewLabel, String heading) {
		try {
			LinearLayout.LayoutParams paramsTextView = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
			paramsTextView.setMargins(0, marginTop, 0, 0);
			textViewLabel.setLayoutParams(paramsTextView);
			textViewLabel.setText(heading);
			textViewLabel.setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void generateTableHeader(TableLayout tableLayout, TableRow.LayoutParams[] paramsArrayTableRowColumns, String... listHeadings) {
		try {
			tableLayout.setStretchAllColumns(true);
			LinearLayout.LayoutParams paramsTableLayout = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
			paramsTableLayout.setMargins(marginLeft, marginTop, marginRight, marginBottom);
			tableLayout.setLayoutParams(paramsTableLayout);
			tableLayout.setPadding(0, 0, 0, 0);
			int colorBgTableRow = MainContainer.mContext.getResources().getColor(R.color.row_background);
			TableRow tableRow = new TableRow(MainContainer.mContext);
			tableRow.setBackgroundColor(colorBgTableRow);
			tableLayout.addView(tableRow);
			for (int i = 0; i < listHeadings.length; i++) {
				CustomEditText editTextHeading = new CustomEditText(listHeadings[i]);
				editTextHeading.customiseEditText(paramsArrayTableRowColumns[i]);
				editTextHeading.setSingleLine(false);
				tableRow.addView(editTextHeading);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void generateTableHeaderNew(TableLayout tableLayout, TableRow.LayoutParams[] paramsArrayTableRowColumns, String... listHeadings) {
		try {
			tableLayout.setStretchAllColumns(true);
			LinearLayout.LayoutParams paramsTableLayout = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
					LinearLayout.LayoutParams.WRAP_CONTENT);
			paramsTableLayout.setMargins(0, 0, 0, 0);
			tableLayout.setLayoutParams(paramsTableLayout);
			tableLayout.setPadding(0, 0, 0, 0);
			int colorBgTableRow = MainContainer.mContext.getResources().getColor(R.color.row_background);
			TableRow tableRow = new TableRow(MainContainer.mContext);
			tableRow.setBackgroundColor(colorBgTableRow);
			tableLayout.addView(tableRow);
			for (int i = 0; i < listHeadings.length; i++) {
				CustomEditText editTextHeading = new CustomEditText(listHeadings[i]);
				editTextHeading.customiseEditText(paramsArrayTableRowColumns[i]);
				editTextHeading.setSingleLine(false);
				tableRow.addView(editTextHeading);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}