package pk.gov.pitb.mea.msh.helpers;


import android.app.TimePickerDialog;
import android.content.Context;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;

import java.util.Calendar;

public class OnClickShowTimePicker implements View.OnClickListener {

    private final String FORMAT_DATE = "dd-MM-yyyy";
    private Context context;
    private EditText editText;
    private DatePicker datePicker;
    private String title;

    public OnClickShowTimePicker(Context context, EditText editText, String title) {
        this.editText = editText;
        this.context = context;
        this.title = title;
        this.editText.setClickable(false);
        this.editText.setFocusable(false);
        this.editText.setFocusableInTouchMode(false);
    }

    public void onClick(View v) {
        showDateDialog();
    }

    private void showDateDialog() {
        try {
            Calendar mcurrentTime = Calendar.getInstance();
            int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
            int minute = mcurrentTime.get(Calendar.MINUTE);
            TimePickerDialog mTimePicker;
            mTimePicker = new TimePickerDialog(context, new TimePickerDialog.OnTimeSetListener() {
                @Override
                public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                    editText.setText( selectedHour + ":" + selectedMinute);
                }
            }, hour, minute, true);//Yes 24 hour time
            mTimePicker.setTitle("Select Time");
            mTimePicker.show();
            //editText.setText(date);
            editText.setError(null);





           /* String previousDate = editText.getText().toString();
            Calendar calendar = Calendar.getInstance();
            if (previousDate.length() > 0) {
                calendar.setTime((new SimpleDateFormat(FORMAT_DATE, Locale.ENGLISH)).parse(previousDate));
            }
            datePicker = new DatePicker(context);
            datePicker.updateDate(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle(title);
            builder.setView(datePicker);
            builder.setPositiveButton("OK", new OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    *//*SimpleDateFormat dateFormat = new SimpleDateFormat(FORMAT_DATE, Locale.ENGLISH);
                    Calendar calendar = Calendar.getInstance();
                    calendar.set(Calendar.YEAR, datePicker.getYear());
                    calendar.set(Calendar.MONTH, datePicker.getMonth());
                    calendar.set(Calendar.DAY_OF_MONTH, datePicker.getDayOfMonth());
                    String date = dateFormat.format(calendar.getTime());*//*
                    Calendar mcurrentTime = Calendar.getInstance();
                    int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                    int minute = mcurrentTime.get(Calendar.MINUTE);
                    TimePickerDialog mTimePicker;
                    mTimePicker = new TimePickerDialog(context, new TimePickerDialog.OnTimeSetListener() {
                        @Override
                        public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                            editText.setText( selectedHour + ":" + selectedMinute);
                        }
                    }, hour, minute, true);//Yes 24 hour time
                    mTimePicker.setTitle("Select Time");
                    mTimePicker.show();
                    //editText.setText(date);
                    editText.setError(null);
                }
            });
            builder.setNegativeButton("Cancel", new OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                }
            });
            builder.create();
            builder.show();*/
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}