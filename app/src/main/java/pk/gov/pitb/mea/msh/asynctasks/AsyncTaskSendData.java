package pk.gov.pitb.mea.msh.asynctasks;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import pk.gov.pitb.mea.msh.ActivityMain;
import pk.gov.pitb.mea.msh.R;
import pk.gov.pitb.mea.msh.bo.ScreenFragment;
import pk.gov.pitb.mea.msh.database.SharedPreferencesEditor;
import pk.gov.pitb.mea.msh.handlers.HandlerAction;
import pk.gov.pitb.mea.msh.handlers.InterfaceFragmentCallBack;
import pk.gov.pitb.mea.msh.helpers.Constants;
import pk.gov.pitb.mea.msh.helpers.LocationTracker;
import pk.gov.pitb.mea.msh.helpers.MainContainer;
import pk.gov.pitb.mea.msh.httpworker.HttpRequestSubmit;
import pk.gov.pitb.mea.msh.models.ClassSentData;

public class AsyncTaskSendData extends AsyncTask<Void, Void, Void> {

	private LocationTracker locationTracker;
	private ProgressDialog uProgressDialog;
	private ArrayList<ScreenFragment> arrayListScreenTitles;
	private SharedPreferencesEditor sharedPreferencesEditor;
	private HttpRequestSubmit httpRequestSubmit;
	private HandlerAction handlerAction;
	private ClassSentData objectSentData;

	public AsyncTaskSendData(LocationTracker locationTracker, ArrayList<ScreenFragment> arrayListScreenTitles) {
		super();
		this.arrayListScreenTitles = arrayListScreenTitles;
		this.locationTracker = locationTracker;
		this.sharedPreferencesEditor = new SharedPreferencesEditor(MainContainer.mContext);
		this.httpRequestSubmit = new HttpRequestSubmit();
		this.objectSentData = new ClassSentData();
	}

	public void execute() throws Exception {
		execute((Void) null);
	}

	@Override
	protected void onPreExecute() {
		try {
			uProgressDialog = new ProgressDialog(MainContainer.mContext);
			uProgressDialog.setCancelable(false);
			uProgressDialog.setTitle("Sending Data");
			uProgressDialog.setMessage("Please Wait...");
			uProgressDialog.setIndeterminate(true);
			uProgressDialog.show();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	protected Void doInBackground(Void... params) {
		try {
			if (arrayListScreenTitles != null) {
				Fragment objFragment = null;
				String screenTag = null;
				for (int i = 0; i < arrayListScreenTitles.size(); i++) {
					screenTag = MainContainer.getScreenTag(arrayListScreenTitles.get(i).name);
					objFragment = MainContainer.mActivity.getFragmentManager().findFragmentByTag(screenTag);
					if (objFragment != null || MainContainer.arrayListOpenedActivities.contains(screenTag)) {
						InterfaceFragmentCallBack obj = (InterfaceFragmentCallBack) objFragment;
						obj.parseObject();
					}
				}
			}
			if (!locationTracker.isLocationSet()) {
				locationTracker.getAccurateLocation();
			}
			SimpleDateFormat sdfDateServer = new SimpleDateFormat(Constants.FORMAT_DATE_SERVER, MainContainer.mLocale);
			SimpleDateFormat sdfDateApp = new SimpleDateFormat(Constants.FORMAT_DATE_APP, MainContainer.mLocale);
			Date dateCurrent = new Date();

			JSONObject jsonObjectAppDetails = new JSONObject();
			jsonObjectAppDetails.put("facility_type_code", MainContainer.mObjectFacility.type_id);
			jsonObjectAppDetails.put("facility_id", MainContainer.mObjectFacility.facility_id);
			jsonObjectAppDetails.put("tehsil_id", MainContainer.mObjectFacility.tehsil_id);
			jsonObjectAppDetails.put("district_id", MainContainer.mObjectFacility.district_id);
			jsonObjectAppDetails.put("name_of_incharge", MainContainer.mObjectFacility.incharge_name);
			jsonObjectAppDetails.put("phone_number", MainContainer.mObjectFacility.phone_number);
			jsonObjectAppDetails.put("visit_date", sdfDateServer.format(dateCurrent));
			jsonObjectAppDetails.put("gps_location", locationTracker.getBetterLocation());
			jsonObjectAppDetails.put("imei_number", MainContainer.mImeiNumber);
			jsonObjectAppDetails.put("form_type", MainContainer.mObjectFacility.form_type);
			jsonObjectAppDetails.put("app_version_no", MainContainer.mContext.getResources().getString(R.string.app_version_name));
			jsonObjectAppDetails.put("facility_version", sharedPreferencesEditor.getFacilityVersion());
			jsonObjectAppDetails.put("structural_version", sharedPreferencesEditor.getStructuralVersion());
			MainContainer.mJsonObjectFormData.put("app_details", jsonObjectAppDetails);

			MainContainer.mObjectActivity.setEmptyValues();
			MainContainer.mObjectActivity.getDefaultAppJSONObject(locationTracker.getNetworkLocation(), locationTracker.getGpsLocation());
			MainContainer.mJsonObjectFormData.put("default_app_details", MainContainer.mObjectActivity.getDefaultAppJSONObject(locationTracker.getGpsLocation(), locationTracker.getNetworkLocation()));

			MainContainer.mObjectActivity.app_version_no = MainContainer.mContext.getResources().getString(R.string.app_version_name);
			MainContainer.mObjectActivity.sending_date_time = jsonObjectAppDetails.getString("visit_date");
			MainContainer.mObjectActivity.json_data = MainContainer.mJsonObjectFormData.toString();

			if(MainContainer.bIsActivityHealthCouncil)
				MainContainer.mObjectActivity.activity_type = Constants.AT_HEALTH_COUNCIL;
			else if(MainContainer.bIsActivityNutrients)
				MainContainer.mObjectActivity.activity_type = Constants.AT_NUTRIENTS;
			else if(MainContainer.bIsActivityHepatitis)
				MainContainer.mObjectActivity.activity_type = Constants.AT_HEPATITIS;
			else if(MainContainer.bIsActivityPatientInterviews)
				MainContainer.mObjectActivity.activity_type = Constants.AT_PATIENT_INTERVIEWS;
			else
				MainContainer.mObjectActivity.activity_type = Constants.AT_MONITORING;

			@SuppressWarnings("unused")
			String testingJSONString = MainContainer.mObjectActivity.json_data;
			handlerAction = httpRequestSubmit.httpRequestSubmitData(MainContainer.mObjectActivity);

			objectSentData.facility_id = jsonObjectAppDetails.getString("facility_id");
			objectSentData.sending_date_time = jsonObjectAppDetails.getString(sdfDateApp.format(dateCurrent));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	protected void onPostExecute(Void result) {
		try {
			if (uProgressDialog != null && uProgressDialog.isShowing()) {
				uProgressDialog.dismiss();
			}
			switch (handlerAction.handlerActionCode) {
			case Constants.AC_HTTP_REQUEST_SUCCESS:
				handlerAction.handlerActionString = "Visit Added Successfully";
				//MainContainer.mObjectActivity.activity_type = Constants.AT_SENT;
				break;
			case Constants.AC_HTTP_REQUEST_EXCEPTION:
				handlerAction.handlerActionString = "Server error. Data saved in device, will be sent later";
				//MainContainer.mObjectActivity.activity_type = Constants.AT_UNSENT;
				MainContainer.mDbAdapter.insertActivity(MainContainer.mObjectActivity);
				break;
			case Constants.AC_INTERNET_NOT_CONNECTED:
				handlerAction.handlerActionString = "Internet error. Data saved in device.\nPlease connect to the Internet and try again later";
				//MainContainer.mObjectActivity.activity_type = Constants.AT_UNSENT;
				MainContainer.mDbAdapter.insertActivity(MainContainer.mObjectActivity);
				break;
			case Constants.AC_INTERNET_NOT_WORKING:
				handlerAction.handlerActionString = "Internet not available. Data saved in device.\nPlease check your Internet settings and try again later";
				//MainContainer.mObjectActivity.activity_type = Constants.AT_UNSENT;
				MainContainer.mDbAdapter.insertActivity(MainContainer.mObjectActivity);
				break;
			}
			MainContainer.mDbAdapter.insertSentData(objectSentData);
			MainContainer.mDbAdapter.deleteSavedPictures();
			AlertDialog.Builder builder = new AlertDialog.Builder(MainContainer.mContext);
			builder.setMessage(handlerAction.handlerActionString);
			builder.setCancelable(false);
			builder.setPositiveButton("Okay", new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					if (!Constants.bDebuging) {
						MainContainer.resetMainContainer();
						Intent intent = new Intent(MainContainer.mActivity, ActivityMain.class);
						intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
						MainContainer.mActivity.startActivity(intent);
						MainContainer.mActivity.finish();
					}
				}
			});
			builder.create().show();
			uProgressDialog.dismiss();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
