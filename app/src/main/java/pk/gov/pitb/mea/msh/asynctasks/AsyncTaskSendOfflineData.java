package pk.gov.pitb.mea.msh.asynctasks;

import java.util.ArrayList;

import pk.gov.pitb.mea.msh.handlers.HandlerAction;
import pk.gov.pitb.mea.msh.handlers.HandlerDataSendOfflineData;
import pk.gov.pitb.mea.msh.helpers.Constants;
import pk.gov.pitb.mea.msh.helpers.MainContainer;
import pk.gov.pitb.mea.msh.httpworker.HttpRequestSubmit;
import pk.gov.pitb.mea.msh.models.ClassDataActivity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.widget.Toast;

public class AsyncTaskSendOfflineData extends AsyncTask<Void, Void, Void> {

	private ArrayList<ClassDataActivity> arrayListDataUnsent;
	private HandlerDataSendOfflineData handlerDataSendOfflineData;
	private HandlerAction handlerAction;
	private HttpRequestSubmit httpRequestSubmit;
	private ProgressDialog uProgressDialog;

	public AsyncTaskSendOfflineData(ArrayList<ClassDataActivity> arrayListDataUnsent, HandlerDataSendOfflineData handlerDataSendOfflineData) {
		super();
		this.arrayListDataUnsent = arrayListDataUnsent;
		this.handlerDataSendOfflineData = handlerDataSendOfflineData;
		httpRequestSubmit = new HttpRequestSubmit();
	}

	public void execute() throws Exception {
		execute((Void) null);
	}

	@Override
	protected void onPreExecute() {
		try {
			uProgressDialog = new ProgressDialog(MainContainer.mContext);
			uProgressDialog.setCancelable(false);
			uProgressDialog.setTitle("Sending Offline Data");
			uProgressDialog.setMessage("Please Wait...");
			uProgressDialog.setIndeterminate(true);
			uProgressDialog.show();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	protected Void doInBackground(Void... params) {
		try {
			for (ClassDataActivity objectDataActivity : arrayListDataUnsent) {
				handlerAction = httpRequestSubmit.httpRequestSubmitData(objectDataActivity);
				if (handlerAction.handlerActionCode == Constants.AC_HTTP_REQUEST_SUCCESS) {
					MainContainer.mDbAdapter.deleteActivity(objectDataActivity.pk_id);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	protected void onPostExecute(Void result) {
		try {
			handlerDataSendOfflineData.onSendComplete(handlerAction);
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (uProgressDialog != null && uProgressDialog.isShowing()) {
			uProgressDialog.dismiss();
		}
	}
}