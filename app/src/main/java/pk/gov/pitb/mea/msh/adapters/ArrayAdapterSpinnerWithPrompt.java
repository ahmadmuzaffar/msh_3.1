package pk.gov.pitb.mea.msh.adapters;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Typeface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class ArrayAdapterSpinnerWithPrompt extends ArrayAdapter<String> {

	private int colorBackground;
	private int colorText;

	public ArrayAdapterSpinnerWithPrompt(Context context, String[] objects, int colorText, int colorBackground) {
		super(context, android.R.layout.simple_spinner_item, objects);
		this.colorBackground = colorBackground;
		this.colorText = colorText;
	}

	public ArrayAdapterSpinnerWithPrompt(Context context, ArrayList<String> objects, int colorText, int colorBackground) {
		super(context, android.R.layout.simple_spinner_item, objects);
		this.colorBackground = colorBackground;
		this.colorText = colorText;
	}

	@Override
	public TextView getDropDownView(int position, View convertView, ViewGroup parent) {
		TextView textView = (TextView) super.getDropDownView(position, convertView, parent);
		if (textView != null) {
			textView.setTextColor(colorText);
			textView.setHintTextColor(colorText);
			if (colorBackground != -1) {
				textView.setBackgroundColor(colorBackground);
			}
			if (position == 0) {
				textView.setTypeface(null, Typeface.BOLD);
				textView.setHint(textView.getText().toString());
				// textView.setText("");
			} else {
				textView.setTypeface(null, Typeface.NORMAL);
			}
		}
		return textView;
	}

	@Override
	public View getView(int position, View view, ViewGroup parent) {
		view = super.getView(position, view, parent);
		if (view != null) {
			TextView textView = (TextView) view;
			textView.setTextColor(colorText);
			textView.setHintTextColor(colorText);
			// if (position == 0) {
			// textView.setHint(textView.getText().toString());
			// textView.setText("");
			// }
		}
		return view;
	}

	@Override
	public boolean isEnabled(int position) {
		return position == 0 ? false : true;
	}
}