package pk.gov.pitb.mea.msh.textwatchers;

import pk.gov.pitb.mea.msh.helpers.MainContainer;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
import android.widget.Toast;

public class TextWatcherCountSingleChild implements TextWatcher {

	private EditText editTextParent = null;
	private EditText editTextChild = null;
	private String editTextCurrentName = null;
	private String editTextParentName = null;
	private final String errorMessageCountExceed = "%s cannot be more than %s";
	private final String errorMessageCountParentEmpty = "%s cannot be entered before %s";

	public TextWatcherCountSingleChild(EditText editTextParent, EditText editTextChild, String editTextCurrentName, String editTextParentName) {
		this.editTextParent = editTextParent;
		this.editTextChild = editTextChild;
		this.editTextCurrentName = editTextCurrentName;
		this.editTextParentName = editTextParentName;
	}

	public void beforeTextChanged(CharSequence s, int start, int count, int after) {
	}

	public void onTextChanged(CharSequence s, int start, int before, int count) {
		if (editTextCurrentName == null) {
			editTextChild.setText("");
		} else {
			String text = this.editTextParent.getText().toString().trim();
			if (text.length() > 0) {
				text = editTextChild.getText().toString().trim();
				if (text.length() > 0) {
					int current = text.length() > 0 ? Integer.parseInt(text) : 0;
					int max = Integer.parseInt(editTextParent.getText().toString());
					if (current > max) {
						Toast.makeText(MainContainer.mContext, String.format(MainContainer.mLocale, errorMessageCountExceed, editTextCurrentName, editTextParentName), Toast.LENGTH_SHORT).show();
						current = max;
						editTextChild.setText("" + current);
						editTextChild.setSelection(editTextChild.getText().toString().length());
					}
				}
			} else if (editTextChild.getText().toString().trim().length() > 0) {
				Toast.makeText(MainContainer.mContext, String.format(MainContainer.mLocale, errorMessageCountParentEmpty, editTextCurrentName, editTextParentName), Toast.LENGTH_SHORT).show();
				editTextChild.setText("");
			}
		}
	}

	public void afterTextChanged(Editable s) {
	}
}