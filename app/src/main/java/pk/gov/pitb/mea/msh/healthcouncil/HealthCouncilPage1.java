package pk.gov.pitb.mea.msh.healthcouncil;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.StateListDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Calendar;

import pk.gov.pitb.mea.msh.R;
import pk.gov.pitb.mea.msh.handlers.HandlerFragmentCallBackHealthCouncil;
import pk.gov.pitb.mea.msh.helpers.AlertDialogs;
import pk.gov.pitb.mea.msh.helpers.Constants;
import pk.gov.pitb.mea.msh.helpers.MainContainer;
import pk.gov.pitb.mea.msh.helpers.OnClickShowDatePicker;
import pk.gov.pitb.mea.msh.helpers.Utilities;
import pk.gov.pitb.mea.msh.models.ClassPicture;


/**
 * Created by Muhammad Abdullah Azam Khan on 10/27/2016.
 */
public class HealthCouncilPage1 extends Fragment implements HandlerFragmentCallBackHealthCouncil {
    private View parentView;
    /*private Globals MainContainer;*/
    private EditText etDhisCode, notificationNumber, date;
    private RadioGroup radioGroup, radioGroupRecord;
    private RadioButton radioButtonYes, radioButtonNo, radioButtonRecordYes, radioButtonRecordNo;
    private Button buttonProceed;
    private TableLayout tableLayout;
    private TableRow.LayoutParams lpEditTextSmall;
    private String errorMessage = "Please fill all fields";
    private ClassPicture picture_1 = null;
    private Button buttonPrevious, buttonNext, buttonSubmit;


    private ClassPicture PICTURE_GENRAL = null;
    private Bitmap decodedByte1 = null, decodedByte2 = null, decodedByte3 = null;
    private String label1 = "", label2 = "", label3 = "";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (parentView == null) {
            parentView = inflater.inflate(R.layout.health_council_page1, container, false);
            /* MainContainer = Globals.getInstance();*/
            initFragmentData();
            generateBody();
            if (MainContainer.mJsonObjectFormData.has(Constants.DB_SCREEN_NAME_HEALTH_COUNCIL_PAGE1)) {
                JSONObject jsonObject = null;
                try {
                    jsonObject = MainContainer.mJsonObjectFormData.getJSONObject(Constants.DB_SCREEN_NAME_HEALTH_COUNCIL_PAGE1);
                    etDhisCode.setText(jsonObject.has("dhis_code") ? jsonObject.getString("dhis_code") : "");
                    if (jsonObject.getString("health_council_notified").equals("yes")) {
                        radioButtonYes.setChecked(true);
                    } else if (jsonObject.getString("health_council_notified").equals("no")) {
                        radioButtonNo.setChecked(true);
                    }

                    if (jsonObject.getString("focal_person_available").equals("yes")) {
                        ((RadioButton) radioGroupRecord.getChildAt(0)).setChecked(true);
                    } else if (jsonObject.getString("focal_person_available").equals("no")) {
                        ((RadioButton) radioGroupRecord.getChildAt(1)).setChecked(true);
                    }
                    notificationNumber.setText(jsonObject.getString("notification_number"));
                    date.setText(jsonObject.getString("date"));

                    String pictureString = jsonObject.has("notification_picture") ? jsonObject.getString("notification_picture") : "";
                    if (!pictureString.equals("N/A")) {
                        loadPicture(pictureString);
                    } else if (pictureString.equals("N/A")) {
                        ((RadioButton) ((RadioGroup) tableLayout.getChildAt(3)).getChildAt(1)).setChecked(true);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

        }
        return parentView;
    }

    private void initFragmentData() {
    }

    private void generateBody() {
        etDhisCode = (EditText) parentView.findViewById(R.id.edittext_dhis_code_page1);
        radioGroupRecord = (RadioGroup) parentView.findViewById(R.id.radiogroup_record_page1);
        radioGroup = (RadioGroup) parentView.findViewById(R.id.radiogroup_heath_council_page1);
        radioButtonYes = (RadioButton) parentView.findViewById(R.id.radiobutton_heath_council_page1_yes);
        radioButtonNo = (RadioButton) parentView.findViewById(R.id.radiobutton_heath_council_page1_no);
        radioButtonRecordYes = (RadioButton) parentView.findViewById(R.id.radiobutton_record_page1_yes);
        radioButtonRecordNo = (RadioButton) parentView.findViewById(R.id.radiobutton_record_page1_no);
        tableLayout = (TableLayout) parentView.findViewById(R.id.table_layout_health_council_page1);


        buttonNext = (Button) parentView.findViewById(R.id.button_next);
        buttonSubmit = (Button) parentView.findViewById(R.id.button_submit);
        buttonPrevious = (Button) parentView.findViewById(R.id.button_previous);
        buttonPrevious.setVisibility(View.GONE);

        int[] margins = new int[]{(int) (MainContainer.mScreenWidth * 0.05), (int) (MainContainer.mScreenWidth * 0.02), (int) (MainContainer.mScreenWidth * 0.05), 0};

        picture_1 = new ClassPicture(R.drawable.image_take_picture_enable, R.drawable.image_take_picture_disable, (int) (MainContainer.mScreenWidth * 0.44),
                (int) (MainContainer.mScreenWidth * 0.44), "Picture 1", "picture_1", margins);
        addPicture(picture_1, null, "Notification?");


        notificationNumber = (EditText) parentView.findViewById(R.id.edittext_notification_number);
        date = (EditText) parentView.findViewById(R.id.date_health_council_page1);
        buttonProceed = (Button) parentView.findViewById(R.id.btn_proceed_health_council_page1);


        lpEditTextSmall = new TableRow.LayoutParams(0, (int) (MainContainer.mScreenWidth * 0.1), 2);
        notificationNumber.setLayoutParams(lpEditTextSmall);
        date.setLayoutParams(lpEditTextSmall);

        LinearLayout.LayoutParams lpButtonProceed = new LinearLayout.LayoutParams((int) (MainContainer.mScreenWidth * 0.5),
                (int) (MainContainer.mScreenWidth * 0.1));
        lpButtonProceed.setMargins(0, (int) (MainContainer.mScreenHeight * 0.05), 0, 0);
        buttonProceed.setLayoutParams(lpButtonProceed);

        date.setOnClickListener(new OnClickShowDatePicker(MainContainer.mContext, date, "Since"));

//        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(RadioGroup radioGroup, int i) {
//                buttonProceed.setEnabled(true);
//                if (radioGroup.getCheckedRadioButtonId()== radioButtonYes.getId()){
//                    tableLayout.setVisibility(View.VISIBLE);
//                }
//                else if (radioGroup.getCheckedRadioButtonId()== radioButtonNo.getId()) {
//                    tableLayout.setVisibility(View.GONE);
//                }
//                else {
//
//                }
//            }
//        });

        radioButtonRecordNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isFormValid()) {
                    MainContainer.activityCallBackHealthCouncil.showLastFragment();
                }
            }
        });

//        buttonProceed.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View view) {
//                if (isFormValid()) {
//                        if (radioGroup.getCheckedRadioButtonId()==radioButtonNo.getId()){
//                        MainContainer.activityCallBackHealthCouncil.showLastFragment();
//                    }
//                    else
//                        MainContainer.activityCallBackHealthCouncil.showNextFragment();
//                } else {
//                    AlertDialogs.getInstance().showDialogOK(null, errorMessage, null, true);
//                }
//            }
//        });
        initClickListeners();
    }

    private void initClickListeners() {
        buttonPrevious.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        buttonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isFormValid()) {
                    if (radioGroup.getCheckedRadioButtonId() == radioButtonNo.getId()) {
                        MainContainer.mJsonObjectFormData.remove(Constants.DB_SCREEN_NAME_HEALTH_COUNCIL_PAGE2);
                        MainContainer.mJsonObjectFormData.remove(Constants.DB_SCREEN_NAME_HEALTH_COUNCIL_PAGE3);
                        MainContainer.mJsonObjectFormData.remove(Constants.DB_SCREEN_NAME_HEALTH_COUNCIL_PAGE4);
                        MainContainer.activityCallBackHealthCouncil.showLastFragment();
                    } else
                        MainContainer.activityCallBackHealthCouncil.showNextFragment();
                } else {
                    AlertDialogs.getInstance().showDialogOK(null, errorMessage, null, true);
                }
            }
        });

        buttonSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
    }


    private void addPicture(ClassPicture object_1, ClassPicture object_2, String textNotification) {
        RelativeLayout relativeLayoutImageButtons = new RelativeLayout(MainContainer.mContext);
        relativeLayoutImageButtons.setPadding(object_1.margins[0], object_1.margins[1], object_1.margins[2], object_1.margins[3]);
        addImageButton(relativeLayoutImageButtons, object_1, true);
        if (object_2 != null) {
            object_2.editTextPartner = object_1.editText;
            addImageButton(relativeLayoutImageButtons, object_2, false);
            object_1.editTextPartner = object_2.editText;
        }
        RadioGroupPicture(relativeLayoutImageButtons, textNotification);
        tableLayout.addView(relativeLayoutImageButtons);
    }


    private void RadioGroupPicture(final RelativeLayout relativeLayoutImageButtons, String text) {
        TextView textView = new TextView(MainContainer.mContext);
        textView.setText(text);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.setMargins(0, 15, 0, 0);
        textView.setLayoutParams(params);
        RadioGroup radioGroup = new RadioGroup(MainContainer.mContext);
        final RadioButton radioButtonAvailable = new RadioButton(MainContainer.mContext);
        radioButtonAvailable.setText("Available");
        //radioButtonAvailable.setChecked(true);

        final RadioButton radioButtonUnavailable = new RadioButton(MainContainer.mContext);
        radioButtonUnavailable.setText("Unavailable");

        radioGroup.setOrientation(LinearLayout.HORIZONTAL);
        radioGroup.addView(radioButtonAvailable);
        radioGroup.addView(radioButtonUnavailable);

        radioGroup.check(radioGroup.getChildAt(0).getId());
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (radioGroup.getChildAt(0).getId() == i) {
                    relativeLayoutImageButtons.setVisibility(View.VISIBLE);
                } else if (radioGroup.getChildAt(1).getId() == i) {
                    relativeLayoutImageButtons.setVisibility(View.GONE);
                }
            }
        });

        tableLayout.addView(textView);
        tableLayout.addView(radioGroup);
    }

    private static int ID = 1;

    private void addImageButton(RelativeLayout layout, ClassPicture object, boolean left) {
        object.editText = new EditText(MainContainer.mContext);
        TextView textView = new TextView(MainContainer.mContext);
        textView.setText("Notification Picture");
        textView.setSingleLine(true);
        textView.setId(ID++);
        RelativeLayout.LayoutParams paramsTextView = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        paramsTextView.addRule(RelativeLayout.ALIGN_PARENT_TOP);
        if (left)
            paramsTextView.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
        else {
            paramsTextView.setMargins((int) (MainContainer.mScreenWidth * 0.02), 0, 0, 0);
            paramsTextView.addRule(RelativeLayout.RIGHT_OF, object.editTextPartner.getId());
        }
        textView.setLayoutParams(paramsTextView);
        object.imageButton = new ImageButton(MainContainer.mContext);
        RelativeLayout.LayoutParams paramsImageView = new RelativeLayout.LayoutParams(object.width, object.height);
        paramsImageView.setMargins(0, object.margins[1], 0, 0);
        if (left)
            paramsImageView.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
        else
            paramsImageView.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
        paramsImageView.addRule(RelativeLayout.BELOW, textView.getId());

        object.imageButton.setLayoutParams(paramsImageView);
        StateListDrawable statesImageButton = new StateListDrawable();
        statesImageButton.addState(new int[]{android.R.attr.state_pressed}, getResources().getDrawable(object.buttonDisable));
        statesImageButton.addState(new int[]{}, getResources().getDrawable(object.buttonEnable));
        object.imageButton.setImageDrawable(statesImageButton);
        object.imageButton.setScaleType(ImageView.ScaleType.FIT_XY);

        setupImageButton(object);
        //layout.addView(object.editText);
        layout.addView(textView);
        layout.addView(object.imageButton);
    }

    void setupImageButton(ClassPicture object) {
        if (object.byteArrayPicture != null) {
            Bitmap bitmap = Utilities.getImage(object.byteArrayPicture);
            object.imageButton.setImageBitmap(bitmap);
            object.imageButton.setScaleType(ImageView.ScaleType.FIT_XY);
        } else {
            StateListDrawable statesImageButton = new StateListDrawable();
            statesImageButton.addState(new int[]{android.R.attr.state_pressed}, getResources().getDrawable(object.buttonDisable));
            statesImageButton.addState(new int[]{}, getResources().getDrawable(object.buttonEnable));
            object.imageButton.setImageDrawable(statesImageButton);
            object.imageButton.setScaleType(ImageView.ScaleType.FIT_XY);
        }
        object.imageButton.setTag(object);
        object.imageButton.setPadding(0, 0, 0, 0);
        object.imageButton.setBackgroundColor(Color.TRANSPARENT);
        object.imageButton.setScaleType(ImageView.ScaleType.FIT_XY);
        object.imageButton.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                PICTURE_GENRAL = (ClassPicture) v.getTag();
                if (PICTURE_GENRAL.byteArrayPicture != null) {
                    String text = PICTURE_GENRAL.editText.getText().toString();
                    previewPicture(Utilities.getImage(PICTURE_GENRAL.byteArrayPicture), text);
                } else {
                    // selectOptionForPicture();
                    Intent camera = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(camera, CODE_PICTURE_NEW);
                }
            }

        });
        object.imageButton.setOnLongClickListener(new View.OnLongClickListener() {

            public boolean onLongClick(View v) {
                PICTURE_GENRAL = (ClassPicture) v.getTag();
                if (PICTURE_GENRAL.byteArrayPicture != null) {
                    new AlertDialog.Builder(v.getContext()).setTitle("Delete Picture").setMessage("Are you sure you want to delete this Picture?")
                            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {

                                public void onClick(DialogInterface dialog, int which) {
                                    PICTURE_GENRAL.byteArrayPicture = null;
                                    StateListDrawable CameraButtonStates = new StateListDrawable();
                                    CameraButtonStates.addState(new int[]{android.R.attr.state_pressed}, getResources().getDrawable(PICTURE_GENRAL.buttonDisable));
                                    CameraButtonStates.addState(new int[]{}, getResources().getDrawable(PICTURE_GENRAL.buttonEnable));
                                    PICTURE_GENRAL.imageButton.setImageDrawable(CameraButtonStates);
                                    PICTURE_GENRAL.imageButton.setScaleType(ImageView.ScaleType.FIT_XY);
                                }
                            }).setNegativeButton("No", new DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface dialog, int which) {
                        }
                    }).show();
                }
                return false;
            }
        });
    }

    private void previewPicture(Bitmap photo, String title) {
        final Dialog loginDialog = new Dialog(MainContainer.mContext);
        loginDialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_BLUR_BEHIND, WindowManager.LayoutParams.FLAG_BLUR_BEHIND);
        loginDialog.setTitle(title);
        LinearLayout linearLayout = new LinearLayout(MainContainer.mContext);
        ImageView previewPicture = new ImageView(MainContainer.mContext);
        linearLayout.setGravity(Gravity.CENTER);
        linearLayout.setPadding((int) (MainContainer.mScreenWidth * 0.02), (int) (MainContainer.mScreenWidth * 0.02), (int) (MainContainer.mScreenWidth * 0.02),
                (int) (MainContainer.mScreenWidth * 0.02));
        linearLayout.addView(previewPicture);
        loginDialog.setContentView(linearLayout);
        loginDialog.show();
        previewPicture.setImageBitmap(photo);
        previewPicture.setScaleType(ImageView.ScaleType.FIT_XY);
    }

    private final int CODE_PICTURE_GALLERY = 1;
    private final int CODE_PICTURE_NEW = 2;

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            String picturePath = "";
            Uri selectedImage;
            byte[] byteArrayTempPicture = null;
            Bitmap bitmapTemp = null;
            if (resultCode == Activity.RESULT_OK) {
                if (requestCode == CODE_PICTURE_GALLERY || requestCode == CODE_PICTURE_NEW) {
                    if (requestCode == CODE_PICTURE_GALLERY) {
                        selectedImage = data.getData();
                        String[] filePathColumn = {MediaStore.Images.Media.DATA};
                        Cursor cursor = MainContainer.mContext.getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                        cursor.moveToFirst();

                        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                        picturePath = cursor.getString(columnIndex);
                        cursor.close();
                        BitmapFactory.Options options = new BitmapFactory.Options();
                        options.inSampleSize = 2;
                        Bitmap PictureTaken = null;
                        PictureTaken = BitmapFactory.decodeFile(picturePath, options);
                        Bitmap resizedBitmap = Bitmap.createScaledBitmap(PictureTaken, PictureTaken.getWidth() / 2, PictureTaken.getHeight() / 2, false);
                        byteArrayTempPicture = Utilities.getBytes(resizedBitmap, picturePath);
                        bitmapTemp = Utilities.getImage(byteArrayTempPicture);
                    } else if (requestCode == CODE_PICTURE_NEW) {
                        try {
                            Bitmap bitmap = (Bitmap) data.getExtras().get("data");
                            Bitmap resizedBitmap = Bitmap.createScaledBitmap(bitmap, bitmap.getWidth() * 2, bitmap.getHeight() * 2, true);

                            File photo = null;
                            FileOutputStream stream;

                            String fileName = java.text.DateFormat.getDateTimeInstance().format(Calendar.getInstance().getTime());
                            fileName = fileName.replaceAll(" ", "");
                            fileName = fileName.replaceAll(":", "");
                            fileName = fileName.replaceAll(",", "");
                            fileName = fileName + ".jpg";
                            photo = new File(android.os.Environment.getExternalStorageDirectory(), fileName);
                            stream = new FileOutputStream(photo);
                            //TODO compress new picture
                            resizedBitmap.compress(Bitmap.CompressFormat.JPEG, 80, stream);

                            selectedImage = Uri.fromFile(photo);
                            picturePath = selectedImage.getPath();

                            bitmapTemp = (Bitmap) data.getExtras().get("data");
                            BitmapFactory.Options options = new BitmapFactory.Options();
                            options.inSampleSize = 2;
                            byteArrayTempPicture = Utilities.getBytes(resizedBitmap, picturePath);
                            bitmapTemp = Utilities.getImage(byteArrayTempPicture);
                        } catch (Exception ex) {
                            Log.e("Picture", ex.toString());
                        }
                    }
                    PICTURE_GENRAL.imageButton.setImageBitmap(bitmapTemp);
                    PICTURE_GENRAL.imageButton.setScaleType(ImageView.ScaleType.FIT_XY);
                    PICTURE_GENRAL.byteArrayPicture = byteArrayTempPicture;
                    PICTURE_GENRAL.editText.setVisibility(View.VISIBLE);
                    //TODO null pointer here
                    if (PICTURE_GENRAL.editTextPartner.getVisibility() == View.GONE)
                        PICTURE_GENRAL.editTextPartner.setVisibility(View.INVISIBLE);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    public String convertBitmapToBase64(byte[] bs) {
        return bs == null ? "" : Base64.encodeToString(bs, Base64.DEFAULT);
    }


    public void loadPicture(String pictureString) {
        AsyncTaskLoadPictures asyncTaskLoadPictures = new AsyncTaskLoadPictures(pictureString);
        try {
            asyncTaskLoadPictures.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public class AsyncTaskLoadPictures extends AsyncTask<Void, Void, Void> {

        /*private Globals MainContainer;*/
        private ProgressDialog uProgressDialog;
        private String pictureString;

        public AsyncTaskLoadPictures(String pictureString) {
            super();
            /*this.MainContainer = Globals.getInstance();*/
            this.pictureString = pictureString;
        }

        public void execute() throws Exception {
            execute((Void) null);
        }

        @Override
        protected void onPreExecute() {
            try {
                uProgressDialog = new ProgressDialog(MainContainer.mContext);
                uProgressDialog.setCancelable(false);
                uProgressDialog.setTitle("Loading pictures");
                uProgressDialog.setMessage("Please Wait...");
                uProgressDialog.setIndeterminate(true);
                uProgressDialog.show();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                loadImages(pictureString);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {

            MainContainer.mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    //stuff that updates ui
                    ImageButton imageButton1 = ((ImageButton) ((RelativeLayout) tableLayout.getChildAt(4)).getChildAt(1));

                    if (decodedByte1 != null) {
                        picture_1.byteArrayPicture = Utilities.getBytes(decodedByte1);
                        picture_1.imageButton.setImageBitmap(decodedByte1);
                        imageButton1.setImageBitmap(decodedByte1);
                    }

                }
            });

            if (uProgressDialog != null && uProgressDialog.isShowing()) {
                uProgressDialog.dismiss();
            }
        }

    }

    private void loadImages(String pictureString) {
        byte[] decodedString = Base64.decode(pictureString, Base64.DEFAULT);
        decodedByte1 = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);

    }


    @Override
    public void onFragmentShown() {

    }

    @Override
    public boolean isFormValid() {
        boolean isValid = true;
        if (etDhisCode.getVisibility() == View.VISIBLE && etDhisCode.getText().toString().equals("")) {
            //isValid = false;
            errorMessage = "Please enter the DHIS code";
            return false;
        }
        if (radioGroup.getVisibility() != View.GONE) {
            if (radioGroup.getCheckedRadioButtonId() == -1) {
                //isValid = false;
                errorMessage = "Please choose Yes or No for Health Council Notified?";
                return false;
            }
        }
        if (radioGroupRecord.getCheckedRadioButtonId() == -1) {
            //isValid = false;
            errorMessage = "Please choose Yes or No for record keep/focal person available?";
            return false;
        }
        if (tableLayout.getVisibility() == View.VISIBLE) {

            if (notificationNumber.getText().toString().equals("")) {
                errorMessage = "Please enter the notification number";
                return false;
            }
            if (date.getText().toString().equals("")) {
                errorMessage = "Please enter the date";
                return false;
            }

            /*if (picture_1.byteArrayPicture == null ) {
                errorMessage = "Please Take Picture to continue";
                return false;
            }*/

            if (tableLayout.getChildAt(4).getVisibility() == View.VISIBLE && picture_1.byteArrayPicture == null) {
                errorMessage = "Please Take Picture to continue";
                return false;
            }

        }
        return isValid;
    }


    @Override
    public String onFragmentChanged(int previousPosition) {
        //errorMessage = null;
        errorMessage = "Please fill all fields";
        isFormValid();
        return errorMessage;
    }

    @Override
    public void parseObject() {

        JSONObject jsonObject = new JSONObject();
        try {
            if (etDhisCode.getVisibility() == View.VISIBLE)
                jsonObject.put("dhis_code", etDhisCode.getText().toString());
            else
                jsonObject.put("dhis_code", "");

            if (radioGroup.getVisibility() != View.GONE) {
                if (radioGroup.getCheckedRadioButtonId() == radioGroup.getChildAt(0).getId()) {
                    jsonObject.put("health_council_notified", "yes");
                } else if (radioGroup.getCheckedRadioButtonId() == radioGroup.getChildAt(1).getId()) {
                    jsonObject.put("health_council_notified", "no");

                } else {
                    jsonObject.put("health_council_notified", "");
                }
            } else
                jsonObject.put("health_council_notified", "");

            if (radioGroupRecord.getCheckedRadioButtonId() == radioGroupRecord.getChildAt(0).getId()) {
                jsonObject.put("focal_person_available", "yes");
            } else if (radioGroupRecord.getCheckedRadioButtonId() == radioGroupRecord.getChildAt(1).getId()) {
                jsonObject.put("focal_person_available", "no");

            } else {
                jsonObject.put("focal_person_available", "");
            }


            if (tableLayout.getChildAt(4).getVisibility() == View.GONE) {
                jsonObject.put("notification_picture", "N/A");
            } else {
                jsonObject.put("notification_picture", convertBitmapToBase64(picture_1.byteArrayPicture));
            }

            jsonObject.put("date", date.getText().toString());
            MainContainer.mJsonObjectFormData.remove(Constants.DB_SCREEN_NAME_HEALTH_COUNCIL_PAGE1);
            MainContainer.mJsonObjectFormData.put(Constants.DB_SCREEN_NAME_HEALTH_COUNCIL_PAGE1, jsonObject);

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


}


