package pk.gov.pitb.mea.msh.adapters;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class ArrayAdapterSimpleSpinner extends ArrayAdapter<String> {

	public ArrayAdapterSimpleSpinner(Context context, int resource, String[] objects) {
		super(context, resource, objects);
	}

	public ArrayAdapterSimpleSpinner(Context context, int resource, ArrayList<String> objects) {
		super(context, resource, objects);
	}

	@Override
	public TextView getDropDownView(int position, View convertView, ViewGroup parent) {
		TextView textView = (TextView) super.getDropDownView(position, convertView, parent);
		if (textView != null) {
			textView.setTextColor(Color.BLACK);
		}
		return textView;
	}

	@Override
	public View getView(int position, View view, ViewGroup parent) {
		view = super.getView(position, view, parent);
		if (view != null) {
			TextView textView = (TextView) view;
			textView.setTextColor(Color.BLACK);
		}
		return view;
	}
}