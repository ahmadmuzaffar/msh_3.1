package pk.gov.pitb.mea.msh.textwatchers;

import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.widget.EditText;

public class TextWatcherCNIC implements TextWatcher {

	private EditText editText = null;

	public TextWatcherCNIC(EditText editText) {
		this.editText = editText;
		this.editText.setInputType(InputType.TYPE_CLASS_NUMBER);
		this.editText.setFilters(new InputFilter[] { new InputFilter.LengthFilter(15) });
	}

	public void beforeTextChanged(CharSequence s, int start, int count, int after) {
	}

	public void onTextChanged(CharSequence s, int start, int before, int count) {
		try {
			String text = editText.getText().toString();
			if (text.length() == 14 && !text.endsWith("-")) {
				editText.setText(text.substring(0, 5) + "-" + text.substring(6, 13) + "-" + text.substring(13));
				editText.setSelection(editText.getText().toString().length());
			} else if (text.length() == 6 && !text.endsWith("-")) {
				editText.setText(text.substring(0, 5) + "-" + text.substring(5));
				editText.setSelection(editText.getText().toString().length());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void afterTextChanged(Editable s) {
	}
}