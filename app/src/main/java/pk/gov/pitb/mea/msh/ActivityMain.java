package pk.gov.pitb.mea.msh;

import android.Manifest;
import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Constructor;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import pk.gov.pitb.mea.msh.asynctasks.AsyncTaskSendData;
import pk.gov.pitb.mea.msh.asynctasks.AsyncTaskSendOfflineData;
import pk.gov.pitb.mea.msh.asynctasks.AsyncTaskSyncApplication;
import pk.gov.pitb.mea.msh.bo.ScreenFragment;
import pk.gov.pitb.mea.msh.dialogs.DialogInformation;
import pk.gov.pitb.mea.msh.dialogs.DialogUnsentActivities;
import pk.gov.pitb.mea.msh.fragmentactivities.FragmentEquipments;
import pk.gov.pitb.mea.msh.handlers.HandlerAction;
import pk.gov.pitb.mea.msh.handlers.HandlerDataSendOfflineData;
import pk.gov.pitb.mea.msh.handlers.HandlerDataSync;
import pk.gov.pitb.mea.msh.handlers.InterfaceActivityCallBack;
import pk.gov.pitb.mea.msh.handlers.InterfaceFragmentCallBack;
import pk.gov.pitb.mea.msh.helpers.AlarmManagerReceiver;
import pk.gov.pitb.mea.msh.helpers.AlertDialogs;
import pk.gov.pitb.mea.msh.helpers.Constants;
import pk.gov.pitb.mea.msh.helpers.LocationTracker;
import pk.gov.pitb.mea.msh.helpers.MainContainer;
import pk.gov.pitb.mea.msh.httpworker.HttpRequestGetPlayStoreVersion;
import pk.gov.pitb.mea.msh.models.ClassDataActivity;
import pk.gov.pitb.mea.msh.models.ClassDialogFacility;
import pk.gov.pitb.mea.msh.models.ClassFacilityData;
import pk.gov.pitb.mea.msh.models.ClassScreenData;
import pk.gov.pitb.mea.msh.models.ClassScreenItemMultiple;
import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.AppSettingsDialog;
import pub.devrel.easypermissions.EasyPermissions;

import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.READ_PHONE_STATE;

public class ActivityMain extends Activity implements InterfaceActivityCallBack /*, EasyPermissions.PermissionCallbacks*/{

	/* Drawer ListView */
	private ActionBarDrawerToggle uDrawerToggle = null;
	private DrawerLayout uDrawerLayout = null;
	private ListView uDrawerList = null;
	private ArrayList<ScreenFragment> arrayListScreenTitles = null;

	/* UI */
	private Button buttonPrevious = null;
	private Button buttonNext = null;
	private Button buttonSubmit = null;

	//    @formatter:off
	private final ScreenFragment[] arrayScreenTitles = new ScreenFragment[] { new ScreenFragment("Facility Status", true), new ScreenFragment("Self Reported Indicators", true),
			new ScreenFragment("Outlook Utilities", true),
//			new ScreenFragment("Method of Waste Disposal", true),
			new ScreenFragment("Vacancy Data", true), new ScreenFragment("Data on Absences", true), new ScreenFragment("Medicines", true),
			new ScreenFragment("Supplies", true), new ScreenFragment("Pictures", true),
//			new ScreenFragment("KPI", true),
			new ScreenFragment("Additional Comments", true)};
	//    @formatter:on

	private DialogInformation dialogFacilityIncharge = null;
	private AdapterDrawerList adapterDrawerList = null;
	private Fragment fragmentCurrent = null;
	private LocationTracker locationTracker = null;
	private final int ACTIVITY_DIALOG_SELECT_FACILITY = 1;

	private ArrayList<ClassScreenItemMultiple> arrayListAllEquipments;
	private final int ASK_PERMISSIONS = 10;
	private String[] PERMISSIONS_STREAM = {
			Manifest.permission.ACCESS_FINE_LOCATION,READ_PHONE_STATE,CAMERA

	};
	protected ArrayList<String> permissions = new ArrayList<>();
	protected boolean permissionsAllowed = false;
	protected int permsRequestCode = 200;
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		} else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
				Point size = new Point();
				getWindowManager().getDefaultDisplay().getSize(size);
				MainContainer.mScreenWidth = size.x;
				MainContainer.mScreenHeight = size.y;
			} else {
				Display display = getWindowManager().getDefaultDisplay();
				MainContainer.mScreenWidth = display.getWidth();
				MainContainer.mScreenHeight = display.getHeight();
			}
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		if(permissionsAllowed)
			MainContainer.setContextOnResume(ActivityMain.this);
		if(locationTracker != null)
			locationTracker.onResume();

	}

	@Override
	protected void onStart() {
		super.onStart();
		if(locationTracker != null)
			locationTracker.onStart();
	}

	@Override
	public void onBackPressed() {
		confirmExit();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		if(locationTracker != null)
			locationTracker.onDestroy();
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		//initializeApplication();
		//methodRequiresPermission();
		askPermissions();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.menu_main, menu);
		return true;
	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		if(uDrawerToggle != null)
			uDrawerToggle.syncState();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (uDrawerToggle.onOptionsItemSelected(item)) {
			return true;
		}
		switch (item.getItemId()) {
			case R.id.item_sync:
				performSync();
				return true;
			case R.id.item_info:
				dialogFacilityIncharge.showFacilityDialog(null);
				return true;
			case R.id.item_unsent:
				showDialogUnsent();
				return true;
			case R.id.item_exit:
				confirmExit();
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}

	private void initializeApplication() {
		MainContainer.initializeOnStartup(ActivityMain.this);
		MainContainer.activityCallBack = ActivityMain.this;
		locationTracker = new LocationTracker(MainContainer.mContext);
		setupActionBar();
		AlarmManagerReceiver alarmManagerReceiver = new AlarmManagerReceiver(MainContainer.mContext);
		alarmManagerReceiver.setTimeTrigger();
		/*ArrayList<ClassDataActivity> arrayListDataUnsent = MainContainer.mDbAdapter.selectAllActivities(Constants.
		UNSENT);*/
		ArrayList<ClassDataActivity> arrayListDataUnsent = MainContainer.mDbAdapter.selectAllActivities(Constants.AT_MONITORING, Constants.AT_HEALTH_COUNCIL, Constants.AT_HEPATITIS, Constants.AT_NUTRIENTS,Constants.AT_PATIENT_INTERVIEWS);
		String playStoreVersion = MainContainer.mSPEditor.getAppPlayStoreVersion();
		if (arrayListDataUnsent.size() == 0
				&& (playStoreVersion == null || playStoreVersion.length() == 0 || MainContainer.mSPEditor.isCheckUpdateNeeded() || MainContainer.mSPEditor.isAppUpdateNeeded(getResources().getString(
				R.string.app_version_name)))) {
			if (playStoreVersion == null || playStoreVersion.length() == 0 || MainContainer.mSPEditor.isCheckUpdateNeeded()) {
				AsyncTaskCheckPlayStoreVersion asyncTaskCheckPlayStoreVersion = new AsyncTaskCheckPlayStoreVersion();
				asyncTaskCheckPlayStoreVersion.execute((Void) null);
			} else {
				postCheckUpdate();
			}
		} else if (MainContainer.mDbAdapter.isApplicationInitialized() && (!MainContainer.isInternetAvailable() || arrayListDataUnsent.size() == 0)) {
			showFacilityDialog();
		} else if (arrayListDataUnsent.size() > 0) {
			sendOfflineData(arrayListDataUnsent);
		} else {
			performSync();
		}
		buttonPrevious.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				buttonPrevious.setEnabled(false);
				int prevPosition = fragmentPositionCurrent;
				fragmentPositionCurrent--;
				if (fragmentPositionCurrent < 0) {
					fragmentPositionCurrent = 0;
				}
				selectItem(prevPosition);
				buttonPrevious.setEnabled(true);
			}
		});
		buttonNext.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				buttonNext.setEnabled(false);
				int prevPosition = fragmentPositionCurrent;
				fragmentPositionCurrent++;
				if (fragmentPositionCurrent >= arrayListScreenTitles.size()) {
					fragmentPositionCurrent = arrayListScreenTitles.size() - 1;
				}
				selectItem(prevPosition);
				buttonNext.setEnabled(true);
			}
		});
		buttonSubmit.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				buttonSubmit.setEnabled(false);
				performSubmit();
				buttonSubmit.setEnabled(true);
			}
		});
	}

	private void postCheckUpdate() {
		String playStoreVersion = MainContainer.mSPEditor.getAppPlayStoreVersion();
		MainContainer.mSPEditor.setLastCheckUpdateDate((new SimpleDateFormat(Constants.FORMAT_DATE_APP, Locale.ENGLISH)).format(Calendar.getInstance().getTime()));
		if (playStoreVersion == null || playStoreVersion.length() == 0 || !MainContainer.mSPEditor.isAppUpdateNeeded(getResources().getString(R.string.app_version_name))) {
			if (MainContainer.mDbAdapter.isApplicationInitialized() && !MainContainer.isInternetAvailable()) {
				showFacilityDialog();
			} else {
				performSync();
			}
		} else {
			AlertDialog.Builder builder = new AlertDialog.Builder(MainContainer.mContext);
			builder.setTitle("Update Available");
			DialogInterface.OnClickListener onUpdate = new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					try {
						startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + MainContainer.mContext.getPackageName())));
					} catch (android.content.ActivityNotFoundException anfe) {
						startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + MainContainer.mContext.getPackageName())));
					}
					finish();
				}
			};
			if (MainContainer.mSPEditor.isAppForceUpdateNeeded(getResources().getString(R.string.app_version_name))) {
				builder.setPositiveButton("Update", onUpdate);
				builder.setMessage("A newer version of application is available. Please update you app to proceed");
				builder.setCancelable(false);
			} else {
				DialogInterface.OnClickListener onContinue = new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						if (MainContainer.mDbAdapter.isApplicationInitialized() && !MainContainer.isInternetAvailable()) {
							showFacilityDialog();
						} else {
							performSync();
						}
					}
				};
				builder.setPositiveButton("Update Now", onUpdate);
				builder.setNegativeButton("Update Later", onContinue);
				builder.setMessage("A newer version of application is available. Please update you app");
				builder.setCancelable(false);
			}
			builder.create().show();
		}
	}

	private class AsyncTaskCheckPlayStoreVersion extends AsyncTask<Void, Void, Void> {

		private ProgressDialog progressDialog;

		@Override
		protected void onPreExecute() {
			try {
				progressDialog = new ProgressDialog(MainContainer.mContext);
				progressDialog.setCancelable(false);
				progressDialog.setTitle("Checking for Updates");
				progressDialog.setMessage("Please Wait...");
				progressDialog.setIndeterminate(true);
				progressDialog.show();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		@Override
		protected Void doInBackground(Void... params) {
			try {
				HttpRequestGetPlayStoreVersion getPlayStoreVersion = new HttpRequestGetPlayStoreVersion(MainContainer.mContext);
				String result = getPlayStoreVersion.getPlayStoreVersion();
				if (result != null && result.length() > 0) {
					MainContainer.mSPEditor.setAppPlayStoreVersion(result);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			progressDialog.dismiss();
			postCheckUpdate();
		}

	}

	private void showFacilityDialog() {
		MainContainer.resetMainContainer();
		((FrameLayout) findViewById(R.id.framelayout_maincontent)).setVisibility(View.GONE);
		Intent intent = new Intent(ActivityMain.this, ActivityDialogSelectFacility.class);
		startActivityForResult(intent, ACTIVITY_DIALOG_SELECT_FACILITY);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		try {
			super.onActivityResult(requestCode, resultCode, data);
			MainContainer.setContextOnResume(ActivityMain.this);
			if (requestCode == ACTIVITY_DIALOG_SELECT_FACILITY) {
				if (resultCode == RESULT_OK && data != null) {
					String bSync = data.getStringExtra(Constants.EXTRAS_SYNC);
					String bLoad = data.getStringExtra(Constants.EXTRAS_LOAD);
					String bMonitoring = data.getStringExtra(Constants.EXTRAS_MONITERING);
					if (bLoad.equals(Constants.VALUE_TRUE_HEALTH_COUNCIL)) {
						try {
							MainContainer.mObjectFacility = new ClassDialogFacility();
							MainContainer.mObjectFacility.setFromJSON(new JSONObject(MainContainer.mSPEditor.getLastSavedFacilityHealthCouncil()));
							ClassFacilityData facilityData = MainContainer.mDbAdapter.selectFacilityByName(MainContainer.mObjectFacility.facility_id, MainContainer.mObjectFacility.facility_name);
							MainContainer.mObjectFacilityData = facilityData;
							MainContainer.bIsActivityHealthCouncil = true;
							Intent intent = new Intent(ActivityMain.this, ActivityMainHealthCouncil.class);
							startActivity(intent);
							finish();
						} catch (/*JSONException*/ Exception e) {
							e.printStackTrace();
							MainContainer.showErrorMessage();
							exitApplication();
						}
					}
					else if (bLoad.equals(Constants.VALUE_TRUE_NUTRIENTS)) {
						try {
							MainContainer.mObjectFacility = new ClassDialogFacility();
							MainContainer.mObjectFacility.setFromJSON(new JSONObject(MainContainer.mSPEditor.getLastSavedFacilityNutrients()));
							ClassFacilityData facilityData = MainContainer.mDbAdapter.selectFacilityByName(MainContainer.mObjectFacility.facility_id, MainContainer.mObjectFacility.facility_name);
							MainContainer.mObjectFacilityData = facilityData;
							MainContainer.bIsActivityNutrients = true;
							Intent intent = new Intent(ActivityMain.this, ActivityMainNutrients.class);
							startActivity(intent);
							finish();
						} catch (/*JSONException*/ Exception e) {
							e.printStackTrace();
							MainContainer.showErrorMessage();
							exitApplication();
						}
					}
					else if (bLoad.equals(Constants.VALUE_TRUE_HEPATITIS)) {
						try {
							MainContainer.mObjectFacility = new ClassDialogFacility();
							MainContainer.mObjectFacility.setFromJSON(new JSONObject(MainContainer.mSPEditor.getLastSavedFacilityHepatitis()));
							ClassFacilityData facilityData = MainContainer.mDbAdapter.selectFacilityByName(MainContainer.mObjectFacility.facility_id, MainContainer.mObjectFacility.facility_name);
							MainContainer.mObjectFacilityData = facilityData;
							MainContainer.bIsActivityHepatitis= true;
							Intent intent = new Intent(ActivityMain.this, ActivityMainHepatitis.class);
							startActivity(intent);
							finish();
						} catch (/*JSONException*/ Exception e) {
							e.printStackTrace();
							MainContainer.showErrorMessage();
							exitApplication();
						}
					}
					else if (bLoad.equals(Constants.VALUE_TRUE_PATIENT_INTERVIEWS)) {
						try {
							MainContainer.mObjectFacility = new ClassDialogFacility();
							MainContainer.mObjectFacility.setFromJSON(new JSONObject(MainContainer.mSPEditor.getLastSavedJSONPatientInterviews()));
							ClassFacilityData facilityData = MainContainer.mDbAdapter.selectFacilityByName(MainContainer.mObjectFacility.facility_id, MainContainer.mObjectFacility.facility_name);
							MainContainer.mObjectFacilityData = facilityData;
							MainContainer.bIsActivityPatientInterviews= true;
							Intent intent = new Intent(ActivityMain.this, ActivityMainPatientInterviews.class);
							startActivity(intent);
							finish();
						} catch (/*JSONException*/ Exception e) {
							e.printStackTrace();
							MainContainer.showErrorMessage();
							exitApplication();
						}
					}
					else if (bLoad.equals(Constants.EXTRAS_MONITERING)) {
						try {
							MainContainer.mObjectFacility = new ClassDialogFacility();
							MainContainer.mObjectFacility.setFromJSON(new JSONObject(MainContainer.mSPEditor.getLastSavedFacilityMonitoring()));
							ClassFacilityData facilityData = MainContainer.mDbAdapter.selectFacilityByName(MainContainer.mObjectFacility.facility_id, MainContainer.mObjectFacility.facility_name);
							MainContainer.mObjectFacilityData = facilityData;
							/*Intent intent = new Intent(ActivityMain.this, ActivityMain.class);
							startActivity(intent);
							finish();*/
							initializeFormData();
						} catch (/*JSONException*/ Exception e) {
							e.printStackTrace();
							MainContainer.showErrorMessage();
							exitApplication();
						}
					}
					else if (bSync.equals(Constants.VALUE_FALSE)) {
						if (bMonitoring.equals(Constants.VALUE_FALSE)){
							//MainContainer.mObjectFacility = new ClassDialogFacility();
							//MainContainer.mObjectFacility.setFromJSON(new JSONObject(MainContainer.mSPEditor.getLastSavedFacilityHealthCouncil()));
							Intent intent = null;
							if (bLoad.equals(Constants.VALUE_FALSE_HEALTH_COUNCIL)){
								MainContainer.bIsActivityHealthCouncil = true;
								intent = new Intent(ActivityMain.this, ActivityMainHealthCouncil.class);
							}

							else if (bLoad.equals(Constants.VALUE_FALSE_NUTRIENTS)){
								MainContainer.bIsActivityNutrients = true;
								intent = new Intent(ActivityMain.this, ActivityMainNutrients.class);
							}

							else if (bLoad.equals(Constants.VALUE_FALSE_HEPATITIS)){
								MainContainer.bIsActivityHepatitis = true;
								intent = new Intent(ActivityMain.this, ActivityMainHepatitis.class);
							}else if (bLoad.equals(Constants.VALUE_FALSE_PATIENT_INTERVIEWS)){
								MainContainer.bIsActivityPatientInterviews = true;
								intent = new Intent(ActivityMain.this, ActivityMainPatientInterviews.class);
							}

							startActivity(intent);
							finish();
						}
						else {
							initializeFormData();
						}
					} else {
						performSync();
					}
				} else {
					finish();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void setupActionBar() {
		uDrawerLayout = (DrawerLayout) findViewById(R.id.drawerlayout_parent);
		uDrawerList = (ListView) findViewById(R.id.listview_leftdrawer);
		buttonPrevious = (Button) findViewById(R.id.button_previous);
		buttonNext = (Button) findViewById(R.id.button_next);
		buttonSubmit = (Button) findViewById(R.id.button_submit);
		uDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
		uDrawerList.setSelector(R.drawable.xml_background_list_item);
		uDrawerList.setOnItemClickListener(new ClickListenerDrawerItem());
		ActionBar actionBar = getActionBar();
		actionBar.setIcon(null);
		actionBar.setBackgroundDrawable(new ColorDrawable(MainContainer.mContext.getResources().getColor(R.color.row_heading_background)));
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setHomeButtonEnabled(true);
		actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_TITLE | ActionBar.DISPLAY_SHOW_HOME | ActionBar.DISPLAY_HOME_AS_UP);
		uDrawerToggle = new ActionBarDrawerToggle(MainContainer.mActivity, uDrawerLayout, R.drawable.ic_drawer, R.string.navigation_drawer, R.string.navigation_drawer) {

			public void onDrawerClosed(View drawerView) {
				invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
			}

			public void onDrawerOpened(View drawerView) {
				invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
				closeKeyBoard();
			}
		};
		uDrawerLayout.setDrawerListener(uDrawerToggle);
	}

	private void closeKeyBoard() {
		try {
			InputMethodManager inputMethodManager = (InputMethodManager) MainContainer.mActivity.getSystemService(Activity.INPUT_METHOD_SERVICE);
			inputMethodManager.hideSoftInputFromWindow(MainContainer.mActivity.getCurrentFocus().getWindowToken(), 0);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void initializeFormData() {
		MainContainer.mJsonObjectFormData = new JSONObject();
		String json = MainContainer.mSPEditor.getLastSavedJSONMonitoring();
		if (json.length() > 0) {

			try {
				MainContainer.mJsonObjectFormData = new JSONObject(json);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		} else {
			MainContainer.mJsonObjectFormData = new JSONObject();
		}

		if (MainContainer.mJsonObjectFormData.has(Constants.DB_SCREEN_NAME_EQUIPMENT)){
			try {
				MainContainer.mJsonObjectFormDataEquipments = MainContainer.mJsonObjectFormData.getJSONObject(Constants.DB_SCREEN_NAME_EQUIPMENT);
				MainContainer.mJsonObjectFormData.remove(Constants.DB_SCREEN_NAME_EQUIPMENT);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}

		((FrameLayout) findViewById(R.id.framelayout_maincontent)).setVisibility(View.VISIBLE);
		MainContainer.mObjectFacilityData = MainContainer.mDbAdapter.selectFacilityData(MainContainer.mObjectFacility.facility_id, MainContainer.mObjectFacility.type_id);
		// dialogFacilityIncharge = new DialogInformation(MainContainer.mObjectFacilityData.getObjectStaffByRole(Constants.ROLE_FACILITY_INCHARGE));
		dialogFacilityIncharge = new DialogInformation();
		// MainContainer.mArrayListPictures = new ArrayList<ClassPicture>();
		// objectDataActivity.setStartDate(simpleDateFormat.format(Calendar.getInstance().getTime()));
		arrayListScreenTitles = new ArrayList<ScreenFragment>(Arrays.asList(arrayScreenTitles));
		if (MainContainer.arrayListOpenedActivities != null) {
			MainContainer.arrayListOpenedActivities.clear();
		}
		MainContainer.arrayListOpenedActivities = new ArrayList<String>();
		adapterDrawerList = new AdapterDrawerList(MainContainer.mContext, R.layout.layout_list_item_drawer, arrayListScreenTitles);
		uDrawerList.setAdapter(adapterDrawerList);
		adapterDrawerList.notifyDataSetChanged();
		for (ScreenFragment obj : arrayScreenTitles) {
			// Must Begin Transaction Before Every Commit
			String screenTag = MainContainer.getScreenTag(obj.name);
			fragmentCurrent = getFragmentManager().findFragmentByTag(screenTag);
			if (fragmentCurrent != null) {
				FragmentTransaction transaction = getFragmentManager().beginTransaction();
				transaction = transaction.remove(fragmentCurrent);
				transaction.commit(); // Only Commits Last Transaction
				// getFragmentManager().popBackStackImmediate();
				// getFragmentManager().executePendingTransactions();
			}
		}
		if (MainContainer.mObjectFacility.form_type.equals(Constants.FORM_TYPE_A)) {
			removeFromList("Self Reported Indicators", "Vacancy Data", "Medicines", "Supplies");
			ClassScreenData objectScreenData = new ClassScreenData();
			objectScreenData.setDefaultValues();
			objectScreenData.screen_id = Constants.DB_SCREEN_ID_EQUIPMENT;
			objectScreenData.screen_name = Constants.DB_SCREEN_NAME_EQUIPMENT;
			arrayListAllEquipments = MainContainer.mDbAdapter.selectScreenItemsMultiple(objectScreenData);
			// , new ScreenFragment("Equipments", true)
			int index = 0;
			for (int i = 0; i < arrayListScreenTitles.size(); i++) {
				if (arrayListScreenTitles.get(i).name.toLowerCase(Locale.ENGLISH).equals("data on absences")) {
					index = i + 1;
					break;
				}
			}
			int pageNum = 1;
			if (arrayListAllEquipments.size() > Constants.END_INDEX_EUIPMENT_1) {
				for (int i = 0; i <= arrayListAllEquipments.size(); i++) {
					if (i > Constants.END_INDEX_EUIPMENT_5) {
						addEquipments(index, pageNum);
						index++;
						pageNum++;
						break;
					} else {
						switch (i) {
							case Constants.END_INDEX_EUIPMENT_1:
							case Constants.END_INDEX_EUIPMENT_2:
							case Constants.END_INDEX_EUIPMENT_3:
							case Constants.END_INDEX_EUIPMENT_4:
							case Constants.END_INDEX_EUIPMENT_5:
								addEquipments(index, pageNum);
								index++;
								pageNum++;
							default:
								break;
						}
					}
				}
			} else {
				arrayListScreenTitles.add(index, new ScreenFragment("Equipments", true, pageNum));
			}
		} else if (MainContainer.mObjectFacility.form_type.equals(Constants.FORM_TYPE_B)) {
			removeFromList("Outlook Utilities", "Method of Waste Disposal", "Data on Absences");
		}
		adapterDrawerList.notifyDataSetChanged();
		fragmentPositionCurrent = 0;
		selectItem(true, -1);
	}

	private void addEquipments(int index, int pageNum) {

		if(pageNum == 1)
			arrayListScreenTitles.add(index, new ScreenFragment(Constants.TITLE_EQUIPMENT + "OBGYN Wards, Paeds Ward and Nursery and Other Wards", true, pageNum));
		else if(pageNum == 2)
			arrayListScreenTitles.add(index, new ScreenFragment(Constants.TITLE_EQUIPMENT + "ICU", true, pageNum));
		else if(pageNum == 3)
			arrayListScreenTitles.add(index, new ScreenFragment(Constants.TITLE_EQUIPMENT + "OBGYN OPD", true, pageNum));
		else if(pageNum == 4)
			arrayListScreenTitles.add(index, new ScreenFragment(Constants.TITLE_EQUIPMENT + "Dental Unit", true, pageNum));
		else if(pageNum == 5)
			arrayListScreenTitles.add(index, new ScreenFragment(Constants.TITLE_EQUIPMENT + "General OPDs, Emergency and Labor Room ", true, pageNum));
		else //if(pageNum == 6)
			arrayListScreenTitles.add(index, new ScreenFragment(Constants.TITLE_EQUIPMENT + "OT, Lab and Blood Bank, Radiology Department, Infection Control and Others", true, pageNum));

	}

	private void removeFromList(String... names) {
		for (int i = 0; i < names.length; i++) {
			for (int j = 0; j < arrayListScreenTitles.size(); j++) {
				if (names[i].toLowerCase(Locale.ENGLISH).equals(arrayListScreenTitles.get(j).name.toLowerCase(Locale.ENGLISH))) {
					arrayListScreenTitles.remove(j--);
				}
			}
		}
	}

	private View previousView = null;
	private int fragmentPositionCurrent = 0;

	private void selectItem(int prevPosition) {
		selectItem(false, prevPosition);
	}

	private void selectItem(boolean isReset, int prevPosition) {
		if (MainContainer.bIsOpen) {
			InterfaceFragmentCallBack obj = null;
			if (fragmentCurrent != null) {
				obj = (InterfaceFragmentCallBack) fragmentCurrent;
			}
			if (obj != null) {
				String message = obj.onFragmentChanged(prevPosition);
				if (message == null) {
					changeFragment(isReset);
				} else {
//					showDialogChangeFragment(prevPosition, message);
					changeFragment(isReset);
				}
			} else {
				changeFragment(isReset);
			}
		} else {
			if (prevPosition != -1) {
				fragmentPositionCurrent = prevPosition;
			}
		}
		uDrawerLayout.closeDrawer(uDrawerList);
	}

	private void changeFragment(boolean isReset) {
		try {

			if (fragmentCurrent!=null)
				saveRecentData();

			if (previousView != null) {
				((TextView) previousView).setTextColor(Color.WHITE);
			}
			if (uDrawerList.getChildCount() > fragmentPositionCurrent) {
				previousView = uDrawerList.getChildAt(fragmentPositionCurrent);
				((TextView) previousView).setTextColor(MainContainer.mContext.getResources().getColor(R.color.listitem_text_activated));
			}
			String screenTag = null;
			if (fragmentPositionCurrent == 0) {
				buttonPrevious.setVisibility(View.GONE);
				buttonNext.setVisibility(View.VISIBLE);
				buttonSubmit.setVisibility(View.GONE);
			} else if (fragmentPositionCurrent == arrayListScreenTitles.size() - 1) {
				buttonPrevious.setVisibility(View.VISIBLE);
				buttonNext.setVisibility(View.GONE);
				buttonSubmit.setVisibility(View.VISIBLE);
			} else {
				buttonPrevious.setVisibility(View.VISIBLE);
				buttonNext.setVisibility(View.VISIBLE);
				buttonSubmit.setVisibility(View.GONE);
			}
			screenTag = MainContainer.getScreenTag(arrayListScreenTitles.get(fragmentPositionCurrent).name);
			if (isReset) {
				fragmentCurrent = null;
			} else {
				fragmentCurrent = getFragmentManager().findFragmentByTag(screenTag);
			}
			if (fragmentCurrent == null || !MainContainer.arrayListOpenedActivities.contains(screenTag)) {
				if (arrayListScreenTitles.get(fragmentPositionCurrent).name.startsWith(Constants.TITLE_EQUIPMENT)) {
					fragmentCurrent = new FragmentEquipments(arrayListAllEquipments, arrayListScreenTitles.get(fragmentPositionCurrent).page);
				} else {
					String className = MainContainer.mPackageName + ".fragmentactivities.Fragment" + arrayListScreenTitles.get(fragmentPositionCurrent).name.replaceAll("[ _-]", "");
					Class<?> classObject = Class.forName(className);
					Constructor<?> constructorDefault = classObject.getConstructor();
					fragmentCurrent = (Fragment) (constructorDefault.newInstance());
				}
				MainContainer.arrayListOpenedActivities.add(screenTag);
			}
			fragmentCurrent.setRetainInstance(true);
			// uEditTextSchoolName.setText(arrayListScreenTitles.get(nCurrentFragmentPosition));
			// getActionBar().setTitle(listScreenTitles.get(currentFragmentPosition));
			FragmentTransaction transaction = getFragmentManager().beginTransaction();
			transaction.replace(R.id.framelayout_maincontent, fragmentCurrent, screenTag);
			transaction.addToBackStack(screenTag);
			transaction.commit();
			((InterfaceFragmentCallBack) fragmentCurrent).onFragmentShown();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void sendOfflineData(ArrayList<ClassDataActivity> arrayListDataUnsent) {
		try {
			AsyncTaskSendOfflineData asyncTaskSendOfflineData = new AsyncTaskSendOfflineData(arrayListDataUnsent, handlerDataSendOfflineData);
			asyncTaskSendOfflineData.execute();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private HandlerDataSendOfflineData handlerDataSendOfflineData = new HandlerDataSendOfflineData() {

		@Override
		public void onSendComplete(HandlerAction handlerAction) {
			postCheckUpdate();
		}
	};

	private void performSync() {
		try {
			((FrameLayout) findViewById(R.id.framelayout_maincontent)).setVisibility(View.GONE);
			MainContainer.mSPEditor.clearLastSavedDataMonitoring();
			AsyncTaskSyncApplication asyncTaskSyncApplication = new AsyncTaskSyncApplication(handlerDataSync);
			asyncTaskSyncApplication.execute();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private HandlerDataSync handlerDataSync = new HandlerDataSync() {

		@Override
		public void onSyncSuccessful(boolean bIsDataFound, String message) {
			if (bIsDataFound) {
				Toast.makeText(MainContainer.mContext, message, Toast.LENGTH_LONG).show();
				showFacilityDialog();
			} else {
				showMessageAndExit("ERROR", message);
			}
		}

		@Override
		public void onSyncError(HandlerAction handlerAction) {
			if (handlerAction == null) {
				handlerAction = new HandlerAction();
				handlerAction.handlerErrorCode = 0;
				handlerAction.handlerActionString = "Server Error. Please Try Again Later";
			}
			switch (handlerAction.handlerErrorCode) {
				case Constants.ERROR_CODE_STRUCTURE_VERSION_UP_TO_DATE:
				case Constants.ERROR_CODE_FACILITY_VERSION_UP_TO_DATE:
					showMessageAndContinue("INFORMATION", handlerAction.handlerActionString);
					break;
				case 0:
				case Constants.ERROR_CODE_NO_IMEI:
				case Constants.ERROR_CODE_INVALID_IMEI:
				case Constants.ERROR_CODE_NO_STRUCTURE_VERSION:
				case Constants.ERROR_CODE_NO_FACILITY_VERSION:
				default:
					MainContainer.mDbAdapter.resetApplication();
					showMessageAndExit("ERROR", handlerAction.handlerActionString);
					break;
			}
		}

		@Override
		public void onNoInternet(boolean bIsConnected) {
			if (MainContainer.mDbAdapter.isApplicationInitialized()) {
				if (bIsConnected) {
					showMessageAndContinue("ERROR", "Internet Not Available. Please Check Your Internet Settings and Try Again Later");
				} else {
					showMessageAndContinue("ERROR", "Internet Not Available. Please Connect to the Internet and Try Again Later");
				}
			} else {
				if (bIsConnected) {
					showMessageAndExit("ERROR", "Internet Not Available. Please Check Your Internet Settings and Try Again Later");
				} else {
					showMessageAndExit("ERROR", "Internet Not Available. Please Connect to the Internet and Try Again Later");
				}
			}
		}
	};

	private void performSubmit() {
		try {
			// TODO To Remove Information Dialog
			if (isValid()) {
				dialogFacilityIncharge.showFacilityDialog(new AsyncTaskSendData(locationTracker, arrayListScreenTitles));
				// if (MainContainer.bIsOpen) {
				// AlertDialog.Builder builder = new AlertDialog.Builder(ActivityMain.this);
				// builder.setMessage("Are you sure you want to send the information?");
				// builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
				//
				// public void onClick(DialogInterface dialogInterface, int id) {
				// if (isValid()) {
				// try {
				// AsyncTaskSendData asyncTaskSendData = new AsyncTaskSendData(locationTracker, arrayListScreenTitles);
				// asyncTaskSendData.execute();
				// } catch (Exception e) {
				// e.printStackTrace();
				// Toast.makeText(getApplicationContext(), "Application Error", Toast.LENGTH_LONG).show();
				// }
				// }
				// }
				// });
				// builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
				//
				// public void onClick(DialogInterface dialogInterface, int id) {
				// }
				// });
				// builder.show();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	protected boolean isValid() {
		try {
			InterfaceFragmentCallBack obj = (InterfaceFragmentCallBack) fragmentCurrent;
			if (MainContainer.bIsOpen) {
				Fragment objFragment;
				String screenTag;
				for (int i = 0; i < arrayListScreenTitles.size(); i++) {
					ScreenFragment object = arrayListScreenTitles.get(i);
					if (object.bIsMandatory) {
						screenTag = MainContainer.getScreenTag(arrayListScreenTitles.get(i).name);
						objFragment = MainContainer.mActivity.getFragmentManager().findFragmentByTag(screenTag);
						if (!Constants.bTesting) {
							if (objFragment == null) {
								int prevPosition = fragmentPositionCurrent;
								fragmentPositionCurrent = i;
								selectItem(prevPosition);
								MainContainer.showMessage("Please fill this form to continue sending");
								return false;
							} else if (!((InterfaceFragmentCallBack) objFragment).isFormValid()) {
								int prevPosition = fragmentPositionCurrent;
								fragmentPositionCurrent = i;
								selectItem(prevPosition);
								MainContainer.showMessage("Please fill mandatory data in this form to continue sending");
								return false;
							}
						}
					}
				}
				return true;
			} else {
				boolean bIsValid = obj.isFormValid();
				return bIsValid;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	private void showDialogUnsent() {
		ArrayList<ClassDataActivity> listUnsent = MainContainer.mDbAdapter.selectAllActivities(Constants.AT_UNSENT);
		if (listUnsent.size() > 0) {
			DialogUnsentActivities dialogUnsentActivities = new DialogUnsentActivities();
			dialogUnsentActivities.showUnsentDialog(listUnsent);
		} else {
			Toast.makeText(MainContainer.mContext, "No Unsent Activity to Display", Toast.LENGTH_LONG).show();
		}
	}

	private void confirmExit() {
		((InterfaceFragmentCallBack) fragmentCurrent).parseObject();
		String json = MainContainer.mJsonObjectFormData.toString();
		if (json.equals(MainContainer.mSPEditor.getLastSavedJSONMonitoring())){
			AlertDialogs.OnDialogClickListener OnYesClick = new AlertDialogs.OnDialogClickListener() {
				@Override
				public void onClick(DialogInterface dialogInterface, int i) {
					/*bSaveData = false;*/
					finish();
				}
			};
			AlertDialogs.getInstance().showDialogYesNo("Changes will be saved", "Are You Sure You Want to Exit Application?",OnYesClick,
					null,true);
		}
		else {
			AlertDialogs.OnDialogClickListener onSavedClicked = new AlertDialogs.OnDialogClickListener() {
				@Override
				public void onClick(DialogInterface dialogInterface, int i) {
					saveRecentData();
					MainContainer.mDbAdapter.insertActivitySave(MainContainer.mObjectActivity);
					/*bSaveData = false;*/
					finish();
				}
			};

			AlertDialogs.OnDialogClickListener onDiscardClicked = new AlertDialogs.OnDialogClickListener() {
				@Override
				public void onClick(DialogInterface dialogInterface, int i) {
					MainContainer.mSPEditor.clearLastSavedDataMonitoring();
					/*bSaveData = false;*/
					finish();
				}
			};

			AlertDialogs.getInstance().showDialogThreeButtons("Unsaved Changes", "Do you want to save data?", "Save and Exit?", onSavedClicked,
					"Discard Data", onDiscardClicked, "Cancel", null, true);
		}
		/*AlertDialog.Builder builder = new AlertDialog.Builder(MainContainer.mContext);
		builder.setTitle("All Entered Data will be Lost");
		builder.setMessage("Are You Sure You Want to Exit Application?");
		builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

			public void onClick(DialogInterface dialogInterface, int id) {
				exitApplication();
			}
		});
		builder.setNegativeButton("No", new DialogInterface.OnClickListener() {

			public void onClick(DialogInterface dialogInterface, int id) {
				if (MainContainer.mObjectFacility == null) {
					showFacilityDialog();
				}
			}
		});
		builder.show();*/
	}

	private void exitApplication() {
		this.finish();
		MainContainer.exitMainContainer();
	}

	private class ClickListenerDrawerItem implements ListView.OnItemClickListener {

		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			int prevPosition = fragmentPositionCurrent;
			fragmentPositionCurrent = position;
			selectItem(prevPosition);
		}
	}

	private class AdapterDrawerList extends ArrayAdapter<ScreenFragment> {

		public AdapterDrawerList(Context context, int resource, List<ScreenFragment> objects) {
			super(context, resource, objects);
		}

		@Override
		public View getView(int position, View view, ViewGroup parent) {
			view = super.getView(position, view, parent);
			if (view != null && position == fragmentPositionCurrent) {
				previousView = view;
				((TextView) view).setTextColor(MainContainer.mContext.getResources().getColor(R.color.listitem_text_activated));
			}
			return view;
		}
	}

	@Override
	public void changeButtonNextState(boolean bState) {
		if (bState) {
			buttonNext.setVisibility(View.VISIBLE);
		} else {
			buttonNext.setVisibility(View.GONE);
		}
	}

	@Override
	public void changeButtonPreviousState(boolean bState) {
		if (bState) {
			buttonPrevious.setVisibility(View.VISIBLE);
		} else {
			buttonPrevious.setVisibility(View.GONE);
		}
	}

	@Override
	public void changeButtonSubmitState(boolean bState) {
		if (bState) {
			buttonSubmit.setVisibility(View.VISIBLE);
		} else {
			buttonSubmit.setVisibility(View.GONE);
		}
	}

	@SuppressWarnings("unused")
	private void showMessage(String title, String message) {
		try {
			AlertDialog.Builder builder = new AlertDialog.Builder(MainContainer.mContext);
			if (title != null) {
				builder.setTitle(title);
			}
			builder.setMessage(message);
			builder.setPositiveButton("OK", null);
			builder.create();
			builder.show();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void showMessageAndContinue(String title, String message) {
		try {
			AlertDialog.Builder builder = new AlertDialog.Builder(MainContainer.mContext);
			if (title != null) {
				builder.setTitle(title);
			}
			builder.setMessage(message);
			builder.setPositiveButton("Retry", new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					performSync();
				}
			});
			builder.setNegativeButton("Continue", new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					showFacilityDialog();
				}
			});
			builder.setCancelable(false);
			builder.create();
			builder.show();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void showMessageAndExit(String title, String message) {
		try {
			AlertDialog.Builder builder = new AlertDialog.Builder(MainContainer.mContext);
			if (title != null) {
				builder.setTitle(title);
			}
			builder.setMessage(message);
			builder.setPositiveButton("Retry", new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					performSync();
				}
			});
			builder.setNegativeButton("Exit", new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					exitApplication();
				}
			});
			builder.setCancelable(false);
			builder.create();
			builder.show();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private class OnChangeFragmentNo implements DialogInterface.OnClickListener {

		private int previousPosition;

		public OnChangeFragmentNo(int previousPosition) {
			this.previousPosition = previousPosition;
		}

		@Override
		public void onClick(DialogInterface arg0, int arg1) {
			if (previousPosition != -1) {
				fragmentPositionCurrent = previousPosition;
			}
		}

	}

	private DialogInterface.OnClickListener onChangeFragmentYes = new DialogInterface.OnClickListener() {

		@Override
		public void onClick(DialogInterface dialog, int which) {
			changeFragment(false);
		}
	};

	private void showDialogChangeFragment(int prevPosition, String message) {
		AlertDialog.Builder builder = new AlertDialog.Builder(ActivityMain.this);
		builder.setTitle("Form Data Incomplete");
		message = message.length() > 0 ? message + "\n" : message;
		message = message + "You will not be able to Submit with Incomplete Data. Continue with incomplete data?";
		builder.setMessage(message);
		builder.setCancelable(false);
		builder.setPositiveButton("Yes", onChangeFragmentYes);
		builder.setNegativeButton("No", new OnChangeFragmentNo(prevPosition));
		builder.create().show();
	}


	private void saveRecentData(){
		((InterfaceFragmentCallBack) fragmentCurrent).parseObject();
		MainContainer.mSPEditor.setLastFragmentPositionMonitoring("" + fragmentPositionCurrent);
		MainContainer.mSPEditor.setLastSavedFacilityMonitoring(MainContainer.mObjectFacility.getJSON().toString());
		MainContainer.mSPEditor.setLastSavedJSONMonitoring(MainContainer.mJsonObjectFormData.toString());
	}
	protected  void askPermissions() {
		populatePermissions();

		if (permissions.size() > 0) {
			if (ActivityCompat.shouldShowRequestPermissionRationale(this,
					CAMERA) ||
					ActivityCompat.shouldShowRequestPermissionRationale(this,
							READ_PHONE_STATE) ||
					ActivityCompat.shouldShowRequestPermissionRationale(this,
							Manifest.permission.ACCESS_FINE_LOCATION)) {
				ActivityCompat.requestPermissions(this, permissions.toArray(new String[permissions.size()]), permsRequestCode);
			} else {
				ActivityCompat.requestPermissions(this, permissions.toArray(new String[permissions.size()]), permsRequestCode);
			}
		} else {
			MainContainer.setContextOnResume(ActivityMain.this);
			initializeApplication();
		}
	}

	private void populatePermissions() {
		permissions.clear();
		if(ActivityCompat.checkSelfPermission(this, CAMERA) != PackageManager.PERMISSION_GRANTED)
			permissions.add("android.permission.CAMERA");

		if(ActivityCompat.checkSelfPermission(this, READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED)
			permissions.add("android.permission.READ_PHONE_STATE");

		if(ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
			permissions.add("android.permission.WRITE_EXTERNAL_STORAGE");

		if(ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED)
			permissions.add("android.permission.ACCESS_FINE_LOCATION");
	}
	@Override
	public void onRequestPermissionsResult(int permsRequestCode, String[] permissions, int[] grantResults){
		switch(permsRequestCode){
			case 200:
				boolean allEnabled = true;
				for(int i = 0; i < grantResults.length; i++) {
					if(grantResults[i] != PackageManager.PERMISSION_GRANTED) {
						allEnabled = false;
						break;
					}
				}
				permissionsAllowed = allEnabled;
				if(allEnabled) {
					initializeApplication();
				} else {
					//showMessageAndExit(getString(R.string.permissions), getString(R.string.please_allow_permission));
					askPermissions();
				}
				break;

		}

	}
	/*private void onPermissionGranted() {
		MainContainer.setContextOnResume(ActivityMain.this);
		permissionsAllowed = true;
		initializeApplication();
	}

	//Permissions
	@AfterPermissionGranted(ASK_PERMISSIONS)
	private void methodRequiresPermission() {
		if (EasyPermissions.hasPermissions(this, PERMISSIONS_STREAM)) {
			onPermissionGranted();
		} else {
			EasyPermissions.requestPermissions(this, "Application requires following permissions to work properly", ASK_PERMISSIONS, PERMISSIONS_STREAM);
		}
	}


	@Override
	public void onPermissionsGranted(int requestCode, List<String> perms) {
		String permission = "";
		if (perms != null) {
			for (int i = 0; i < perms.size(); i++) {
				permission += perms.get(i);
			}
		}
		//Toast.makeText(SplashActivity.this, permission, Toast.LENGTH_LONG).show();
	}

	@Override
	public void onPermissionsDenied(int requestCode, List<String> perms) {
		String permissionsName = "";
		String permission = "";
		if (perms != null) {
			for (int i = 0; i < perms.size(); i++) {
				permission += perms.get(i);
				permissionsName += perms.get(i) + "\n";
			}
		}
		new AppSettingsDialog.Builder(this, getString(R.string.please_allow_permission) + "\n" + permissionsName)
				.setTitle("Settings")
				.setPositiveButton("OK")
				.setNegativeButton("Cancel",null)
				.setRequestCode(ASK_PERMISSIONS)
				.build()
				.show();
	}


	@Override
	public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
		super.onRequestPermissionsResult(requestCode, permissions, grantResults);
		EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
	}*/
}