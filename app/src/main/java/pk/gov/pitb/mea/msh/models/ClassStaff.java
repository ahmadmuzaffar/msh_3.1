package pk.gov.pitb.mea.msh.models;

import org.json.JSONObject;

public class ClassStaff {

	public int pk_id, fk_id;
	public String staff_name;
	public String staff_designation_id;
	public String staff_mobile;
	public String staff_cnic;
	public String staff_role;
	public String staff_fk;
	public boolean bIsCommentsReqd;
	public String comments;

	public ClassStaff() {
		setEmptyValues();
	}

	public ClassStaff(ClassStaff copyObject) {
		copyValues(copyObject);
	}

	public void copyValues(ClassStaff copyObject) {
		this.pk_id = copyObject.pk_id;
		this.fk_id = copyObject.fk_id;
		this.staff_name = copyObject.staff_name;
		this.staff_designation_id = copyObject.staff_designation_id;
		this.staff_mobile = copyObject.staff_mobile;
		this.staff_cnic = copyObject.staff_cnic;
		this.staff_role = copyObject.staff_role;
		this.staff_fk = copyObject.staff_fk;
		this.bIsCommentsReqd = copyObject.bIsCommentsReqd;
		this.comments = copyObject.comments;
	}

	public void setEmptyValues() {
		pk_id = -1;
		fk_id = -1;
		staff_name = "";
		staff_designation_id = "";
		staff_mobile = "";
		staff_cnic = "";
		staff_role = "";
		staff_fk = "";
		bIsCommentsReqd = false;
		comments = "";
	}

	public JSONObject getJSON() {
		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject.put("staff_mobile", staff_mobile);
			jsonObject.put("fid_id", staff_designation_id);
			jsonObject.put("staff_cnic", staff_cnic);
			jsonObject.put("staff_name", staff_name);
			jsonObject.put("comments", comments);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jsonObject;
	}
}