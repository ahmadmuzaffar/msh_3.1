package pk.gov.pitb.mea.msh.fragmentactivities;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import pk.gov.pitb.mea.msh.R;
import pk.gov.pitb.mea.msh.handlers.InterfaceFragmentCallBack;
import pk.gov.pitb.mea.msh.helpers.Constants;
import pk.gov.pitb.mea.msh.helpers.MainContainer;
import pk.gov.pitb.mea.msh.helpers.UIHelper;
import pk.gov.pitb.mea.msh.models.ClassFacilityItem;
import pk.gov.pitb.mea.msh.models.ClassScreenData;
import pk.gov.pitb.mea.msh.models.ClassScreenItem;
import pk.gov.pitb.mea.msh.models.ClassScreenItemMultiple;
import pk.gov.pitb.mea.msh.textwatchers.TextWatcherCountTwoChilds;
import pk.gov.pitb.mea.msh.views.CustomEditText;

public class FragmentVacancyData extends Fragment implements InterfaceFragmentCallBack {

	private boolean bIsSanctionedEditable;
	private ArrayList<ClassScreenItemMultiple> arrayListAllVacancyStatus;
	private ArrayList<ClassFacilityItem> arrayListVacancyStatusSanctioned;

	private int indexVacancyData;
	private View parentView;
	private LinearLayout linearLayoutMain, linearLayoutStart, linearLayoutEnd;
	RadioGroup rg1, rg2;
	RadioButton rb1, rb2, rb3, rb4, rb5, rb6;
	private TextView[] textViewsVacancyStatus;
	private TableLayout[] tableLayoutsVacancyStatus;

	@Override
	public void parseObject() {
		try {
			JSONArray jsonArrayPositions = new JSONArray();
			for (int j = 0; j < tableLayoutsVacancyStatus.length; j++) {
				ClassScreenItemMultiple objectMultiple = arrayListAllVacancyStatus.get(j);
				for (int i = 1; i < tableLayoutsVacancyStatus[j].getChildCount(); i++) {
					View child = tableLayoutsVacancyStatus[j].getChildAt(i);
					ClassScreenItem object = objectMultiple.arrayListFields.get(i - 1);
					if (child instanceof TableRow) {
						JSONObject jsonObjectChild = new JSONObject();

						TableRow tableRow = (TableRow) child;
						EditText sanctioned = (EditText) tableRow.getChildAt(2);
						EditText vacant = (EditText) tableRow.getChildAt(3);
						EditText filled = (EditText) tableRow.getChildAt(4);

						jsonObjectChild.put("vp_id", object.item_id);
						jsonObjectChild.put("position", object.item_name);
						jsonObjectChild.put("sanctioned", sanctioned.getText().toString().trim());
						jsonObjectChild.put("vacant", vacant.getText().toString().trim());
						jsonObjectChild.put("filled", filled.getText().toString().trim());

						jsonArrayPositions.put(jsonObjectChild);
					}
				}
			}

			JSONObject jsonObject = new JSONObject();
			String focalPerson = "";
			String gynaecologist = "";
			String anesthetist = "";

			if(rb1.isChecked())
				focalPerson = "yes";
			else if(rb2.isChecked())
				focalPerson = "no";

			if(linearLayoutEnd.getVisibility() == View.VISIBLE){

				if(rb3.isChecked())
					gynaecologist = "yes";
				else if(rb4.isChecked())
					gynaecologist = "no";

				if(rb5.isChecked())
					anesthetist = "yes";
				else if(rb6.isChecked())
					anesthetist = "no";
			}

			jsonObject.put("posts", jsonArrayPositions);
			jsonObject.put("focal_person", focalPerson);
			jsonObject.put("gynaecologist", gynaecologist);
			jsonObject.put("anesthetist", anesthetist);

			MainContainer.mJsonObjectFormData.remove(Constants.DB_SCREEN_NAME_VACANCY_DATA);
			MainContainer.mJsonObjectFormData.put(Constants.DB_SCREEN_NAME_VACANCY_DATA, jsonObject);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	private void loadSavedData(){
		try {
			if (MainContainer.mJsonObjectFormData.has(Constants.DB_SCREEN_NAME_VACANCY_DATA)){
				JSONObject jsonObject = MainContainer.mJsonObjectFormData.getJSONObject(Constants.DB_SCREEN_NAME_VACANCY_DATA);
				JSONArray jsonArray = jsonObject.getJSONArray("posts");
				String focalPerson = jsonObject.getString("focal_person");
				String gynaecologist = jsonObject.getString("gynaecologist");
				String anesthetist = jsonObject.getString("anesthetist");
				int arrayCount = 0 ;
				for (int j = 0; j < tableLayoutsVacancyStatus.length; j++) {
					ClassScreenItemMultiple objectMultiple = arrayListAllVacancyStatus.get(j);
					for (int i = 1; i < tableLayoutsVacancyStatus[j].getChildCount(); i++) {
						View child = tableLayoutsVacancyStatus[j].getChildAt(i);
						if (child instanceof TableRow) {
							JSONObject jsonObjectChild = jsonArray.getJSONObject(arrayCount++);
							TableRow tableRow = (TableRow) child;
							EditText sanctioned = (EditText) tableRow.getChildAt(2);
							EditText vacant = (EditText) tableRow.getChildAt(3);
							EditText filled = (EditText) tableRow.getChildAt(4);

							sanctioned.setText(jsonObjectChild.getString("sanctioned"));
							vacant.setText(jsonObjectChild.getString("vacant"));
							filled.setText(jsonObjectChild.getString("filled"));
						}
					}
				}
				if(focalPerson.equals("yes")) {
					rb1.setChecked(true);

					if (gynaecologist.equals("yes"))
						rb3.setChecked(true);
					else if (gynaecologist.equals("no"))
						rb4.setChecked(true);

					if (anesthetist.equals("yes"))
						rb5.setChecked(true);
					else if (anesthetist.equals("no"))
						rb6.setChecked(true);
				}
				else if(focalPerson.equals("no"))
					rb2.setChecked(true);
			}
		}
		catch (Exception e){

		}
	}
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		if (parentView == null) {
			parentView = inflater.inflate(R.layout.fragment_default, container, false);
			initializeData();
			generateBody();

			linearLayoutMain.setVisibility(View.GONE);
			linearLayoutEnd.setVisibility(View.GONE);

			loadSavedData();
		}
		return parentView;
	}

	private void initializeData() {
		try {
			ClassScreenData objectScreenData = new ClassScreenData();
			objectScreenData.setDefaultValues();
			objectScreenData.screen_id = Constants.DB_SCREEN_ID_VACANCY_DATA;
			objectScreenData.screen_name = Constants.DB_SCREEN_NAME_VACANCY_DATA;
			arrayListAllVacancyStatus = MainContainer.mDbAdapter.selectScreenItemsMultiple(objectScreenData);
			arrayListVacancyStatusSanctioned = MainContainer.mObjectFacilityData.getListSanctionedStaff();
			bIsSanctionedEditable = MainContainer.mObjectFacilityData.getIsSanctionedStaffEditable();

			rb1 = new RadioButton(getActivity());
			rb2 = new RadioButton(getActivity());
			rb3 = new RadioButton(getActivity());
			rb4 = new RadioButton(getActivity());
			rb5 = new RadioButton(getActivity());
			rb6 = new RadioButton(getActivity());

			rb1.setText("Yes");
			rb1.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					linearLayoutMain.setVisibility(View.VISIBLE);

					addQuestion("Other than gynaecologist, is there a postgraduate trainee present with training to conduct C section?", linearLayoutEnd, rb3, rb4);
					addQuestion("Other than anesthetist, is there a doctor with at least two years anesthesia training present to administer anesthesia?", linearLayoutEnd, rb5, rb6);

					linearLayoutEnd.setVisibility(View.VISIBLE);
				}
			});

			rb2.setText("No");
			rb2.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					linearLayoutMain.setVisibility(View.GONE);

					if(linearLayoutEnd.getVisibility() == View.VISIBLE) {
						rg1 = (RadioGroup) linearLayoutEnd.getChildAt(1);
						rg1.removeAllViews();

						rg2 = (RadioGroup) linearLayoutEnd.getChildAt(3);
						rg2.removeAllViews();
						linearLayoutEnd.removeAllViews();

						rb3.setChecked(false);
						rb4.setChecked(false);
						rb5.setChecked(false);
						rb6.setChecked(false);

						linearLayoutEnd.setVisibility(View.GONE);
					}


				}
			});

			rb3.setText("Yes");
			rb4.setText("No");
			rb5.setText("Yes");
			rb6.setText("No");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void addQuestion(String text, LinearLayout layout, RadioButton radioButton1, RadioButton radioButton2){

		TextView question = new TextView(getActivity());
		question.setText(text);
		layout.addView(question);

		RadioGroup radioGroup = new RadioGroup(getActivity());
		radioGroup.addView(radioButton1);
		radioGroup.addView(radioButton2);
		radioGroup.setOrientation(LinearLayout.HORIZONTAL);

		layout.addView(radioGroup);

	}

	private void generateBody() {
		try {
			linearLayoutMain = (LinearLayout) parentView.findViewById(R.id.linearlayout_main);
			linearLayoutStart = (LinearLayout) parentView.findViewById(R.id.linearlayout_start);
			linearLayoutEnd = (LinearLayout) parentView.findViewById(R.id.linearlayout_end);
			textViewsVacancyStatus = new TextView[arrayListAllVacancyStatus.size()];
			tableLayoutsVacancyStatus = new TableLayout[arrayListAllVacancyStatus.size()];

			addQuestion("Is focal person for checking staff availability present?", linearLayoutStart, rb1, rb2);

			TableRow.LayoutParams[] paramsArrayTableRowColumns = new TableRow.LayoutParams[5];
			paramsArrayTableRowColumns[0] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.1), TableRow.LayoutParams.MATCH_PARENT);
			paramsArrayTableRowColumns[1] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.35), TableRow.LayoutParams.WRAP_CONTENT);
			paramsArrayTableRowColumns[2] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.18), TableRow.LayoutParams.MATCH_PARENT);
			paramsArrayTableRowColumns[3] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.15), TableRow.LayoutParams.MATCH_PARENT);
			paramsArrayTableRowColumns[4] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.15), TableRow.LayoutParams.MATCH_PARENT);
			for (int i = 1; i < paramsArrayTableRowColumns.length; i++) {
				paramsArrayTableRowColumns[i].setMargins(2, 0, 0, 0);
			}

			UIHelper uiContainer = new UIHelper();
			indexVacancyData = 0;
			for (int i = 0; i < arrayListAllVacancyStatus.size(); i++) {
				textViewsVacancyStatus[i] = new TextView(MainContainer.mContext);
				tableLayoutsVacancyStatus[i] = new TableLayout(MainContainer.mContext);
				if (arrayListAllVacancyStatus.get(i).arrayListFields.size() > 0) {
					linearLayoutMain.addView(textViewsVacancyStatus[i]);
					linearLayoutMain.addView(tableLayoutsVacancyStatus[i]);
					uiContainer.generateTableLabel(textViewsVacancyStatus[i], arrayListAllVacancyStatus.get(i).label);
					uiContainer.generateTableHeader(tableLayoutsVacancyStatus[i], paramsArrayTableRowColumns, new String[] { "No.", "Position", "Sanctioned", "Vacant", "Filled" });
					generateTableVacancyStatus(paramsArrayTableRowColumns, tableLayoutsVacancyStatus[i], arrayListAllVacancyStatus.get(i).arrayListFields);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void generateTableVacancyStatus(TableRow.LayoutParams[] paramsArrayTableRowColumns, TableLayout tableLayout, ArrayList<ClassScreenItem> arrayListItems) {
		int colorBgTableRow = MainContainer.mContext.getResources().getColor(R.color.row_background);
		for (int i = 0; i < arrayListItems.size(); i++, indexVacancyData++) {
			boolean bIsOdd = i % 2 == 0;

			TableRow tableRow = new TableRow(MainContainer.mContext);
			CustomEditText editTextSerialNum = new CustomEditText(bIsOdd, "" + (indexVacancyData + 1));
			CustomEditText editTextPosition = new CustomEditText(bIsOdd, arrayListItems.get(i).item_name);
			CustomEditText editTextSanctioned = new CustomEditText(bIsOdd);
			CustomEditText editTextVacant = new CustomEditText(bIsOdd);
			CustomEditText editTextFilled = new CustomEditText(bIsOdd);

			editTextSerialNum.customiseEditText(paramsArrayTableRowColumns[0]);
			editTextPosition.customiseEditText(paramsArrayTableRowColumns[1]);
			editTextSanctioned.customiseEditText(paramsArrayTableRowColumns[2]);
			editTextVacant.customiseEditText(paramsArrayTableRowColumns[3]);
			editTextFilled.customiseEditText(paramsArrayTableRowColumns[4]);

			tableRow.setBackgroundColor(colorBgTableRow);
			tableLayout.addView(tableRow);

			editTextVacant.setInputTypeNumberEditable();
			editTextFilled.setInputTypeNumberNonEditable();
			editTextFilled.setHint("None");

			editTextSanctioned.setText(MainContainer.searchForValue(arrayListItems.get(i).item_id, arrayListVacancyStatusSanctioned));
			if (bIsSanctionedEditable) {
				editTextSanctioned.setInputTypeNumberEditable();
			} else {
				editTextSanctioned.setInputTypeNumberNonEditable();
			}

			editTextSanctioned.addTextChangedListener(new TextWatcherCountTwoChilds(editTextSanctioned, editTextVacant, editTextFilled, null, null));
			editTextVacant.addTextChangedListener(new TextWatcherCountTwoChilds(editTextSanctioned, editTextVacant, editTextFilled, "Vacant Count", "Sanctioned Count"));

			tableRow.addView(editTextSerialNum);
			tableRow.addView(editTextPosition);
			tableRow.addView(editTextSanctioned);
			tableRow.addView(editTextVacant);
			tableRow.addView(editTextFilled);
		}
	}

	@Override
	public void onFragmentShown() {
	}

	private String errorMessage;

	@Override
	public String onFragmentChanged(int previousPosition) {
		errorMessage = null;
		isFormValid();
		return errorMessage;
	}

	@Override
	public boolean isFormValid() {
		try {
			boolean bIsValid = true;
			if(linearLayoutMain.getVisibility() == View.VISIBLE) {
				for (int j = 0; j < tableLayoutsVacancyStatus.length; j++) {
					for (int i = 1; i < tableLayoutsVacancyStatus[j].getChildCount(); i++) {
						View child = tableLayoutsVacancyStatus[j].getChildAt(i);
						if (child instanceof TableRow) {
							TableRow tableRow = (TableRow) child;
							String vacant = ((EditText) tableRow.getChildAt(3)).getText().toString().trim();
							if (vacant.length() == 0) {
								bIsValid = false;
							}
						}
					}
				}
			}
			if (!bIsValid) {
				errorMessage = "Please fill all vacant fields to continue";
			}
			return bIsValid;
		} catch (Exception e) {
			e.printStackTrace();
			MainContainer.showErrorMessage();
			return false;
		}
	}
}