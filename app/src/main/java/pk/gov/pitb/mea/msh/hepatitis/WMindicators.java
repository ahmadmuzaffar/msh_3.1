package pk.gov.pitb.mea.msh.hepatitis;

import android.app.Fragment;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.util.ArrayList;

import pk.gov.pitb.mea.msh.R;
import pk.gov.pitb.mea.msh.bo.ClassScreenItemMultiSelect;
import pk.gov.pitb.mea.msh.handlers.HandlerFragmentCallBackHepatitis;
import pk.gov.pitb.mea.msh.helpers.AlertDialogs;
import pk.gov.pitb.mea.msh.helpers.Constants;
import pk.gov.pitb.mea.msh.helpers.MainContainer;
import pk.gov.pitb.mea.msh.helpers.UIHelper;
import pk.gov.pitb.mea.msh.models.ClassScreenData;
import pk.gov.pitb.mea.msh.models.ClassScreenItem;
import pk.gov.pitb.mea.msh.views.CustomEditText;
import pk.gov.pitb.mea.msh.views.CustomToggleButtonTwoState;


/**
 * Created by Muhammad Abdullah Azam Khan.
 */
public class WMindicators extends Fragment implements HandlerFragmentCallBackHepatitis {

    /*private Globals MainContainer;*/
    private View parentView;

    private ArrayList<ClassScreenItem> arrayListDisplays;
    private ArrayList<ClassScreenItem> arrayListIncineratorInspection;
    private ArrayList<ClassScreenItem> arrayListIncineratorInspectionBefore;
    private ArrayList<ClassScreenItem> arrayListYellowRoom;
    private ArrayList<ClassScreenItem> arrayListBurialPit;
    private ArrayList<ClassScreenItem> arrayListInfectionControl;
    private ArrayList<ClassScreenItem> arrayListHWManagementArticle;
    private ArrayList<ClassScreenItem> arrayListICEMaterialDisplay;
    private ArrayList<ClassScreenItem> arrayListIncineratorInspectionSection9;
    private ArrayList<ClassScreenItem> arrayListWasteManagement;
    private ArrayList<ClassScreenItem> arrayListOutsideHospitalBuilding;


    private Button buttonPrevious, buttonNext, buttonSubmit, buttonProceed;

    //private ArrayList<ClassScreenItem> arrayListDisplaysDropdown;

    private LinearLayout linearLayoutMain;
    private EditText commentBox;

    private TextView textViewDisplays;
    private TableLayout tableLayoutDisplays;

    private TextView textViewIncineratorInspectionSection1;
    private TableLayout tableLayoutIncineratorInspectionSection1;
    private TableLayout tableLayoutIncineratorInspectionSectionBefore;

    private TextView textViewYellowRoomSection2;
    private TableLayout tableLayoutYellowRoomSection2;

    private TextView textViewBurialPitSection3;
    private TableLayout tableLayoutBurialPitSection3;

    private TextView textViewInfectionControlSection4;
    private TableLayout tableLayoutInfectionControlSection4;

    private TextView textViewHWManagementArticleSection5;
    private TableLayout tableLayoutHWManagementArticleSection5;

    private TextView textViewICEMaterialDisplaySection6;
    private TableLayout tableLayoutICEMaterialDisplaySection6;

    private TextView textViewWasteManagementSection7;
    private TableLayout tableLayoutWasteManagementSection7;

    private TextView textViewOutsideHospitalBuildingSection8;
    private TableLayout tableLayoutOutsideHospitalBuildingSection8;

    private TextView textViewIncineratorInspectionSection9;
    private TableLayout tableLayoutIncineratorInspectionSection9;

    private TextView textViewWardBins;
    private EditText editTextWards, editTextWardBins;

    RadioButton radioButtonIncineratorYes, radioButtonIncineratorNo;

    @Override
    public void parseObject() {
        try {

            JSONArray jsonArraySectionBefore = new JSONArray();
            if(tableLayoutIncineratorInspectionSectionBefore.getVisibility() == View.VISIBLE) {
                jsonArraySectionBefore = new JSONArray();
                for (int i = 1; i < tableLayoutIncineratorInspectionSectionBefore.getChildCount(); i++) {
                    View child = tableLayoutIncineratorInspectionSectionBefore.getChildAt(i);
                    if (child instanceof TableRow) {
                        JSONObject jsonObjectChild = new JSONObject();

                        TableRow tableRow = (TableRow) child;
                        EditText editText1 = (EditText) tableRow.getChildAt(1);
                        EditText editText2 = (EditText) tableRow.getChildAt(2);
                        EditText editText3 = (EditText) tableRow.getChildAt(3);
                        EditText editText4 = (EditText) tableRow.getChildAt(4);
                        EditText editText5 = (EditText) tableRow.getChildAt(5);

                        jsonObjectChild.put("mwd_lying_open", editText1.getText().toString());
                        jsonObjectChild.put("mwd_burnt", editText2.getText().toString());
                        jsonObjectChild.put("mwd_buried", editText3.getText().toString());
                        jsonObjectChild.put("mwd_carried_away", editText4.getText().toString());
                        jsonObjectChild.put("mwd_forwarded", editText5.getText().toString());

                        jsonArraySectionBefore.put(jsonObjectChild);
                    }
                }
            }

            JSONArray jsonArraySection5 = new JSONArray();
            for (int i = 1; i < tableLayoutIncineratorInspectionSection1.getChildCount(); i++) {
                View child = tableLayoutIncineratorInspectionSection1.getChildAt(i);
                ClassScreenItem object = arrayListIncineratorInspection.get(i - 1);
                if (child instanceof TableRow) {
                    JSONObject jsonObjectChild = new JSONObject();

                    TableRow tableRow = (TableRow) child;
                    CustomToggleButtonTwoState displayed = (CustomToggleButtonTwoState) tableRow.getChildAt(2);

                    String displayedID = "";

                    if (!displayed.isCheckChanged()) {
                        displayedID = "";
                    } else if (displayed.isChecked()) {
                        displayedID = /*arrayListDisplaysDropdown.get(0).item_id*/"yes";
                    } else if (displayed.isEnabled()) {
                        displayedID = /*arrayListDisplaysDropdown.get(1).item_id*/"no";
                    }
                    jsonObjectChild.put("wm_id", object.item_id);
                    jsonObjectChild.put("wm_name", object.item_name);
                    jsonObjectChild.put("status", displayedID);

                    jsonArraySection5.put(jsonObjectChild);
                }
            }

            JSONArray jsonArraySection6 = new JSONArray();
            for (int i = 1; i < tableLayoutYellowRoomSection2.getChildCount(); i++) {
                View child = tableLayoutYellowRoomSection2.getChildAt(i);
                ClassScreenItem object = arrayListYellowRoom.get(i - 1);
                if (child instanceof TableRow) {
                    JSONObject jsonObjectChild = new JSONObject();

                    TableRow tableRow = (TableRow) child;
                    CustomToggleButtonTwoState displayed = (CustomToggleButtonTwoState) tableRow.getChildAt(2);

                    String displayedID = "";

                    if (!displayed.isCheckChanged()) {
                        displayedID = "";
                    } else if (displayed.isChecked()) {
                        displayedID = /*arrayListDisplaysDropdown.get(0).item_id*/"yes";
                    } else if (displayed.isEnabled()) {
                        displayedID = /*arrayListDisplaysDropdown.get(1).item_id*/"no";
                    }
                    jsonObjectChild.put("wm_id", object.item_id);
                    jsonObjectChild.put("wm_name", object.item_name);
                    jsonObjectChild.put("status", displayedID);

                    jsonArraySection6.put(jsonObjectChild);
                }
            }

            JSONArray jsonArraySection7 = new JSONArray();
            for (int i = 1; i < tableLayoutBurialPitSection3.getChildCount(); i++) {
                View child = tableLayoutBurialPitSection3.getChildAt(i);
                ClassScreenItem object = arrayListBurialPit.get(i - 1);
                if (child instanceof TableRow) {
                    JSONObject jsonObjectChild = new JSONObject();

                    TableRow tableRow = (TableRow) child;
                    CustomToggleButtonTwoState displayed = (CustomToggleButtonTwoState) tableRow.getChildAt(2);

                    String displayedID = "";

                    if (!displayed.isCheckChanged()) {
                        displayedID = "";
                    } else if (displayed.isChecked()) {
                        displayedID = /*arrayListDisplaysDropdown.get(0).item_id*/"yes";
                    } else if (displayed.isEnabled()) {
                        displayedID = /*arrayListDisplaysDropdown.get(1).item_id*/"no";
                    }
                    jsonObjectChild.put("wm_id", object.item_id);
                    jsonObjectChild.put("wm_name", object.item_name);
                    jsonObjectChild.put("status", displayedID);

                    jsonArraySection7.put(jsonObjectChild);
                }
            }

            JSONArray jsonArraySection8 = new JSONArray();
            for (int i = 1; i < tableLayoutInfectionControlSection4.getChildCount(); i++) {
                View child = tableLayoutInfectionControlSection4.getChildAt(i);
                ClassScreenItem object = arrayListInfectionControl.get(i - 1);
                if (child instanceof TableRow) {
                    JSONObject jsonObjectChild = new JSONObject();

                    TableRow tableRow = (TableRow) child;
                    CustomToggleButtonTwoState displayed = (CustomToggleButtonTwoState) tableRow.getChildAt(2);

                    String displayedID = "";

                    if (!displayed.isCheckChanged()) {
                        displayedID = "";
                    } else if (displayed.isChecked()) {
                        displayedID = /*arrayListDisplaysDropdown.get(0).item_id*/"yes";
                    } else if (displayed.isEnabled()) {
                        displayedID = /*arrayListDisplaysDropdown.get(1).item_id*/"no";
                    }
                    jsonObjectChild.put("wm_id", object.item_id);
                    jsonObjectChild.put("wm_name", object.item_name);
                    jsonObjectChild.put("status", displayedID);

                    jsonArraySection8.put(jsonObjectChild);
                }
            }

            JSONArray jsonArraySection9 = new JSONArray();
            for (int i = 1; i < tableLayoutHWManagementArticleSection5.getChildCount(); i++) {
                View child = tableLayoutHWManagementArticleSection5.getChildAt(i);
                ClassScreenItem object = arrayListHWManagementArticle.get(i - 1);
                if (child instanceof TableRow) {
                    JSONObject jsonObjectChild = new JSONObject();

                    TableRow tableRow = (TableRow) child;
                    CustomToggleButtonTwoState displayed = (CustomToggleButtonTwoState) tableRow.getChildAt(2);

                    String displayedID = "";

                    if (!displayed.isCheckChanged()) {
                        displayedID = "";
                    } else if (displayed.isChecked()) {
                        displayedID = /*arrayListDisplaysDropdown.get(0).item_id*/"yes";
                    } else if (displayed.isEnabled()) {
                        displayedID = /*arrayListDisplaysDropdown.get(1).item_id*/"no";
                    }
                    jsonObjectChild.put("wm_id", object.item_id);
                    jsonObjectChild.put("wm_name", object.item_name);
                    jsonObjectChild.put("status", displayedID);

                    jsonArraySection9.put(jsonObjectChild);
                }
            }

            JSONArray jsonArraySection10 = new JSONArray();
            for (int i = 1; i < tableLayoutICEMaterialDisplaySection6.getChildCount(); i++) {
                View child = tableLayoutICEMaterialDisplaySection6.getChildAt(i);
                ClassScreenItem object = arrayListICEMaterialDisplay.get(i - 1);
                if (child instanceof TableRow) {
                    JSONObject jsonObjectChild = new JSONObject();

                    TableRow tableRow = (TableRow) child;
                    CustomToggleButtonTwoState displayed = (CustomToggleButtonTwoState) tableRow.getChildAt(2);

                    String displayedID = "";

                    if (!displayed.isCheckChanged()) {
                        displayedID = "";
                    } else if (displayed.isChecked()) {
                        displayedID = /*arrayListDisplaysDropdown.get(0).item_id*/"yes";
                    } else if (displayed.isEnabled()) {
                        displayedID = /*arrayListDisplaysDropdown.get(1).item_id*/"no";
                    }
                    jsonObjectChild.put("wm_id", object.item_id);
                    jsonObjectChild.put("wm_name", object.item_name);
                    jsonObjectChild.put("status", displayedID);

                    jsonArraySection10.put(jsonObjectChild);
                }
            }

            JSONArray jsonArraySection11 = new JSONArray();
            for (int i = 1; i < tableLayoutWasteManagementSection7.getChildCount(); i++) {
                View child = tableLayoutWasteManagementSection7.getChildAt(i);
                ClassScreenItem object = arrayListWasteManagement.get(i - 1);
                if (child instanceof TableRow) {
                    JSONObject jsonObjectChild = new JSONObject();

                    TableRow tableRow = (TableRow) child;
                    CustomToggleButtonTwoState displayed = (CustomToggleButtonTwoState) tableRow.getChildAt(2);

                    String displayedID = "";

                    if (!displayed.isCheckChanged()) {
                        displayedID = "";
                    } else if (displayed.isChecked()) {
                        displayedID = /*arrayListDisplaysDropdown.get(0).item_id*/"yes";
                    } else if (displayed.isEnabled()) {
                        displayedID = /*arrayListDisplaysDropdown.get(1).item_id*/"no";
                    }
                    jsonObjectChild.put("wm_id", object.item_id);
                    jsonObjectChild.put("wm_name", object.item_name);
                    jsonObjectChild.put("status", displayedID);

                    jsonArraySection11.put(jsonObjectChild);
                }
            }

            JSONArray jsonArraySection12 = new JSONArray();
            for (int i = 1; i < tableLayoutOutsideHospitalBuildingSection8.getChildCount(); i++) {
                View child = tableLayoutOutsideHospitalBuildingSection8.getChildAt(i);
                ClassScreenItem object = arrayListOutsideHospitalBuilding.get(i - 1);
                if (child instanceof TableRow) {
                    JSONObject jsonObjectChild = new JSONObject();

                    TableRow tableRow = (TableRow) child;
                    CustomToggleButtonTwoState displayed = (CustomToggleButtonTwoState) tableRow.getChildAt(2);

                    String displayedID = "";

                    if (!displayed.isCheckChanged()) {
                        displayedID = "";
                    } else if (displayed.isChecked()) {
                        displayedID = /*arrayListDisplaysDropdown.get(0).item_id*/"yes";
                    } else if (displayed.isEnabled()) {
                        displayedID = /*arrayListDisplaysDropdown.get(1).item_id*/"no";
                    }
                    jsonObjectChild.put("wm_id", object.item_id);
                    jsonObjectChild.put("wm_name", object.item_name);
                    jsonObjectChild.put("status", displayedID);

                    jsonArraySection12.put(jsonObjectChild);
                }
            }

            JSONArray jsonArraySection13 = new JSONArray();
            if(tableLayoutIncineratorInspectionSection9.getVisibility() == View.VISIBLE) {
                jsonArraySection13 = new JSONArray();
                for (int i = 1; i < tableLayoutIncineratorInspectionSection9.getChildCount(); i++) {
                    View child = tableLayoutIncineratorInspectionSection9.getChildAt(i);
                    ClassScreenItem object = arrayListIncineratorInspectionSection9.get(i - 1);
                    if (child instanceof TableRow) {
                        JSONObject jsonObjectChild = new JSONObject();

                        TableRow tableRow = (TableRow) child;
                        CustomToggleButtonTwoState displayed = (CustomToggleButtonTwoState) tableRow.getChildAt(2);

                        String displayedID = "";

                        if (!displayed.isCheckChanged()) {
                            displayedID = "";
                        } else if (displayed.isChecked()) {
                            displayedID = /*arrayListDisplaysDropdown.get(0).item_id*/"yes";
                        } else if (displayed.isEnabled()) {
                            displayedID = /*arrayListDisplaysDropdown.get(1).item_id*/"no";
                        }
                        jsonObjectChild.put("wm_id", object.item_id);
                        jsonObjectChild.put("wm_name", object.item_name);
                        jsonObjectChild.put("status", displayedID);

                        jsonArraySection13.put(jsonObjectChild);
                    }
                }
            }

            JSONObject jsonObject = new JSONObject();

            if (radioButtonIncineratorYes.isChecked()) {
                jsonObject.put("incinerator_facility", "yes");
            } else if (radioButtonIncineratorNo.isChecked()) {
                jsonObject.put("incinerator_facility", "no");
            } else {
                jsonObject.put("incinerator_facility", "");
            }

            jsonObject.put("comments", commentBox.getText().toString());

            jsonObject.put("sectionBefore", jsonArraySectionBefore);
            jsonObject.put("section1", jsonArraySection5);
            jsonObject.put("section2", jsonArraySection6);
            jsonObject.put("section3", jsonArraySection7);
            jsonObject.put("section4", jsonArraySection8);
            jsonObject.put("section5", jsonArraySection9);
            jsonObject.put("section6", jsonArraySection10);
            jsonObject.put("section7", jsonArraySection11);
            jsonObject.put("section8", jsonArraySection12);
            jsonObject.put("section9", jsonArraySection13);

            jsonObject.put("wards_in_facility", editTextWards.getText().toString().trim());
            jsonObject.put("set_of_bins_in_wards", editTextWardBins.getText().toString().trim());
            MainContainer.mJsonObjectFormData.remove(Constants.DB_SCREEN_NAME_WMI);
            MainContainer.mJsonObjectFormData.put(Constants.DB_SCREEN_NAME_WMI, jsonObject);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void loadSavedData() {
        try {
            if (MainContainer.mJsonObjectFormData.has(Constants.DB_SCREEN_NAME_WMI)) {
                JSONObject jsonObjectComment = MainContainer.mJsonObjectFormData.getJSONObject(Constants.DB_SCREEN_NAME_WMI);

                JSONArray JSONArraySections1 = jsonObjectComment.has("section1") ? jsonObjectComment.getJSONArray("section1") : new JSONArray();

                for (int i = 0; i < JSONArraySections1.length(); i++) {
                    boolean bIsOdd = i % 2 == 0;

                    View child = tableLayoutIncineratorInspectionSection1.getChildAt(i + 1);
                    JSONObject jObject = JSONArraySections1.getJSONObject(i);

                    if (child instanceof TableRow) {

                        TableRow tableRow = (TableRow) child;
                        CustomToggleButtonTwoState displayedToggleButton = (CustomToggleButtonTwoState) tableRow.getChildAt(2);
                        String IncineratorInspection_ID = jObject.getString("wm_id");
                        String dd_ID = jObject.getString("status");
                        if (dd_ID.equals("yes")) {

                            if (i == 0)
                                tableLayoutIncineratorInspectionSection9.setVisibility(View.VISIBLE);

                            displayedToggleButton.setbIsCheckChanged(true);
                            displayedToggleButton.setChecked(true);
                            displayedToggleButton.setBackgroundResource(bIsOdd ? R.drawable.toggle_tick_odd : R.drawable.toggle_tick_even);
                        } else if (dd_ID.equals("no")) {

                            if (i == 0)
                                tableLayoutIncineratorInspectionSection9.setVisibility(View.GONE);

                            displayedToggleButton.setbIsCheckChanged(true);
                            displayedToggleButton.setChecked(false);
                            displayedToggleButton.setBackgroundResource(bIsOdd ? R.drawable.toggle_cross_odd : R.drawable.toggle_cross_even);
                        }

                    }
                }

                JSONArray JSONArraySections2 = jsonObjectComment.has("section2") ? jsonObjectComment.getJSONArray("section2") : new JSONArray();

                for (int i = 0; i < JSONArraySections2.length(); i++) {
                    boolean bIsOdd = i % 2 == 0;

                    View child = tableLayoutYellowRoomSection2.getChildAt(i + 1);
                    JSONObject jObject = JSONArraySections2.getJSONObject(i);

                    if (child instanceof TableRow) {

                        TableRow tableRow = (TableRow) child;
                        CustomToggleButtonTwoState displayedToggleButton = (CustomToggleButtonTwoState) tableRow.getChildAt(2);
                        String YellowRoom_ID = jObject.getString("wm_id");
                        String dd_ID = jObject.getString("status");
                        if (dd_ID.equals("yes")) {
                            displayedToggleButton.setbIsCheckChanged(true);
                            displayedToggleButton.setChecked(true);
                            displayedToggleButton.setBackgroundResource(bIsOdd ? R.drawable.toggle_tick_odd : R.drawable.toggle_tick_even);
                        } else if (dd_ID.equals("no")) {
                            displayedToggleButton.setbIsCheckChanged(true);
                            displayedToggleButton.setChecked(false);
                            displayedToggleButton.setBackgroundResource(bIsOdd ? R.drawable.toggle_cross_odd : R.drawable.toggle_cross_even);
                        }

                    }
                }

                JSONArray JSONArraySections3 = jsonObjectComment.has("section3") ? jsonObjectComment.getJSONArray("section3") : new JSONArray();

                for (int i = 0; i < JSONArraySections3.length(); i++) {
                    boolean bIsOdd = i % 2 == 0;

                    View child = tableLayoutBurialPitSection3.getChildAt(i + 1);
                    JSONObject jObject = JSONArraySections3.getJSONObject(i);

                    if (child instanceof TableRow) {

                        TableRow tableRow = (TableRow) child;
                        CustomToggleButtonTwoState displayedToggleButton = (CustomToggleButtonTwoState) tableRow.getChildAt(2);
                        String BurialPit_ID = jObject.getString("wm_id");
                        String dd_ID = jObject.getString("status");
                        if (dd_ID.equals("yes")) {
                            displayedToggleButton.setbIsCheckChanged(true);
                            displayedToggleButton.setChecked(true);
                            displayedToggleButton.setBackgroundResource(bIsOdd ? R.drawable.toggle_tick_odd : R.drawable.toggle_tick_even);
                        } else if (dd_ID.equals("no")) {
                            displayedToggleButton.setbIsCheckChanged(true);
                            displayedToggleButton.setChecked(false);
                            displayedToggleButton.setBackgroundResource(bIsOdd ? R.drawable.toggle_cross_odd : R.drawable.toggle_cross_even);
                        }

                    }
                }

                JSONArray JSONArraySections4 = jsonObjectComment.has("section4") ? jsonObjectComment.getJSONArray("section4") : new JSONArray();

                for (int i = 0; i < JSONArraySections4.length(); i++) {
                    boolean bIsOdd = i % 2 == 0;

                    View child = tableLayoutInfectionControlSection4.getChildAt(i + 1);
                    JSONObject jObject = JSONArraySections4.getJSONObject(i);

                    if (child instanceof TableRow) {

                        TableRow tableRow = (TableRow) child;
                        CustomToggleButtonTwoState displayedToggleButton = (CustomToggleButtonTwoState) tableRow.getChildAt(2);
                        String InfectionControl_ID = jObject.getString("wm_id");
                        String dd_ID = jObject.getString("status");
                        if (dd_ID.equals("yes")) {
                            displayedToggleButton.setbIsCheckChanged(true);
                            displayedToggleButton.setChecked(true);
                            displayedToggleButton.setBackgroundResource(bIsOdd ? R.drawable.toggle_tick_odd : R.drawable.toggle_tick_even);
                        } else if (dd_ID.equals("no")) {
                            displayedToggleButton.setbIsCheckChanged(true);
                            displayedToggleButton.setChecked(false);
                            displayedToggleButton.setBackgroundResource(bIsOdd ? R.drawable.toggle_cross_odd : R.drawable.toggle_cross_even);
                        }

                    }
                }

                JSONArray JSONArraySections5 = jsonObjectComment.has("section5") ? jsonObjectComment.getJSONArray("section5") : new JSONArray();

                for (int i = 0; i < JSONArraySections5.length(); i++) {
                    boolean bIsOdd = i % 2 == 0;

                    View child = tableLayoutHWManagementArticleSection5.getChildAt(i + 1);
                    JSONObject jObject = JSONArraySections5.getJSONObject(i);

                    if (child instanceof TableRow) {

                        TableRow tableRow = (TableRow) child;
                        CustomToggleButtonTwoState displayedToggleButton = (CustomToggleButtonTwoState) tableRow.getChildAt(2);
                        String HWManagementArticle_ID = jObject.getString("wm_id");
                        String dd_ID = jObject.getString("status");
                        if (dd_ID.equals("yes")) {
                            displayedToggleButton.setbIsCheckChanged(true);
                            displayedToggleButton.setChecked(true);
                            displayedToggleButton.setBackgroundResource(bIsOdd ? R.drawable.toggle_tick_odd : R.drawable.toggle_tick_even);
                        } else if (dd_ID.equals("no")) {
                            displayedToggleButton.setbIsCheckChanged(true);
                            displayedToggleButton.setChecked(false);
                            displayedToggleButton.setBackgroundResource(bIsOdd ? R.drawable.toggle_cross_odd : R.drawable.toggle_cross_even);
                        }

                    }
                }

                JSONArray JSONArraySections6 = jsonObjectComment.has("section6") ? jsonObjectComment.getJSONArray("section6") : new JSONArray();

                for (int i = 0; i < JSONArraySections6.length(); i++) {
                    boolean bIsOdd = i % 2 == 0;

                    View child = tableLayoutICEMaterialDisplaySection6.getChildAt(i + 1);
                    JSONObject jObject = JSONArraySections6.getJSONObject(i);

                    if (child instanceof TableRow) {

                        TableRow tableRow = (TableRow) child;
                        CustomToggleButtonTwoState displayedToggleButton = (CustomToggleButtonTwoState) tableRow.getChildAt(2);
                        String ICEMaterialDisplay_ID = jObject.getString("wm_id");
                        String dd_ID = jObject.getString("status");
                        if (dd_ID.equals("yes")) {
                            displayedToggleButton.setbIsCheckChanged(true);
                            displayedToggleButton.setChecked(true);
                            displayedToggleButton.setBackgroundResource(bIsOdd ? R.drawable.toggle_tick_odd : R.drawable.toggle_tick_even);
                        } else if (dd_ID.equals("no")) {
                            displayedToggleButton.setbIsCheckChanged(true);
                            displayedToggleButton.setChecked(false);
                            displayedToggleButton.setBackgroundResource(bIsOdd ? R.drawable.toggle_cross_odd : R.drawable.toggle_cross_even);
                        }

                    }
                }

                JSONArray JSONArraySections7 = jsonObjectComment.has("section7") ? jsonObjectComment.getJSONArray("section7") : new JSONArray();

                for (int i = 0; i < JSONArraySections7.length(); i++) {
                    boolean bIsOdd = i % 2 == 0;

                    View child = tableLayoutWasteManagementSection7.getChildAt(i + 1);
                    JSONObject jObject = JSONArraySections7.getJSONObject(i);

                    if (child instanceof TableRow) {

                        TableRow tableRow = (TableRow) child;
                        CustomToggleButtonTwoState displayedToggleButton = (CustomToggleButtonTwoState) tableRow.getChildAt(2);
                        String ICEMaterialDisplay_ID = jObject.getString("wm_id");
                        String dd_ID = jObject.getString("status");
                        if (dd_ID.equals("yes")) {
                            displayedToggleButton.setbIsCheckChanged(true);
                            displayedToggleButton.setChecked(true);
                            displayedToggleButton.setBackgroundResource(bIsOdd ? R.drawable.toggle_tick_odd : R.drawable.toggle_tick_even);
                        } else if (dd_ID.equals("no")) {
                            displayedToggleButton.setbIsCheckChanged(true);
                            displayedToggleButton.setChecked(false);
                            displayedToggleButton.setBackgroundResource(bIsOdd ? R.drawable.toggle_cross_odd : R.drawable.toggle_cross_even);
                        }

                    }
                }

                JSONArray JSONArraySections8 = jsonObjectComment.has("section8") ? jsonObjectComment.getJSONArray("section8") : new JSONArray();

                for (int i = 0; i < JSONArraySections8.length(); i++) {
                    boolean bIsOdd = i % 2 == 0;

                    View child = tableLayoutOutsideHospitalBuildingSection8.getChildAt(i + 1);
                    JSONObject jObject = JSONArraySections8.getJSONObject(i);

                    if (child instanceof TableRow) {

                        TableRow tableRow = (TableRow) child;
                        CustomToggleButtonTwoState displayedToggleButton = (CustomToggleButtonTwoState) tableRow.getChildAt(2);
                        String ICEMaterialDisplay_ID = jObject.getString("wm_id");
                        String dd_ID = jObject.getString("status");
                        if (dd_ID.equals("yes")) {
                            displayedToggleButton.setbIsCheckChanged(true);
                            displayedToggleButton.setChecked(true);
                            displayedToggleButton.setBackgroundResource(bIsOdd ? R.drawable.toggle_tick_odd : R.drawable.toggle_tick_even);
                        } else if (dd_ID.equals("no")) {
                            displayedToggleButton.setbIsCheckChanged(true);
                            displayedToggleButton.setChecked(false);
                            displayedToggleButton.setBackgroundResource(bIsOdd ? R.drawable.toggle_cross_odd : R.drawable.toggle_cross_even);
                        }

                    }
                }

                JSONArray JSONArraySections9 = jsonObjectComment.has("section9") ? jsonObjectComment.getJSONArray("section9") : new JSONArray();

                for (int i = 0; i < JSONArraySections9.length(); i++) {
                    boolean bIsOdd = i % 2 == 0;

                    View child = tableLayoutIncineratorInspectionSection9.getChildAt(i + 1);
                    JSONObject jObject = JSONArraySections9.getJSONObject(i);

                    if (child instanceof TableRow) {

                        TableRow tableRow = (TableRow) child;
                        CustomToggleButtonTwoState displayedToggleButton = (CustomToggleButtonTwoState) tableRow.getChildAt(2);
                        String IncineratorInspection_ID = jObject.getString("wm_id");
                        String dd_ID = jObject.getString("status");
                        if (dd_ID.equals("yes")) {
                            displayedToggleButton.setbIsCheckChanged(true);
                            displayedToggleButton.setChecked(true);
                            displayedToggleButton.setBackgroundResource(bIsOdd ? R.drawable.toggle_tick_odd : R.drawable.toggle_tick_even);
                        } else if (dd_ID.equals("no")) {
                            displayedToggleButton.setbIsCheckChanged(true);
                            displayedToggleButton.setChecked(false);
                            displayedToggleButton.setBackgroundResource(bIsOdd ? R.drawable.toggle_cross_odd : R.drawable.toggle_cross_even);
                        }

                    }
                }

                String wards_in_facility = jsonObjectComment.has("wards_in_facility") ? jsonObjectComment.getString("wards_in_facility") : "";
                editTextWards.setText(wards_in_facility);
                String bins_in_wards = jsonObjectComment.has("set_of_bins_in_wards") ? jsonObjectComment.getString("set_of_bins_in_wards") : "";
                editTextWardBins.setText(bins_in_wards);

                String incineratorAvailable = jsonObjectComment.getString("incinerator_facility");
                String commentString = jsonObjectComment.getString("comments");

                if (incineratorAvailable.equals("yes")) {
                    radioButtonIncineratorYes.setChecked(true);
                } else if (incineratorAvailable.equals("no")) {
                    radioButtonIncineratorNo.setChecked(true);
                }

                commentBox.setText(commentString);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String errorMessage;

    @Override
    public String onFragmentChanged(int previousPosition) {
        errorMessage = null;
        isFormValid();
        return errorMessage;
    }

    @Override
    public boolean isFormValid() {
        try {
            boolean bIsValid = true;

            if(tableLayoutIncineratorInspectionSectionBefore.getVisibility() == View.VISIBLE){
                for (int i = 1; i < tableLayoutIncineratorInspectionSectionBefore.getChildCount(); i++) {
                    View child = tableLayoutIncineratorInspectionSectionBefore.getChildAt(i);
                    if (child instanceof TableRow) {
                        TableRow tableRow = (TableRow) child;
                        CustomEditText editText1 = (CustomEditText) tableRow.getChildAt(1);
                        CustomEditText editText2 = (CustomEditText) tableRow.getChildAt(2);
                        CustomEditText editText3 = (CustomEditText) tableRow.getChildAt(3);
                        CustomEditText editText4 = (CustomEditText) tableRow.getChildAt(4);
                        CustomEditText editText5 = (CustomEditText) tableRow.getChildAt(5);

                        if(editText1.getText().toString().equals("")){
                            bIsValid = false;
                            errorMessage = "Please Enter Values in Table Before Incineratoe Inspection";
                            return bIsValid;
                        }
                        if(editText2.getText().toString().equals("")){
                            bIsValid = false;
                            errorMessage = "Please Enter Values in Table Before Incineratoe Inspection";
                            return bIsValid;
                        }
                        if(editText3.getText().toString().equals("")){
                            bIsValid = false;
                            errorMessage = "Please Enter Values in Table Before Incineratoe Inspection";
                            return bIsValid;
                        }
                        if(editText4.getText().toString().equals("")){
                            bIsValid = false;
                            errorMessage = "Please Enter Values in Table Before Incineratoe Inspection";
                            return bIsValid;
                        }
                        if(editText5.getText().toString().equals("")){
                            bIsValid = false;
                            errorMessage = "Please Enter Values in Table Before Incineratoe Inspection";
                            return bIsValid;
                        }
                    }
                }
            }

            for (int i = 1; i < tableLayoutIncineratorInspectionSection1.getChildCount(); i++) {
                View child = tableLayoutIncineratorInspectionSection1.getChildAt(i);
                if (child instanceof TableRow) {
                    TableRow tableRow = (TableRow) child;
                    CustomToggleButtonTwoState displayed = (CustomToggleButtonTwoState) tableRow.getChildAt(2);

                    if (!displayed.isCheckChanged()) {
                        bIsValid = false;
                        errorMessage = "Please select all display values from Incinerator Inspection to continue";
                        return bIsValid;
                    }
                }
            }

            for (int i = 1; i < tableLayoutYellowRoomSection2.getChildCount(); i++) {
                View child = tableLayoutYellowRoomSection2.getChildAt(i);
                if (child instanceof TableRow) {
                    TableRow tableRow = (TableRow) child;
                    CustomToggleButtonTwoState displayed = (CustomToggleButtonTwoState) tableRow.getChildAt(2);

                    if (!displayed.isCheckChanged()) {
                        bIsValid = false;
                        errorMessage = "Please select all display values from Yellow Room to continue";
                        return bIsValid;
                    }
                }
            }

            for (int i = 1; i < tableLayoutBurialPitSection3.getChildCount(); i++) {
                View child = tableLayoutBurialPitSection3.getChildAt(i);
                if (child instanceof TableRow) {
                    TableRow tableRow = (TableRow) child;
                    CustomToggleButtonTwoState displayed = (CustomToggleButtonTwoState) tableRow.getChildAt(2);

                    if (!displayed.isCheckChanged()) {
                        bIsValid = false;
                        errorMessage = "Please select all display values from Burial Pit to continue";
                        return bIsValid;
                    }
                }
            }

            if(tableLayoutInfectionControlSection4.getVisibility() == View.VISIBLE) {
                for (int i = 1; i < tableLayoutInfectionControlSection4.getChildCount(); i++) {
                    View child = tableLayoutInfectionControlSection4.getChildAt(i);
                    if (child instanceof TableRow) {
                        TableRow tableRow = (TableRow) child;
                        CustomToggleButtonTwoState displayed = (CustomToggleButtonTwoState) tableRow.getChildAt(2);

                        if (!displayed.isCheckChanged()) {
                            bIsValid = false;
                            errorMessage = "Please select all display values from Infection Control to continue";
                            return bIsValid;
                        }
                    }
                }
            }

            for (int i = 1; i < tableLayoutHWManagementArticleSection5.getChildCount(); i++) {
                View child = tableLayoutHWManagementArticleSection5.getChildAt(i);
                if (child instanceof TableRow) {
                    TableRow tableRow = (TableRow) child;
                    CustomToggleButtonTwoState displayed = (CustomToggleButtonTwoState) tableRow.getChildAt(2);

                    if (!displayed.isCheckChanged()) {
                        bIsValid = false;
                        errorMessage = "Please select all display values from Hospital Waste Management Articles to continue";
                        return bIsValid;
                    }
                }
            }

            for (int i = 1; i < tableLayoutICEMaterialDisplaySection6.getChildCount(); i++) {
                View child = tableLayoutICEMaterialDisplaySection6.getChildAt(i);
                if (child instanceof TableRow) {
                    TableRow tableRow = (TableRow) child;
                    CustomToggleButtonTwoState displayed = (CustomToggleButtonTwoState) tableRow.getChildAt(2);

                    if (!displayed.isCheckChanged()) {
                        bIsValid = false;
                        errorMessage = "Please select all display values from ICE Material Display to continue";
                        return bIsValid;
                    }
                }
            }

            for (int i = 1; i < tableLayoutWasteManagementSection7.getChildCount(); i++) {
                View child = tableLayoutWasteManagementSection7.getChildAt(i);
                if (child instanceof TableRow) {
                    TableRow tableRow = (TableRow) child;
                    CustomToggleButtonTwoState displayed = (CustomToggleButtonTwoState) tableRow.getChildAt(2);

                    if (!displayed.isCheckChanged()) {
                        bIsValid = false;
                        errorMessage = "Please select all display values from Waste Management to continue";
                        return bIsValid;
                    }
                }
            }

            for (int i = 1; i < tableLayoutOutsideHospitalBuildingSection8.getChildCount(); i++) {
                View child = tableLayoutOutsideHospitalBuildingSection8.getChildAt(i);
                if (child instanceof TableRow) {
                    TableRow tableRow = (TableRow) child;
                    CustomToggleButtonTwoState displayed = (CustomToggleButtonTwoState) tableRow.getChildAt(2);

                    if (!displayed.isCheckChanged()) {
                        bIsValid = false;
                        errorMessage = "Please select all display values from Outside hospital building to continue";
                        return bIsValid;
                    }
                }
            }

            if (tableLayoutIncineratorInspectionSection9.getVisibility() == View.VISIBLE) {
                for (int i = 1; i < tableLayoutIncineratorInspectionSection9.getChildCount(); i++) {
                    View child = tableLayoutIncineratorInspectionSection9.getChildAt(i);
                    if (child instanceof TableRow) {
                        TableRow tableRow = (TableRow) child;
                        CustomToggleButtonTwoState displayed = (CustomToggleButtonTwoState) tableRow.getChildAt(2);

                        if (!displayed.isCheckChanged()) {
                            bIsValid = false;
                            errorMessage = "Please select all display values from Incinerator Inspection Sub Table to continue";
                            return bIsValid;
                        }
                    }
                }
            }

//            if (editTextWards.getText().toString().trim().length() == 0) {
//                bIsValid = false;
//                errorMessage = "Please fill Wards in the facility to continue";
//                return bIsValid;
//            }
//
//            if (editTextWardBins.getText().toString().trim().length() == 0) {
//                bIsValid = false;
//                errorMessage = "Please fill No. of Bins in wards to continue";
//                return bIsValid;
//            }

            if(commentBox.getText().toString().equals("")){
                bIsValid = false;
                errorMessage = "Please enter comments to continue";
                return bIsValid;
            }
            parseObject();
            return bIsValid;
        } catch (Exception e) {
            e.printStackTrace();
            MainContainer.showMessage("Error Processing Your Request");
            return false;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (parentView == null) {
            parentView = inflater.inflate(R.layout.fragment_hepatitis_wmi, container, false);
            /*MainContainer = Globals.getInstance();*/
            initFragmentData();
            generateBody();
            loadSavedData();
            listeners();
        }
        return parentView;
    }

    private void listeners() {

        TableRow child = (TableRow) tableLayoutIncineratorInspectionSection1.getChildAt(1);
        final CustomToggleButtonTwoState button = (CustomToggleButtonTwoState) child.getChildAt(2);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (button.isChecked()) {
                    tableLayoutIncineratorInspectionSection9.setVisibility(View.VISIBLE);
                    textViewIncineratorInspectionSection9.setVisibility(View.VISIBLE);
                } else {
                    tableLayoutIncineratorInspectionSection9.setVisibility(View.GONE);
                    textViewIncineratorInspectionSection9.setVisibility(View.GONE);
                }
            }
        });

    }

    private void initFragmentData() {
        try {

            radioButtonIncineratorYes = new RadioButton(MainContainer.mContext);
            radioButtonIncineratorNo = new RadioButton(MainContainer.mContext);

            radioButtonIncineratorYes.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    tableLayoutIncineratorInspectionSectionBefore.setVisibility(View.GONE);
                }
            });

            radioButtonIncineratorNo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    tableLayoutIncineratorInspectionSectionBefore.setVisibility(View.VISIBLE);
                }
            });

            arrayListIncineratorInspectionBefore = new ArrayList<>();

            ClassScreenData objectScreenData = new ClassScreenData();
            objectScreenData.setDefaultValues();
            objectScreenData.screen_id = Constants.DB_SCREEN_ID_WMI;
            objectScreenData.screen_name = Constants.DB_SCREEN_NAME_WMI;

            objectScreenData.section_id = "";
            objectScreenData.type_id = Constants.DB_TYPE_ID_HEPATITIS;
            objectScreenData.type_name = Constants.DB_TYPE_NAME_HEPATITIS;

            objectScreenData.section_name = "section1";
            objectScreenData.section_value = "fields";
            objectScreenData = MainContainer.mDbAdapter.selectScreenData(objectScreenData);
            arrayListIncineratorInspection = MainContainer.mDbAdapter.selectScreenItems(objectScreenData);

            objectScreenData.section_name = "section2";
            objectScreenData.section_value = "fields";
            objectScreenData = MainContainer.mDbAdapter.selectScreenData(objectScreenData);
            arrayListYellowRoom = MainContainer.mDbAdapter.selectScreenItems(objectScreenData);

            objectScreenData.section_name = "section3";
            objectScreenData.section_value = "fields";
            objectScreenData = MainContainer.mDbAdapter.selectScreenData(objectScreenData);
            arrayListBurialPit = MainContainer.mDbAdapter.selectScreenItems(objectScreenData);

            objectScreenData.section_name = "section4";
            objectScreenData.section_value = "fields";
            objectScreenData = MainContainer.mDbAdapter.selectScreenData(objectScreenData);
            arrayListInfectionControl = MainContainer.mDbAdapter.selectScreenItems(objectScreenData);

            objectScreenData.section_name = "section5";
            objectScreenData.section_value = "fields";
            objectScreenData = MainContainer.mDbAdapter.selectScreenData(objectScreenData);
            arrayListHWManagementArticle = MainContainer.mDbAdapter.selectScreenItems(objectScreenData);

            objectScreenData.section_name = "section6";
            objectScreenData.section_value = "fields";
            objectScreenData = MainContainer.mDbAdapter.selectScreenData(objectScreenData);
            arrayListICEMaterialDisplay = MainContainer.mDbAdapter.selectScreenItems(objectScreenData);

            objectScreenData.section_name = "section7";
            objectScreenData.section_value = "fields";
            objectScreenData = MainContainer.mDbAdapter.selectScreenData(objectScreenData);
            arrayListWasteManagement = MainContainer.mDbAdapter.selectScreenItems(objectScreenData);

            objectScreenData.section_name = "section8";
            objectScreenData.section_value = "fields";
            objectScreenData = MainContainer.mDbAdapter.selectScreenData(objectScreenData);
            arrayListOutsideHospitalBuilding = MainContainer.mDbAdapter.selectScreenItems(objectScreenData);

            objectScreenData.section_name = "section9";
            objectScreenData.section_value = "fields";
            objectScreenData = MainContainer.mDbAdapter.selectScreenData(objectScreenData);
            arrayListIncineratorInspectionSection9 = MainContainer.mDbAdapter.selectScreenItems(objectScreenData);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }  //Done

    private void generateBody() {
        try {

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            params.setMargins(2, 0, 0, 0);

            TextView question = new TextView(getActivity());
            question.setText("If the incinerator available in the facility status?");
            question.setLayoutParams(params);

            commentBox = new EditText(getActivity());

            params.setMargins(2, 0, 0, 5);
            final Field f = TextView.class.getDeclaredField("mCursorDrawableRes");
            f.setAccessible(true);
            f.set(commentBox, R.drawable.xml_color_cursor);
            commentBox.setHint("Enter Comments");
            commentBox.setLayoutParams(params);
            commentBox.setTextColor(getResources().getColor(R.color.row_body_text));
            commentBox.setBackgroundColor(getResources().getColor(R.color.row_body_background_single));

            RadioGroup radioGroup = new RadioGroup(getActivity());
            radioButtonIncineratorYes.setText("Yes");
            radioButtonIncineratorNo.setText("No");
            radioGroup.addView(radioButtonIncineratorYes);
            radioGroup.addView(radioButtonIncineratorNo);
            radioGroup.setOrientation(LinearLayout.HORIZONTAL);

            linearLayoutMain = (LinearLayout) parentView.findViewById(R.id.linearlayout_main);
            buttonNext = (Button) parentView.findViewById(R.id.button_next);
            buttonSubmit = (Button) parentView.findViewById(R.id.button_submit);
            buttonPrevious = (Button) parentView.findViewById(R.id.button_previous);
            buttonPrevious.setVisibility(View.GONE);

            textViewWardBins = (TextView) parentView.findViewById(R.id.ward_bin_availability);
            editTextWards = (EditText) parentView.findViewById(R.id.no_of_wards);
            editTextWardBins = (EditText) parentView.findViewById(R.id.ward_with_bins);

//            textViewDisplays = new TextView(MainContainer.mContext);
//            tableLayoutDisplays = new TableLayout(MainContainer.mContext);
//            linearLayoutMain.addView(textViewDisplays);
//            linearLayoutMain.addView(tableLayoutDisplays);

            textViewIncineratorInspectionSection1 = new TextView(MainContainer.mContext);
            tableLayoutIncineratorInspectionSectionBefore = new TableLayout(MainContainer.mContext);
            tableLayoutIncineratorInspectionSection1 = new TableLayout(MainContainer.mContext);
            linearLayoutMain.addView(textViewIncineratorInspectionSection1, ((ViewGroup) textViewWardBins.getParent()).indexOfChild(textViewWardBins));
            linearLayoutMain.addView(question, ((ViewGroup) textViewWardBins.getParent()).indexOfChild(textViewWardBins));
            linearLayoutMain.addView(radioGroup, ((ViewGroup) textViewWardBins.getParent()).indexOfChild(textViewWardBins));
            linearLayoutMain.addView(tableLayoutIncineratorInspectionSectionBefore, ((ViewGroup) textViewWardBins.getParent()).indexOfChild(textViewWardBins));
            linearLayoutMain.addView(tableLayoutIncineratorInspectionSection1, ((ViewGroup) textViewWardBins.getParent()).indexOfChild(textViewWardBins));

            textViewIncineratorInspectionSection9 = new TextView(MainContainer.mContext);
            tableLayoutIncineratorInspectionSection9 = new TableLayout(MainContainer.mContext);
            linearLayoutMain.addView(textViewIncineratorInspectionSection9, ((ViewGroup) textViewWardBins.getParent()).indexOfChild(textViewWardBins));
            linearLayoutMain.addView(tableLayoutIncineratorInspectionSection9, ((ViewGroup) textViewWardBins.getParent()).indexOfChild(textViewWardBins));

            textViewYellowRoomSection2 = new TextView(MainContainer.mContext);
            tableLayoutYellowRoomSection2 = new TableLayout(MainContainer.mContext);
            linearLayoutMain.addView(textViewYellowRoomSection2, ((ViewGroup) textViewWardBins.getParent()).indexOfChild(textViewWardBins));
            linearLayoutMain.addView(tableLayoutYellowRoomSection2, ((ViewGroup) textViewWardBins.getParent()).indexOfChild(textViewWardBins));


            textViewBurialPitSection3 = new TextView(MainContainer.mContext);
            tableLayoutBurialPitSection3 = new TableLayout(MainContainer.mContext);
            linearLayoutMain.addView(textViewBurialPitSection3, ((ViewGroup) textViewWardBins.getParent()).indexOfChild(textViewWardBins));
            linearLayoutMain.addView(tableLayoutBurialPitSection3, ((ViewGroup) textViewWardBins.getParent()).indexOfChild(textViewWardBins));

            textViewInfectionControlSection4 = new TextView(MainContainer.mContext);
            tableLayoutInfectionControlSection4 = new TableLayout(MainContainer.mContext);
            linearLayoutMain.addView(textViewInfectionControlSection4, ((ViewGroup) textViewWardBins.getParent()).indexOfChild(textViewWardBins));
            linearLayoutMain.addView(tableLayoutInfectionControlSection4, ((ViewGroup) textViewWardBins.getParent()).indexOfChild(textViewWardBins));

            textViewHWManagementArticleSection5 = new TextView(MainContainer.mContext);
            tableLayoutHWManagementArticleSection5 = new TableLayout(MainContainer.mContext);
            linearLayoutMain.addView(textViewHWManagementArticleSection5, ((ViewGroup) textViewWardBins.getParent()).indexOfChild(textViewWardBins));
            linearLayoutMain.addView(tableLayoutHWManagementArticleSection5, ((ViewGroup) textViewWardBins.getParent()).indexOfChild(textViewWardBins));

            textViewICEMaterialDisplaySection6 = new TextView(MainContainer.mContext);
            tableLayoutICEMaterialDisplaySection6 = new TableLayout(MainContainer.mContext);
            linearLayoutMain.addView(textViewICEMaterialDisplaySection6, ((ViewGroup) textViewWardBins.getParent()).indexOfChild(textViewWardBins));
            linearLayoutMain.addView(tableLayoutICEMaterialDisplaySection6, ((ViewGroup) textViewWardBins.getParent()).indexOfChild(textViewWardBins));

            textViewWasteManagementSection7 = new TextView(MainContainer.mContext);
            tableLayoutWasteManagementSection7 = new TableLayout(MainContainer.mContext);
            linearLayoutMain.addView(textViewWasteManagementSection7, ((ViewGroup)textViewWardBins.getParent()).indexOfChild(textViewWardBins));
            linearLayoutMain.addView(tableLayoutWasteManagementSection7, ((ViewGroup)textViewWardBins.getParent()).indexOfChild(textViewWardBins));

            textViewOutsideHospitalBuildingSection8 = new TextView(MainContainer.mContext);
            tableLayoutOutsideHospitalBuildingSection8 = new TableLayout(MainContainer.mContext);
            linearLayoutMain.addView(textViewOutsideHospitalBuildingSection8, ((ViewGroup)textViewWardBins.getParent()).indexOfChild(textViewWardBins));
            linearLayoutMain.addView(tableLayoutOutsideHospitalBuildingSection8, ((ViewGroup)textViewWardBins.getParent()).indexOfChild(textViewWardBins));

            linearLayoutMain.addView(commentBox, ((ViewGroup) textViewWardBins.getParent()).indexOfChild(textViewWardBins));

            LinearLayout.LayoutParams lpButtonProceed = new LinearLayout.LayoutParams((int) (MainContainer.mScreenWidth * 0.5),
                    (int) (MainContainer.mScreenWidth * 0.1));
            lpButtonProceed.setMargins(0, (int) (MainContainer.mScreenHeight * 0.05), 0, 0);
            buttonProceed = new Button(MainContainer.mContext);
            buttonProceed.setBackgroundDrawable(MainContainer.mActivity.getResources().getDrawable(R.drawable.background_btn_continue));
            buttonProceed.setGravity(Gravity.CENTER);
            buttonProceed.setLayoutParams(lpButtonProceed);
            //linearLayoutMain.addView(buttonProceed);

            buttonProceed.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    /*MainContainer.activityCallBackHepatitis.showNextFragment();*/

					/*if(isFormValid()) {
						if (MainContainer.mObjectFacilityData.facility_sentinel_second_type.equals(Constants.DB_SECOND_TYPE_ID_SENTINEL)) {
							MainContainer.activityCallBackHepatitis.showNextFragment();
						} else {
							MainContainer.activityCallBackHepatitis.submitData();
						}
					}else{
						AlertDialogs.getInstance().showDialogOK(null, errorMessage, null, true);
					}*/
                    //MainContainer.activityCallBackNutrients.submitData();
                }
            });


/*            TableRow.LayoutParams[] paramsArrayTableRowColumns = new TableRow.LayoutParams[3];
            paramsArrayTableRowColumns[0] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.1), TableRow.LayoutParams.MATCH_PARENT);
            paramsArrayTableRowColumns[1] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.66), (int) (MainContainer.mScreenWidth * 0.08));
            paramsArrayTableRowColumns[2] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.2), (int) (MainContainer.mScreenWidth * 0.08));
            for (int i = 1; i < paramsArrayTableRowColumns.length; i++) {
                paramsArrayTableRowColumns[i].setMargins(2, 0, 0, 0);
            }

            UIHelper uiContainer = new UIHelper();
            uiContainer.generateTableLabel(textViewDisplays, "Waste Management Indicators");

            uiContainer.generateTableHeader(tableLayoutDisplays, paramsArrayTableRowColumns, new String[]{"Sr#", "Name", "Status"});
            generateTableDisplays(paramsArrayTableRowColumns);

            if (tableLayoutDisplays.getChildCount() > 1) {
                textViewDisplays.setVisibility(View.VISIBLE);
                tableLayoutDisplays.setVisibility(View.VISIBLE);
            } else {
                textViewDisplays.setVisibility(View.GONE);
                tableLayoutDisplays.setVisibility(View.GONE);
            }*/

            TableRow.LayoutParams[] paramsArrayTableRowColumnsIncineratorInspection = new TableRow.LayoutParams[3];
            paramsArrayTableRowColumnsIncineratorInspection[0] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.1),
                    (int) (MainContainer.mScreenWidth * 0.08));
            paramsArrayTableRowColumnsIncineratorInspection[1] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.66),
                    (int) (MainContainer.mScreenWidth * 0.08));
            paramsArrayTableRowColumnsIncineratorInspection[2] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.2),
                    (int) (MainContainer.mScreenWidth * 0.08));

            for (int i = 1; i < paramsArrayTableRowColumnsIncineratorInspection.length; i++) {
                paramsArrayTableRowColumnsIncineratorInspection[i].setMargins(2, 0, 0, 0);
            }

            TableRow.LayoutParams[] paramsArrayTableRowColumnsIncineratorInspectionBefore = new TableRow.LayoutParams[6];
            paramsArrayTableRowColumnsIncineratorInspectionBefore[0] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.15),
                    ViewGroup.LayoutParams.MATCH_PARENT);
            paramsArrayTableRowColumnsIncineratorInspectionBefore[1] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.15),
                    ViewGroup.LayoutParams.MATCH_PARENT);
            paramsArrayTableRowColumnsIncineratorInspectionBefore[2] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.15),
                    ViewGroup.LayoutParams.MATCH_PARENT);
            paramsArrayTableRowColumnsIncineratorInspectionBefore[3] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.15),
                    ViewGroup.LayoutParams.MATCH_PARENT);
            paramsArrayTableRowColumnsIncineratorInspectionBefore[4] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.15),
                    ViewGroup.LayoutParams.MATCH_PARENT);
            paramsArrayTableRowColumnsIncineratorInspectionBefore[5] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.15),
                    ViewGroup.LayoutParams.MATCH_PARENT);

            for (int i = 1; i < paramsArrayTableRowColumnsIncineratorInspectionBefore.length; i++) {
                paramsArrayTableRowColumnsIncineratorInspectionBefore[i].setMargins(2, 0, 0, 0);
            }

            UIHelper uiContainer = new UIHelper();
            uiContainer.generateTableLabel(textViewIncineratorInspectionSection1, "Incinerator Inspection");

            uiContainer.generateTableHeader(tableLayoutIncineratorInspectionSectionBefore, paramsArrayTableRowColumnsIncineratorInspectionBefore, new String[]{"Item", "Lying Open", "Burny by other means", "Buried", "Carried away by municipality", "Forwarded to other facility"});
            generateTableIncineratorInspectionBefore(paramsArrayTableRowColumnsIncineratorInspectionBefore);

            uiContainer.generateTableHeader(tableLayoutIncineratorInspectionSection1, paramsArrayTableRowColumnsIncineratorInspection, new String[]{"No.", "Name", "Status"});
            generateTableIncineratorInspection(paramsArrayTableRowColumnsIncineratorInspection);

            if (tableLayoutIncineratorInspectionSection1.getChildCount() > 1) {
                textViewIncineratorInspectionSection1.setVisibility(View.VISIBLE);
                tableLayoutIncineratorInspectionSection1.setVisibility(View.VISIBLE);
            } else {
                textViewIncineratorInspectionSection1.setVisibility(View.GONE);
                tableLayoutIncineratorInspectionSection1.setVisibility(View.GONE);
            }

            tableLayoutIncineratorInspectionSectionBefore.setVisibility(View.GONE);
//            if (tableLayoutIncineratorInspectionSectionBefore.getChildCount() > 1) {
//                tableLayoutIncineratorInspectionSectionBefore.setVisibility(View.VISIBLE);
//            } else {
//                tableLayoutIncineratorInspectionSectionBefore.setVisibility(View.GONE);
//            }

            TableRow.LayoutParams[] paramsArrayTableRowColumnsIncineratorInspectionSection9 = new TableRow.LayoutParams[3];
            paramsArrayTableRowColumnsIncineratorInspectionSection9[0] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.1),
                    (int) (MainContainer.mScreenWidth * 0.08));
            paramsArrayTableRowColumnsIncineratorInspectionSection9[1] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.66),
                    (int) (MainContainer.mScreenWidth * 0.08));
            paramsArrayTableRowColumnsIncineratorInspectionSection9[2] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.2),
                    (int) (MainContainer.mScreenWidth * 0.08));

            for (int i = 1; i < paramsArrayTableRowColumnsIncineratorInspectionSection9.length; i++) {
                paramsArrayTableRowColumnsIncineratorInspectionSection9[i].setMargins(2, 0, 0, 0);
            }

            uiContainer.generateTableLabel(textViewIncineratorInspectionSection9, "Sub Table");

            uiContainer.generateTableHeader(tableLayoutIncineratorInspectionSection9, paramsArrayTableRowColumnsIncineratorInspectionSection9, new String[]{"No.", "Name", "Status"});
            generateTableIncineratorInspectionSection9(paramsArrayTableRowColumnsIncineratorInspectionSection9);

            textViewIncineratorInspectionSection9.setVisibility(View.GONE);
            tableLayoutIncineratorInspectionSection9.setVisibility(View.GONE);

            TableRow.LayoutParams[] paramsArrayTableRowColumnsYellowRoom = new TableRow.LayoutParams[3];
            paramsArrayTableRowColumnsYellowRoom[0] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.1),
                    (int) (MainContainer.mScreenWidth * 0.08));
            paramsArrayTableRowColumnsYellowRoom[1] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.66),
                    (int) (MainContainer.mScreenWidth * 0.08));
            paramsArrayTableRowColumnsYellowRoom[2] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.2),
                    (int) (MainContainer.mScreenWidth * 0.08));

            for (int i = 1; i < paramsArrayTableRowColumnsYellowRoom.length; i++) {
                paramsArrayTableRowColumnsYellowRoom[i].setMargins(2, 0, 0, 0);
            }

            uiContainer.generateTableLabel(textViewYellowRoomSection2, "Yellow Room");

            uiContainer.generateTableHeader(tableLayoutYellowRoomSection2, paramsArrayTableRowColumnsYellowRoom, new String[]{"No.", "Name", "Status"});
            generateTableYellowRoom(paramsArrayTableRowColumnsYellowRoom);

            if (tableLayoutYellowRoomSection2.getChildCount() > 1) {
                textViewYellowRoomSection2.setVisibility(View.VISIBLE);
                tableLayoutYellowRoomSection2.setVisibility(View.VISIBLE);
            } else {
                textViewYellowRoomSection2.setVisibility(View.GONE);
                tableLayoutYellowRoomSection2.setVisibility(View.GONE);
            }

            TableRow.LayoutParams[] paramsArrayTableRowColumnsBurialPit = new TableRow.LayoutParams[3];
            paramsArrayTableRowColumnsBurialPit[0] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.1),
                    (int) (MainContainer.mScreenWidth * 0.08));
            paramsArrayTableRowColumnsBurialPit[1] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.66),
                    (int) (MainContainer.mScreenWidth * 0.08));
            paramsArrayTableRowColumnsBurialPit[2] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.2),
                    (int) (MainContainer.mScreenWidth * 0.08));

            for (int i = 1; i < paramsArrayTableRowColumnsBurialPit.length; i++) {
                paramsArrayTableRowColumnsBurialPit[i].setMargins(2, 0, 0, 0);
            }

            uiContainer.generateTableLabel(textViewBurialPitSection3, "Burial Pit");

            uiContainer.generateTableHeader(tableLayoutBurialPitSection3, paramsArrayTableRowColumnsBurialPit, new String[]{"No.", "Name", "Status"});
            generateTableBurialPit(paramsArrayTableRowColumnsBurialPit);

            if (tableLayoutBurialPitSection3.getChildCount() > 1) {
                textViewBurialPitSection3.setVisibility(View.VISIBLE);
                tableLayoutBurialPitSection3.setVisibility(View.VISIBLE);
            } else {
                textViewBurialPitSection3.setVisibility(View.GONE);
                tableLayoutBurialPitSection3.setVisibility(View.GONE);
            }

            TableRow.LayoutParams[] paramsArrayTableRowColumnsInfectionControl = new TableRow.LayoutParams[3];
            paramsArrayTableRowColumnsInfectionControl[0] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.1),
                    (int) (MainContainer.mScreenWidth * 0.08));
            paramsArrayTableRowColumnsInfectionControl[1] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.66),
                    (int) (MainContainer.mScreenWidth * 0.08));
            paramsArrayTableRowColumnsInfectionControl[2] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.2),
                    (int) (MainContainer.mScreenWidth * 0.08));

            for (int i = 1; i < paramsArrayTableRowColumnsInfectionControl.length; i++) {
                paramsArrayTableRowColumnsInfectionControl[i].setMargins(2, 0, 0, 0);
            }

            uiContainer.generateTableLabel(textViewInfectionControlSection4, "Infection Control");

            uiContainer.generateTableHeader(tableLayoutInfectionControlSection4, paramsArrayTableRowColumnsInfectionControl, new String[]{"No.", "Name", "Status"});
            generateTableInfectionControl(paramsArrayTableRowColumnsInfectionControl);

            if (tableLayoutInfectionControlSection4.getChildCount() > 1) {
                textViewInfectionControlSection4.setVisibility(View.VISIBLE);
                tableLayoutInfectionControlSection4.setVisibility(View.VISIBLE);
            } else {
                textViewInfectionControlSection4.setVisibility(View.GONE);
                tableLayoutInfectionControlSection4.setVisibility(View.GONE);
            }

            TableRow.LayoutParams[] paramsArrayTableRowColumnsHWManagementArticle = new TableRow.LayoutParams[3];
            paramsArrayTableRowColumnsHWManagementArticle[0] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.1),
                    (int) (MainContainer.mScreenWidth * 0.08));
            paramsArrayTableRowColumnsHWManagementArticle[1] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.66),
                    (int) (MainContainer.mScreenWidth * 0.08));
            paramsArrayTableRowColumnsHWManagementArticle[2] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.2),
                    (int) (MainContainer.mScreenWidth * 0.08));

            for (int i = 1; i < paramsArrayTableRowColumnsHWManagementArticle.length; i++) {
                paramsArrayTableRowColumnsHWManagementArticle[i].setMargins(2, 0, 0, 0);
            }

            uiContainer.generateTableLabel(textViewHWManagementArticleSection5, "Hospital Waste Management Articles");

            uiContainer.generateTableHeader(tableLayoutHWManagementArticleSection5, paramsArrayTableRowColumnsHWManagementArticle, new String[]{"No.", "Name", "Status"});
            generateTableHWManagementArticle(paramsArrayTableRowColumnsHWManagementArticle);

            if (tableLayoutHWManagementArticleSection5.getChildCount() > 1) {
                textViewHWManagementArticleSection5.setVisibility(View.VISIBLE);
                tableLayoutHWManagementArticleSection5.setVisibility(View.VISIBLE);
            } else {
                textViewHWManagementArticleSection5.setVisibility(View.GONE);
                tableLayoutHWManagementArticleSection5.setVisibility(View.GONE);
            }

            TableRow.LayoutParams[] paramsArrayTableRowColumnsICEMaterialDisplay = new TableRow.LayoutParams[3];
            paramsArrayTableRowColumnsICEMaterialDisplay[0] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.1),
                    (int) (MainContainer.mScreenWidth * 0.08));
            paramsArrayTableRowColumnsICEMaterialDisplay[1] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.66),
                    (int) (MainContainer.mScreenWidth * 0.08));
            paramsArrayTableRowColumnsICEMaterialDisplay[2] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.2),
                    (int) (MainContainer.mScreenWidth * 0.08));

            for (int i = 1; i < paramsArrayTableRowColumnsICEMaterialDisplay.length; i++) {
                paramsArrayTableRowColumnsICEMaterialDisplay[i].setMargins(2, 0, 0, 0);
            }

            uiContainer.generateTableLabel(textViewICEMaterialDisplaySection6, "ICE Material Display");

            uiContainer.generateTableHeader(tableLayoutICEMaterialDisplaySection6, paramsArrayTableRowColumnsICEMaterialDisplay, new String[]{"No.", "Name", "Status"});
            generateTableICEMaterialDisplay(paramsArrayTableRowColumnsICEMaterialDisplay);

            if (tableLayoutICEMaterialDisplaySection6.getChildCount() > 1) {
                textViewICEMaterialDisplaySection6.setVisibility(View.VISIBLE);
                tableLayoutICEMaterialDisplaySection6.setVisibility(View.VISIBLE);
            } else {
                textViewICEMaterialDisplaySection6.setVisibility(View.GONE);
                tableLayoutICEMaterialDisplaySection6.setVisibility(View.GONE);
            }

            TableRow.LayoutParams[] paramsArrayTableRowColumnsWasteManagement = new TableRow.LayoutParams[3];
            paramsArrayTableRowColumnsWasteManagement[0] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.1),
                    (int) (MainContainer.mScreenWidth * 0.08));
            paramsArrayTableRowColumnsWasteManagement[1] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.66),
                    (int) (MainContainer.mScreenWidth * 0.08));
            paramsArrayTableRowColumnsWasteManagement[2] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.2),
                    (int) (MainContainer.mScreenWidth * 0.08));

            for (int i = 1; i < paramsArrayTableRowColumnsWasteManagement.length; i++) {
                paramsArrayTableRowColumnsWasteManagement[i].setMargins(2, 0, 0, 0);
            }

            uiContainer.generateTableLabel(textViewWasteManagementSection7, "Waste Management");

            uiContainer.generateTableHeader(tableLayoutWasteManagementSection7, paramsArrayTableRowColumnsWasteManagement, new String[]{"No.", "Name", "Status"});
            generateTableWasteManagement(paramsArrayTableRowColumnsWasteManagement);

            if (tableLayoutWasteManagementSection7.getChildCount() > 1) {
                textViewWasteManagementSection7.setVisibility(View.VISIBLE);
                tableLayoutWasteManagementSection7.setVisibility(View.VISIBLE);
            } else {
                textViewWasteManagementSection7.setVisibility(View.GONE);
                tableLayoutWasteManagementSection7.setVisibility(View.GONE);
            }

            TableRow.LayoutParams[] paramsArrayTableRowColumnsOutsideBuilding = new TableRow.LayoutParams[3];
            paramsArrayTableRowColumnsOutsideBuilding[0] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.1),
                    (int) (MainContainer.mScreenWidth * 0.08));
            paramsArrayTableRowColumnsOutsideBuilding[1] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.66),
                    (int) (MainContainer.mScreenWidth * 0.08));
            paramsArrayTableRowColumnsOutsideBuilding[2] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.2),
                    (int) (MainContainer.mScreenWidth * 0.08));

            for (int i = 1; i < paramsArrayTableRowColumnsOutsideBuilding.length; i++) {
                paramsArrayTableRowColumnsOutsideBuilding[i].setMargins(2, 0, 0, 0);
            }

            uiContainer.generateTableLabel(textViewOutsideHospitalBuildingSection8, "Outside Hospital Building");

            uiContainer.generateTableHeader(tableLayoutOutsideHospitalBuildingSection8, paramsArrayTableRowColumnsOutsideBuilding, new String[]{"No.", "Name", "Status"});
            generateTableOutsideHospitalBuilding(paramsArrayTableRowColumnsOutsideBuilding);

            if (tableLayoutOutsideHospitalBuildingSection8.getChildCount() > 1) {
                textViewOutsideHospitalBuildingSection8.setVisibility(View.VISIBLE);
                tableLayoutOutsideHospitalBuildingSection8.setVisibility(View.VISIBLE);
            } else {
                textViewOutsideHospitalBuildingSection8.setVisibility(View.GONE);
                tableLayoutOutsideHospitalBuildingSection8.setVisibility(View.GONE);
            }

            initClickListeners();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }    //Done

    private void initClickListeners() {
        buttonPrevious.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainContainer.activityCallBackHepatitis.showPreviousFragment();
            }
        });

        buttonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isFormValid()) {
                    if (MainContainer.mObjectFacilityData.facility_sentinel_second_type.equals(Constants.DB_SECOND_TYPE_ID_SENTINEL)) {
                        MainContainer.activityCallBackHepatitis.showNextFragment();
                    } else {
                        MainContainer.activityCallBackHepatitis.submitData();
                    }
                } else {
                    AlertDialogs.getInstance().showDialogOK(null, errorMessage, null, true);
                }
            }
        });

        buttonSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
    }

    private void generateTableIncineratorInspectionBefore(TableRow.LayoutParams[] paramsArrayTableRowColumns) {
        int serialNum = 0;
        int colorBgTableRow = MainContainer.mContext.getResources().getColor(R.color.row_background);
        for (int i = 0; i < 1; i++) {
            boolean bIsOdd = i % 2 == 0;
            serialNum++;
//
            TableRow tableRow = new TableRow(MainContainer.mContext);

            TextView textViewDisposal = new TextView(getActivity());
            CustomEditText editTextLying = new CustomEditText(bIsOdd, "");
            CustomEditText editTextBurnt = new CustomEditText(bIsOdd, "");
            CustomEditText editTextBurried = new CustomEditText(bIsOdd, "");
            CustomEditText editTextCarried = new CustomEditText(bIsOdd, "");
            CustomEditText editTextForwarded = new CustomEditText(bIsOdd, "");

            textViewDisposal.setLayoutParams(paramsArrayTableRowColumns[0]);
            textViewDisposal.setText("Disposal of Hospital Waste");
            textViewDisposal.setTextColor(getResources().getColor(R.color.row_body_text));
            if (bIsOdd)
                textViewDisposal.setBackgroundColor(getResources().getColor(R.color.row_body_background_odd));
            else
                textViewDisposal.setBackgroundColor(getResources().getColor(R.color.row_body_background_even));
            editTextLying.customiseEditText(paramsArrayTableRowColumns[1]);
            editTextBurnt.customiseEditText(paramsArrayTableRowColumns[2]);
            editTextBurried.customiseEditText(paramsArrayTableRowColumns[3]);
            editTextCarried.customiseEditText(paramsArrayTableRowColumns[4]);
            editTextForwarded.customiseEditText(paramsArrayTableRowColumns[5]);

            tableRow.setBackgroundColor(colorBgTableRow);
            tableRow.addView(textViewDisposal);
            tableRow.addView(editTextLying);
            tableRow.addView(editTextBurnt);
            tableRow.addView(editTextBurried);
            tableRow.addView(editTextCarried);
            tableRow.addView(editTextForwarded);
            tableLayoutIncineratorInspectionSectionBefore.addView(tableRow);
        }
    }

    private void generateTableIncineratorInspection(TableRow.LayoutParams[] paramsArrayTableRowColumns) {
        int serialNum = 0;
        int colorBgTableRow = MainContainer.mContext.getResources().getColor(R.color.row_background);

        for (ClassScreenItem display : arrayListIncineratorInspection) {
            boolean bIsOdd = serialNum % 2 == 0;
            serialNum++;

            TableRow tableRow = new TableRow(MainContainer.mContext);
            CustomEditText editTextSerialNum = new CustomEditText(bIsOdd, "" + serialNum);
            CustomEditText editTextLocation = new CustomEditText(bIsOdd, display.item_name);
            CustomToggleButtonTwoState toggleButtonDisplayed = new CustomToggleButtonTwoState(bIsOdd);

            editTextSerialNum.customiseEditText(paramsArrayTableRowColumns[0]);
            editTextLocation.customiseEditText(paramsArrayTableRowColumns[1]);
            toggleButtonDisplayed.customiseToggleButton(paramsArrayTableRowColumns[2]);

            tableRow.setBackgroundColor(colorBgTableRow);
            tableRow.addView(editTextSerialNum);
            tableRow.addView(editTextLocation);
            tableRow.addView(toggleButtonDisplayed);
            tableLayoutIncineratorInspectionSection1.addView(tableRow);
        }
    }

    private void generateTableYellowRoom(TableRow.LayoutParams[] paramsArrayTableRowColumns) {
        int serialNum = 0;
        int colorBgTableRow = MainContainer.mContext.getResources().getColor(R.color.row_background);
        for (ClassScreenItem display : arrayListYellowRoom) {
            boolean bIsOdd = serialNum % 2 == 0;
            serialNum++;

            TableRow tableRow = new TableRow(MainContainer.mContext);
            CustomEditText editTextSerialNum = new CustomEditText(bIsOdd, "" + serialNum);
            CustomEditText editTextLocation = new CustomEditText(bIsOdd, display.item_name);
            CustomToggleButtonTwoState toggleButtonDisplayed = new CustomToggleButtonTwoState(bIsOdd);

            editTextSerialNum.customiseEditText(paramsArrayTableRowColumns[0]);
            editTextLocation.customiseEditText(paramsArrayTableRowColumns[1]);
            toggleButtonDisplayed.customiseToggleButton(paramsArrayTableRowColumns[2]);

            tableRow.setBackgroundColor(colorBgTableRow);
            tableRow.addView(editTextSerialNum);
            tableRow.addView(editTextLocation);
            tableRow.addView(toggleButtonDisplayed);
            tableLayoutYellowRoomSection2.addView(tableRow);
        }
    }

    private void generateTableBurialPit(TableRow.LayoutParams[] paramsArrayTableRowColumns) {
        int serialNum = 0;
        int colorBgTableRow = MainContainer.mContext.getResources().getColor(R.color.row_background);
        for (ClassScreenItem display : arrayListBurialPit) {
            boolean bIsOdd = serialNum % 2 == 0;
            serialNum++;

            TableRow tableRow = new TableRow(MainContainer.mContext);
            CustomEditText editTextSerialNum = new CustomEditText(bIsOdd, "" + serialNum);
            CustomEditText editTextLocation = new CustomEditText(bIsOdd, display.item_name);
            CustomToggleButtonTwoState toggleButtonDisplayed = new CustomToggleButtonTwoState(bIsOdd);

            editTextSerialNum.customiseEditText(paramsArrayTableRowColumns[0]);
            editTextLocation.customiseEditText(paramsArrayTableRowColumns[1]);
            toggleButtonDisplayed.customiseToggleButton(paramsArrayTableRowColumns[2]);

            tableRow.setBackgroundColor(colorBgTableRow);
            tableRow.addView(editTextSerialNum);
            tableRow.addView(editTextLocation);
            tableRow.addView(toggleButtonDisplayed);
            tableLayoutBurialPitSection3.addView(tableRow);
        }
    }

    private void generateTableInfectionControl(TableRow.LayoutParams[] paramsArrayTableRowColumns) {
        int serialNum = 0;
        int colorBgTableRow = MainContainer.mContext.getResources().getColor(R.color.row_background);
        for (ClassScreenItem display : arrayListInfectionControl) {
            boolean bIsOdd = serialNum % 2 == 0;
            serialNum++;

            TableRow tableRow = new TableRow(MainContainer.mContext);
            CustomEditText editTextSerialNum = new CustomEditText(bIsOdd, "" + serialNum);
            CustomEditText editTextLocation = new CustomEditText(bIsOdd, display.item_name);
            CustomToggleButtonTwoState toggleButtonDisplayed = new CustomToggleButtonTwoState(bIsOdd);

            editTextSerialNum.customiseEditText(paramsArrayTableRowColumns[0]);
            editTextLocation.customiseEditText(paramsArrayTableRowColumns[1]);
            toggleButtonDisplayed.customiseToggleButton(paramsArrayTableRowColumns[2]);

            tableRow.setBackgroundColor(colorBgTableRow);
            tableRow.addView(editTextSerialNum);
            tableRow.addView(editTextLocation);
            tableRow.addView(toggleButtonDisplayed);
            tableLayoutInfectionControlSection4.addView(tableRow);
        }
    }

    private void generateTableHWManagementArticle(TableRow.LayoutParams[] paramsArrayTableRowColumns) {
        int serialNum = 0;
        int colorBgTableRow = MainContainer.mContext.getResources().getColor(R.color.row_background);
        for (ClassScreenItem display : arrayListHWManagementArticle) {
            boolean bIsOdd = serialNum % 2 == 0;
            serialNum++;

            TableRow tableRow = new TableRow(MainContainer.mContext);
            CustomEditText editTextSerialNum = new CustomEditText(bIsOdd, "" + serialNum);
            CustomEditText editTextLocation = new CustomEditText(bIsOdd, display.item_name);
            CustomToggleButtonTwoState toggleButtonDisplayed = new CustomToggleButtonTwoState(bIsOdd);
            editTextSerialNum.customiseEditText(paramsArrayTableRowColumns[0]);
            editTextLocation.customiseEditText(paramsArrayTableRowColumns[1]);
            toggleButtonDisplayed.customiseToggleButton(paramsArrayTableRowColumns[2]);

            tableRow.setBackgroundColor(colorBgTableRow);
            tableRow.addView(editTextSerialNum);
            tableRow.addView(editTextLocation);
            tableRow.addView(toggleButtonDisplayed);
            tableLayoutHWManagementArticleSection5.addView(tableRow);
        }
    }

    private void generateTableICEMaterialDisplay(TableRow.LayoutParams[] paramsArrayTableRowColumns) {
        int serialNum = 0;
        int colorBgTableRow = MainContainer.mContext.getResources().getColor(R.color.row_background);
        for (ClassScreenItem display : arrayListICEMaterialDisplay) {
            boolean bIsOdd = serialNum % 2 == 0;
            serialNum++;

            TableRow tableRow = new TableRow(MainContainer.mContext);
            CustomEditText editTextSerialNum = new CustomEditText(bIsOdd, "" + serialNum);
            CustomEditText editTextLocation = new CustomEditText(bIsOdd, display.item_name);
            CustomToggleButtonTwoState toggleButtonDisplayed = new CustomToggleButtonTwoState(bIsOdd);

            editTextSerialNum.customiseEditText(paramsArrayTableRowColumns[0]);
            editTextLocation.customiseEditText(paramsArrayTableRowColumns[1]);
            toggleButtonDisplayed.customiseToggleButton(paramsArrayTableRowColumns[2]);

            tableRow.setBackgroundColor(colorBgTableRow);
            tableRow.addView(editTextSerialNum);
            tableRow.addView(editTextLocation);
            tableRow.addView(toggleButtonDisplayed);
            tableLayoutICEMaterialDisplaySection6.addView(tableRow);
        }
    }

    private void generateTableWasteManagement(TableRow.LayoutParams[] paramsArrayTableRowColumns) {
        int serialNum = 0;
        int colorBgTableRow = MainContainer.mContext.getResources().getColor(R.color.row_background);
        for (ClassScreenItem display : arrayListWasteManagement) {
            boolean bIsOdd = serialNum % 2 == 0;
            serialNum++;

            TableRow tableRow = new TableRow(MainContainer.mContext);
            CustomEditText editTextSerialNum = new CustomEditText(bIsOdd, "" + serialNum);
            CustomEditText editTextLocation = new CustomEditText(bIsOdd, display.item_name);
            CustomToggleButtonTwoState toggleButtonDisplayed = new CustomToggleButtonTwoState(bIsOdd);

            editTextSerialNum.customiseEditText(paramsArrayTableRowColumns[0]);
            editTextLocation.customiseEditText(paramsArrayTableRowColumns[1]);
            toggleButtonDisplayed.customiseToggleButton(paramsArrayTableRowColumns[2]);

            tableRow.setBackgroundColor(colorBgTableRow);
            tableRow.addView(editTextSerialNum);
            tableRow.addView(editTextLocation);
            tableRow.addView(toggleButtonDisplayed);
            tableLayoutWasteManagementSection7.addView(tableRow);
        }
    }

    private void generateTableOutsideHospitalBuilding(TableRow.LayoutParams[] paramsArrayTableRowColumns) {
        int serialNum = 0;
        int colorBgTableRow = MainContainer.mContext.getResources().getColor(R.color.row_background);
        for (ClassScreenItem display : arrayListOutsideHospitalBuilding) {
            boolean bIsOdd = serialNum % 2 == 0;
            serialNum++;

            TableRow tableRow = new TableRow(MainContainer.mContext);
            CustomEditText editTextSerialNum = new CustomEditText(bIsOdd, "" + serialNum);
            CustomEditText editTextLocation = new CustomEditText(bIsOdd, display.item_name);
            CustomToggleButtonTwoState toggleButtonDisplayed = new CustomToggleButtonTwoState(bIsOdd);

            editTextSerialNum.customiseEditText(paramsArrayTableRowColumns[0]);
            editTextLocation.customiseEditText(paramsArrayTableRowColumns[1]);
            toggleButtonDisplayed.customiseToggleButton(paramsArrayTableRowColumns[2]);

            tableRow.setBackgroundColor(colorBgTableRow);
            tableRow.addView(editTextSerialNum);
            tableRow.addView(editTextLocation);
            tableRow.addView(toggleButtonDisplayed);
            tableLayoutOutsideHospitalBuildingSection8.addView(tableRow);
        }
    }

    private void generateTableIncineratorInspectionSection9(TableRow.LayoutParams[] paramsArrayTableRowColumns) {

        int serialNum = 0;
        int colorBgTableRow = MainContainer.mContext.getResources().getColor(R.color.row_background);

        for (ClassScreenItem display : arrayListIncineratorInspectionSection9) {
            boolean bIsOdd = serialNum % 2 == 0;
            serialNum++;

            TableRow tableRow = new TableRow(MainContainer.mContext);
            CustomEditText editTextSerialNum = new CustomEditText(bIsOdd, "" + serialNum);
            CustomEditText editTextLocation = new CustomEditText(bIsOdd, display.item_name);
            CustomToggleButtonTwoState toggleButtonDisplayed = new CustomToggleButtonTwoState(bIsOdd);

            editTextSerialNum.customiseEditText(paramsArrayTableRowColumns[0]);
            editTextLocation.customiseEditText(paramsArrayTableRowColumns[1]);
            toggleButtonDisplayed.customiseToggleButton(paramsArrayTableRowColumns[2]);

            tableRow.setBackgroundColor(colorBgTableRow);
            tableRow.addView(editTextSerialNum);
            tableRow.addView(editTextLocation);
            tableRow.addView(toggleButtonDisplayed);
            tableLayoutIncineratorInspectionSection9.addView(tableRow);
        }

    }


    @Override
    public void onFragmentShown() {
    }
}