package pk.gov.pitb.mea.msh.helpers;

public class Constants {

    /* TESTING DATA */
    public static final boolean bDebuging = false;
    // public static final boolean bDebuging = true;
    public static final boolean bTesting = false;
    //public static final boolean bTesting = true;

    /* SERVER URLs */
    ///* DEV */private static final String BASE_URL = "http://msh.punjab.gov.pk/dev/api";
    /* DEV */private static final String BASE_URL = "http://dev.bapps.pitb.gov.pk/msh/api/";
//    /* LIVE */private static final String BASE_URL = "https://msh.punjab.gov.pk/api/";
    public static final String URL_UPLOAD_PICTURE = BASE_URL + "/upload_image";
    public static final String URL_SUBMIT = BASE_URL + "/save";
    public static final String URL_SUBMIT_HEALTH_COUNCIL = BASE_URL + "/save_health_council";
    public static final String URL_SUBMIT_NUTRIENTS = BASE_URL + "/save_nutrition";
    public static final String URL_SUBMIT_HEPATITIS = BASE_URL + "/save_hepatitis";
    public static final String URL_SUBMIT_MONITORING = BASE_URL + "/save";
    public static final String URL_SUBMIT_PATIENT_INTERVIEW = BASE_URL + "/save_patient_exit_interview";
    public static final String URL_SYNC_STRUCTURE = BASE_URL + "/get_structure/?card_id=%s&version=%s";
    public static final String URL_SYNC_FACILITY = BASE_URL + "/get_facility_data/?card_id=%s&version=%s";

    /* FORM TYPES */
    public static final String FORM_TYPE_A = "A";
    public static final String FORM_TYPE_B = "B";

    /* ACTIVITY DATA VALUES */
    public static final String VALUE_TRUE = "true";
    public static final String VALUE_FALSE = "false";

    /* APP UPDATE */
    public static String APP_UPDATE_OPTIONAL = "a";

    /* ACTIVITY DATA EXTRAS */
    public static final String EXTRAS_SYNC = "sync";


    /* ACTIVITY TYPE IDENTIFIER STRINGS */
    public static final String AT_SENT = "sent";
    public static final String AT_UNSENT = "unsent";
    public static final String AT_OUTDATED = "outdated";

    public static final String AT_HEALTH_COUNCIL = "health_council";
    public static final String AT_MONITORING = "monitoring";
    public static final String AT_NUTRIENTS = "nutrients";
    public static final String AT_HEPATITIS = "hepatitis";
    public static final String AT_PATIENT_INTERVIEWS = "save_patient_exit_interview";
    /* DATE FORMAT */
    public static final String FORMAT_DATE_APP = "yyyy-MM-dd HH:mm:ss";
    public static final String FORMAT_DATE_SERVER = "dd-MM-yyyy HH:mm:ss";
    public static final int UPDATE_HOUR_INTERVAL = 6;

    /* HEALTH COUNCIL */
    public static final String VALUE_TRUE_HEALTH_COUNCIL = "true_health_council";
    public static final String VALUE_TRUE_HEPATITIS = "true_hepatitis";
    public static final String VALUE_TRUE_NUTRIENTS = "true_nutrients";

    public static final String VALUE_FALSE_HEALTH_COUNCIL = "false_health_council";
    public static final String VALUE_FALSE_HEPATITIS = "false_hepatitis";
    public static final String VALUE_FALSE_NUTRIENTS = "false_nutrients";
    public static final String VALUE_TRUE_PATIENT_INTERVIEWS = "true_patient_interviews";
    public static final String VALUE_FALSE_PATIENT_INTERVIEWS = "false_patient_interviews";
    public static final String EXTRAS_MONITERING = "monitering";
    public static final String EXTRAS_LOAD = "load";


    /* DATE LIMITS */
    public static final int LENGTH_CNIC = 15;
    public static final int MIN_LENGTH_MOBILE_NUMBER = 11;
    public static final int MAX_LENGTH_MOBILE_NUMBER = 11;

    /* HANDLER ACTION CODES */
    public static final int AC_DEFAULT = -1;
    public static final int AC_SHOW_DIALOG = 0;
    public static final int AC_INTERNET_NOT_CONNECTED = 1;
    public static final int AC_INTERNET_NOT_WORKING = 2;
    public static final int AC_HTTP_REQUEST_SUCCESS = 3;
    public static final int AC_HTTP_REQUEST_ERROR = 4;
    public static final int AC_HTTP_REQUEST_EXCEPTION = 5;
    public static final int AC_HTTP_REQUEST_DATA_OUTDATED = 6;
    public static final int AC_HTTP_REQUEST_NO_DATA = 7;

    /* SERVER ERROR CODES */
    public static final int ERROR_CODE_NO_IMEI = 1;
    public static final int ERROR_CODE_INVALID_IMEI = 2;
    public static final int ERROR_CODE_NO_STRUCTURE_VERSION = 3;
    public static final int ERROR_CODE_STRUCTURE_VERSION_UP_TO_DATE = 4;
    public static final int ERROR_CODE_NO_FACILITY_VERSION = 5;
    public static final int ERROR_CODE_FACILITY_VERSION_UP_TO_DATE = 6;

    /* ROLE CODES */
    public static final String ROLE_MO = "MO";
    public static final String ROLE_FACILITY_INCHARGE = "facility incharge";

    /* SCREEN CODES */
    public static final String DB_TYPE_ID_DHQ = "1";
    public static final String DB_TYPE_ID_THQ = "2";
    public static final String DB_TYPE_ID_DHQTEACHING = "3";
    public static final String DB_TYPE_ID_NUTRITION = "4";
    public static final String DB_TYPE_ID_HEPATITIS = "5";
    public static final String DB_TYPE_PMO = "1";


    public static final String DB_TYPE_NAME_DHQ = "DHQ";
    public static final String DB_TYPE_NAME_THQ = "THQ";
    public static final String DB_TYPE_NAME_DHQTEACHING = "DHQTeaching";
    public static final String DB_TYPE_NAME_NUTRITION = "Nutrition";
    public static final String DB_TYPE_NAME_HEPATITIS = "Hepatitis";
    public static final String DB_TYPE_NAME_PATIENT_INTERVIEWS = "patient_exit_interview";
    public static final String DB_SCREEN_ID_PATIENT_INTERVIEWS = "26";

    public static final String DB_SCREEN_ID_FACILITY_STATUS = "0";
    public static final String DB_SCREEN_ID_FACILITY_INCHARGE = "1";
    public static final String DB_SCREEN_ID_SELF_REPORTED_INDICATORS = "2";
    public static final String DB_SCREEN_ID_OUTLOOK_UTILITIES = "3";
    public static final String DB_SCREEN_ID_METHOD_OF_WASTE_DISPOSAL = "4";
    public static final String DB_SCREEN_ID_VACANCY_DATA = "5";
    public static final String DB_SCREEN_ID_DATA_ON_ABSENCES = "6";
    public static final String DB_SCREEN_ID_MEDICINES = "7";
    public static final String DB_SCREEN_ID_SUPPLIES = "8";
    public static final String DB_SCREEN_ID_EQUIPMENT = "9";
    public static final String DB_SCREEN_ID_COMMENTS = "10";
    public static final String DB_SCREEN_ID_KPI = "11";
    public static final String DB_SCREEN_ID_OTP = "22";
    public static final String DB_SCREEN_ID_SCS = "23";
    public static final String DB_SCREEN_ID_WMI = "24";
    public static final String DB_SCREEN_ID_SENTINEL = "25";

    // public static final String DB_SCREEN_ID_UTILITIES = "5";
    // public static final String DB_SCREEN_ID_DISPLAYS = "6";
    // public static final String DB_SCREEN_ID_NON_MED_EQUIPMENT = "10";
    // public static final String DB_SCREEN_ID_PATIENTS_DATA = "11";
    // public static final String DB_SCREEN_ID_BUILDING_STATUS = "12";
    // public static final String DB_SCREEN_ID_LHS_MEDICINES = "13";
    // public static final String DB_SCREEN_ID_PICTURES = "14";

    public static final String DB_SCREEN_NAME_FACILITY_STATUS = "facility_status";
    public static final String DB_SCREEN_NAME_FACILITY_INCHARGE = "facility_incharge";
    public static final String DB_SCREEN_NAME_SELF_REPORTED_INDICATORS = "self_reported_indicators";
    public static final String DB_SCREEN_NAME_OUTLOOK_UTILITIES = "outlook";
    public static final String DB_SCREEN_NAME_METHOD_OF_WASTE_DISPOSAL = "disposal_of_hospital_waste";
    public static final String DB_SCREEN_NAME_VACANCY_DATA = "vacancy_data";
    public static final String DB_SCREEN_NAME_DATA_ON_ABSENCES = "data_on_absences";
    public static final String DB_SCREEN_NAME_MEDICINES = "medicines";
    public static final String DB_SCREEN_NAME_SUPPLIES = "supplies";
    public static final String DB_SCREEN_NAME_EQUIPMENT = "equipment";
    public static final String DB_SCREEN_NAME_COMMENTS = "additional_comments";
    public static final String DB_SCREEN_NAME_PICTURES = "pictures";
    public static final String DB_SCREEN_NAME_KPI = "kpi";
    public static final String DB_SCREEN_NAME_OTP = "otp";
    public static final String DB_SCREEN_NAME_SCS = "sc";
    public static final String DB_SCREEN_NAME_WMI = "waste_management_indicators";
    public static final String DB_SCREEN_NAME_SENTINEL = "sentinel";

    public static final String DB_SCREEN_NAME_PATIENT_INTERVIEW_PAGE1 = "patient_interview_page_1";
    public static final String DB_SCREEN_NAME_PATIENT_INTERVIEW_PAGE2 = "patient_interview_page_2";

    // public static final String DB_SCREEN_NAME_SELF_REPORTED_INDICATORS = "self_reported_indicators";
    // public static final String DB_SCREEN_NAME_FACILITY_INCHARGE = "facility_incharge";
    // public static final String DB_SCREEN_NAME_VACANCY_STATUS = "vacancy_status";
    // public static final String DB_SCREEN_NAME_ATTENDANCE = "attendance";
    // public static final String DB_SCREEN_NAME_OUTLOOK = "outlook";
    // public static final String DB_SCREEN_NAME_UTILITIES = "utilities";
    // public static final String DB_SCREEN_NAME_DISPLAYS = "displays";
    // public static final String DB_SCREEN_NAME_MEDICINES = "medicines";
    // public static final String DB_SCREEN_NAME_VACCINES = "vaccines";
    // public static final String DB_SCREEN_NAME_MED_EQUIPMENT = "med_equipment";
    // public static final String DB_SCREEN_NAME_NON_MED_EQUIPMENT = "non_medical_equipment";
    // public static final String DB_SCREEN_NAME_PATIENTS_DATA = "patients_data";
    // public static final String DB_SCREEN_NAME_LHS_MEDICINES = "lhs_medicines";
    // public static final String DB_SCREEN_NAME_PICTURES = "pictures";

    /* FRAGMENT EQUIPMENTS */
    public static final String TITLE_EQUIPMENT = "Equipments - ";
    public static final int FRAGMENT_EQUIPMENT_1 = 1;
    public static final int FRAGMENT_EQUIPMENT_2 = 2;
    public static final int FRAGMENT_EQUIPMENT_3 = 3;
    public static final int FRAGMENT_EQUIPMENT_4 = 4;
    public static final int FRAGMENT_EQUIPMENT_5 = 5;
    public static final int FRAGMENT_EQUIPMENT_6 = 6;

    public static final int END_INDEX_EUIPMENT_1 = 3;
    public static final int END_INDEX_EUIPMENT_2 = 4;
    public static final int END_INDEX_EUIPMENT_3 = 5;
    public static final int END_INDEX_EUIPMENT_4 = 6;
    public static final int END_INDEX_EUIPMENT_5 = 9;

    /* FRAGMENT EQUIPMENTS */
    public static final String PICTURE_STATUS_UNSENT = "unsent";
    public static final String PICTURE_STATUS_SENT = "sent";


    /* Health Council */
    public static final String DB_SCREEN_NAME_HEALTH_COUNCIL_PAGE1 = "health_council_page1";
    public static final String DB_SCREEN_NAME_HEALTH_COUNCIL_PAGE2 = "health_council_page2";
    public static final String DB_SCREEN_NAME_HEALTH_COUNCIL_PAGE3 = "health_council_page3";
    public static final String DB_SCREEN_NAME_HEALTH_COUNCIL_PAGE4 = "health_council_page4";
    public static final String DB_SCREEN_NAME_HEALTH_COUNCIL_PAGE5 = "health_council_page5";

    /* MAIN TABS FACILITY SECOND TYPES */
    public static final int FACILITY_SECOND_TYPE_NUTRITION = 1;
    public static final int FACILITY_SECOND_TYPE_HEPATITIS = 2;

    public static final String DB_SECOND_TYPE_ID_DEFAULT = "0";
    public static final String DB_SECOND_TYPE_ID_OTP = "1";
    public static final String DB_SECOND_TYPE_ID_SC = "2";
    public static final String DB_SECOND_TYPE_ID_OTP_PLUS_SC = "3";
    public static final String DB_SECOND_TYPE_ID_SENTINEL = "1";


    public static final String PICTURE_SAVE_DATA = "picture_save_data";


}