package pk.gov.pitb.mea.msh.nutrients;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import pk.gov.pitb.mea.msh.R;
import pk.gov.pitb.mea.msh.handlers.HandlerFragmentCallBackNutrients;
import pk.gov.pitb.mea.msh.helpers.AlertDialogs;
import pk.gov.pitb.mea.msh.helpers.Constants;
import pk.gov.pitb.mea.msh.helpers.MainContainer;
import pk.gov.pitb.mea.msh.helpers.UIHelper;
import pk.gov.pitb.mea.msh.models.ClassScreenData;
import pk.gov.pitb.mea.msh.models.ClassScreenItem;
import pk.gov.pitb.mea.msh.textwatchers.TextWatcherAutoFill;
import pk.gov.pitb.mea.msh.textwatchers.TextWatcherCountManyChilds;
import pk.gov.pitb.mea.msh.views.CustomEditText;
import pk.gov.pitb.mea.msh.views.CustomToggleButtonTwoState;


/**
 * Created by Muhammad Abdullah Azam Khan.
 */
public class OTPs extends Fragment implements HandlerFragmentCallBackNutrients {

	/*private Globals MainContainer;*/
	private View parentView;
	private ArrayList<ClassScreenItem> arrayListDisplays;
	private ArrayList<ClassScreenItem> arrayListHr;
	//private ArrayList<ClassScreenItem> arrayListDisplaysDropdown;
	private ArrayList<ClassScreenItem> arrayListNonFunctionalNonRepairableEnabled;
	private ArrayList<ClassScreenItem> arrayListNoOfCuredEnabled;
	private ArrayList<ClassScreenItem> arrayListNoOfRefferedEnabled;

	private TextView textViewCommentsOTP;
	private EditText editTextCommentsOTP;
	private LinearLayout linearLayoutMain;
	private TextView textViewDisplays;
	private TableLayout tableLayoutDisplays;
	private TextView textViewMedicinesMaintained = null;
	private RadioGroup radioGroupMedicinesMaintained = null;
	private RadioButton radioButtonMedicinesMaintainedYes = null;
	private RadioButton radioButtonMedicinesMaintainedNo = null;
	private TextView textViewRegistersMaintained = null;
	private RadioGroup radioGroupRegistersMaintained = null;
	private RadioButton radioButtonRegistersMaintainedYes = null;
	private RadioButton radioButtonRegistersMaintainedNo = null;
	private RadioButton radioButtonRegisterNotKept = null;


	//private boolean bIsEquipmentsTotalAvailableEditable;
	private ArrayList<ClassScreenItem> arrayListEquipments;
	//private ArrayList<ClassFacilityItem> arrayListEquipmentsTotalAvailable;

	private ArrayList<ClassScreenItem> arrayListIndicators;
	private ArrayList<ClassScreenItem> arrayListMedicines;

	private TextView textViewEquipments;
	private TableLayout tableLayoutEquipments;

	private TextView textViewIndicators;
	private TableLayout tableLayoutIndicators;

	private TextView textViewMedicines;
	private TableLayout tableLayoutMedicines;


	private TextView textViewHr;
	private TableLayout tableLayoutHr;
	private Button buttonProceed;
	private Button buttonPrevious, buttonNext, buttonSubmit;


	@Override
	public void parseObject() {
		try {
			JSONArray jsonArraySection4 = new JSONArray();
			for (int i = 1; i < tableLayoutDisplays.getChildCount(); i++) {
				View child = tableLayoutDisplays.getChildAt(i);
				ClassScreenItem object = arrayListDisplays.get(i - 1);
				if (child instanceof TableRow) {
					JSONObject jsonObjectChild = new JSONObject();

					TableRow tableRow = (TableRow) child;
					CustomToggleButtonTwoState displayed = (CustomToggleButtonTwoState) tableRow.getChildAt(2);

					String displayedID = "";

					if (!displayed.isCheckChanged()) {
						displayedID="";
					}else if (displayed.isChecked()) {
						displayedID = /*arrayListDisplaysDropdown.get(0).item_id*/"yes";
					} else if (displayed.isEnabled()) {
						displayedID = /*arrayListDisplaysDropdown.get(1).item_id*/"no";
					}
					jsonObjectChild.put("otp_id", object.item_id);
					jsonObjectChild.put("otp_name", object.item_name);
					jsonObjectChild.put("status", displayedID);

					jsonArraySection4.put(jsonObjectChild);
				}
			}

			JSONArray jsonArraySection5 = new JSONArray();
			for (int i = 1; i < tableLayoutHr.getChildCount(); i++) {
				View child = tableLayoutHr.getChildAt(i);
				ClassScreenItem object = arrayListHr.get(i - 1);
				if (child instanceof TableRow) {
					JSONObject jsonObjectChild = new JSONObject();

					TableRow tableRow = (TableRow) child;
					CustomToggleButtonTwoState assigned = (CustomToggleButtonTwoState) tableRow.getChildAt(2);
					CustomToggleButtonTwoState present = (CustomToggleButtonTwoState) tableRow.getChildAt(3);

					String assignedID = "", presentID="";

					if (!assigned.isCheckChanged()) {
						assignedID="";
					}else if (assigned.isChecked()) {
						assignedID = "yes";
					} else if (assigned.isEnabled()) {
						assignedID = "no";
					}

					if (!present.isCheckChanged()) {
						presentID="";
					}else if (present.isChecked()) {
						presentID = "yes";
					} else if (present.isEnabled()) {
						presentID = "no";
					}
					jsonObjectChild.put("otp_id", object.item_id);
					jsonObjectChild.put("otp_name", object.item_name);
					jsonObjectChild.put("assigned", assignedID);
					jsonObjectChild.put("present",presentID);


					jsonArraySection5.put(jsonObjectChild);
				}
			}


			JSONArray jsonArraySection1 = new JSONArray();
			for (int i = 1; i < tableLayoutEquipments.getChildCount(); i++) {
				View child = tableLayoutEquipments.getChildAt(i);
				ClassScreenItem object = arrayListEquipments.get(i - 1);
				JSONObject jsonObjectChild = new JSONObject();

				TableRow tableRow = (TableRow) child;
				EditText available = (EditText) tableRow.getChildAt(2);
				EditText functional = (EditText) tableRow.getChildAt(3);
				EditText repairable = (EditText) tableRow.getChildAt(4);
				EditText nonRepairable = (EditText) tableRow.getChildAt(5);

				jsonObjectChild.put("otp_id", object.item_id);
				jsonObjectChild.put("supplies", object.item_name);
				jsonObjectChild.put("total_available", available.getText().toString().trim());
				jsonObjectChild.put("functional", functional.getText().toString().trim());
				jsonObjectChild.put("non_functional_but_repairable", repairable.getText().toString().trim());
				jsonObjectChild.put("non_repairable", nonRepairable.getText().toString().trim());

				jsonArraySection1.put(jsonObjectChild);
			}

			JSONArray jsonArraySection3 = new JSONArray();
			for (int i = 1; i < tableLayoutIndicators.getChildCount(); i++) {
				View child = tableLayoutIndicators.getChildAt(i);
				ClassScreenItem object = arrayListIndicators.get(i - 1);
				JSONObject jsonObjectChild = new JSONObject();

				TableRow tableRow = (TableRow) child;
				EditText available = (EditText) tableRow.getChildAt(2);
				EditText functional = (EditText) tableRow.getChildAt(3);
				EditText repairable = (EditText) tableRow.getChildAt(4);
				EditText nonRepairable = (EditText) tableRow.getChildAt(5);

				jsonObjectChild.put("otp_id", object.item_id);
				jsonObjectChild.put("otp_name", object.item_name);
				jsonObjectChild.put("referred", available.getText().toString().trim());
				jsonObjectChild.put("admitted", functional.getText().toString().trim());
				jsonObjectChild.put("cured", repairable.getText().toString().trim());
				jsonObjectChild.put("stabilization", nonRepairable.getText().toString().trim());

				jsonArraySection3.put(jsonObjectChild);
			}

			JSONArray jsonArraySection2 = new JSONArray();
			for (int i = 1; i < tableLayoutMedicines.getChildCount(); i++) {
				View child = tableLayoutMedicines.getChildAt(i);
				ClassScreenItem object = arrayListMedicines.get(i - 1);
				JSONObject jsonObjectChild = new JSONObject();

				TableRow tableRow = (TableRow) child;
				EditText available = (EditText) tableRow.getChildAt(2);
				EditText functional = (EditText) tableRow.getChildAt(3);
				EditText repairable = (EditText) tableRow.getChildAt(4);
				EditText nonRepairable = (EditText) tableRow.getChildAt(5);

				jsonObjectChild.put("otp_id", object.item_id);
				jsonObjectChild.put("otp_name", object.item_name);
				jsonObjectChild.put("consumption", available.getText().toString().trim());
				jsonObjectChild.put("balance", functional.getText().toString().trim());
				jsonObjectChild.put("physical_stock", repairable.getText().toString().trim());
				jsonObjectChild.put("expired", nonRepairable.getText().toString().trim());

				jsonArraySection2.put(jsonObjectChild);
			}

			JSONObject jsonObject = new JSONObject();
			jsonObject.put("section1", jsonArraySection1);
			jsonObject.put("section2", jsonArraySection2);
			jsonObject.put("section3", jsonArraySection3);
			jsonObject.put("section4", jsonArraySection4);
			jsonObject.put("section5", jsonArraySection5);
			jsonObject.put("otp_comments", editTextCommentsOTP.getText().toString().trim());
			if (radioGroupMedicinesMaintained.getCheckedRadioButtonId()== radioButtonMedicinesMaintainedYes.getId()){
				jsonObject.put("medicines_maintained", "yes");
			}
			else if (radioGroupMedicinesMaintained.getCheckedRadioButtonId()== radioButtonMedicinesMaintainedNo.getId()){
				jsonObject.put("medicines_maintained", "no");
			}
			else {
				jsonObject.put("medicines_maintained", "");
			}

			if (radioGroupRegistersMaintained.getCheckedRadioButtonId()== radioButtonRegistersMaintainedYes.getId()){
				jsonObject.put("register_maintained", "yes");
			}
			else if (radioGroupRegistersMaintained.getCheckedRadioButtonId()== radioButtonRegistersMaintainedNo.getId()){
				jsonObject.put("register_maintained", "no");
			}else if (radioGroupRegistersMaintained.getCheckedRadioButtonId()== radioButtonRegisterNotKept.getId()){
				jsonObject.put("register_maintained", "register_not_kept");
			}
			else {
				jsonObject.put("register_maintained", "");
			}
			MainContainer.mJsonObjectFormData.remove(Constants.DB_SCREEN_NAME_OTP);
			MainContainer.mJsonObjectFormData.put(Constants.DB_SCREEN_NAME_OTP, jsonObject);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	public void loadSavedData(){
		try{
			if(MainContainer.mJsonObjectFormData.has(Constants.DB_SCREEN_NAME_OTP)){
				JSONObject jsonObjectComment = MainContainer.mJsonObjectFormData.getJSONObject(Constants.DB_SCREEN_NAME_OTP);
				JSONArray JSONArraySections = jsonObjectComment.has("section4") ? jsonObjectComment.getJSONArray("section4") : new JSONArray();

				for (int i = 0; i < JSONArraySections.length(); i++) {
					boolean bIsOdd = i % 2 == 0;

					View child = tableLayoutDisplays.getChildAt(i+1);
					JSONObject jObject =  JSONArraySections.getJSONObject(i);

					if (child instanceof TableRow) {

						TableRow tableRow = (TableRow) child;
						CustomToggleButtonTwoState displayedToggleButton = (CustomToggleButtonTwoState) tableRow.getChildAt(2);
						String displayedID = jObject.getString("otp_id");
						String dd_ID = jObject.getString("status");
						if(dd_ID.equals("yes")){
							displayedToggleButton.setbIsCheckChanged(true);
							displayedToggleButton.setChecked(true);
							displayedToggleButton.setBackgroundResource(bIsOdd ? R.drawable.toggle_tick_odd : R.drawable.toggle_tick_even);
						}else if(dd_ID.equals("no")){
							displayedToggleButton.setbIsCheckChanged(true);
							displayedToggleButton.setChecked(false);
							displayedToggleButton.setBackgroundResource(bIsOdd ? R.drawable.toggle_cross_odd : R.drawable.toggle_cross_even);
						}

					}
				}
				String stringSection2 = jsonObjectComment.has("section1") ? jsonObjectComment.getString("section1") : "";
				JSONArray jsonArraySection2 = new JSONArray(stringSection2);
				for (int i = 1; i < tableLayoutEquipments.getChildCount(); i++) {
					View child = tableLayoutEquipments.getChildAt(i);
					JSONObject jsonObjectsections = jsonArraySection2.getJSONObject(i-1);

					if (child instanceof TableRow) {

						TableRow tableRow = (TableRow) child;
						EditText available = (EditText) tableRow.getChildAt(2);
						EditText functional = (EditText) tableRow.getChildAt(3);
						EditText repairable = (EditText) tableRow.getChildAt(4);
						EditText nonRepairable = (EditText) tableRow.getChildAt(5);

						String med_equipment_id = jsonObjectsections.has("otp_id")? jsonObjectsections.getString("otp_id"):"";
						String supplies = jsonObjectsections.has("supplies")? jsonObjectsections.getString("supplies"):"";
						String sTotal_available = jsonObjectsections.has("total_available")? jsonObjectsections.getString("total_available"):"";
						String sFunctional = jsonObjectsections.has("functional")? jsonObjectsections.getString("functional"):"";
						String sNon_functional_but_repairable = jsonObjectsections.has("non_functional_but_repairable")? jsonObjectsections.getString("non_functional_but_repairable"):"";
						String sNon_repairable = jsonObjectsections.has("non_repairable")? jsonObjectsections.getString("non_repairable"):"";

						available.setText(sTotal_available);
						if(sFunctional.length()>0 && functional.getText().length() == 0)
							functional.setText(sFunctional);
						if(sNon_functional_but_repairable.length()>0 && repairable.getText().length() == 0)
							repairable.setText(sNon_functional_but_repairable);
						if(sNon_repairable.length()>0)
							nonRepairable.setText(sNon_repairable);
					}
				}

				String stringSection3 = jsonObjectComment.has("section3") ? jsonObjectComment.getString("section3") : "";
				JSONArray jsonArraySection3 = new JSONArray(stringSection3);
				for (int i = 1; i < tableLayoutIndicators.getChildCount(); i++) {
					View child = tableLayoutIndicators.getChildAt(i);
					JSONObject jsonObjectsections = jsonArraySection3.getJSONObject(i-1);

					if (child instanceof TableRow) {

						TableRow tableRow = (TableRow) child;
						EditText available = (EditText) tableRow.getChildAt(2);
						EditText functional = (EditText) tableRow.getChildAt(3);
						EditText repairable = (EditText) tableRow.getChildAt(4);
						EditText nonRepairable = (EditText) tableRow.getChildAt(5);

						String sTotal_available = jsonObjectsections.has("referred")? jsonObjectsections.getString("referred"):"";
						String sFunctional = jsonObjectsections.has("admitted")? jsonObjectsections.getString("admitted"):"";
						String sNon_functional_but_repairable = jsonObjectsections.has("cured")? jsonObjectsections.getString("cured"):"";
						String sNon_repairable = jsonObjectsections.has("stabilization")? jsonObjectsections.getString("stabilization"):"";


						available.setText(sTotal_available);
						functional.setText(sFunctional);
						repairable.setText(sNon_functional_but_repairable);
						nonRepairable.setText(sNon_repairable);

					}
				}

				String stringSection4 = jsonObjectComment.has("section2") ? jsonObjectComment.getString("section2") : "";
				JSONArray jsonArraySection4 = new JSONArray(stringSection4);
				for (int i = 1; i < tableLayoutMedicines.getChildCount(); i++) {
					View child = tableLayoutMedicines.getChildAt(i);
					JSONObject jsonObjectsections = jsonArraySection4.getJSONObject(i-1);

					if (child instanceof TableRow) {

						TableRow tableRow = (TableRow) child;
						EditText available = (EditText) tableRow.getChildAt(2);
						EditText functional = (EditText) tableRow.getChildAt(3);
						EditText repairable = (EditText) tableRow.getChildAt(4);
						EditText nonRepairable = (EditText) tableRow.getChildAt(5);

						String sTotal_available = jsonObjectsections.has("consumption")? jsonObjectsections.getString("consumption"):"";
						String sFunctional = jsonObjectsections.has("balance")? jsonObjectsections.getString("balance"):"";
						String sNon_functional_but_repairable = jsonObjectsections.has("physical_stock")? jsonObjectsections.getString("physical_stock"):"";
						String sNon_repairable = jsonObjectsections.has("expired")? jsonObjectsections.getString("expired"):"";

						available.setText(sTotal_available);
						functional.setText(sFunctional);
						repairable.setText(sNon_functional_but_repairable);
						nonRepairable.setText(sNon_repairable);

					}
				}
				String stringSection5 = jsonObjectComment.has("section5") ? jsonObjectComment.getString("section5") : "";
				JSONArray jsonArraySection5 = new JSONArray(stringSection5);
				for (int i = 1; i < tableLayoutHr.getChildCount(); i++) {
					View child = tableLayoutHr.getChildAt(i);
					JSONObject jsonObjectsections = jsonArraySection5.getJSONObject(i-1);
					boolean bIsOdd = i % 2 == 0;
					if (child instanceof TableRow) {
						JSONObject jsonObjectChild = new JSONObject();

						TableRow tableRow = (TableRow) child;
						CustomToggleButtonTwoState assigned = (CustomToggleButtonTwoState) tableRow.getChildAt(2);
						CustomToggleButtonTwoState present = (CustomToggleButtonTwoState) tableRow.getChildAt(3);

						String dd_ID = jsonObjectsections.getString("assigned");
						if(dd_ID.equals("yes")){
							assigned.setbIsCheckChanged(true);
							assigned.setChecked(true);
							assigned.setBackgroundResource(bIsOdd ? R.drawable.toggle_tick_odd : R.drawable.toggle_tick_even);
						}else if(dd_ID.equals("no")){
							assigned.setbIsCheckChanged(true);
							assigned.setChecked(false);
							assigned.setBackgroundResource(bIsOdd ? R.drawable.toggle_cross_odd : R.drawable.toggle_cross_even);
						}

						dd_ID = jsonObjectsections.getString("present");
						if(dd_ID.equals("yes")){
							present.setbIsCheckChanged(true);
							present.setChecked(true);
							present.setBackgroundResource(bIsOdd ? R.drawable.toggle_tick_odd : R.drawable.toggle_tick_even);
						}else if(dd_ID.equals("no")){
							present.setbIsCheckChanged(true);
							present.setChecked(false);
							present.setBackgroundResource(bIsOdd ? R.drawable.toggle_cross_odd : R.drawable.toggle_cross_even);
						}

					}
				}
				String otp_comments = jsonObjectComment.has("otp_comments") ? jsonObjectComment.getString("otp_comments") : "";
				editTextCommentsOTP.setText(otp_comments);

				String medicines_maintained = jsonObjectComment.has("medicines_maintained") ? jsonObjectComment.getString("medicines_maintained") : "";
				if (medicines_maintained.equals("yes")){
					((RadioButton)radioGroupMedicinesMaintained.getChildAt(0)).setChecked(true);
				} else if (medicines_maintained.equals("no")) {
					((RadioButton)radioGroupMedicinesMaintained.getChildAt(1)).setChecked(true);
				}

				String register_maintained = jsonObjectComment.has("register_maintained") ? jsonObjectComment.getString("register_maintained") : "";
				if (register_maintained.equals("yes")){
					((RadioButton)radioGroupRegistersMaintained.getChildAt(0)).setChecked(true);
				} else if (register_maintained.equals("no")) {
					((RadioButton)radioGroupRegistersMaintained.getChildAt(1)).setChecked(true);
				} else if (register_maintained.equals("register_not_kept")) {
					((RadioButton)radioGroupRegistersMaintained.getChildAt(2)).setChecked(true);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	private String errorMessage;

	@Override
	public String onFragmentChanged(int previousPosition) {
		errorMessage = null;
		isFormValid();
		return errorMessage;
	}

	@Override
	public boolean isFormValid() {
		try {
			boolean bIsValid = true;
			for (int i = 1; i < tableLayoutDisplays.getChildCount(); i++) {
				View child = tableLayoutDisplays.getChildAt(i);
				if (child instanceof TableRow) {
					TableRow tableRow = (TableRow) child;
					CustomToggleButtonTwoState displayed = (CustomToggleButtonTwoState) tableRow.getChildAt(2);

					if (!displayed.isCheckChanged()) {
						bIsValid = false;
						errorMessage = "Please select all display values to continue";
						return bIsValid;
					}
				}
			}
			for (int i = 1; i < tableLayoutHr.getChildCount(); i++) {
				View child = tableLayoutHr.getChildAt(i);
				if (child instanceof TableRow) {
					TableRow tableRow = (TableRow) child;
					CustomToggleButtonTwoState assigned = (CustomToggleButtonTwoState) tableRow.getChildAt(2);
					CustomToggleButtonTwoState present = (CustomToggleButtonTwoState) tableRow.getChildAt(3);


					if (!assigned.isCheckChanged()) {
						bIsValid = false;
						errorMessage = "Please select all values in assigned column of HR table to continue";
						return bIsValid;
					}
					if (!present.isCheckChanged()) {
						bIsValid = false;
						errorMessage = "Please select all values in present column of HR table to continue";
						return bIsValid;
					}

				}
			}

			for (int i = 1; i < tableLayoutEquipments.getChildCount(); i++) {
				View child = tableLayoutEquipments.getChildAt(i);
				TableRow tableRow = (TableRow) child;
				EditText available = (EditText) tableRow.getChildAt(2);
				EditText functional = (EditText) tableRow.getChildAt(3);
				EditText repairable = (EditText) tableRow.getChildAt(4);
				EditText nonRepairable = (EditText) tableRow.getChildAt(5);

				if (available.getText().toString().trim().length() == 0 || functional.getText().toString().trim().length() == 0
						|| repairable.getText().toString().trim().length() == 0 || nonRepairable.getText().toString().trim().length() == 0) {
					bIsValid = false;
					errorMessage = "Please fill all Equipments details to continue";
					return bIsValid;
				}
			}

			for (int i = 1; i < tableLayoutIndicators.getChildCount(); i++) {
				View child = tableLayoutIndicators.getChildAt(i);
				TableRow tableRow = (TableRow) child;
				EditText available = (EditText) tableRow.getChildAt(2);
				EditText functional = (EditText) tableRow.getChildAt(3);
				EditText repairable = (EditText) tableRow.getChildAt(4);
				EditText nonRepairable = (EditText) tableRow.getChildAt(5);


				if (available.getText().toString().trim().length() == 0 || functional.getText().toString().trim().length() == 0
						|| repairable.getText().toString().trim().length() == 0 || nonRepairable.getText().toString().trim().length() == 0
						) {
					bIsValid = false;
					errorMessage = "Please fill all Indicators details to continue";
					return bIsValid;
				}
			}

			for (int i = 1; i < tableLayoutMedicines.getChildCount(); i++) {
				View child = tableLayoutMedicines.getChildAt(i);
				TableRow tableRow = (TableRow) child;
				EditText available = (EditText) tableRow.getChildAt(2);
				EditText functional = (EditText) tableRow.getChildAt(3);
				EditText repairable = (EditText) tableRow.getChildAt(4);
				EditText nonRepairable = (EditText) tableRow.getChildAt(5);


				if (available.getText().toString().trim().length() == 0 || functional.getText().toString().trim().length() == 0
						|| repairable.getText().toString().trim().length() == 0 || nonRepairable.getText().toString().trim().length() == 0
						) {
					bIsValid = false;
					errorMessage = "Please fill all Medicines details to continue";
					return bIsValid;
				}
			}
			if (radioGroupMedicinesMaintained.getCheckedRadioButtonId()==-1){
				errorMessage = "Select Yes or No option for medicines maintained";
				return false;
			}
			if (radioGroupRegistersMaintained.getCheckedRadioButtonId()==-1){
				errorMessage = "Select Yes or No or Register not kept option for registers maintained";
				return false;
			}
			parseObject();
			return bIsValid;
		} catch (Exception e) {
			e.printStackTrace();
			MainContainer.showMessage("Error Processing Your Request");
			return false;
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		if (parentView == null) {
			parentView = inflater.inflate(R.layout.fragment_otps, container, false);
			/*MainContainer = Globals.getInstance();*/
			initFragmentData();
			generateBody();
			loadSavedData();
		}
		return parentView;
	}

	private void initFragmentData() {
		try {
			ClassScreenData objectScreenData = new ClassScreenData();
			objectScreenData.setDefaultValues();
			objectScreenData.screen_id = Constants.DB_SCREEN_ID_OTP;
			objectScreenData.screen_name = Constants.DB_SCREEN_NAME_OTP;

			objectScreenData.section_id = "";
			objectScreenData.type_id = Constants.DB_TYPE_ID_NUTRITION;
			objectScreenData.type_name = Constants.DB_TYPE_NAME_NUTRITION;
			objectScreenData.section_name = "section3";
			objectScreenData.section_value = "fields";
			objectScreenData = MainContainer.mDbAdapter.selectScreenData(objectScreenData);
			arrayListIndicators = MainContainer.mDbAdapter.selectScreenItems(objectScreenData);
			objectScreenData.section_value = "cured_enabled";
			arrayListNoOfCuredEnabled = MainContainer.mDbAdapter.selectScreenItems(objectScreenData);
			objectScreenData.section_value = "stabilization_enabled";
			arrayListNoOfRefferedEnabled = MainContainer.mDbAdapter.selectScreenItems(objectScreenData);

			//objectScreenData.section_id = "2";
			objectScreenData.section_name = "section2";
			objectScreenData.section_value = "fields";
			objectScreenData = MainContainer.mDbAdapter.selectScreenData(objectScreenData);
			arrayListMedicines = MainContainer.mDbAdapter.selectScreenItems(objectScreenData);

			//objectScreenData.section_id = "1";
			objectScreenData.section_name = "section1";
			objectScreenData.section_value = "fields";
			objectScreenData = MainContainer.mDbAdapter.selectScreenData(objectScreenData);
			arrayListEquipments = MainContainer.mDbAdapter.selectScreenItems(objectScreenData);
			objectScreenData.section_value = "non_functional_repairable_enabled";
			arrayListNonFunctionalNonRepairableEnabled = MainContainer.mDbAdapter.selectScreenItems(objectScreenData);

			//objectScreenData.section_id = "4";
			objectScreenData.section_name = "section4";
			objectScreenData.section_value = "fields";
			objectScreenData = MainContainer.mDbAdapter.selectScreenData(objectScreenData);
			arrayListDisplays = MainContainer.mDbAdapter.selectScreenItems(objectScreenData);


			//objectScreenData.section_id = "5";
			objectScreenData.section_name = "section5";
			objectScreenData.section_value = "fields";
			objectScreenData = MainContainer.mDbAdapter.selectScreenData(objectScreenData);
			arrayListHr = MainContainer.mDbAdapter.selectScreenItems(objectScreenData);


			/*objectScreenData.screen_id = Constants.DB_SCREEN_ID_OUTLOOK_UTILITIES;
			objectScreenData.screen_name = Constants.DB_SCREEN_NAME_OUTLOOK_UTILITIES;

			objectScreenData.section_id = "1";
			objectScreenData.section_name = "section1";
			objectScreenData.section_value = "fields";
			objectScreenData = MainContainer.mDbAdapter.selectScreenData(objectScreenData);
			arrayListDisplays = MainContainer.mDbAdapter.selectScreenItems(objectScreenData);
			arrayListEquipments = MainContainer.mDbAdapter.selectScreenItems(objectScreenData);
			arrayListIndicators = MainContainer.mDbAdapter.selectScreenItems(objectScreenData);
			arrayListMedicines = MainContainer.mDbAdapter.selectScreenItems(objectScreenData);
			arrayListHr = MainContainer.mDbAdapter.selectScreenItems(objectScreenData);*/

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void generateBody() {
		try {

			linearLayoutMain = (LinearLayout) parentView.findViewById(R.id.linearlayout_main);
			buttonNext = (Button) parentView.findViewById(R.id.button_next);
			buttonSubmit = (Button) parentView.findViewById(R.id.button_submit);
			buttonPrevious = (Button) parentView.findViewById(R.id.button_previous);
			buttonPrevious.setVisibility(View.GONE);

			textViewCommentsOTP = (TextView) parentView.findViewById(R.id.testview_comments_otp);
			editTextCommentsOTP = (EditText) parentView.findViewById(R.id.edittext_comments_otp);

			textViewMedicinesMaintained = (TextView) parentView.findViewById(R.id.labelradiobutton_medicine_maintained);
			radioGroupMedicinesMaintained = (RadioGroup) parentView.findViewById(R.id.radiogroup_medicine_maintained);
			radioButtonMedicinesMaintainedYes = (RadioButton) parentView.findViewById(R.id.radiobutton_medicine_maintained_yes);
			radioButtonMedicinesMaintainedNo = (RadioButton) parentView.findViewById(R.id.radiobutton_medicine_maintained_no);

			textViewRegistersMaintained= (TextView) parentView.findViewById(R.id.labelradiobutton_registers_maintained);
			radioGroupRegistersMaintained = (RadioGroup) parentView.findViewById(R.id.radiogroup_registers_maintained);
			radioButtonRegistersMaintainedYes = (RadioButton) parentView.findViewById(R.id.radiobutton_registers_maintained_yes);
			radioButtonRegistersMaintainedNo = (RadioButton) parentView.findViewById(R.id.radiobutton_registers_maintained_no);
			radioButtonRegisterNotKept = (RadioButton) parentView.findViewById(R.id.radiobutton_not_have_registers);

			textViewHr = new TextView(MainContainer.mContext);
			tableLayoutHr = new TableLayout(MainContainer.mContext);
			linearLayoutMain.addView(textViewHr, ((ViewGroup) textViewMedicinesMaintained.getParent()).indexOfChild(textViewMedicinesMaintained));
			linearLayoutMain.addView(tableLayoutHr, ((ViewGroup) textViewMedicinesMaintained.getParent()).indexOfChild(textViewMedicinesMaintained));


			textViewEquipments = new TextView(MainContainer.mContext);
			tableLayoutEquipments = new TableLayout(MainContainer.mContext);
			linearLayoutMain.addView(textViewEquipments, ((ViewGroup) textViewMedicinesMaintained.getParent()).indexOfChild(textViewMedicinesMaintained));
			linearLayoutMain.addView(tableLayoutEquipments, ((ViewGroup) textViewMedicinesMaintained.getParent()).indexOfChild(textViewMedicinesMaintained));

			textViewMedicines = new TextView(MainContainer.mContext);
			tableLayoutMedicines = new TableLayout(MainContainer.mContext);
			linearLayoutMain.addView(textViewMedicines, ((ViewGroup) textViewMedicinesMaintained.getParent()).indexOfChild(textViewMedicinesMaintained));
			linearLayoutMain.addView(tableLayoutMedicines, ((ViewGroup) textViewMedicinesMaintained.getParent()).indexOfChild(textViewMedicinesMaintained));

			textViewIndicators = new TextView(MainContainer.mContext);
			tableLayoutIndicators = new TableLayout(MainContainer.mContext);
			linearLayoutMain.addView(textViewIndicators, ((ViewGroup) textViewRegistersMaintained.getParent()).indexOfChild(textViewRegistersMaintained));
			linearLayoutMain.addView(tableLayoutIndicators, ((ViewGroup) textViewRegistersMaintained.getParent()).indexOfChild(textViewRegistersMaintained));

			textViewDisplays = new TextView(MainContainer.mContext);
			tableLayoutDisplays = new TableLayout(MainContainer.mContext);
			linearLayoutMain.addView(textViewDisplays, ((ViewGroup) textViewCommentsOTP.getParent()).indexOfChild(textViewCommentsOTP));
			linearLayoutMain.addView(tableLayoutDisplays, ((ViewGroup) textViewCommentsOTP.getParent()).indexOfChild(textViewCommentsOTP));

			LinearLayout.LayoutParams paramsEditText = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
					(int) (MainContainer.mScreenHeight * 0.35));
			paramsEditText.setMargins((int) (MainContainer.mScreenHeight * 0.01), 0, (int) (MainContainer.mScreenHeight * 0.01), (int) (MainContainer.mScreenHeight * 0.02));
			editTextCommentsOTP.setLayoutParams(paramsEditText);
			editTextCommentsOTP.setOnTouchListener(onTouchListenerScroll);

			LinearLayout.LayoutParams lpButtonProceed = new LinearLayout.LayoutParams((int) (MainContainer.mScreenWidth * 0.5),
					(int) (MainContainer.mScreenWidth * 0.1));
			lpButtonProceed.setMargins(0, (int) (MainContainer.mScreenHeight * 0.05), 0, 0);
			buttonProceed = new Button(MainContainer.mContext);
			buttonProceed.setBackgroundDrawable(MainContainer.mActivity.getResources().getDrawable(R.drawable.background_btn_continue));
			buttonProceed.setGravity(Gravity.CENTER);
			buttonProceed.setLayoutParams(lpButtonProceed);
			//linearLayoutMain.addView(buttonProceed);

			buttonProceed.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					/*if(isFormValid()) {
						if (MainContainer.mObjectFacilityData.facility_nutrition_second_type.equals(Constants.DB_SECOND_TYPE_ID_OTP)) {
							MainContainer.activityCallBackNutrients.submitData();
						} else {
							MainContainer.activityCallBackNutrients.showNextFragment();
						}
					}else{
						AlertDialogs.getInstance().showDialogOK(null, errorMessage, null, true);
					}*/
				}
			});

			TableRow.LayoutParams[] paramsArrayTableRowColumns = new TableRow.LayoutParams[3];
			paramsArrayTableRowColumns[0] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.1), TableRow.LayoutParams.MATCH_PARENT);
			paramsArrayTableRowColumns[1] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.66), (int) (MainContainer.mScreenWidth * 0.08));
			paramsArrayTableRowColumns[2] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.2), (int) (MainContainer.mScreenWidth * 0.08));
			for (int i = 1; i < paramsArrayTableRowColumns.length; i++) {
				paramsArrayTableRowColumns[i].setMargins(2, 0, 0, 0);
			}

			UIHelper uiContainer = new UIHelper();
			uiContainer.generateTableLabel(textViewDisplays, "Infrastructure and Protocol Displayed");

			uiContainer.generateTableHeader(tableLayoutDisplays, paramsArrayTableRowColumns, new String[] { "Sr#", "Name", "Status" });
			generateTableDisplays(paramsArrayTableRowColumns);

			if (tableLayoutDisplays.getChildCount() > 1) {
				textViewDisplays.setVisibility(View.VISIBLE);
				tableLayoutDisplays.setVisibility(View.VISIBLE);
			} else {
				textViewDisplays.setVisibility(View.GONE);
				tableLayoutDisplays.setVisibility(View.GONE);
			}



			TableRow.LayoutParams[] paramsArrayTableRowColumnsEquipments = new TableRow.LayoutParams[6];
			paramsArrayTableRowColumnsEquipments[0] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.1),
					TableRow.LayoutParams.MATCH_PARENT);
			paramsArrayTableRowColumnsEquipments[1] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.26),
					TableRow.LayoutParams.MATCH_PARENT);
			paramsArrayTableRowColumnsEquipments[2] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.15),
					TableRow.LayoutParams.MATCH_PARENT);
			paramsArrayTableRowColumnsEquipments[3] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.15),
					TableRow.LayoutParams.MATCH_PARENT);
			paramsArrayTableRowColumnsEquipments[4] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.15),
					TableRow.LayoutParams.WRAP_CONTENT);
			paramsArrayTableRowColumnsEquipments[5] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.15),
					TableRow.LayoutParams.MATCH_PARENT);
			for (int i = 1; i < paramsArrayTableRowColumnsEquipments.length; i++) {
				paramsArrayTableRowColumnsEquipments[i].setMargins(2, 0, 0, 0);
			}

			uiContainer.generateTableLabel(textViewEquipments, "Equipments");
			uiContainer.generateTableHeader(tableLayoutEquipments, paramsArrayTableRowColumnsEquipments, "No.", "Equipment", "Total Available",
					"Functional", "Non-Functional but Repairable", "Non-Repairable");
			generateTableEquipmentsDetails(paramsArrayTableRowColumnsEquipments);

			if (tableLayoutEquipments.getChildCount() > 1) {
				textViewEquipments.setVisibility(View.VISIBLE);
				tableLayoutEquipments.setVisibility(View.VISIBLE);
			} else {
				textViewEquipments.setVisibility(View.GONE);
				tableLayoutEquipments.setVisibility(View.GONE);
			}



			TableRow.LayoutParams[] paramsArrayTableRowColumnsIndicators = new TableRow.LayoutParams[6];
			paramsArrayTableRowColumnsIndicators[0] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.1),
					TableRow.LayoutParams.MATCH_PARENT);
			paramsArrayTableRowColumnsIndicators[1] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.26),
					TableRow.LayoutParams.MATCH_PARENT);
			paramsArrayTableRowColumnsIndicators[2] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.15),
					TableRow.LayoutParams.MATCH_PARENT);
			paramsArrayTableRowColumnsIndicators[3] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.15),
					TableRow.LayoutParams.MATCH_PARENT);
			paramsArrayTableRowColumnsIndicators[4] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.15),
					TableRow.LayoutParams.WRAP_CONTENT);
			paramsArrayTableRowColumnsIndicators[5] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.15),
					TableRow.LayoutParams.MATCH_PARENT);
			for (int i = 1; i < paramsArrayTableRowColumnsIndicators.length; i++) {
				paramsArrayTableRowColumnsIndicators[i].setMargins(2, 0, 0, 0);
			}

			uiContainer.generateTableLabel(textViewIndicators, "Indicators");
			uiContainer.generateTableHeader(tableLayoutIndicators, paramsArrayTableRowColumnsIndicators, "No.", "Name", "Total no. of children referred by LHWs",
					"Total no. of children admitted in OTP", "Total no. of children cured in OTP",
					"Total no. of children referred to Stabilization Centres");
			generateTableIndicators(paramsArrayTableRowColumnsIndicators);

			if (tableLayoutIndicators.getChildCount() > 1) {
				textViewIndicators.setVisibility(View.VISIBLE);
				tableLayoutIndicators.setVisibility(View.VISIBLE);
			} else {
				textViewIndicators.setVisibility(View.GONE);
				tableLayoutIndicators.setVisibility(View.GONE);
			}

			TableRow.LayoutParams[] paramsArrayTableRowColumnsMedicines = new TableRow.LayoutParams[6];
			paramsArrayTableRowColumnsMedicines[0] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.1),
					TableRow.LayoutParams.MATCH_PARENT);
			paramsArrayTableRowColumnsMedicines[1] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.26),
					TableRow.LayoutParams.MATCH_PARENT);
			paramsArrayTableRowColumnsMedicines[2] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.15),
					TableRow.LayoutParams.MATCH_PARENT);
			paramsArrayTableRowColumnsMedicines[3] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.15),
					TableRow.LayoutParams.MATCH_PARENT);
			paramsArrayTableRowColumnsMedicines[4] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.15),
					TableRow.LayoutParams.WRAP_CONTENT);
			paramsArrayTableRowColumnsMedicines[5] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.15),
					TableRow.LayoutParams.MATCH_PARENT);
			for (int i = 1; i < paramsArrayTableRowColumnsMedicines.length; i++) {
				paramsArrayTableRowColumnsMedicines[i].setMargins(2, 0, 0, 0);
			}

			uiContainer.generateTableLabel(textViewMedicines, "Medicines");
			uiContainer.generateTableHeader(tableLayoutMedicines, paramsArrayTableRowColumnsMedicines, "No.", "Name", "Consumption during previous month",
					"Balance at a time of visit", "Physical Stock Count",
					"Expired");
			generateTableMedicines(paramsArrayTableRowColumnsMedicines);

			if (tableLayoutMedicines.getChildCount() > 1) {
				textViewMedicines.setVisibility(View.VISIBLE);
				tableLayoutMedicines.setVisibility(View.VISIBLE);
			} else {
				textViewMedicines.setVisibility(View.GONE);
				tableLayoutMedicines.setVisibility(View.GONE);
			}

			TableRow.LayoutParams[] paramsArrayTableRowColumnsHr = new TableRow.LayoutParams[4];
			paramsArrayTableRowColumnsHr[0] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.1),
					(int) (MainContainer.mScreenWidth * 0.08));
			paramsArrayTableRowColumnsHr[1] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.26),
					(int) (MainContainer.mScreenWidth * 0.08));
			paramsArrayTableRowColumnsHr[2] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.27),
					(int) (MainContainer.mScreenWidth * 0.08));
			paramsArrayTableRowColumnsHr[3] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.27),
					(int) (MainContainer.mScreenWidth * 0.08));
			for (int i = 1; i < paramsArrayTableRowColumnsHr.length; i++) {
				paramsArrayTableRowColumnsHr[i].setMargins(2, 0, 0, 0);
			}

			uiContainer.generateTableLabel(textViewHr, "HR");
			uiContainer.generateTableHeader(tableLayoutHr, paramsArrayTableRowColumnsHr, "No.", "Name", "Assigned",
					"Present");
			generateTableHr(paramsArrayTableRowColumnsHr);

			if (tableLayoutHr.getChildCount() > 1) {
				textViewHr.setVisibility(View.VISIBLE);
				tableLayoutHr.setVisibility(View.VISIBLE);
			} else {
				textViewHr.setVisibility(View.GONE);
				tableLayoutHr.setVisibility(View.GONE);
			}

			initClickListeners();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	private void initClickListeners() {
		buttonPrevious.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				MainContainer.activityCallBackNutrients.showPreviousFragment();
			}
		});

		buttonNext.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				if(isFormValid()) {
					if (MainContainer.mObjectFacilityData.facility_nutrition_second_type.equals(Constants.DB_SECOND_TYPE_ID_OTP)) {
						MainContainer.activityCallBackNutrients.submitData();
					} else {
						MainContainer.activityCallBackNutrients.showNextFragment();
					}
				}else{
					AlertDialogs.getInstance().showDialogOK(null, errorMessage, null, true);
				}
			}
		});

		buttonSubmit.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {

			}
		});
	}


	private String getEnabledIDFor(String id, ArrayList<ClassScreenItem> listEnable) {
		for (ClassScreenItem item : listEnable) {
			if (item.item_id.equals(id)) {
				return item.item_name;
			}
		}
		return "";
	}
	private View.OnTouchListener onTouchListenerScroll = new View.OnTouchListener() {

		@SuppressLint("ClickableViewAccessibility")
		@Override
		public boolean onTouch(View v, final MotionEvent motionEvent) {
			try {
				v.getParent().requestDisallowInterceptTouchEvent(true);
				switch (motionEvent.getAction() & MotionEvent.ACTION_MASK) {
					case MotionEvent.ACTION_UP:
						v.getParent().requestDisallowInterceptTouchEvent(false);
						break;
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			return false;
		}
	};
	private void generateTableDisplays(TableRow.LayoutParams[] paramsArrayTableRowColumns) {
		int serialNum = 0;
		int colorBgTableRow = MainContainer.mContext.getResources().getColor(R.color.row_background);
		for (ClassScreenItem display : arrayListDisplays) {
			boolean bIsOdd = serialNum % 2 == 0;
			serialNum++;

			TableRow tableRow = new TableRow(MainContainer.mContext);
			CustomEditText editTextSerialNum = new CustomEditText(bIsOdd, "" + serialNum);
			CustomEditText editTextLocation = new CustomEditText(bIsOdd, display.item_name);
			CustomToggleButtonTwoState toggleButtonDisplayed = new CustomToggleButtonTwoState(bIsOdd);

			editTextSerialNum.customiseEditText(paramsArrayTableRowColumns[0]);
			editTextLocation.customiseEditText(paramsArrayTableRowColumns[1]);
			toggleButtonDisplayed.customiseToggleButton(paramsArrayTableRowColumns[2]);

			tableRow.setBackgroundColor(colorBgTableRow);
			tableRow.addView(editTextSerialNum);
			tableRow.addView(editTextLocation);
			tableRow.addView(toggleButtonDisplayed);
			tableLayoutDisplays.addView(tableRow);
		}
	}


	private void generateTableHr(TableRow.LayoutParams[] paramsArrayTableRowColumns) {
		int serialNum = 0;
		int colorBgTableRow = MainContainer.mContext.getResources().getColor(R.color.row_background);
		for (ClassScreenItem display : arrayListHr) {
			boolean bIsOdd = serialNum % 2 == 0;
			serialNum++;

			TableRow tableRow = new TableRow(MainContainer.mContext);
			CustomEditText editTextSerialNum = new CustomEditText(bIsOdd, "" + serialNum);
			CustomEditText editTextName = new CustomEditText(bIsOdd, display.item_name);
			CustomToggleButtonTwoState toggleButtonAssgined = new CustomToggleButtonTwoState(bIsOdd);
			CustomToggleButtonTwoState toggleButtonPresent = new CustomToggleButtonTwoState(bIsOdd);

			editTextSerialNum.customiseEditText(paramsArrayTableRowColumns[0]);
			editTextName.customiseEditText(paramsArrayTableRowColumns[1]);
			toggleButtonAssgined.customiseToggleButton(paramsArrayTableRowColumns[2]);
			toggleButtonPresent.customiseToggleButton(paramsArrayTableRowColumns[3]);


			tableRow.setBackgroundColor(colorBgTableRow);
			tableRow.addView(editTextSerialNum);
			tableRow.addView(editTextName);
			tableRow.addView(toggleButtonAssgined);
			tableRow.addView(toggleButtonPresent);
			tableLayoutHr.addView(tableRow);
		}
	}

	private void generateTableEquipmentsDetails(TableRow.LayoutParams[] paramsArrayTableRowColumns) {
		String[] arrayChildsLabels = new String[] { "Functional Count", "Non-Functional Count", "Non-Repairable Count" };
		int colorBgTableRow = MainContainer.mContext.getResources().getColor(R.color.row_background);
		if(arrayListEquipments != null) {
			for (int index = 0; index < arrayListEquipments.size(); index++) {
				boolean bIsOdd = index % 2 == 0;

				TableRow tableRow = new TableRow(MainContainer.mContext);
				CustomEditText editTextSerialNum = new CustomEditText(bIsOdd, "" + (index + 1));
				CustomEditText editTextEquipment = new CustomEditText(bIsOdd, arrayListEquipments.get(index).item_name);
				CustomEditText editTextAvailable = new CustomEditText(bIsOdd);
				final CustomEditText editTextFunctional = new CustomEditText(bIsOdd);
				final CustomEditText editTextNonFunctional = new CustomEditText(bIsOdd);
				final CustomEditText editTextNonRepairable = new CustomEditText(bIsOdd);

				editTextSerialNum.customiseEditText(paramsArrayTableRowColumns[0]);
				editTextEquipment.customiseEditText(paramsArrayTableRowColumns[1]);
				editTextAvailable.customiseEditText(paramsArrayTableRowColumns[2]);
				editTextFunctional.customiseEditText(paramsArrayTableRowColumns[3]);
				editTextNonFunctional.customiseEditText(paramsArrayTableRowColumns[4]);
				editTextNonRepairable.customiseEditText(paramsArrayTableRowColumns[5]);

				tableRow.setBackgroundColor(colorBgTableRow);
				tableLayoutEquipments.addView(tableRow);

				editTextFunctional.setInputTypeNumberEditable();
				editTextNonFunctional.setInputTypeNumberEditable();
				editTextNonRepairable.setInputTypeNumberEditable();
				editTextAvailable.setInputTypeNumberEditable();

				/*editTextAvailable.setText(MainContainer.searchForValue(arrayListEquipments.get(index).item_id, arrayListEquipmentsTotalAvailable));
				if (bIsEquipmentsTotalAvailableEditable) {
					editTextAvailable.setInputTypeNumberEditable();
				} else {
					editTextAvailable.setInputTypeNumberNonEditable();
				}*/
				String disabledID = getEnabledIDFor(arrayListEquipments.get(index).item_id,arrayListNonFunctionalNonRepairableEnabled);
				if (disabledID.length() > 0) {
					if (disabledID.equals("0")) {
						editTextNonFunctional.setInputTypeNumberNonEditable();
						editTextNonFunctional.setText("0");
					}
				}
				EditText[] arrayChildsEditTexts = new EditText[]{editTextFunctional, editTextNonFunctional, editTextNonRepairable};
				TextWatcherCountManyChilds textWatcherCount = new TextWatcherCountManyChilds(editTextAvailable, null, "Available Count",
						arrayChildsEditTexts, arrayChildsLabels, true);
				textWatcherCount.setTextWatcherChilds();

				editTextAvailable.addTextChangedListener(new TextWatcherAutoFill(editTextAvailable, arrayChildsEditTexts));

				tableRow.addView(editTextSerialNum);
				tableRow.addView(editTextEquipment);
				tableRow.addView(editTextAvailable);
				tableRow.addView(editTextFunctional);
				tableRow.addView(editTextNonFunctional);
				tableRow.addView(editTextNonRepairable);

			}
		}
	}

	private void generateTableIndicators(TableRow.LayoutParams[] paramsArrayTableRowColumns) {
		/*String[] arrayChildsLabels = new String[] { "Admitted Count", "Cured Count", "Stabilization Count" };*/
		int colorBgTableRow = MainContainer.mContext.getResources().getColor(R.color.row_background);
		if(arrayListIndicators != null) {
			for (int index = 0; index < arrayListIndicators.size(); index++) {
				boolean bIsOdd = index % 2 == 0;

				TableRow tableRow = new TableRow(MainContainer.mContext);
				CustomEditText editTextSerialNum = new CustomEditText(bIsOdd, "" + (index + 1));
				CustomEditText editTextEquipment = new CustomEditText(bIsOdd, arrayListIndicators.get(index).item_name);
				CustomEditText editTextAvailable = new CustomEditText(bIsOdd);
				CustomEditText editTextFunctional = new CustomEditText(bIsOdd);
				CustomEditText editTextNonFunctional = new CustomEditText(bIsOdd);
				CustomEditText editTextNonRepairable = new CustomEditText(bIsOdd);


				editTextSerialNum.customiseEditText(paramsArrayTableRowColumns[0]);
				editTextEquipment.customiseEditText(paramsArrayTableRowColumns[1]);
				editTextAvailable.customiseEditText(paramsArrayTableRowColumns[2]);
				editTextFunctional.customiseEditText(paramsArrayTableRowColumns[3]);
				editTextNonFunctional.customiseEditText(paramsArrayTableRowColumns[4]);
				editTextNonRepairable.customiseEditText(paramsArrayTableRowColumns[5]);


				tableRow.setBackgroundColor(colorBgTableRow);
				tableLayoutIndicators.addView(tableRow);

				editTextFunctional.setInputTypeNumberEditable();
				editTextAvailable.setInputTypeNumberEditable();
				editTextNonFunctional.setInputTypeNumberEditable();
				editTextNonRepairable.setInputTypeNumberEditable();

				/*editTextAvailable.setText(MainContainer.searchForValue(arrayListEquipments.get(index).item_id, arrayListEquipmentsTotalAvailable));
				if (bIsEquipmentsTotalAvailableEditable) {
					editTextAvailable.setInputTypeNumberEditable();
				} else {
					editTextAvailable.setInputTypeNumberNonEditable();
				}*/

				/*EditText[] arrayChildsEditTexts = new EditText[]{editTextFunctional, editTextNonFunctional, editTextNonRepairable};
				TextWatcherCountManyChilds textWatcherCount = new TextWatcherCountManyChilds(editTextAvailable, null, "Referred Count",
						arrayChildsEditTexts, arrayChildsLabels);
				textWatcherCount.setTextWatcherChilds();*/

				tableRow.addView(editTextSerialNum);
				tableRow.addView(editTextEquipment);
				tableRow.addView(editTextAvailable);
				tableRow.addView(editTextFunctional);
				tableRow.addView(editTextNonFunctional);
				tableRow.addView(editTextNonRepairable);
				String enalableID = getEnabledIDFor(arrayListIndicators.get(index).item_id,arrayListNoOfCuredEnabled);
				if (enalableID.length() > 0) {
					if (enalableID.equals("0")) {
						editTextNonFunctional.setInputTypeNumberNonEditable();
						editTextNonFunctional.setText("0");
					}
				}
				 enalableID = getEnabledIDFor(arrayListIndicators.get(index).item_id,arrayListNoOfRefferedEnabled);
				if (enalableID.length() > 0) {
					if (enalableID.equals("0")) {
						editTextNonRepairable.setInputTypeNumberNonEditable();
						editTextNonRepairable.setText("0");
					}
				}
			}
		}
	}

	private void generateTableMedicines(TableRow.LayoutParams[] paramsArrayTableRowColumns) {
		/*String[] arrayChildsLabels = new String[] { "Admitted Count", "Cured Count", "Stabilization Count" };*/
		int colorBgTableRow = MainContainer.mContext.getResources().getColor(R.color.row_background);
		if(arrayListMedicines != null) {
			for (int index = 0; index < arrayListMedicines.size(); index++) {
				boolean bIsOdd = index % 2 == 0;

				TableRow tableRow = new TableRow(MainContainer.mContext);
				CustomEditText editTextSerialNum = new CustomEditText(bIsOdd, "" + (index + 1));
				CustomEditText editTextEquipment = new CustomEditText(bIsOdd, arrayListMedicines.get(index).item_name);
				CustomEditText editTextAvailable = new CustomEditText(bIsOdd);
				CustomEditText editTextFunctional = new CustomEditText(bIsOdd);
				CustomEditText editTextNonFunctional = new CustomEditText(bIsOdd);
				CustomEditText editTextNonRepairable = new CustomEditText(bIsOdd);

				editTextSerialNum.customiseEditText(paramsArrayTableRowColumns[0]);
				editTextEquipment.customiseEditText(paramsArrayTableRowColumns[1]);
				editTextAvailable.customiseEditText(paramsArrayTableRowColumns[2]);
				editTextFunctional.customiseEditText(paramsArrayTableRowColumns[3]);
				editTextNonFunctional.customiseEditText(paramsArrayTableRowColumns[4]);
				editTextNonRepairable.customiseEditText(paramsArrayTableRowColumns[5]);



				tableRow.setBackgroundColor(colorBgTableRow);
				tableLayoutMedicines.addView(tableRow);

				editTextFunctional.setInputTypeNumberEditable();
				editTextAvailable.setInputTypeNumberEditable();
				editTextNonFunctional.setInputTypeNumberEditable();
				editTextNonRepairable.setInputTypeNumberEditable();

				/*editTextAvailable.setText(MainContainer.searchForValue(arrayListEquipments.get(index).item_id, arrayListEquipmentsTotalAvailable));
				if (bIsEquipmentsTotalAvailableEditable) {
					editTextAvailable.setInputTypeNumberEditable();
				} else {
					editTextAvailable.setInputTypeNumberNonEditable();
				}*/

				/*EditText[] arrayChildsEditTexts = new EditText[]{editTextFunctional, editTextNonFunctional, editTextNonRepairable};
				TextWatcherCountManyChilds textWatcherCount = new TextWatcherCountManyChilds(editTextAvailable, null, "Referred Count",
						arrayChildsEditTexts, arrayChildsLabels);
				textWatcherCount.setTextWatcherChilds();*/

				tableRow.addView(editTextSerialNum);
				tableRow.addView(editTextEquipment);
				tableRow.addView(editTextAvailable);
				tableRow.addView(editTextFunctional);
				tableRow.addView(editTextNonFunctional);
				tableRow.addView(editTextNonRepairable);
			}
		}
	}

	@Override
	public void onFragmentShown() {
	}
}