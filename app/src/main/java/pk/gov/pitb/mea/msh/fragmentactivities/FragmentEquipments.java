package pk.gov.pitb.mea.msh.fragmentactivities;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import pk.gov.pitb.mea.msh.R;
import pk.gov.pitb.mea.msh.handlers.InterfaceFragmentCallBack;
import pk.gov.pitb.mea.msh.helpers.Constants;
import pk.gov.pitb.mea.msh.helpers.MainContainer;
import pk.gov.pitb.mea.msh.helpers.UIHelper;
import pk.gov.pitb.mea.msh.models.ClassFacilityItem;
import pk.gov.pitb.mea.msh.models.ClassScreenData;
import pk.gov.pitb.mea.msh.models.ClassScreenItem;
import pk.gov.pitb.mea.msh.models.ClassScreenItemMultiple;
import pk.gov.pitb.mea.msh.textwatchers.TextWatcherCountManyChilds;
import pk.gov.pitb.mea.msh.textwatchers.TextWatcherCountSingleChild;
import pk.gov.pitb.mea.msh.views.CustomEditText;
import pk.gov.pitb.mea.msh.views.CustomToggleButtonTwoState;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.app.Notification;
import android.app.assist.AssistStructure;
import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.ToggleButton;

import static android.media.CamcorderProfile.get;

public class FragmentEquipments extends Fragment implements InterfaceFragmentCallBack {

	private final static String TAG_ARRAY_EQUIPMENTS = "equipments";

	private boolean bIsEquipmentsTotalAvailableEditable;
	private ArrayList<ClassScreenItemMultiple> arrayListAllEquipments;
	private ArrayList<ClassFacilityItem> arrayListEquipmentsTotalAvailable;

	private int fragmentCategory;
	private int startIndex;
	private int endIndex;
	private int indexEquipments;
	private View parentView;
	private LinearLayout linearLayoutMain;
	private TextView[] textViewsEquipments;
	private TableLayout[] tableLayoutsEquipments;
	private TableLayout tableLayoutHWManagementArticleSection5;
	private ArrayList<ClassScreenItem> arrayListHWManagementArticle;
	private TextView textViewHWManagementArticleSection5;

	public FragmentEquipments(ArrayList<ClassScreenItemMultiple> arrayListAllEquipments, int category) {
		super();
		this.arrayListAllEquipments = arrayListAllEquipments;
		this.fragmentCategory = category;
	}

	@Override
	public void parseObject() {
		try {
			JSONObject jsonObject = null;
			JSONArray jsonArrayEquipments = null;
			if (fragmentCategory == Constants.FRAGMENT_EQUIPMENT_1 || !MainContainer.mJsonObjectFormData.has(Constants.DB_SCREEN_NAME_EQUIPMENT)) {
				jsonObject = new JSONObject();
				jsonArrayEquipments = new JSONArray();
			} else {
				jsonObject = MainContainer.mJsonObjectFormData.getJSONObject(Constants.DB_SCREEN_NAME_EQUIPMENT);
				if (jsonObject.has(TAG_ARRAY_EQUIPMENTS)) {
					jsonArrayEquipments = jsonObject.getJSONArray(TAG_ARRAY_EQUIPMENTS);
				} else {
					jsonArrayEquipments = new JSONArray();
				}
			}
			for (int j = 0; j < tableLayoutsEquipments.length; j++) {
				ClassScreenItemMultiple objectMultiple = arrayListAllEquipments.get(startIndex + j);
				for (int i = 1; i < tableLayoutsEquipments[j].getChildCount(); i++) {
					View child = tableLayoutsEquipments[j].getChildAt(i);
					ClassScreenItem object = objectMultiple.arrayListFields.get(i - 1);
					JSONObject jsonObjectChild = new JSONObject();

					TableRow tableRow = (TableRow) child;
					//ToggleButton present = (ToggleButton) tableRow.getChildAt(1);
					EditText available = (EditText) tableRow.getChildAt(1);
					EditText functional = (EditText) tableRow.getChildAt(2);
					EditText repairable = (EditText) tableRow.getChildAt(3);
					EditText nonRepairable = (EditText) tableRow.getChildAt(4);
					EditText tagged = (EditText) tableRow.getChildAt(5);
					/*String presentID = "";
					if (present.isChecked()) {
						presentID = "y";
					} else if (present.isEnabled()) {
						presentID = "n";
					}
					jsonObjectChild.put("present", presentID);*/
					jsonObjectChild.put("eq_id", object.item_id);
					jsonObjectChild.put("equipment_name", object.item_name);
					jsonObjectChild.put("total_available", available.getText().toString().trim());
					jsonObjectChild.put("functional", functional.getText().toString().trim());
					jsonObjectChild.put("non_functional_but_repairable", repairable.getText().toString().trim());
					jsonObjectChild.put("non_repairable", nonRepairable.getText().toString().trim());
					jsonObjectChild.put("tagged", tagged.getText().toString().trim());

					jsonArrayEquipments.put(jsonObjectChild);
				}
			}

			JSONArray jsonArraySection9 = new JSONArray();
			for (int i = 1; i < tableLayoutHWManagementArticleSection5.getChildCount(); i++) {
				View child = tableLayoutHWManagementArticleSection5.getChildAt(i);
				ClassScreenItem object = arrayListHWManagementArticle.get(i - 1);
				if (child instanceof TableRow) {
					JSONObject jsonObjectChild = new JSONObject();

					TableRow tableRow = (TableRow) child;
					CustomToggleButtonTwoState displayed = (CustomToggleButtonTwoState) tableRow.getChildAt(2);

					String displayedID = "";

					if (!displayed.isCheckChanged()) {
						displayedID = "";
					} else if (displayed.isChecked()) {
						displayedID = /*arrayListDisplaysDropdown.get(0).item_id*/"yes";
					} else if (displayed.isEnabled()) {
						displayedID = /*arrayListDisplaysDropdown.get(1).item_id*/"no";
					}
					jsonObjectChild.put("wm_id", object.item_id);
					jsonObjectChild.put("wm_name", object.item_name);
					jsonObjectChild.put("status", displayedID);

					jsonArraySection9.put(jsonObjectChild);
				}
			}

			jsonObject.remove(TAG_ARRAY_EQUIPMENTS);
			jsonObject.put(TAG_ARRAY_EQUIPMENTS, jsonArrayEquipments);

			if (tableLayoutHWManagementArticleSection5.getVisibility() == View.VISIBLE)
				jsonObject.put("waste_management_article_section", jsonArraySection9);
			MainContainer.mJsonObjectFormData.remove(Constants.DB_SCREEN_NAME_EQUIPMENT);
			MainContainer.mJsonObjectFormData.put(Constants.DB_SCREEN_NAME_EQUIPMENT, jsonObject);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void loadSavedData() {
		try {
			if (!(MainContainer.mJsonObjectFormDataEquipments.toString().equals(""))) {
				JSONObject jsonObject = MainContainer.mJsonObjectFormDataEquipments;/*MainContainer.mJsonObjectFormData.getJSONObject(Constants.DB_SCREEN_NAME_EQUIPMENT);*/
				JSONArray jsonArray = jsonObject.getJSONArray(TAG_ARRAY_EQUIPMENTS);

				for (int j = 0; j < tableLayoutsEquipments.length; j++) {
					ClassScreenItemMultiple objectMultiple = arrayListAllEquipments.get(startIndex + j);
					int arrayCount = 0;
					for (int i = 1; i < tableLayoutsEquipments[j].getChildCount(); i++) {
						View child = tableLayoutsEquipments[j].getChildAt(i);
						ClassScreenItem object = objectMultiple.arrayListFields.get(i - 1);
						JSONObject jsonObjectChild = jsonArray.getJSONObject(arrayCount++);
						if (object.item_id.equals(jsonObjectChild.getString("eq_id"))) {
							TableRow tableRow = (TableRow) child;
							//ToggleButton present = (ToggleButton) tableRow.getChildAt(1);
							EditText available = (EditText) tableRow.getChildAt(1);
							EditText functional = (EditText) tableRow.getChildAt(2);
							EditText repairable = (EditText) tableRow.getChildAt(3);
							EditText nonRepairable = (EditText) tableRow.getChildAt(4);
							EditText tagged = (EditText) tableRow.getChildAt(5);


							available.setText(jsonObjectChild.getString("total_available"));
							functional.setText(jsonObjectChild.getString("functional"));
							repairable.setText(jsonObjectChild.getString("non_functional_but_repairable"));
							nonRepairable.setText(jsonObjectChild.getString("non_repairable"));
							tagged.setText(jsonObjectChild.getString("tagged"));
						} else {
							if (arrayCount != jsonArray.length()) {
								i--;
							}
						}
					}
				}

				JSONArray JSONArraySections5 = jsonObject.has("section5") ? jsonObject.getJSONArray("waste_management_article_section") : new JSONArray();

				if (JSONArraySections5.length() == 0) {
					tableLayoutHWManagementArticleSection5.setVisibility(View.GONE);
				} else {
					for (int i = 0; i < JSONArraySections5.length(); i++) {
						boolean bIsOdd = i % 2 == 0;

						View child = tableLayoutHWManagementArticleSection5.getChildAt(i + 1);
						JSONObject jObject = JSONArraySections5.getJSONObject(i);

						if (child instanceof TableRow) {

							TableRow tableRow = (TableRow) child;
							CustomToggleButtonTwoState displayedToggleButton = (CustomToggleButtonTwoState) tableRow.getChildAt(2);
							String HWManagementArticle_ID = jObject.getString("wm_id");
							String dd_ID = jObject.getString("status");
							if (dd_ID.equals("yes")) {
								displayedToggleButton.setbIsCheckChanged(true);
								displayedToggleButton.setChecked(true);
								displayedToggleButton.setBackgroundResource(bIsOdd ? R.drawable.toggle_tick_odd : R.drawable.toggle_tick_even);
							} else if (dd_ID.equals("no")) {
								displayedToggleButton.setbIsCheckChanged(true);
								displayedToggleButton.setChecked(false);
								displayedToggleButton.setBackgroundResource(bIsOdd ? R.drawable.toggle_cross_odd : R.drawable.toggle_cross_even);
							}

						}
					}
				}

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		if (parentView == null) {
			parentView = inflater.inflate(R.layout.fragment_default, container, false);
			initializeData();
			generateBody();
			loadSavedData();
		}
		return parentView;
	}

	private void initializeData() {
		try {
			ClassScreenData objectScreenData = new ClassScreenData();
			objectScreenData.setDefaultValues();
			objectScreenData.screen_id = Constants.DB_SCREEN_ID_EQUIPMENT;
			objectScreenData.screen_name = Constants.DB_SCREEN_NAME_EQUIPMENT;
			arrayListAllEquipments = MainContainer.mDbAdapter.selectScreenItemsMultiple(objectScreenData);
			arrayListEquipmentsTotalAvailable = MainContainer.mObjectFacilityData.getListTotalAvailableMedEquip();
			bIsEquipmentsTotalAvailableEditable = MainContainer.mObjectFacilityData.getIsTotalAvailableMedEquipEditable();

//			objectScreenData.section_name = "waste_management_article_section";
//			objectScreenData.section_value = "fields";
//			objectScreenData = MainContainer.mDbAdapter.selectScreenData(objectScreenData);
//			arrayListHWManagementArticle = MainContainer.mDbAdapter.selectScreenItems(objectScreenData);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void generateBody() {
		try {
			linearLayoutMain = (LinearLayout) parentView.findViewById(R.id.linearlayout_main);

			TableRow.LayoutParams[] paramsArrayTableRowColumns = new TableRow.LayoutParams[6];
			paramsArrayTableRowColumns[0] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.20), TableRow.LayoutParams.MATCH_PARENT);
			paramsArrayTableRowColumns[1] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.15), TableRow.LayoutParams.MATCH_PARENT);
			paramsArrayTableRowColumns[2] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.15), TableRow.LayoutParams.MATCH_PARENT);
			paramsArrayTableRowColumns[3] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.15), TableRow.LayoutParams.MATCH_PARENT);
			paramsArrayTableRowColumns[4] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.15), TableRow.LayoutParams.WRAP_CONTENT);
			paramsArrayTableRowColumns[5] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.15), TableRow.LayoutParams.MATCH_PARENT);
			for (int i = 1; i < paramsArrayTableRowColumns.length; i++) {
				paramsArrayTableRowColumns[i].setMargins(2, 0, 0, 0);
			}

			loadTableData();
			// asyncTaskLoadEquipments.execute((Void) null);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void generateTableHWManagementArticle(TableRow.LayoutParams[] paramsArrayTableRowColumns) {
		int serialNum = 0;
		int colorBgTableRow = MainContainer.mContext.getResources().getColor(R.color.row_background);
		for (ClassScreenItem display : arrayListHWManagementArticle) {
			boolean bIsOdd = serialNum % 2 == 0;
			serialNum++;

			TableRow tableRow = new TableRow(MainContainer.mContext);
			CustomEditText editTextSerialNum = new CustomEditText(bIsOdd, "" + serialNum);
			CustomEditText editTextLocation = new CustomEditText(bIsOdd, display.item_name);
			CustomToggleButtonTwoState toggleButtonDisplayed = new CustomToggleButtonTwoState(bIsOdd);
			editTextSerialNum.customiseEditText(paramsArrayTableRowColumns[0]);
			editTextLocation.customiseEditText(paramsArrayTableRowColumns[1]);
			toggleButtonDisplayed.customiseToggleButton(paramsArrayTableRowColumns[2]);

			tableRow.setBackgroundColor(colorBgTableRow);
			tableRow.addView(editTextSerialNum);
			tableRow.addView(editTextLocation);
			tableRow.addView(toggleButtonDisplayed);
			tableLayoutHWManagementArticleSection5.addView(tableRow);
		}
	}

	private void loadTableData() {
		int size = arrayListAllEquipments.size();
		switch (fragmentCategory) {
			case Constants.FRAGMENT_EQUIPMENT_6:
				startIndex = Constants.END_INDEX_EUIPMENT_5;
				endIndex = arrayListAllEquipments.size();
				break;
			case Constants.FRAGMENT_EQUIPMENT_5:
				startIndex = Constants.END_INDEX_EUIPMENT_4;
				endIndex = size > Constants.END_INDEX_EUIPMENT_5 ? Constants.END_INDEX_EUIPMENT_5 : size;
				break;
			case Constants.FRAGMENT_EQUIPMENT_4:
				startIndex = Constants.END_INDEX_EUIPMENT_3;
				endIndex = size > Constants.END_INDEX_EUIPMENT_4 ? Constants.END_INDEX_EUIPMENT_4 : size;
				break;
			case Constants.FRAGMENT_EQUIPMENT_3:
				startIndex = Constants.END_INDEX_EUIPMENT_2;
				endIndex = size > Constants.END_INDEX_EUIPMENT_3 ? Constants.END_INDEX_EUIPMENT_3 : size;
				break;
			case Constants.FRAGMENT_EQUIPMENT_2:
				startIndex = Constants.END_INDEX_EUIPMENT_1;
				endIndex = size > Constants.END_INDEX_EUIPMENT_2 ? Constants.END_INDEX_EUIPMENT_2 : size;
				break;
			case Constants.FRAGMENT_EQUIPMENT_1:
			default:
				startIndex = 0;
				endIndex = size > Constants.END_INDEX_EUIPMENT_1 ? Constants.END_INDEX_EUIPMENT_1 : size;
				break;
		}
		size = endIndex - startIndex;
		textViewsEquipments = new TextView[size];
		tableLayoutsEquipments = new TableLayout[size];
		linearLayoutMain.setVisibility(View.GONE);
		TableRow.LayoutParams[] paramsArrayTableRowColumnsHeader;
		paramsArrayTableRowColumnsHeader = new TableRow.LayoutParams[6];
		paramsArrayTableRowColumnsHeader[0] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.20), TableRow.LayoutParams.MATCH_PARENT);
		paramsArrayTableRowColumnsHeader[1] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.15), TableRow.LayoutParams.MATCH_PARENT);
		paramsArrayTableRowColumnsHeader[2] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.15), TableRow.LayoutParams.MATCH_PARENT);
		paramsArrayTableRowColumnsHeader[3] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.15), TableRow.LayoutParams.MATCH_PARENT);
		paramsArrayTableRowColumnsHeader[4] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.15), TableRow.LayoutParams.WRAP_CONTENT);
		paramsArrayTableRowColumnsHeader[5] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.15), TableRow.LayoutParams.MATCH_PARENT);

		TableRow.LayoutParams[] paramsArrayTableRowColumnsFooter;
		paramsArrayTableRowColumnsFooter = new TableRow.LayoutParams[6];
		paramsArrayTableRowColumnsFooter[0] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.20), TableRow.LayoutParams.MATCH_PARENT);
		paramsArrayTableRowColumnsFooter[1] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.15), TableRow.LayoutParams.MATCH_PARENT);
		paramsArrayTableRowColumnsFooter[2] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.15), TableRow.LayoutParams.MATCH_PARENT);
		paramsArrayTableRowColumnsFooter[3] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.15), TableRow.LayoutParams.MATCH_PARENT);
		paramsArrayTableRowColumnsFooter[4] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.15), TableRow.LayoutParams.MATCH_PARENT);
		paramsArrayTableRowColumnsFooter[5] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.15), TableRow.LayoutParams.MATCH_PARENT);
		for (int i = 1; i < paramsArrayTableRowColumnsHeader.length; i++) {
			paramsArrayTableRowColumnsHeader[i].setMargins(2, 0, 0, 0);
			paramsArrayTableRowColumnsFooter[i].setMargins(2, 0, 0, 0);
		}

		UIHelper uiContainer = new UIHelper();
		indexEquipments = 0;
		for (int i = 0, j = startIndex; i < size; i++, j++) {
			textViewsEquipments[i] = new TextView(MainContainer.mContext);
			tableLayoutsEquipments[i] = new TableLayout(MainContainer.mContext);
			if (arrayListAllEquipments.get(i).arrayListFields.size() > 0) {
				linearLayoutMain.addView(textViewsEquipments[i]);
				linearLayoutMain.addView(tableLayoutsEquipments[i]);
				uiContainer.generateTableLabel(textViewsEquipments[i], arrayListAllEquipments.get(j).label);
				uiContainer.generateTableHeader(tableLayoutsEquipments[i], paramsArrayTableRowColumnsHeader, "Equipment", "Total Available", "Functional",
						"Non-Functional but Repairable", "Non-Repairable", "Tagged");

				generateTableEquipmentsDetails(paramsArrayTableRowColumnsFooter, tableLayoutsEquipments[i], arrayListAllEquipments.get(j).arrayListFields);
			}
		}

//		TableRow.LayoutParams[] paramsArrayTableRowColumnsHWManagementArticle = new TableRow.LayoutParams[3];
//		paramsArrayTableRowColumnsHWManagementArticle[0] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.1),
//				(int) (MainContainer.mScreenWidth * 0.08));
//		paramsArrayTableRowColumnsHWManagementArticle[1] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.66),
//				(int) (MainContainer.mScreenWidth * 0.08));
//		paramsArrayTableRowColumnsHWManagementArticle[2] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.2),
//				(int) (MainContainer.mScreenWidth * 0.08));
//
//		for (int i = 1; i < paramsArrayTableRowColumnsHWManagementArticle.length; i++) {
//			paramsArrayTableRowColumnsHWManagementArticle[i].setMargins(2, 0, 0, 0);
//		}
//
//		uiContainer.generateTableLabel(textViewHWManagementArticleSection5, "Hospital Waste Management Articles");
//
//		uiContainer.generateTableHeader(tableLayoutHWManagementArticleSection5, paramsArrayTableRowColumnsHWManagementArticle, new String[]{"No.", "Name", "Status"});
//		generateTableHWManagementArticle(paramsArrayTableRowColumnsHWManagementArticle);
//
//		if (tableLayoutHWManagementArticleSection5.getChildCount() > 1) {
//			textViewHWManagementArticleSection5.setVisibility(View.VISIBLE);
//			tableLayoutHWManagementArticleSection5.setVisibility(View.VISIBLE);
//		} else {
//			textViewHWManagementArticleSection5.setVisibility(View.GONE);
//			tableLayoutHWManagementArticleSection5.setVisibility(View.GONE);
//		}
//
//		textViewHWManagementArticleSection5 = new TextView(MainContainer.mContext);
//		tableLayoutHWManagementArticleSection5 = new TableLayout(MainContainer.mContext);
//		linearLayoutMain.addView(textViewHWManagementArticleSection5);
//		linearLayoutMain.addView(tableLayoutHWManagementArticleSection5);

		linearLayoutMain.setVisibility(View.VISIBLE);
	}

	// private AsyncTask<Void, Void, Void> asyncTaskLoadEquipments = new AsyncTask<Void, Void, Void>() {
	//
	// private ProgressDialog progressDialog;
	//
	// @Override
	// protected void onPreExecute() {
	// try {
	// progressDialog = new ProgressDialog(MainContainer.mContext);
	// progressDialog.setCancelable(false);
	// progressDialog.setTitle("Loading Equipments");
	// progressDialog.setMessage("Please Wait...");
	// progressDialog.setIndeterminate(true);
	// progressDialog.show();
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	// }
	//
	// @Override
	// protected Void doInBackground(Void... params) {
	// try {
	// Thread.sleep(1000);
	// } catch (InterruptedException e) {
	// e.printStackTrace();
	// }
	// return null;
	// }
	//
	// @Override
	// protected void onPostExecute(Void result) {
	// loadTableData();
	// progressDialog.dismiss();
	// }
	// };

	private void generateTableEquipmentsDetails(TableRow.LayoutParams[] paramsArrayTableRowColumns, TableLayout tableLayout, ArrayList<ClassScreenItem> arrayListItems) {
		String[] arrayChildsLabels = new String[]{"Functional Count", "Non-Functional Count", "Non-Repairable Count"};
		int colorBgTableRow = MainContainer.mContext.getResources().getColor(R.color.row_background);
		for (int i = 0; i < arrayListItems.size(); i++, indexEquipments++) {
			boolean bIsOdd = i % 2 == 0;

			TableRow tableRow = new TableRow(MainContainer.mContext);
			CustomEditText editTextEquipment = new CustomEditText(bIsOdd, (indexEquipments + 1) + ". " + arrayListItems.get(i).item_name);
			CustomEditText editTextAvailable = new CustomEditText(bIsOdd);
			CustomEditText editTextFunctional = new CustomEditText(bIsOdd);
			CustomEditText editTextNonFunctional = new CustomEditText(bIsOdd);
			CustomEditText editTextNonRepairable = new CustomEditText(bIsOdd);
			//CustomToggleButtonTwoState toggleButtonOperatorPresent = new CustomToggleButtonTwoState(bIsOdd);
			CustomEditText editTextTagged = new CustomEditText(bIsOdd);

			editTextEquipment.customiseEditText(paramsArrayTableRowColumns[0]);
			//toggleButtonOperatorPresent.customiseToggleButton(paramsArrayTableRowColumns[1]);
			editTextAvailable.customiseEditText(paramsArrayTableRowColumns[1]);
			editTextFunctional.customiseEditText(paramsArrayTableRowColumns[2]);
			editTextNonFunctional.customiseEditText(paramsArrayTableRowColumns[3]);
			editTextNonRepairable.customiseEditText(paramsArrayTableRowColumns[4]);
			editTextTagged.customiseEditText(paramsArrayTableRowColumns[5]);

			//	toggleButtonOperatorPresent.changeDrawable(3);
			tableRow.setBackgroundColor(colorBgTableRow);

			editTextEquipment.setOnClickListener(onClickShowName);
			editTextFunctional.setInputTypeNumberEditable();
			editTextNonFunctional.setInputTypeNumberEditable();
			editTextNonRepairable.setInputTypeNumberEditable();
			editTextTagged.setInputTypeNumberEditable();

			editTextAvailable.setText(MainContainer.searchForValue(arrayListItems.get(i).item_id, arrayListEquipmentsTotalAvailable));
			if (bIsEquipmentsTotalAvailableEditable) {
				editTextAvailable.setInputTypeNumberEditable();
			} else {
				editTextAvailable.setInputTypeNumberNonEditable();
			}
			boolean childCountLessNotMoreThanMaxCount = false;
			if (arrayListItems.get(i).item_name.toLowerCase().contains("air conditioners") || arrayListItems.get(i).item_name.toLowerCase().contains("generator"))
				childCountLessNotMoreThanMaxCount = true;

			EditText[] arrayChildsEditTexts = new EditText[]{editTextFunctional, editTextNonFunctional, editTextNonRepairable};
			TextWatcherCountManyChilds textWatcherCount = new TextWatcherCountManyChilds(editTextAvailable, null, "Available Count", arrayChildsEditTexts, arrayChildsLabels, childCountLessNotMoreThanMaxCount);
			textWatcherCount.setTextWatcherChilds();
			textWatcherCount.setTextWatcherParent(editTextTagged);

			TextWatcherCountSingleChild textWatcherMaxCount = new TextWatcherCountSingleChild(editTextAvailable, editTextTagged, "Tagged Count", "Available Count");

			editTextTagged.addTextChangedListener(textWatcherMaxCount);

			tableRow.addView(editTextEquipment);
			//	tableRow.addView(toggleButtonOperatorPresent);
			tableRow.addView(editTextAvailable);
			tableRow.addView(editTextFunctional);
			tableRow.addView(editTextNonFunctional);
			tableRow.addView(editTextNonRepairable);
			tableRow.addView(editTextTagged);
			tableLayout.addView(tableRow);
		}
	}

	private View.OnClickListener onClickShowName = new View.OnClickListener() {

		@SuppressLint("InflateParams")
		@Override
		public void onClick(View view) {
			try {
				LayoutInflater layoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				View layout = layoutInflater.inflate(R.layout.layout_pop_up_message, null);
				((TextView) layout).setText(((EditText) view).getText().toString().trim());

				PopupWindow popupWindow = new PopupWindow(getActivity());
				popupWindow.setContentView(layout);
				popupWindow.setWidth(LinearLayout.LayoutParams.WRAP_CONTENT);
				popupWindow.setHeight(LinearLayout.LayoutParams.WRAP_CONTENT);
				popupWindow.setBackgroundDrawable(new BitmapDrawable());
				popupWindow.setOutsideTouchable(true);
				popupWindow.showAsDropDown(view);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	};

	@Override
	public void onFragmentShown() {
	}

	private String errorMessage;

	@Override
	public String onFragmentChanged(int previousPosition) {
		errorMessage = null;
		isFormValid();
		return errorMessage;
	}

	@Override
	public boolean isFormValid() {
		try {
			boolean bIsValid = true;
			for (int j = 0; j < tableLayoutsEquipments.length; j++) {
				for (int i = 1; i < tableLayoutsEquipments[j].getChildCount(); i++) {
					View child = tableLayoutsEquipments[j].getChildAt(i);
					TableRow tableRow = (TableRow) child;
					//CustomToggleButtonTwoState present = (CustomToggleButtonTwoState) tableRow.getChildAt(1);
					EditText available = (EditText) tableRow.getChildAt(1);
					EditText functional = (EditText) tableRow.getChildAt(2);
					EditText repairable = (EditText) tableRow.getChildAt(3);
					EditText nonRepairable = (EditText) tableRow.getChildAt(4);
					EditText tagged = (EditText) tableRow.getChildAt(5);

					if (/*!present.isCheckChanged() || */available.getText().toString().trim().length() == 0 || functional.getText().toString().trim().length() == 0
							|| repairable.getText().toString().trim().length() == 0 || nonRepairable.getText().toString().trim().length() == 0 || tagged.getText().toString().trim().length() == 0) {
						bIsValid = false;
						break;
					}
				}
			}
			if (!bIsValid) {
				errorMessage = "Please fill all Equipments details to continue";
			}
			return bIsValid;
		} catch (Exception e) {
			e.printStackTrace();
			MainContainer.showMessage("Error Processing Your Request");
			return false;
		}
	}
}