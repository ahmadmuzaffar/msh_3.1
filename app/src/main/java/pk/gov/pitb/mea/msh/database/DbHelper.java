package pk.gov.pitb.mea.msh.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DbHelper extends SQLiteOpenHelper {

	private static final String DB_NAME = "MSHApp.db";
	private static final int DB_VERSION = 10;
	private Context context;

	public final String pk_id = "pk_id";
	public final String fk_id = "fk_id";

	public final String TABLE_ACTIVITY_DATA = "ActivityData";
	public final String app_version_no = "app_version_no";
	public final String activity_type = "activity_type";
	public final String sending_date_time = "sending_date_time";
	public final String json_data = "json_data";

	//@formatter:off
	private final String sqlCreateTable_ActivityData =
			"CREATE TABLE "+ TABLE_ACTIVITY_DATA +" (" +
					pk_id + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
					app_version_no + " VARCHAR, "	+
					activity_type + " VARCHAR, "	+
					sending_date_time + " VARCHAR, "	+
					json_data + " VARCHAR "+	");";
	private final String sqlDropTable_ActivityData = "DROP TABLE IF EXISTS " + TABLE_ACTIVITY_DATA + ";";
	//@formatter:on

	public final String TABLE_PICTURE_DATA = "PictureData";
	public final String picture_name = "picture_name";
	public final String picture_parameter = "picture_parameter";
	public final String picture_status = "picture_status";
	public final String picture_data = "picture_data";

	//@formatter:off
	private final String sqlCreateTable_PictureData =
			"CREATE TABLE "+ TABLE_PICTURE_DATA +" (" +
					pk_id + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
					fk_id + " INTEGER, "	+
					picture_name + " VARCHAR, "	+
					picture_parameter + " VARCHAR, "	+
					picture_status + " VARCHAR, "	+
					picture_data + " BLOB "+	");";
	private final String sqlDropTable_PictureData = "DROP TABLE IF EXISTS " + TABLE_PICTURE_DATA + ";";
	//@formatter:on

	public final String TABLE_SCREEN_DATA = "ScreenData";
	public final String type_id = "type_id";
	public final String type_name = "type_name";
	public final String screen_id = "screen_id";
	public final String screen_name = "screen_name";
	public final String section_id = "section_id";
	public final String section_name = "section_name";
	public final String section_value = "section_value";

	//@formatter:off
	private final String sqlCreateTable_ScreenData =
			"CREATE TABLE "+ TABLE_SCREEN_DATA +" (" +
					pk_id + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
					type_id + " VARCHAR, "	+
					type_name + " VARCHAR, "	+
					screen_id + " VARCHAR, "	+
					screen_name + " VARCHAR, "	+
					section_id + " VARCHAR, "	+
					section_name + " VARCHAR, "	+
					section_value + " VARCHAR "+	");";
	private final String sqlDropTable_ScreenData = "DROP TABLE IF EXISTS " + TABLE_SCREEN_DATA + ";";
	//@formatter:on

	public final String TABLE_SCREEN_ITEM = "ScreenItem";
	public final String item_id = "item_id";
	public final String item_name = "item_name";

	//@formatter:off
	private final String sqlCreateTable_ScreenItem =
			"CREATE TABLE "+ TABLE_SCREEN_ITEM +" (" +
					pk_id + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
					item_id + " VARCHAR, "	+
					item_name + " VARCHAR, "	+
					fk_id + " INTEGER "+	");";
	private final String sqlDropTable_ScreenItem = "DROP TABLE IF EXISTS " + TABLE_SCREEN_ITEM + ";";
	//@formatter:on

	public final String TABLE_FACILITY_DATA = "FacilityData";
	public final String facility_id = "facility_id";
	public final String facility_type_code = "facility_type_code";
	public final String facility_type = "facility_type";
	public final String facility_name = "facility_name";
	public final String tehsil_id = "tehsil_id";
	public final String tehsil_name = "tehsil_name";
	public final String district_id = "district_id";
	public final String district_name = "district_name";
	public final String json_arrays_data = "json_arrays_data";
	public final String facility_nutrition_second_type  = "facility_nutrition_second_type";
	public final String facility_sentinel_second_type  = "facility_sentinel_second_type";
	//@formatter:off
	private final String sqlCreateTable_FacilityData =
			"CREATE TABLE "+ TABLE_FACILITY_DATA +" (" +
					pk_id + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
					facility_id + " VARCHAR, "	+
					facility_type_code + " VARCHAR, "	+
					facility_type + " VARCHAR, "	+
					facility_name + " VARCHAR, "	+
					tehsil_id + " VARCHAR, "	+
					tehsil_name + " VARCHAR, "	+
					district_id + " VARCHAR, "	+
					district_name + " VARCHAR, "	+
					facility_nutrition_second_type + " VARCHAR, "	+
					facility_sentinel_second_type + " VARCHAR, "	+
					json_arrays_data + " VARCHAR "+	");";
	private final String sqlDropTable_FacilityData = "DROP TABLE IF EXISTS " + TABLE_FACILITY_DATA + ";";
	//@formatter:on

	public final String TABLE_STAFF_DATA = "StaffData";
	public final String staff_name = "staff_name";
	public final String staff_designation_id = "staff_designation_id";
	public final String staff_mobile = "staff_mobile";
	public final String staff_cnic = "staff_cnic";
	public final String staff_role = "staff_role";

	//@formatter:off
	private final String sqlCreateTable_StaffData =
			"CREATE TABLE "+ TABLE_STAFF_DATA +" (" +
					pk_id + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
					fk_id + " INTEGER, " +
					staff_name + " VARCHAR, "	+
					staff_designation_id + " VARCHAR, "	+
					staff_mobile + " VARCHAR, "	+
					staff_cnic + " VARCHAR, "	+
					staff_role + " VARCHAR "+	");";
	private final String sqlDropTable_StaffData = "DROP TABLE IF EXISTS " + TABLE_STAFF_DATA + ";";
	//@formatter:on

	public final String TABLE_SENT_DATA = "SentData";
	//@formatter:off
	private final String sqlCreateTable_SentData =
			"CREATE TABLE "+ TABLE_SENT_DATA +" (" +
					pk_id + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
					facility_id + " VARCHAR, "	+
					sending_date_time + " VARCHAR "+	");";
	private final String sqlDropTable_SentData = "DROP TABLE IF EXISTS " + TABLE_SENT_DATA + ";";
	//@formatter:on

	public DbHelper(Context c) {
		super(c, DB_NAME, null, DB_VERSION);
		context = c;
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(sqlCreateTable_ActivityData);
		db.execSQL(sqlCreateTable_PictureData);
		db.execSQL(sqlCreateTable_ScreenData);
		db.execSQL(sqlCreateTable_ScreenItem);
		db.execSQL(sqlCreateTable_FacilityData);
		db.execSQL(sqlCreateTable_StaffData);
		db.execSQL(sqlCreateTable_SentData);
		SharedPreferencesEditor sharedPreferencesEditor = new SharedPreferencesEditor(context);
		sharedPreferencesEditor.resetVersions();
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL(sqlDropTable_ActivityData);
		db.execSQL(sqlDropTable_PictureData);
		db.execSQL(sqlDropTable_ScreenData);
		db.execSQL(sqlDropTable_ScreenItem);
		db.execSQL(sqlDropTable_FacilityData);
		db.execSQL(sqlDropTable_StaffData);
		db.execSQL(sqlDropTable_SentData);
		onCreate(db);
	}
}