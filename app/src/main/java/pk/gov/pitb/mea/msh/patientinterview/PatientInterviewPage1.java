package pk.gov.pitb.mea.msh.patientinterview;

import android.app.Fragment;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import pk.gov.pitb.mea.msh.R;
import pk.gov.pitb.mea.msh.adapters.ArrayAdapterSpinnerItem;
import pk.gov.pitb.mea.msh.adapters.ArrayAdapterSpinnerScreenItem;
import pk.gov.pitb.mea.msh.adapters.ArrayAdapterSpinnerSimpleScreenItem;
import pk.gov.pitb.mea.msh.handlers.HandlerFragmentCallBackPatientInterviews;
import pk.gov.pitb.mea.msh.helpers.AlertDialogs;
import pk.gov.pitb.mea.msh.helpers.Constants;
import pk.gov.pitb.mea.msh.helpers.MainContainer;
import pk.gov.pitb.mea.msh.models.ClassScreenData;
import pk.gov.pitb.mea.msh.models.ClassScreenItem;
import pk.gov.pitb.mea.msh.textwatchers.TextWatcherCNIC;


public class PatientInterviewPage1 extends Fragment implements HandlerFragmentCallBackPatientInterviews {
    private View parentView = null;
    private MainContainer mGlobals;
    private RadioGroup  radioGroupGender, radioGroupAvailability/*, radioGroupAge*/;
    private Button buttonProceed;
    private String errorMessage;
    private LinearLayout.LayoutParams lpSpinner,lpsSpinner;
    private EditText etName, etCnic, etPhone;
    private LinearLayout linearLayoutPatientForm;
    private ArrayList<ClassScreenItem> ageArray = new ArrayList<ClassScreenItem>();
    private ArrayList<ClassScreenItem> patientVisitingArray = new ArrayList<ClassScreenItem>();
    private Spinner spinnerAgeGroup, spinnerPatientVisiting;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (parentView == null){
            parentView = inflater.inflate(R.layout.fragment_patient_interview_page1, container, false);
            /*mGlobals = Globals.getInstance();*/
            initData();
            generateBody();

            if (mGlobals.mJsonObjectFormData.has(Constants.DB_SCREEN_NAME_PATIENT_INTERVIEW_PAGE1)) {
                JSONObject jsonObject = null;
                try {
                    jsonObject = mGlobals.mJsonObjectFormData.getJSONObject(Constants.DB_SCREEN_NAME_PATIENT_INTERVIEW_PAGE1);
                    String stringValue;

                    stringValue = jsonObject.has("patient_availability") ? jsonObject.getString("patient_availability"):"";
                    if (stringValue.equals("yes")){
                        ((RadioButton)radioGroupAvailability.getChildAt(0)).setChecked(true);
                    }
                    else if (stringValue.equals("no")) {
                        ((RadioButton)radioGroupAvailability.getChildAt(1)).setChecked(true);
                    }

                    stringValue = jsonObject.has("age_group") ? jsonObject.getString("age_group"):"";
                    if (!stringValue.equals("")){
                        if (!stringValue.equals("")) {
                            //spinnerTransport.setSelection(transportArray.indexOf() /*Integer.parseInt(stringValue)*/);
                            int position = 0;
                            for(int k=0;k<ageArray.size();k++){
                                if(ageArray.get(k).item_id.equals(stringValue)){
                                    position=k;
                                    break;

                                }
                            }
                            spinnerAgeGroup.setSelection(position);
                        }
                    }



                    stringValue = jsonObject.has("patient_is_visiting ") ? jsonObject.getString("patient_is_visiting "):"";
                    if (!stringValue.equals("")){
                        if (!stringValue.equals("")) {
                            //spinnerTransport.setSelection(transportArray.indexOf() /*Integer.parseInt(stringValue)*/);
                            int position = 0;
                            for(int k=0;k<patientVisitingArray.size();k++){
                                if(patientVisitingArray.get(k).item_id.equals(stringValue)){
                                    position=k;
                                    break;

                                }
                            }
                            spinnerPatientVisiting.setSelection(position);
                        }
                    }
                    /*if (!stringValue.equals("")) {
                        for (int i = 0; i < radioGroupAge.getChildCount(); i++) {
                            if (radioGroupAge.getChildAt(i).getId() == Integer.parseInt(stringValue)) {
                                ((RadioButton) radioGroupAge.getChildAt(i)).setChecked(true);
                                break;
                            }
                        }
                    }*/



                    stringValue = jsonObject.has("gender") ? jsonObject.getString("gender"):"";
                    if (stringValue.equals("male")){
                        ((RadioButton)radioGroupGender.getChildAt(0)).setChecked(true);
                    }
                    else if (stringValue.equals("female")) {
                        ((RadioButton)radioGroupGender.getChildAt(1)).setChecked(true);
                    }



                    stringValue = jsonObject.has("name") ? jsonObject.getString("name"):"";
                    etName.setText(stringValue);
                    stringValue = jsonObject.has("cnic") ? jsonObject.getString("cnic"):"";
                    etCnic.setText(stringValue);
                    stringValue = jsonObject.has("phone_number") ? jsonObject.getString("phone_number"):"";
                    etPhone.setText(stringValue);



                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }


        }
        return parentView;
    }

    private void initData() {
        ClassScreenData objectScreenData = new ClassScreenData();
        objectScreenData.setDefaultValues();
        objectScreenData.type_id = "";
        objectScreenData.type_name = "";

        objectScreenData.screen_id = Constants.DB_SCREEN_ID_PATIENT_INTERVIEWS;
        objectScreenData.screen_name = Constants.DB_TYPE_NAME_PATIENT_INTERVIEWS;

        objectScreenData.section_id = "1";
        objectScreenData.section_name = "section1";
        objectScreenData.section_value = "age_group";

        objectScreenData = mGlobals.mDbAdapter.selectScreenData(objectScreenData);
        ageArray = mGlobals.mDbAdapter.selectScreenItems(objectScreenData);
        ageArray.add(0, new ClassScreenItem("0", "Select the relevant option"));

//updates
        objectScreenData.section_value = "patient_is_visiting";
        objectScreenData = mGlobals.mDbAdapter.selectScreenData(objectScreenData);
        patientVisitingArray = mGlobals.mDbAdapter.selectScreenItems(objectScreenData);
        patientVisitingArray.add(0, new ClassScreenItem("0", "Select the relevant option"));
    }

    private void generateBody() {

        //radioGroupReachFacility = (RadioGroup) parentView.findViewById(R.id.radio_group_reach_facility);
        radioGroupGender = (RadioGroup) parentView.findViewById(R.id.radio_group_gender);
        /*radioGroupAge = (RadioGroup) parentView.findViewById(R.id.radio_group_age);*/
        radioGroupAvailability = (RadioGroup) parentView.findViewById(R.id.radio_group_availability);

        spinnerAgeGroup = (Spinner) parentView.findViewById(R.id.spinner_age_group);
        spinnerPatientVisiting=(Spinner)parentView.findViewById(R.id.spinner_patient_visiting);

        lpSpinner = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, (int) (mGlobals.mScreenWidth * 0.1));
        lpSpinner.setMargins(0,10,0,0);
        spinnerAgeGroup.setLayoutParams(lpSpinner);

        lpsSpinner = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, (int) (mGlobals.mScreenWidth * 0.1));
        lpsSpinner.setMargins(0, 10, 0, 0);
        spinnerPatientVisiting.setLayoutParams(lpsSpinner);



        ArrayAdapterSpinnerSimpleScreenItem arrayAdapter =new ArrayAdapterSpinnerSimpleScreenItem(getActivity(), ageArray,Color.BLACK,-1);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerAgeGroup.setAdapter(arrayAdapter);

        //updates
        ArrayAdapterSpinnerSimpleScreenItem arrayAdapterPatientVisit =new ArrayAdapterSpinnerSimpleScreenItem(getActivity(), patientVisitingArray,Color.BLACK,-1);
        arrayAdapterPatientVisit.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerPatientVisiting.setAdapter(arrayAdapterPatientVisit);


        linearLayoutPatientForm = (LinearLayout) parentView.findViewById(R.id.layout_patient_form);


        radioGroupAvailability.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (radioGroup.getChildAt(0).getId()==i){
                    linearLayoutPatientForm.setVisibility(View.VISIBLE);
                }
                else if (radioGroup.getChildAt(1).getId()==i){
                    linearLayoutPatientForm.setVisibility(View.GONE);
                }
            }
        });


        //spinnerTransport = (Spinner) parentView.findViewById(R.id.spinner_transport);



        /*for (int i = 0; i <timeReachArray.size() ; i++) {
            RadioButton button = new RadioButton(getActivity());
            button.setText(timeReachArray.get(i).item_name);
            button.setId(Integer.parseInt(timeReachArray.get(i).item_id));
            // button.setTag(TrafficWardanApplication.syncDataObject.getTrafficStatus().get(i).getTrafficStatusIsHaveReason());
            radioGroupReachFacility.addView(button);
        }*/

        //spinnerTransport.setAdapter(new ArrayAdapterSpinnerItem(getActivity(), android.R.layout.simple_spinner_dropdown_item, transportArray));


       /* for (int i = 0; i <ageArray.size() ; i++) {
            RadioButton button = new RadioButton(getActivity());
            button.setText(ageArray.get(i).item_name);
            button.setId(Integer.parseInt(ageArray.get(i).item_id));
            // button.setTag(TrafficWardanApplication.syncDataObject.getTrafficStatus().get(i).getTrafficStatusIsHaveReason());
            radioGroupAge.addView(button);
        }
*/
        //editTextReachFacility = (EditText) parentView.findViewById(R.id.et_reach_facility);
        etName = (EditText) parentView.findViewById(R.id.et_name);
        etCnic = (EditText) parentView.findViewById(R.id.et_cnic);
        etPhone = (EditText) parentView.findViewById(R.id.et_phone_number);
        buttonProceed = (Button) parentView.findViewById(R.id.button_patient_information);


        etCnic.addTextChangedListener(new TextWatcherCNIC(etCnic));

        LinearLayout.LayoutParams lpButtonProceed = new LinearLayout.LayoutParams((int) (mGlobals.mScreenWidth * 0.5),
                (int) (mGlobals.mScreenWidth * 0.1));
        lpButtonProceed.setMargins(0, (int) (mGlobals.mScreenHeight * 0.05), 0, 0);
        buttonProceed.setLayoutParams(lpButtonProceed);

        buttonProceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isFormValid()) {
                    if (linearLayoutPatientForm.getVisibility()==View.GONE){
                        mGlobals.activityCallBackPatientInterviews.submitData();
                    }
                    else {
                        mGlobals.activityCallBackPatientInterviews.showNextFragment();
                    }
                } else {
                    AlertDialogs.getInstance().showDialogOK(null, errorMessage, null, true);
                }
            }
        });




        //spinnerTransport.setLayoutParams(lpSpinner);



    }





    @Override
    public void onFragmentShown() {

    }

    @Override
    public boolean isFormValid() {
        boolean bIsValid = true;

        /*if (radioGroupReachFacility.getCheckedRadioButtonId()==-1){
            bIsValid = false;
            errorMessage = "Please select the option for the time to reach the facility.";
        }
        else*/
        if (linearLayoutPatientForm.getVisibility()==View.VISIBLE) {

            if (radioGroupGender.getCheckedRadioButtonId() == -1) {
                bIsValid = false;
                errorMessage = "Please select the gender.";
            }
            else if (spinnerAgeGroup.getSelectedItemPosition()==0){
                bIsValid = false;
                errorMessage = "Please select the age group.";
            }

            else if  (spinnerPatientVisiting.getSelectedItemPosition()==0){
                bIsValid = false;
                errorMessage = "Please select the visiting type.";
            }
        }
        if (radioGroupAvailability.getCheckedRadioButtonId()==-1){
            bIsValid = false;
            errorMessage = "Please choose yes or no for the patient availability .";
        }

        if (etCnic.length()>0 && etCnic.length()!=15){
            bIsValid = false;
            errorMessage = "Please enter a valid CNIC.";
        }
        if (etPhone.length()>0 && etPhone.length()!=11){
            bIsValid = false;
            errorMessage = "Please enter a valid Phone Number.";
        }

//updates


       /* else if (spinnerTransport.getSelectedItemPosition()==0){
            bIsValid = false;
            errorMessage = "Please select the mode of transport.";
        }*/

        /*else if (editTextReachFacility.getVisibility()== View.VISIBLE && editTextReachFacility.getText().toString().equals("")){
            bIsValid = false;
            errorMessage = "Please answer.";
        }*/

        return bIsValid;
    }

    @Override
    public String onFragmentChanged(int previousPosition) {
        errorMessage = null;
        isFormValid();
        return errorMessage;

    }

    @Override
    public void parseObject() {
        JSONObject jsonObject = new JSONObject();
        try {

            jsonObject.put("gender", "");
            jsonObject.put("name","");
            jsonObject.put("cnic", "");
            jsonObject.put("phone_number", "");
            jsonObject.put("age_group", "");


            if (radioGroupAvailability.getCheckedRadioButtonId()==radioGroupAvailability.getChildAt(0).getId()){
                jsonObject.put("patient_availability", "yes");
            }
            else if (radioGroupAvailability.getCheckedRadioButtonId()==radioGroupAvailability.getChildAt(1).getId()){
                jsonObject.put("patient_availability", "no");
            }

            if (linearLayoutPatientForm.getVisibility()==View.VISIBLE){
                if (radioGroupGender.getCheckedRadioButtonId()==radioGroupGender.getChildAt(0).getId()){
                    jsonObject.put("gender", "male");
                }
                else if (radioGroupGender.getCheckedRadioButtonId()==radioGroupGender.getChildAt(1).getId()){
                    jsonObject.put("gender", "female");

                }

                /*for (int i = 0 ; i < radioGroupAge.getChildCount(); i++){
                    if (radioGroupAge.getCheckedRadioButtonId()==radioGroupAge.getChildAt(i).getId()){
                        jsonObject.put("age_group", ((RadioButton)radioGroupAge.getChildAt(i)).getText().toString());
                        break;
                    }
                }*/

              /*  if (radioGroupAge.getChildCount()>0){
                    jsonObject.put("age_group", radioGroupAge.getCheckedRadioButtonId());
                }*/

                jsonObject.put("age_group", ((ClassScreenItem)spinnerAgeGroup.getSelectedItem()).item_id);
                //updates
                jsonObject.put("patient_is_visiting ", ((ClassScreenItem)spinnerPatientVisiting.getSelectedItem()).item_id);
                jsonObject.put("name",etName.getText().toString());
                jsonObject.put("cnic", etCnic.getText().toString());
                jsonObject.put("phone_number", etPhone.getText().toString());

            }
            else {

                JSONObject jsonObjectPage2 = new JSONObject();
                jsonObjectPage2.put("illness", "");
                jsonObjectPage2.put("instructions_to_visit", "");
                jsonObjectPage2.put("instructions_take_medicines", "");
                jsonObjectPage2.put("check_pulse", "");
                jsonObjectPage2.put("check_temperature", "");
                jsonObjectPage2.put("check_blood_pressure", "");
                jsonObjectPage2.put("satisfied_attention", "");
                jsonObjectPage2.put("recommend_facility", "");
                jsonObjectPage2.put("treatment", "");
                jsonObjectPage2.put("money", "");
                jsonObjectPage2.put("medical_problems", "");
                jsonObjectPage2.put("person_listen", "");
                jsonObjectPage2.put("time_spent", "");
                jsonObjectPage2.put("time_wait","");
                jsonObjectPage2.put("see_today", "");
                jsonObjectPage2.put("facility_dispensary", "");
                jsonObjectPage2.put("medicine_quantity", "");
                jsonObjectPage2.put("other_illness", "");

                mGlobals.mJsonObjectFormData.remove(Constants.DB_SCREEN_NAME_PATIENT_INTERVIEW_PAGE2);
                mGlobals.mJsonObjectFormData.put(Constants.DB_SCREEN_NAME_PATIENT_INTERVIEW_PAGE2, jsonObjectPage2);
            }



            mGlobals.mJsonObjectFormData.remove(Constants.DB_SCREEN_NAME_PATIENT_INTERVIEW_PAGE1);
            mGlobals.mJsonObjectFormData.put(Constants.DB_SCREEN_NAME_PATIENT_INTERVIEW_PAGE1, jsonObject);

        }
        catch (Exception e){
            e.printStackTrace();
        }



    }
}