package pk.gov.pitb.mea.msh.healthcouncil;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import org.json.JSONException;
import org.json.JSONObject;

import pk.gov.pitb.mea.msh.R;
import pk.gov.pitb.mea.msh.handlers.HandlerFragmentCallBackHealthCouncil;
import pk.gov.pitb.mea.msh.helpers.AlertDialogs;
import pk.gov.pitb.mea.msh.helpers.Constants;
import pk.gov.pitb.mea.msh.helpers.MainContainer;


/**
 * Created by murtaza on 10/27/2016.
 */
public class HealthCouncilPage5 extends Fragment implements HandlerFragmentCallBackHealthCouncil {
    private View parentView;
    /*private Globals*/ /*mGlobals*/;
    private EditText etComments, etPhoneNumber;
    private Button buttonProceed;
    private String errorMessage = "Please fill all fields";
    private Button buttonPrevious, buttonNext, buttonSubmit;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (parentView==null){
            parentView = inflater.inflate(R.layout.health_council_page5,container, false);
            /*MainContainer = Globals.getInstance();*/
            initFragmentData();
            generateBody();
            if (MainContainer.mJsonObjectFormData.has(Constants.DB_SCREEN_NAME_HEALTH_COUNCIL_PAGE5)) {
                JSONObject jsonObject = null;
                try {
                    jsonObject = MainContainer.mJsonObjectFormData.getJSONObject(Constants.DB_SCREEN_NAME_HEALTH_COUNCIL_PAGE5);
                    etComments.setText(jsonObject.getString("comments"));
                    etPhoneNumber.setText(jsonObject.getString("phone_number"));

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }
        return parentView;
    }


    private void initFragmentData() {
    }


    private void generateBody() {
        etComments = (EditText) parentView.findViewById(R.id.edittext_comments_health_council);
        etPhoneNumber = (EditText) parentView.findViewById(R.id.et_health_council_page5_number);

        buttonNext = (Button) parentView.findViewById(R.id.button_next);
        buttonNext.setVisibility(View.GONE);
        buttonSubmit = (Button) parentView.findViewById(R.id.button_submit);
        buttonSubmit.setVisibility(View.VISIBLE);
        buttonPrevious = (Button) parentView.findViewById(R.id.button_previous);


        LinearLayout.LayoutParams lpEditTextSmall = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                (int) (MainContainer.mScreenWidth * 0.1));
        lpEditTextSmall.setMargins(0, 20, 0, 0);
        etPhoneNumber.setLayoutParams(lpEditTextSmall);

        LinearLayout.LayoutParams paramsEditText = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                (int) (MainContainer.mScreenHeight * 0.35));
        paramsEditText.setMargins(0, 0, 0, (int) (MainContainer.mScreenHeight * 0.02));

        etComments.setLayoutParams(paramsEditText);


        buttonProceed = (Button) parentView.findViewById(R.id.btn_proceed_health_council_page5);
        LinearLayout.LayoutParams lpButtonProceed = new LinearLayout.LayoutParams((int) (MainContainer.mScreenWidth * 0.5),
                (int) (MainContainer.mScreenWidth * 0.1));
        lpButtonProceed.setMargins(0, (int) (MainContainer.mScreenHeight * 0.05), 0, 0);
        buttonProceed.setLayoutParams(lpButtonProceed);

        buttonProceed.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                if (isFormValid()) {
                    MainContainer.activityCallBackHealthCouncil.submitData();
                } else {
                    AlertDialogs.getInstance().showDialogOK(null, errorMessage, null, true);
                }
            }
        });

        initClickListeners();
    }


    private void initClickListeners() {
        buttonPrevious.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (MainContainer.mJsonObjectFormData.has(Constants.DB_SCREEN_NAME_HEALTH_COUNCIL_PAGE2)
                        && MainContainer.mJsonObjectFormData.has(Constants.DB_SCREEN_NAME_HEALTH_COUNCIL_PAGE3)
                        && MainContainer.mJsonObjectFormData.has(Constants.DB_SCREEN_NAME_HEALTH_COUNCIL_PAGE4))
                    MainContainer.activityCallBackHealthCouncil.showPreviousFragment(1);
                else if (MainContainer.mJsonObjectFormData.has(Constants.DB_SCREEN_NAME_HEALTH_COUNCIL_PAGE2)
                        && MainContainer.mJsonObjectFormData.has(Constants.DB_SCREEN_NAME_HEALTH_COUNCIL_PAGE3)
                        && !MainContainer.mJsonObjectFormData.has(Constants.DB_SCREEN_NAME_HEALTH_COUNCIL_PAGE4))
                    MainContainer.activityCallBackHealthCouncil.showPreviousFragment(2);
                else
                    MainContainer.activityCallBackHealthCouncil.showPreviousFragment(4);
            }
        });

        buttonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isFormValid()) {
                    MainContainer.activityCallBackHealthCouncil.submitData();
                } else {
                    AlertDialogs.getInstance().showDialogOK(null, errorMessage, null, true);
                }
            }
        });

        buttonSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isFormValid()) {
                    MainContainer.activityCallBackHealthCouncil.submitData();
                } else {
                    AlertDialogs.getInstance().showDialogOK(null, errorMessage, null, true);
                }
            }
        });
    }



    @Override
    public void onFragmentShown() {

    }

    @Override
    public boolean isFormValid() {
        boolean isValid = true;
        if (etComments.getText().toString().equals("")){
            errorMessage = "Please write comments ";
            return false;
        }

        if(etPhoneNumber.getVisibility() != View.GONE)
            if (etPhoneNumber.getText().toString().equals("") || etPhoneNumber.getText().length()!=11){
                errorMessage = "Please enter complete phone number";
                return false;
            }

        return isValid;
    }

    @Override
    public String onFragmentChanged(int previousPosition) {
        return null;
    }

    @Override
    public void parseObject() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("comments", etComments.getText().toString());

            if(etPhoneNumber.getVisibility() == View.GONE)
                etPhoneNumber.setText("");

            jsonObject.put("phone_number", etPhoneNumber.getText().toString());

            MainContainer.mJsonObjectFormData.remove(Constants.DB_SCREEN_NAME_HEALTH_COUNCIL_PAGE5);
            MainContainer.mJsonObjectFormData.put(Constants.DB_SCREEN_NAME_HEALTH_COUNCIL_PAGE5, jsonObject);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
