package pk.gov.pitb.mea.msh.handlers;


public abstract class HandlerDataSync {
	public abstract void onSyncSuccessful(boolean bIsDataFound, String message);
	public abstract void onSyncError(HandlerAction handlerAction);
	public abstract void onNoInternet(boolean bIsConnected);
}
