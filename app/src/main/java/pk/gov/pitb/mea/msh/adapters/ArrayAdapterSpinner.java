package pk.gov.pitb.mea.msh.adapters;

import java.util.ArrayList;

import pk.gov.pitb.mea.msh.R;
import pk.gov.pitb.mea.msh.helpers.MainContainer;
import android.content.Context;
import android.text.InputType;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class ArrayAdapterSpinner extends ArrayAdapter<String> {

	private int colorBackground = 0;
	private int colorText = 0;

	public ArrayAdapterSpinner(Context context, int resource, String[] objects, boolean bIsOdd) {
		super(context, resource, objects);
		this.colorBackground = bIsOdd ? MainContainer.mContext.getResources().getColor(R.color.row_body_background_odd) : MainContainer.mContext.getResources().getColor(
				R.color.row_body_background_even);
		this.colorText = MainContainer.mContext.getResources().getColor(R.color.row_body_text);
	}

	public ArrayAdapterSpinner(Context context, int resource, ArrayList<String> objects, boolean bIsOdd) {
		super(context, resource, objects);
		this.colorBackground = bIsOdd ? MainContainer.mContext.getResources().getColor(R.color.row_body_background_odd) : MainContainer.mContext.getResources().getColor(
				R.color.row_body_background_even);
		this.colorText = MainContainer.mContext.getResources().getColor(R.color.row_body_text);
	}

	public void changeBackground(boolean bIsOdd) {
		this.colorBackground = bIsOdd ? MainContainer.mContext.getResources().getColor(R.color.row_body_background_odd) : MainContainer.mContext.getResources().getColor(
				R.color.row_body_background_even);
	}

	@Override
	public TextView getDropDownView(int position, View convertView, ViewGroup parent) {
		TextView textView = (TextView) super.getDropDownView(position, convertView, parent);
		if (textView != null) {
			textView.setBackgroundColor(colorBackground);
			textView.setTextColor(colorText);
		}
		return textView;
	}

	@Override
	public View getView(int position, View view, ViewGroup parent) {
		view = super.getView(position, view, parent);
		if (view != null) {
			TextView textView = (TextView) view;
			textView.setTextColor(colorText);
			textView.setInputType(InputType.TYPE_TEXT_FLAG_MULTI_LINE);
		}
		return view;
	}
}