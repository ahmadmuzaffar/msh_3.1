package pk.gov.pitb.mea.msh.bo;

import pk.gov.pitb.mea.msh.models.ClassScreenItem;

public class ClassScreenItemMultiSelect {

	public ClassScreenItem item;
	public boolean bIsSelected;

	public ClassScreenItemMultiSelect() {
		reset();
	}

	public ClassScreenItemMultiSelect(ClassScreenItem item, boolean bIsSelected) {
		this.item = item;
		this.bIsSelected = bIsSelected;
	}

	public void reset() {
		item = new ClassScreenItem();
		bIsSelected = false;
	}

	@Override
	public String toString() {
		return item.toString() + ", Selected: " + bIsSelected;
	}
}