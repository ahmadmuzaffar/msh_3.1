package pk.gov.pitb.mea.msh.healthcouncil;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.StateListDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.InputType;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Calendar;

import pk.gov.pitb.mea.msh.R;
import pk.gov.pitb.mea.msh.handlers.HandlerFragmentCallBackHealthCouncil;
import pk.gov.pitb.mea.msh.helpers.AlertDialogs;
import pk.gov.pitb.mea.msh.helpers.Constants;
import pk.gov.pitb.mea.msh.helpers.MainContainer;
import pk.gov.pitb.mea.msh.helpers.Utilities;
import pk.gov.pitb.mea.msh.models.ClassPicture;
import pk.gov.pitb.mea.msh.views.CustomEditText;


/**
 * Created by murtaza on 10/27/2016.
 */
public class HealthCouncilPage3 extends Fragment implements HandlerFragmentCallBackHealthCouncil {

    private View parentView;
    /*private Globals MainContainer;*/
    EditText et_fundsLastMonth;
    private Button buttonProceed;
    private TableLayout tableShow;
    private Button buttonPrevious, buttonNext, buttonSubmit;

    private ClassPicture picture_1 = null;

    private ClassPicture PICTURE_GENRAL = null;
    private Bitmap decodedByte1 = null;
    private String errorMessage = "Please fill all fields";
    private String tableData[] = {"Opening Balance (Rs.)", "Funds Utilized in Last Month (Rs.)", "Balance Amount of Last Month (Rs.)",
            "Funds available in account on 1st of current month (Rs.)"};
    private RadioButton radioButtonAvailable;
    private RadioButton radioButtonUnavailable;
    private String picturePath;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (parentView == null) {
            parentView = inflater.inflate(R.layout.health_council_page3, container, false);
            /*MainContainer = Globals.getInstance();*/
            initFragmentData();
            generateBody();
            if (MainContainer.mJsonObjectFormData.has(Constants.DB_SCREEN_NAME_HEALTH_COUNCIL_PAGE3)) {
                JSONObject jsonObject = null;
                try {
                    jsonObject = MainContainer.mJsonObjectFormData.getJSONObject(Constants.DB_SCREEN_NAME_HEALTH_COUNCIL_PAGE3);

                    String pictureString = jsonObject.has("bank_statement_picture") ? jsonObject.getString("bank_statement_picture") : "";
                    if (!pictureString.equals("N/A")) {
                        loadPicture(pictureString);
                    } else if (pictureString.equals("N/A")) {
                        ((RadioButton) ((RadioGroup) tableShow.getChildAt(1)).getChildAt(1)).setChecked(true);
                    }
                    et_fundsLastMonth.setText(jsonObject.getString("funds_utilized_last_month"));

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }
        return parentView;
    }

    private void initFragmentData() {
    }

    private void generateBody() {


        buttonNext = (Button) parentView.findViewById(R.id.button_next);
        buttonSubmit = (Button) parentView.findViewById(R.id.button_submit);
        buttonPrevious = (Button) parentView.findViewById(R.id.button_previous);
        et_fundsLastMonth = (EditText) parentView.findViewById(R.id.fundsLastMonth);

        int[] margins = new int[]{(int) (MainContainer.mScreenWidth * 0.05), (int) (MainContainer.mScreenWidth * 0.02), (int) (MainContainer.mScreenWidth * 0.05), 0};

        tableShow = (TableLayout) parentView.findViewById(R.id.table_layout);

        picture_1 = new ClassPicture(R.drawable.image_take_picture_enable, R.drawable.image_take_picture_disable, (int) (MainContainer.mScreenWidth * 0.44),
                (int) (MainContainer.mScreenWidth * 0.44), "Picture 1", "picture_1", margins);
        addPicture(picture_1, null, "Bank Statement picture?");

        buttonProceed = (Button) parentView.findViewById(R.id.btn_proceed_health_council_page3);
        LinearLayout.LayoutParams lpButtonProceed = new LinearLayout.LayoutParams((int) (MainContainer.mScreenWidth * 0.5),
                (int) (MainContainer.mScreenWidth * 0.1));
        lpButtonProceed.setMargins(0, (int) (MainContainer.mScreenHeight * 0.05), 0, 0);
        buttonProceed.setLayoutParams(lpButtonProceed);

        buttonProceed.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                if (isFormValid()) {
                    MainContainer.activityCallBackHealthCouncil.showNextFragment();
                } else {
                    AlertDialogs.getInstance().showDialogOK(null, errorMessage, null, true);
                }
            }
        });

        initClickListeners();
    }

    private void initClickListeners() {
        buttonPrevious.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainContainer.activityCallBackHealthCouncil.showPreviousFragment(1);
            }
        });

        buttonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isFormValid()) {
                    MainContainer.activityCallBackHealthCouncil.showNextFragment();
                } else {
                    AlertDialogs.getInstance().showDialogOK(null, errorMessage, null, true);
                }
            }
        });

        buttonSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isFormValid()) {
                    MainContainer.activityCallBackNutrients.submitData();
                } else {
                    AlertDialogs.getInstance().showDialogOK(null, errorMessage, null, true);
                }
            }
        });
    }

    private void addPicture(ClassPicture object_1, ClassPicture object_2, String textNotification) {
        RelativeLayout relativeLayoutImageButtons = new RelativeLayout(MainContainer.mContext);
        relativeLayoutImageButtons.setPadding(object_1.margins[0], object_1.margins[1], object_1.margins[2], object_1.margins[3]);
        addImageButton(relativeLayoutImageButtons, object_1, true);
        if (object_2 != null) {
            object_2.editTextPartner = object_1.editText;
            addImageButton(relativeLayoutImageButtons, object_2, false);
            object_1.editTextPartner = object_2.editText;
        }
        RadioGroupPicture(relativeLayoutImageButtons, textNotification);
        tableShow.addView(relativeLayoutImageButtons);
    }

    private void RadioGroupPicture(final RelativeLayout relativeLayoutImageButtons, String text) {
        TextView textView = new TextView(MainContainer.mContext);
        textView.setText(text);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.setMargins(0, 15, 0, 0);
        textView.setLayoutParams(params);
        RadioGroup radioGroup = new RadioGroup(MainContainer.mContext);
        radioButtonAvailable = new RadioButton(MainContainer.mContext);
        radioButtonAvailable.setText("Available");
        //radioButtonAvailable.setChecked(true);

        radioButtonUnavailable = new RadioButton(MainContainer.mContext);
        radioButtonUnavailable.setText("Unavailable");

        radioGroup.setOrientation(LinearLayout.HORIZONTAL);
        radioGroup.addView(radioButtonAvailable);
        radioGroup.addView(radioButtonUnavailable);

        radioGroup.check(radioGroup.getChildAt(0).getId());
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (radioGroup.getChildAt(0).getId() == i) {
                    relativeLayoutImageButtons.setVisibility(View.VISIBLE);
                } else if (radioGroup.getChildAt(1).getId() == i) {
                    relativeLayoutImageButtons.setVisibility(View.GONE);
                }
            }
        });

        tableShow.addView(textView);
        tableShow.addView(radioGroup);
    }

    private static int ID = 1;

    private void addImageButton(RelativeLayout layout, ClassPicture object, boolean left) {
        object.editText = new EditText(MainContainer.mContext);
        TextView textView = new TextView(MainContainer.mContext);
        textView.setText("Bank Statement");
        textView.setSingleLine(true);
        textView.setId(ID++);
        RelativeLayout.LayoutParams paramsTextView = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        paramsTextView.addRule(RelativeLayout.ALIGN_PARENT_TOP);
        if (left)
            paramsTextView.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
        else {
            paramsTextView.setMargins((int) (MainContainer.mScreenWidth * 0.02), 0, 0, 0);
            paramsTextView.addRule(RelativeLayout.RIGHT_OF, object.editTextPartner.getId());
        }
        textView.setLayoutParams(paramsTextView);
        object.imageButton = new ImageButton(MainContainer.mContext);
        RelativeLayout.LayoutParams paramsImageView = new RelativeLayout.LayoutParams(object.width, object.height);
        paramsImageView.setMargins(0, object.margins[1], 0, 0);
        if (left)
            paramsImageView.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
        else
            paramsImageView.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
        paramsImageView.addRule(RelativeLayout.BELOW, textView.getId());

        object.imageButton.setLayoutParams(paramsImageView);
        StateListDrawable statesImageButton = new StateListDrawable();
        statesImageButton.addState(new int[]{android.R.attr.state_pressed}, getResources().getDrawable(object.buttonDisable));
        statesImageButton.addState(new int[]{}, getResources().getDrawable(object.buttonEnable));
        object.imageButton.setImageDrawable(statesImageButton);
        object.imageButton.setScaleType(ImageView.ScaleType.FIT_XY);

        setupImageButton(object);
        //layout.addView(object.editText);
        layout.addView(textView);
        layout.addView(object.imageButton);
    }

    void setupImageButton(ClassPicture object) {
        if (object.byteArrayPicture != null) {
            Bitmap bitmap = Utilities.getImage(object.byteArrayPicture);
            object.imageButton.setImageBitmap(bitmap);
            object.imageButton.setScaleType(ImageView.ScaleType.FIT_XY);
        } else {
            StateListDrawable statesImageButton = new StateListDrawable();
            statesImageButton.addState(new int[]{android.R.attr.state_pressed}, getResources().getDrawable(object.buttonDisable));
            statesImageButton.addState(new int[]{}, getResources().getDrawable(object.buttonEnable));
            object.imageButton.setImageDrawable(statesImageButton);
            object.imageButton.setScaleType(ImageView.ScaleType.FIT_XY);
        }
        object.imageButton.setTag(object);
        object.imageButton.setPadding(0, 0, 0, 0);
        object.imageButton.setBackgroundColor(Color.TRANSPARENT);
        object.imageButton.setScaleType(ImageView.ScaleType.FIT_XY);
        object.imageButton.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                PICTURE_GENRAL = (ClassPicture) v.getTag();
                if (PICTURE_GENRAL.byteArrayPicture != null) {
                    String text = PICTURE_GENRAL.editText.getText().toString();
                    previewPicture(Utilities.getImage(PICTURE_GENRAL.byteArrayPicture), text);
                } else {
                    // selectOptionForPicture();
                    Intent camera = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(camera, CODE_PICTURE_NEW);
                }
            }

        });
        object.imageButton.setOnLongClickListener(new View.OnLongClickListener() {

            public boolean onLongClick(View v) {
                PICTURE_GENRAL = (ClassPicture) v.getTag();
                if (PICTURE_GENRAL.byteArrayPicture != null) {
                    new AlertDialog.Builder(v.getContext()).setTitle("Delete Picture").setMessage("Are you sure you want to delete this Picture?")
                            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {

                                public void onClick(DialogInterface dialog, int which) {
                                    PICTURE_GENRAL.byteArrayPicture = null;
                                    StateListDrawable CameraButtonStates = new StateListDrawable();
                                    CameraButtonStates.addState(new int[]{android.R.attr.state_pressed}, getResources().getDrawable(PICTURE_GENRAL.buttonDisable));
                                    CameraButtonStates.addState(new int[]{}, getResources().getDrawable(PICTURE_GENRAL.buttonEnable));
                                    PICTURE_GENRAL.imageButton.setImageDrawable(CameraButtonStates);
                                    PICTURE_GENRAL.imageButton.setScaleType(ImageView.ScaleType.FIT_XY);
                                }
                            }).setNegativeButton("No", new DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface dialog, int which) {
                        }
                    }).show();
                }
                return false;
            }
        });
    }

    private void previewPicture(Bitmap photo, String title) {
        final Dialog loginDialog = new Dialog(MainContainer.mContext);
        loginDialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_BLUR_BEHIND, WindowManager.LayoutParams.FLAG_BLUR_BEHIND);
        loginDialog.setTitle(title);
        LinearLayout linearLayout = new LinearLayout(MainContainer.mContext);
        ImageView previewPicture = new ImageView(MainContainer.mContext);
        linearLayout.setGravity(Gravity.CENTER);
        linearLayout.setPadding((int) (MainContainer.mScreenWidth * 0.02), (int) (MainContainer.mScreenWidth * 0.02), (int) (MainContainer.mScreenWidth * 0.02),
                (int) (MainContainer.mScreenWidth * 0.02));
        linearLayout.addView(previewPicture);
        loginDialog.setContentView(linearLayout);
        loginDialog.show();
        previewPicture.setImageBitmap(photo);
        previewPicture.setScaleType(ImageView.ScaleType.FIT_XY);
    }

    private final int CODE_PICTURE_GALLERY = 1;
    private final int CODE_PICTURE_NEW = 2;

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            picturePath = "";
            Uri selectedImage;
            byte[] byteArrayTempPicture = null;
            Bitmap bitmapTemp = null;
            if (resultCode == Activity.RESULT_OK) {
                if (requestCode == CODE_PICTURE_GALLERY || requestCode == CODE_PICTURE_NEW) {
                    if (requestCode == CODE_PICTURE_GALLERY) {
                        selectedImage = data.getData();
                        String[] filePathColumn = {MediaStore.Images.Media.DATA};
                        Cursor cursor = MainContainer.mContext.getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                        cursor.moveToFirst();

                        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                        picturePath = cursor.getString(columnIndex);
                        cursor.close();
                        BitmapFactory.Options options = new BitmapFactory.Options();
                        options.inSampleSize = 2;
                        Bitmap PictureTaken = null;
                        PictureTaken = BitmapFactory.decodeFile(picturePath, options);
                        Bitmap resizedBitmap = Bitmap.createScaledBitmap(PictureTaken, PictureTaken.getWidth() / 2, PictureTaken.getHeight() / 2, false);
                        byteArrayTempPicture = Utilities.getBytes(resizedBitmap, picturePath);
                        bitmapTemp = Utilities.getImage(byteArrayTempPicture);
                    } else if (requestCode == CODE_PICTURE_NEW) {
                        try {
                            Bitmap bitmap = (Bitmap) data.getExtras().get("data");
                            Bitmap resizedBitmap = Bitmap.createScaledBitmap(bitmap, bitmap.getWidth() * 2, bitmap.getHeight() * 2, true);

                            File photo = null;
                            FileOutputStream stream;

                            String fileName = java.text.DateFormat.getDateTimeInstance().format(Calendar.getInstance().getTime());
                            fileName = fileName.replaceAll(" ", "");
                            fileName = fileName.replaceAll(":", "");
                            fileName = fileName.replaceAll(",", "");
                            fileName = fileName + ".jpg";
                            photo = new File(android.os.Environment.getExternalStorageDirectory(), fileName);
                            stream = new FileOutputStream(photo);
                            //TODO compress new picture
                            resizedBitmap.compress(Bitmap.CompressFormat.JPEG, 80, stream);

                            selectedImage = Uri.fromFile(photo);
                            picturePath = selectedImage.getPath();

                            bitmapTemp = (Bitmap) data.getExtras().get("data");
                            BitmapFactory.Options options = new BitmapFactory.Options();
                            options.inSampleSize = 2;
                            byteArrayTempPicture = Utilities.getBytes(resizedBitmap, picturePath);
                            bitmapTemp = Utilities.getImage(byteArrayTempPicture);
                        } catch (Exception ex) {
                            Log.e("Picture", ex.toString());
                        }
                    }
                    PICTURE_GENRAL.imageButton.setImageBitmap(bitmapTemp);
                    PICTURE_GENRAL.imageButton.setScaleType(ImageView.ScaleType.FIT_XY);
                    PICTURE_GENRAL.byteArrayPicture = byteArrayTempPicture;
                    PICTURE_GENRAL.editText.setVisibility(View.VISIBLE);
                    //TODO null pointer here
                    if (PICTURE_GENRAL.editTextPartner.getVisibility() == View.GONE)
                        PICTURE_GENRAL.editTextPartner.setVisibility(View.INVISIBLE);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    public String convertBitmapToBase64(byte[] bs) {
        return bs == null ? "" : Base64.encodeToString(bs, Base64.DEFAULT);
    }


    public void loadPicture(String pictureString) {
        AsyncTaskLoadPictures asyncTaskLoadPictures = new AsyncTaskLoadPictures(pictureString);
        try {
            asyncTaskLoadPictures.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public class AsyncTaskLoadPictures extends AsyncTask<Void, Void, Void> {

        /*private Globals MainContainer;*/
        private ProgressDialog uProgressDialog;
        private String pictureString;

        public AsyncTaskLoadPictures(String pictureString) {
            super();
            /*this.MainContainer = Globals.getInstance();*/
            this.pictureString = pictureString;
        }

        public void execute() throws Exception {
            execute((Void) null);
        }

        @Override
        protected void onPreExecute() {
            try {
                uProgressDialog = new ProgressDialog(MainContainer.mContext);
                uProgressDialog.setCancelable(false);
                uProgressDialog.setTitle("Loading pictures");
                uProgressDialog.setMessage("Please Wait...");
                uProgressDialog.setIndeterminate(true);
                uProgressDialog.show();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                loadImages(pictureString);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {

            MainContainer.mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    //stuff that updates ui
                    ImageButton imageButton1 = ((ImageButton) ((RelativeLayout) tableShow.getChildAt(2)).getChildAt(1));

                    if (decodedByte1 != null) {
                        picture_1.byteArrayPicture = Utilities.getBytes(decodedByte1);
                        picture_1.imageButton.setImageBitmap(decodedByte1);
                        imageButton1.setImageBitmap(decodedByte1);
                    }

                }
            });

            if (uProgressDialog != null && uProgressDialog.isShowing()) {
                uProgressDialog.dismiss();
            }
        }

    }

    private void loadImages(String pictureString) {
        byte[] decodedString = Base64.decode(pictureString, Base64.DEFAULT);
        decodedByte1 = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);

    }

    @Override
    public void onFragmentShown() {

    }

    @Override
    public boolean isFormValid() {
        boolean isValid = true;
        if(radioButtonAvailable.isChecked() && picturePath.equals("")){
            errorMessage = "Please Select a picture";
            return false;
        }
        if (et_fundsLastMonth.length() == 0) {
            errorMessage = "Please enter funds utilized last month";
            return false;
        }
        return isValid;
    }

    @Override
    public String onFragmentChanged(int previousPosition) {
        return null;
    }

    @Override
    public void parseObject() {

        JSONObject jsonObject = new JSONObject();
        try {
            if (tableShow.getVisibility() == View.GONE || tableShow.getChildAt(2).getVisibility() == View.GONE) {
                jsonObject.put("bank_statement_picture", "N/A");
            } else {
                jsonObject.put("bank_statement_picture", convertBitmapToBase64(picture_1.byteArrayPicture));
            }

            jsonObject.put("funds_utilized_last_month", et_fundsLastMonth.getText().toString());

            jsonObject.put("jointed_account_opened", "");
            jsonObject.put("funds_transferred", "");
            jsonObject.put("funds_utilized", "");
            jsonObject.put("opening_balance", "");
            jsonObject.put("funds_utilized_last_quarter", "");
            jsonObject.put("balance_amount_last_quarter","");
            jsonObject.put("funds_avaliable_after_first_quarter","");

            MainContainer.mJsonObjectFormData.remove(Constants.DB_SCREEN_NAME_HEALTH_COUNCIL_PAGE3);
            MainContainer.mJsonObjectFormData.put(Constants.DB_SCREEN_NAME_HEALTH_COUNCIL_PAGE3, jsonObject);


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
