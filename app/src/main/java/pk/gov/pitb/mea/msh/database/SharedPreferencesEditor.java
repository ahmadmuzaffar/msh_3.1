package pk.gov.pitb.mea.msh.database;

import android.content.Context;
import android.content.SharedPreferences;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import pk.gov.pitb.mea.msh.helpers.Constants;
import pk.gov.pitb.mea.msh.helpers.MainContainer;

public class SharedPreferencesEditor {

	private final String FM_SYNC_DATE_TIME = "dd-MM-yyyy HH:mm:ss";

	private final String PREF_FILE_APP_DEFAULTS = "FileAppDefaults";
	private final String PREF_APP_PLAY_STORE_VERSION = "AppPlayStoreVersion";
	private final String PREF_APP_IS_CHECKING_UPDATE = "AppIsCheckingUpdate";
	private final String PREF_APP_LAST_CHECK_UPDATE_DATE = "AppLastCheckUpdateDate";

	protected String getStringValue(String fileName, String key, String defaultValue) {
		try {
			SharedPreferences sharedPreferences = mContext.getSharedPreferences(fileName, Context.MODE_PRIVATE);
			String value = sharedPreferences.getString(key, defaultValue);
			return value;
		} catch (Exception e) {
			e.printStackTrace();
			return defaultValue;
		}
	}

	protected boolean setStringValue(String fileName, String key, String value) {
		try {
			SharedPreferences.Editor editor = mContext.getSharedPreferences(fileName, Context.MODE_PRIVATE).edit();
			editor.putString(key, value);
			return editor.commit();
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	protected boolean clearFileValues(String fileName) {
		try {
			SharedPreferences.Editor editor = mContext.getSharedPreferences(fileName, Context.MODE_PRIVATE).edit();
			editor.clear();
			return editor.commit();
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	protected boolean clearAppDefaults() {
		try {
			return clearFileValues(PREF_FILE_APP_DEFAULTS);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public boolean resetOnReboot() {
		try {
			return setIsCheckingForUpdate(Constants.VALUE_FALSE);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public String getAppPlayStoreVersion() {
		try {
			return getStringValue(PREF_FILE_APP_DEFAULTS, PREF_APP_PLAY_STORE_VERSION, "");
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public boolean setAppPlayStoreVersion(String version) {
		return setStringValue(PREF_FILE_APP_DEFAULTS, PREF_APP_PLAY_STORE_VERSION, version);
	}

	public boolean isAppUpdateNeeded(String currentVersion) {
		try {
			String playStoreVersion = getStringValue(PREF_FILE_APP_DEFAULTS, PREF_APP_PLAY_STORE_VERSION, "");
			if (playStoreVersion.length() > 0) {
				double currentVersionDouble = Double.parseDouble(currentVersion.replace(Constants.APP_UPDATE_OPTIONAL, ""));
				double playStoreVersionDouble = Double.parseDouble(playStoreVersion.replace(Constants.APP_UPDATE_OPTIONAL, ""));
				if (playStoreVersionDouble > currentVersionDouble) {
					return true;
				}
			}
			return false;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public boolean isAppForceUpdateNeeded(String currentVersion) {
		try {
			if (isAppUpdateNeeded(currentVersion)) {
				String playStoreVersion = getStringValue(PREF_FILE_APP_DEFAULTS, PREF_APP_PLAY_STORE_VERSION, "");
				if (playStoreVersion.toLowerCase(Locale.ENGLISH).endsWith(Constants.APP_UPDATE_OPTIONAL)) {
					return false;
				}
				return true;
			}
			return false;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public boolean setIsCheckingForUpdate(String isSyncing) {
		try {
			return setStringValue(PREF_FILE_APP_DEFAULTS, PREF_APP_IS_CHECKING_UPDATE, isSyncing);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public boolean getIsCheckingForUpdate() {
		try {
			String isSyncing = getStringValue(PREF_FILE_APP_DEFAULTS, PREF_APP_IS_CHECKING_UPDATE, Constants.VALUE_FALSE);
			return isSyncing.equals(Constants.VALUE_FALSE) ? false : true;
		} catch (Exception e) {
			e.printStackTrace();
			return true;
		}
	}

	public boolean setLastCheckUpdateDate(String syncDate) {
		try {
			return setStringValue(PREF_FILE_APP_DEFAULTS, PREF_APP_LAST_CHECK_UPDATE_DATE, syncDate);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public String getLastCheckUpdateDate() {
		try {
			return getStringValue(PREF_FILE_APP_DEFAULTS, PREF_APP_LAST_CHECK_UPDATE_DATE, "");
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}

	public boolean isCheckUpdateNeeded() {
		try {
			Calendar calendarCurrent = Calendar.getInstance();
			Calendar calendarLastUpdate = (Calendar) calendarCurrent.clone();
			String lastSyncDateTime = getLastCheckUpdateDate();
			if (lastSyncDateTime.length() > 0) {
				SimpleDateFormat sdf = new SimpleDateFormat(Constants.FORMAT_DATE_APP, mLocale);
				calendarLastUpdate.setTime(sdf.parse(lastSyncDateTime));
				long diff = calendarCurrent.getTimeInMillis() - calendarLastUpdate.getTimeInMillis();
				if ((diff / (Constants.UPDATE_HOUR_INTERVAL * 60 * 60 * 1000)) > 1) {
					return true;
				} else {
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return true;
		}
	}

	/* SHARED PREFERENCES CONSTANTS */
	private final String PREF_FILE_SYNC = "SyncDetails";
	private final String PREF_IS_SYNCING = "IsSyncing";
	private final String PREF_DATA_OUTDATED = "IsDataOutdated";
	private final String PREF_UPDATE_CHECKED = "UpdateChecked";
	private final String PREF_PLAYSTORE_VERSION = "PlayStoreVersion";

	/* APPLICATION CONSTANTS */
	private final String PREF_FILE_APP = "ApplicationDetails";
	private final String PREF_STRUCTURAL_VERSION = "StructuralVersion";
	private final String PREF_FACILITY_VERSION = "FacilityVersion";
	private final String PREF_IS_PMO = "IsPMO";
	private final String PREF_LAST_CLEAR_DATE = "LastClearDate";

	private Context mContext = null;
	private Locale mLocale = null;

	public SharedPreferencesEditor(Context context) {
		this.mContext = context;
		this.mLocale = Locale.ENGLISH;
	}

	public boolean resetVersions() {
		try {
			return setStructuralVersion("0") && setFacilityVersion("0") && setLastClearDate("");
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public String getLastClearDate() {
		try {
			SharedPreferences sharedPreferences = mContext.getSharedPreferences(PREF_FILE_APP, Context.MODE_PRIVATE);
			String valueLastClearDate = sharedPreferences.getString(PREF_LAST_CLEAR_DATE, "");
			return valueLastClearDate;
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}

	public boolean setLastClearDate(String valueLastClearDate) {
		try {
			SharedPreferences.Editor editor = mContext.getSharedPreferences(PREF_FILE_APP, Context.MODE_PRIVATE).edit();
			editor.putString(PREF_LAST_CLEAR_DATE, valueLastClearDate);
			return editor.commit();
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public String getStructuralVersion() {
		try {
			SharedPreferences sharedPreferences = mContext.getSharedPreferences(PREF_FILE_APP, Context.MODE_PRIVATE);
			String valueStructuralVersion = sharedPreferences.getString(PREF_STRUCTURAL_VERSION, "0");
			return valueStructuralVersion;
		} catch (Exception e) {
			e.printStackTrace();
			return "0";
		}
	}

	public boolean setStructuralVersion(String valueStructuralVersion) {
		try {
			SharedPreferences.Editor editor = mContext.getSharedPreferences(PREF_FILE_APP, Context.MODE_PRIVATE).edit();
			editor.putString(PREF_STRUCTURAL_VERSION, valueStructuralVersion);
			return editor.commit();
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public String getFacilityVersion() {
		try {
			SharedPreferences sharedPreferences = mContext.getSharedPreferences(PREF_FILE_APP, Context.MODE_PRIVATE);
			String valueFacilityVersion = sharedPreferences.getString(PREF_FACILITY_VERSION, "0");
			return valueFacilityVersion;
		} catch (Exception e) {
			e.printStackTrace();
			return "0";
		}
	}
	public String getIsPMO() {
		try {
			SharedPreferences sharedPreferences = mContext.getSharedPreferences(PREF_FILE_APP, Context.MODE_PRIVATE);
			String valueIsPMO = sharedPreferences.getString(PREF_IS_PMO, "0");
			return valueIsPMO;
		} catch (Exception e) {
			e.printStackTrace();
			return "0";
		}
	}
	public boolean setFacilityVersion(String valueFacilityVersion) {
		try {
			SharedPreferences.Editor editor = mContext.getSharedPreferences(PREF_FILE_APP, Context.MODE_PRIVATE).edit();
			editor.putString(PREF_FACILITY_VERSION, valueFacilityVersion);
			return editor.commit();
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	public boolean setIsPMO(String valuePMO) {
		try {
			SharedPreferences.Editor editor = mContext.getSharedPreferences(PREF_FILE_APP, Context.MODE_PRIVATE).edit();
			editor.putString(PREF_IS_PMO, valuePMO);
			return editor.commit();
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	public boolean setIsDataOutdated(String isDataOutdated) {
		try {
			SharedPreferences.Editor editor = mContext.getSharedPreferences(PREF_FILE_SYNC, Context.MODE_PRIVATE).edit();
			editor.putString(PREF_DATA_OUTDATED, isDataOutdated);
			return editor.commit();
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public boolean setIsSyncing(String isSyncing) {
		try {
			SharedPreferences.Editor editor = mContext.getSharedPreferences(PREF_FILE_SYNC, Context.MODE_PRIVATE).edit();
			editor.putString(PREF_IS_SYNCING, isSyncing);
			return editor.commit();
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public boolean getIsSyncing() {
		try {
			SharedPreferences sharedPreferences = mContext.getSharedPreferences(PREF_FILE_SYNC, Context.MODE_PRIVATE);
			String isSyncing = sharedPreferences.getString(PREF_IS_SYNCING, "false");
			return isSyncing.equals("false") ? false : true;
		} catch (Exception e) {
			e.printStackTrace();
			return true;
		}
	}

	public boolean isUpdateNeeded() {
		try {
			SharedPreferences sharedPreferences = mContext.getSharedPreferences(PREF_FILE_SYNC, Context.MODE_PRIVATE);
			SimpleDateFormat sdf = new SimpleDateFormat(FM_SYNC_DATE_TIME, mLocale);
			String currentDateTime = sdf.format(Calendar.getInstance().getTime());
			Date currentDate = sdf.parse(currentDateTime);
			String lastSyncDateTime = sharedPreferences.getString(PREF_UPDATE_CHECKED, "");
			if (lastSyncDateTime.length() > 0) {
				Date lastSyncDate = sdf.parse(lastSyncDateTime);
				long diff = currentDate.getTime() - lastSyncDate.getTime();
				if ((diff / (12 * 60 * 60 * 1000)) > 1) {
					return true;
				}
			} else {
				return true;
			}
			return false;
		} catch (Exception e) {
			e.printStackTrace();
			return true;
		}
	}

	public boolean setLastUpdate() {
		try {
			SharedPreferences.Editor editor = mContext.getSharedPreferences(PREF_FILE_SYNC, Context.MODE_PRIVATE).edit();
			SimpleDateFormat sdf = new SimpleDateFormat(FM_SYNC_DATE_TIME, mLocale);
			editor.putString(PREF_UPDATE_CHECKED, sdf.format(Calendar.getInstance().getTime()));
			return editor.commit();
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public boolean setPlayStoreVersion(String playStoreVersion) {
		try {
			SharedPreferences.Editor editor = mContext.getSharedPreferences(PREF_FILE_SYNC, Context.MODE_PRIVATE).edit();
			editor.putString(PREF_PLAYSTORE_VERSION, playStoreVersion);
			return editor.commit();
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	private final String PREF_FILE_USER = "UserDetails";
	private final String PREF_MEA_NAME = "MEAName";
	private final String PREF_MEA_DISTRICT = "MEADistrict";

	public boolean updateUserInformation(String meaName, String meaDistrict) {
		try {
			SimpleDateFormat sdf = new SimpleDateFormat(FM_SYNC_DATE_TIME, mLocale);
			SharedPreferences.Editor editor = mContext.getSharedPreferences(PREF_FILE_USER, Context.MODE_PRIVATE).edit();
			editor.putString(PREF_UPDATE_CHECKED, sdf.format(Calendar.getInstance().getTime()));
			editor.putString(PREF_MEA_NAME, meaName);
			editor.putString(PREF_MEA_DISTRICT, meaDistrict);
			return editor.commit();
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	//**************************** Health Council **************************************

	private final String PREF_FILE_SAVED_DATA_HEALTH_COUNCIL = "SavedDataHealthCouncil";
	private final String PREF_LAST_FRAGMENT_POSITION_HEALTH_COUNCIL = "LastFragmentPositionHealthCouncil";
	private final String PREF_LAST_SAVED_JSON_HEALTH_COUNCIL = "LastSavedJSONHealthCouncil";
	private final String PREF_LAST_SAVED_FACILITY_HEALTH_COUNCIL = "LastSavedFacilityHealthCouncil";

	public boolean clearLastSavedDataHealthCouncil() {
		try {
			setLastFragmentPositionHealthCouncil("0");
			setLastSavedJSONHealthCouncil("");
			setLastSavedFacilityHealthCouncil("");
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public String getLastFragmentPositionHealthCouncil() {
		try {
			SharedPreferences sharedPreferences = mContext.getSharedPreferences(PREF_FILE_SAVED_DATA_HEALTH_COUNCIL, Context.MODE_PRIVATE);
			String valueTehsil = sharedPreferences.getString(PREF_LAST_FRAGMENT_POSITION_HEALTH_COUNCIL, "0");
			return valueTehsil;
		} catch (Exception e) {
			e.printStackTrace();
			return "0";
		}
	}

	public boolean setLastFragmentPositionHealthCouncil(String value) {
		try {
			SharedPreferences.Editor editor = mContext.getSharedPreferences(PREF_FILE_SAVED_DATA_HEALTH_COUNCIL, Context.MODE_PRIVATE).edit();
			editor.putString(PREF_LAST_FRAGMENT_POSITION_HEALTH_COUNCIL, value);
			return editor.commit();
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public String getLastSavedJSONHealthCouncil() {
		try {
			SharedPreferences sharedPreferences = mContext.getSharedPreferences(PREF_FILE_SAVED_DATA_HEALTH_COUNCIL, Context.MODE_PRIVATE);
			String valueTehsil = sharedPreferences.getString(PREF_LAST_SAVED_JSON_HEALTH_COUNCIL, "");
			return valueTehsil;
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}

	public boolean setLastSavedJSONHealthCouncil(String value) {
		try {
			SharedPreferences.Editor editor = mContext.getSharedPreferences(PREF_FILE_SAVED_DATA_HEALTH_COUNCIL, Context.MODE_PRIVATE).edit();
			editor.putString(PREF_LAST_SAVED_JSON_HEALTH_COUNCIL, value);
			return editor.commit();
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public String getLastSavedFacilityHealthCouncil() {
		try {
			SharedPreferences sharedPreferences = mContext.getSharedPreferences(PREF_FILE_SAVED_DATA_HEALTH_COUNCIL, Context.MODE_PRIVATE);
			String valueTehsil = sharedPreferences.getString(PREF_LAST_SAVED_FACILITY_HEALTH_COUNCIL, "");
			return valueTehsil;
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}

	public boolean setLastSavedFacilityHealthCouncil(String value) {
		try {
			SharedPreferences.Editor editor = mContext.getSharedPreferences(PREF_FILE_SAVED_DATA_HEALTH_COUNCIL, Context.MODE_PRIVATE).edit();
			editor.putString(PREF_LAST_SAVED_FACILITY_HEALTH_COUNCIL, value);
			return editor.commit();
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}



	//**************************** Nutrients **************************************

	private final String PREF_FILE_SAVED_DATA_NUTRIENTS = "SavedDataNutrients";
	private final String PREF_LAST_FRAGMENT_POSITION_NUTRIENTS = "LastFragmentPositionNutrients";
	private final String PREF_LAST_SAVED_JSON_NUTRIENTS = "LastSavedJSONNutrients";
	private final String PREF_LAST_SAVED_FACILITY_NUTRIENTS = "LastSavedFacilityNutrients";

	public boolean clearLastSavedDataNutrients() {
		try {
			setLastFragmentPositionNutrients("0");
			setLastSavedJSONNutrients("");
			setLastSavedFacilityNutrients("");
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public String getLastFragmentPositionNutrients() {
		try {
			SharedPreferences sharedPreferences = mContext.getSharedPreferences(PREF_FILE_SAVED_DATA_NUTRIENTS, Context.MODE_PRIVATE);
			String valueTehsil = sharedPreferences.getString(PREF_LAST_FRAGMENT_POSITION_NUTRIENTS, "0");
			return valueTehsil;
		} catch (Exception e) {
			e.printStackTrace();
			return "0";
		}
	}

	public boolean setLastFragmentPositionNutrients(String value) {
		try {
			SharedPreferences.Editor editor = mContext.getSharedPreferences(PREF_FILE_SAVED_DATA_NUTRIENTS, Context.MODE_PRIVATE).edit();
			editor.putString(PREF_LAST_FRAGMENT_POSITION_NUTRIENTS, value);
			return editor.commit();
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public String getLastSavedJSONNutrients() {
		try {
			SharedPreferences sharedPreferences = mContext.getSharedPreferences(PREF_FILE_SAVED_DATA_NUTRIENTS, Context.MODE_PRIVATE);
			String valueTehsil = sharedPreferences.getString(PREF_LAST_SAVED_JSON_NUTRIENTS, "");
			return valueTehsil;
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}

	public boolean setLastSavedJSONNutrients(String value) {
		try {
			SharedPreferences.Editor editor = mContext.getSharedPreferences(PREF_FILE_SAVED_DATA_NUTRIENTS, Context.MODE_PRIVATE).edit();
			editor.putString(PREF_LAST_SAVED_JSON_NUTRIENTS, value);
			return editor.commit();
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public String getLastSavedFacilityNutrients() {
		try {
			SharedPreferences sharedPreferences = mContext.getSharedPreferences(PREF_FILE_SAVED_DATA_NUTRIENTS, Context.MODE_PRIVATE);
			String valueTehsil = sharedPreferences.getString(PREF_LAST_SAVED_FACILITY_NUTRIENTS, "");
			return valueTehsil;
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}

	public boolean setLastSavedFacilityNutrients(String value) {
		try {
			SharedPreferences.Editor editor = mContext.getSharedPreferences(PREF_FILE_SAVED_DATA_NUTRIENTS, Context.MODE_PRIVATE).edit();
			editor.putString(PREF_LAST_SAVED_FACILITY_NUTRIENTS, value);
			return editor.commit();
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	//**************************** Hepatitis **************************************

	private final String PREF_FILE_SAVED_DATA_HEPATITIS = "SavedDataHepatitis";
	private final String PREF_LAST_FRAGMENT_POSITION_HEPATITIS = "LastFragmentPositionHepatitis";
	private final String PREF_LAST_SAVED_JSON_HEPATITIS = "LastSavedJSONHepatitis";
	private final String PREF_LAST_SAVED_FACILITY_HEPATITIS = "LastSavedFacilityHepatitis";

	public boolean clearLastSavedDataHepatitis() {
		try {
			setLastFragmentPositionHepatitis("0");
			setLastSavedJSONHepatitis("");
			setLastSavedFacilityHepatitis("");
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public String getLastFragmentPositionHepatitis() {
		try {
			SharedPreferences sharedPreferences = mContext.getSharedPreferences(PREF_FILE_SAVED_DATA_HEPATITIS, Context.MODE_PRIVATE);
			String valueTehsil = sharedPreferences.getString(PREF_LAST_FRAGMENT_POSITION_HEPATITIS, "0");
			return valueTehsil;
		} catch (Exception e) {
			e.printStackTrace();
			return "0";
		}
	}

	public boolean setLastFragmentPositionHepatitis(String value) {
		try {
			SharedPreferences.Editor editor = mContext.getSharedPreferences(PREF_FILE_SAVED_DATA_HEPATITIS, Context.MODE_PRIVATE).edit();
			editor.putString(PREF_LAST_FRAGMENT_POSITION_HEPATITIS, value);
			return editor.commit();
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public String getLastSavedJSONHepatitis() {
		try {
			SharedPreferences sharedPreferences = mContext.getSharedPreferences(PREF_FILE_SAVED_DATA_HEPATITIS, Context.MODE_PRIVATE);
			String valueTehsil = sharedPreferences.getString(PREF_LAST_SAVED_JSON_HEPATITIS, "");
			return valueTehsil;
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}

	public boolean setLastSavedJSONHepatitis(String value) {
		try {
			SharedPreferences.Editor editor = mContext.getSharedPreferences(PREF_FILE_SAVED_DATA_HEPATITIS, Context.MODE_PRIVATE).edit();
			editor.putString(PREF_LAST_SAVED_JSON_HEPATITIS, value);
			return editor.commit();
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public String getLastSavedFacilityHepatitis() {
		try {
			SharedPreferences sharedPreferences = mContext.getSharedPreferences(PREF_FILE_SAVED_DATA_HEPATITIS, Context.MODE_PRIVATE);
			String valueTehsil = sharedPreferences.getString(PREF_LAST_SAVED_FACILITY_HEPATITIS, "");
			return valueTehsil;
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}

	public boolean setLastSavedFacilityHepatitis(String value) {
		try {
			SharedPreferences.Editor editor = mContext.getSharedPreferences(PREF_FILE_SAVED_DATA_HEPATITIS, Context.MODE_PRIVATE).edit();
			editor.putString(PREF_LAST_SAVED_FACILITY_HEPATITIS, value);
			return editor.commit();
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

//**************************** Patient Interview **************************************

	private final String PREF_FILE_SAVED_DATA_PATIENT_INTERVIEWS = "SavedDataPatientInterviews";
	private final String PREF_LAST_FRAGMENT_POSITION_PATIENT_INTERVIEWS = "LastFragmentPositionPatientInterviews";
	private final String PREF_LAST_SAVED_JSON_PATIENT_INTERVIEWS = "LastSavedJSONPatientInterviews";
	private final String PREF_LAST_SAVED_FACILITY_PATIENT_INTERVIEWS = "LastSavedFacilityPatientInterviews";

	public boolean clearLastSavedDataPatientInterviews() {
		try {
			setLastFragmentPositionPatientInterviews("0");
			setLastSavedJSONPatientInterviews("");
			setLastSavedFacilityPatientInterviews("");
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	public String getLastFragmentPositionPatientInterviews() {
		try {
			SharedPreferences sharedPreferences = mContext.getSharedPreferences(PREF_FILE_SAVED_DATA_PATIENT_INTERVIEWS, Context.MODE_PRIVATE);
			String valueTehsil = sharedPreferences.getString(PREF_LAST_FRAGMENT_POSITION_PATIENT_INTERVIEWS, "0");
			return valueTehsil;
		} catch (Exception e) {
			e.printStackTrace();
			return "0";
		}
	}

	public boolean setLastFragmentPositionPatientInterviews(String value) {
		try {
			SharedPreferences.Editor editor = mContext.getSharedPreferences(PREF_FILE_SAVED_DATA_PATIENT_INTERVIEWS, Context.MODE_PRIVATE).edit();
			editor.putString(PREF_LAST_FRAGMENT_POSITION_PATIENT_INTERVIEWS, value);
			return editor.commit();
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public String getLastSavedJSONPatientInterviews() {
		try {
			SharedPreferences sharedPreferences = mContext.getSharedPreferences(PREF_FILE_SAVED_DATA_PATIENT_INTERVIEWS, Context.MODE_PRIVATE);
			String valueTehsil = sharedPreferences.getString(PREF_LAST_SAVED_JSON_PATIENT_INTERVIEWS, "");
			return valueTehsil;
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}

	public boolean setLastSavedJSONPatientInterviews(String value) {
		try {
			SharedPreferences.Editor editor = mContext.getSharedPreferences(PREF_FILE_SAVED_DATA_PATIENT_INTERVIEWS, Context.MODE_PRIVATE).edit();
			editor.putString(PREF_LAST_SAVED_JSON_PATIENT_INTERVIEWS, value);
			return editor.commit();
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public String getLastSavedFacilityPatientInterviews() {
		try {
			SharedPreferences sharedPreferences = mContext.getSharedPreferences(PREF_FILE_SAVED_DATA_PATIENT_INTERVIEWS, Context.MODE_PRIVATE);
			String valueTehsil = sharedPreferences.getString(PREF_LAST_SAVED_FACILITY_PATIENT_INTERVIEWS, "");
			return valueTehsil;
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}

	public boolean setLastSavedFacilityPatientInterviews(String value) {
		try {
			SharedPreferences.Editor editor = mContext.getSharedPreferences(PREF_FILE_SAVED_DATA_PATIENT_INTERVIEWS, Context.MODE_PRIVATE).edit();
			editor.putString(PREF_LAST_SAVED_FACILITY_PATIENT_INTERVIEWS, value);
			return editor.commit();
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	//**************************** Monitoring **************************************

	private final String PREF_FILE_SAVED_DATA_MONITORING = "SavedDataMonitoring";
	private final String PREF_LAST_FRAGMENT_POSITION_MONITORING = "LastFragmentPositionMonitoring";
	private final String PREF_LAST_SAVED_JSON_MONITORING = "LastSavedJSONMonitoring";
	private final String PREF_LAST_SAVED_FACILITY_MONITORING = "LastSavedFacilityMonitoring";

	public boolean clearLastSavedDataMonitoring() {
		try {
			setLastFragmentPositionMonitoring("0");
			setLastSavedJSONMonitoring("");
			setLastSavedFacilityMonitoring("");
			MainContainer.mDbAdapter.deleteSavedPicture();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public String getLastFragmentPositionMonitoring() {
		try {
			SharedPreferences sharedPreferences = mContext.getSharedPreferences(PREF_FILE_SAVED_DATA_MONITORING, Context.MODE_PRIVATE);
			String valueTehsil = sharedPreferences.getString(PREF_LAST_FRAGMENT_POSITION_MONITORING, "0");
			return valueTehsil;
		} catch (Exception e) {
			e.printStackTrace();
			return "0";
		}
	}

	public boolean setLastFragmentPositionMonitoring(String value) {
		try {
			SharedPreferences.Editor editor = mContext.getSharedPreferences(PREF_FILE_SAVED_DATA_MONITORING, Context.MODE_PRIVATE).edit();
			editor.putString(PREF_LAST_FRAGMENT_POSITION_MONITORING, value);
			return editor.commit();
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public String getLastSavedJSONMonitoring() {
		try {
			SharedPreferences sharedPreferences = mContext.getSharedPreferences(PREF_FILE_SAVED_DATA_MONITORING, Context.MODE_PRIVATE);
			String valueTehsil = sharedPreferences.getString(PREF_LAST_SAVED_JSON_MONITORING, "");
			return valueTehsil;
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}

	public boolean setLastSavedJSONMonitoring(String value) {
		try {
			SharedPreferences.Editor editor = mContext.getSharedPreferences(PREF_FILE_SAVED_DATA_MONITORING, Context.MODE_PRIVATE).edit();
			editor.putString(PREF_LAST_SAVED_JSON_MONITORING, value);
			return editor.commit();
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public String getLastSavedFacilityMonitoring() {
		try {
			SharedPreferences sharedPreferences = mContext.getSharedPreferences(PREF_FILE_SAVED_DATA_MONITORING, Context.MODE_PRIVATE);
			String valueTehsil = sharedPreferences.getString(PREF_LAST_SAVED_FACILITY_MONITORING, "");
			return valueTehsil;
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}

	public boolean setLastSavedFacilityMonitoring(String value) {
		try {
			SharedPreferences.Editor editor = mContext.getSharedPreferences(PREF_FILE_SAVED_DATA_MONITORING, Context.MODE_PRIVATE).edit();
			editor.putString(PREF_LAST_SAVED_FACILITY_MONITORING, value);
			return editor.commit();
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}




}