package pk.gov.pitb.mea.msh.models;

public class ClassSentData {

	public int pk_id;
	public String sending_date_time, facility_id;

	public ClassSentData() {
		setEmptyValues();
	}

	public void setEmptyValues() {
		pk_id = -1;
		sending_date_time = "";
		facility_id = "";
	}
}