package pk.gov.pitb.mea.msh.fragmentactivities;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.util.ArrayList;

import pk.gov.pitb.mea.msh.R;
import pk.gov.pitb.mea.msh.database.SharedPreferencesEditor;
import pk.gov.pitb.mea.msh.handlers.InterfaceFragmentCallBack;
import pk.gov.pitb.mea.msh.helpers.Constants;
import pk.gov.pitb.mea.msh.helpers.MainContainer;
import pk.gov.pitb.mea.msh.helpers.UIHelper;
import pk.gov.pitb.mea.msh.models.ClassScreenData;
import pk.gov.pitb.mea.msh.models.ClassScreenItem;
import pk.gov.pitb.mea.msh.models.ClassScreenItemMultiple;
import pk.gov.pitb.mea.msh.textwatchers.TextWatcherCountSingleChild;
import pk.gov.pitb.mea.msh.views.CustomEditText;

public class FragmentMedicines extends Fragment implements InterfaceFragmentCallBack {

    private ArrayList<ClassScreenItemMultiple> arrayListAllMedicines;
    private ArrayList<Integer> arrayListRandomIndeces;

    private int indexMedicine;
    private View parentView;
    private LinearLayout linearLayoutMain, linearLayoutStart, linearLayoutEnd;
    RadioGroup rg1;
    RadioButton rb1, rb2, rb3, rb4;
    EditText commentBox;
    private TextView[] textViewsMedicines;
    private TableLayout[] tableLayoutsMedicines;
    private SharedPreferencesEditor sharedPreferencesEditor;

    @Override
    public void parseObject() {
        try {
            JSONArray jsonArrayMedicines = new JSONArray();
            for (int j = 0; j < tableLayoutsMedicines.length; j++) {
                ClassScreenItemMultiple objectMultiple = arrayListAllMedicines.get(j);
                for (int i = 1; i < tableLayoutsMedicines[j].getChildCount(); i++) {
                    View child = tableLayoutsMedicines[j].getChildAt(i);
                    ClassScreenItem object = objectMultiple.arrayListFields.get(i - 1);
                    JSONObject jsonObjectChild = new JSONObject();

                    TableRow tableRow = (TableRow) child;
                    EditText consumption = (EditText) tableRow.getChildAt(1);
                    EditText balance = (EditText) tableRow.getChildAt(2);
                    EditText stock = (EditText) tableRow.getChildAt(3);


                    jsonObjectChild.put("medicine_id", object.item_id);
                    jsonObjectChild.put("medicines", object.item_name);
                    jsonObjectChild.put("consumption_during_previous_month", consumption.getText().toString().trim());
                    jsonObjectChild.put("balance_at_visit_time", balance.getText().toString().trim());
                    jsonObjectChild.put("physical_stock", stock.getText().toString().trim());

                    if (sharedPreferencesEditor.getIsPMO().equals(Constants.DB_TYPE_PMO)) {
                        EditText recording = (EditText) tableRow.getChildAt(4);
                        EditText expired = (EditText) tableRow.getChildAt(5);
                        jsonObjectChild.put("recording", recording.getText().toString().trim());
                        jsonObjectChild.put("expired_medicines", expired.getText().toString().trim());
                    } else {
                        EditText expired = (EditText) tableRow.getChildAt(4);
                        jsonObjectChild.put("expired_medicines", expired.getText().toString().trim());
                        jsonObjectChild.put("recording", "");
                    }


                    jsonArrayMedicines.put(jsonObjectChild);
                }
            }

            JSONObject jsonObject = new JSONObject();
            String focalPerson = "";
            String medicinesExpired = "";
            String comment = "";

            if (rb1.isChecked())
                focalPerson = "yes";
            else if (rb2.isChecked())
                focalPerson = "no";

            if (linearLayoutEnd.getVisibility() == View.VISIBLE) {

                if (rb3.isChecked()) {
                    medicinesExpired = "yes";

                    comment = commentBox.getText().toString();
                } else if (rb4.isChecked())
                    medicinesExpired = "no";

            }

            jsonObject.put("medicines", jsonArrayMedicines);
            jsonObject.put("focal_person", focalPerson);
            jsonObject.put("expired_medicines_names", medicinesExpired);
            jsonObject.put("comments", comment);

            MainContainer.mJsonObjectFormData.remove(Constants.DB_SCREEN_NAME_MEDICINES);
            MainContainer.mJsonObjectFormData.put(Constants.DB_SCREEN_NAME_MEDICINES, jsonObject);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void loadSavedData() {
        try {
            if (MainContainer.mJsonObjectFormData.has(Constants.DB_SCREEN_NAME_MEDICINES)) {
                JSONObject jsonObject = MainContainer.mJsonObjectFormData.getJSONObject(Constants.DB_SCREEN_NAME_MEDICINES);
                JSONArray jsonArray = jsonObject.getJSONArray("medicines");
                String focalPerson = jsonObject.getString("focal_person");
                String medicinesExpired = jsonObject.getString("expired_medicines_names");
                String comment = jsonObject.getString("comments");
                int arrayCount = 0;
                for (int j = 0; j < tableLayoutsMedicines.length; j++) {
                    for (int i = 1; i < tableLayoutsMedicines[j].getChildCount(); i++) {
                        View child = tableLayoutsMedicines[j].getChildAt(i);
                        JSONObject jsonObjectChild = jsonArray.getJSONObject(arrayCount++);
                        TableRow tableRow = (TableRow) child;
                        //ToggleButton present = (ToggleButton) tableRow.getChildAt(1);
                        EditText consumption = (EditText) tableRow.getChildAt(1);
                        EditText balance = (EditText) tableRow.getChildAt(2);
                        EditText stock = (EditText) tableRow.getChildAt(3);
                        if (sharedPreferencesEditor.getIsPMO().equals(Constants.DB_TYPE_PMO)) {
                            EditText recording = (EditText) tableRow.getChildAt(4);
                            EditText expired = (EditText) tableRow.getChildAt(5);
                            recording.setText(jsonObjectChild.getString("recording"));
                            expired.setText(jsonObjectChild.getString("expired_medicines"));
                        } else {
                            EditText expired = (EditText) tableRow.getChildAt(4);
                            expired.setText(jsonObjectChild.getString("expired_medicines"));
                        }
                        consumption.setText(jsonObjectChild.getString("consumption_during_previous_month"));
                        balance.setText(jsonObjectChild.getString("balance_at_visit_time"));
                        stock.setText(jsonObjectChild.getString("physical_stock"));


                    }
                }

                if (focalPerson.equals("yes")) {
                    rb1.setChecked(true);

                    if (medicinesExpired.equals("yes")) {
                        rb3.setChecked(true);

                        commentBox.setText(comment);
                    } else if (medicinesExpired.equals("no"))
                        rb4.setChecked(true);
                } else if (focalPerson.equals("no"))
                    rb2.setChecked(true);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (parentView == null) {
            parentView = inflater.inflate(R.layout.fragment_default, container, false);
            initializeData();
            generateBody();

            linearLayoutMain.setVisibility(View.GONE);
            linearLayoutEnd.setVisibility(View.GONE);

            loadSavedData();
        }
        return parentView;
    }

    private void initializeData() {
        try {
            ClassScreenData objectScreenData = new ClassScreenData();
            objectScreenData.setDefaultValues();
            objectScreenData.screen_id = Constants.DB_SCREEN_ID_MEDICINES;
            objectScreenData.screen_name = Constants.DB_SCREEN_NAME_MEDICINES;
            arrayListAllMedicines = MainContainer.mDbAdapter.selectScreenItemsMultiple(objectScreenData);

            // long seed = System.nanoTime();
            // Random rand = new Random();
            // int randomNum = rand.nextInt((max - min) + 1) + min;
            arrayListRandomIndeces = new ArrayList<Integer>();
            int max = 0;
            for (ClassScreenItemMultiple item : arrayListAllMedicines) {
                max = max + item.arrayListFields.size();
            }
            int min = 0;
            int size = max > 20 ? 20 : max;
            for (int i = 0; i < size; ) {
                int randomNum = min + (int) (Math.random() * ((max - min) + 1));
                if (!arrayListRandomIndeces.contains(randomNum) && randomNum < max) {
                    arrayListRandomIndeces.add(randomNum);
                    i++;
                }
            }

            commentBox = new EditText(getActivity());

            rb1 = new RadioButton(getActivity());
            rb2 = new RadioButton(getActivity());
            rb3 = new RadioButton(getActivity());
            rb4 = new RadioButton(getActivity());

            final Field f = TextView.class.getDeclaredField("mCursorDrawableRes");
            f.setAccessible(true);
            f.set(commentBox, R.drawable.xml_color_cursor);
            commentBox.setHint("Enter Medicines Names");
            commentBox.setBackgroundColor(getResources().getColor(R.color.row_body_background_single));

            rb1.setText("Yes");
            rb1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    linearLayoutMain.setVisibility(View.VISIBLE);

                    addQuestion("Are any expired medicines present other than the list?", linearLayoutEnd, rb3, rb4);

                    linearLayoutEnd.setVisibility(View.VISIBLE);
                }
            });

            rb2.setText("No");
            rb2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    linearLayoutMain.setVisibility(View.GONE);

                    if (linearLayoutEnd.getVisibility() == View.VISIBLE) {
                        rg1 = (RadioGroup) linearLayoutEnd.getChildAt(1);
                        rg1.removeAllViews();

                        linearLayoutEnd.removeAllViews();
                        commentBox.setText("");
                        rb3.setChecked(false);
                        rb4.setChecked(false);

                        linearLayoutEnd.setVisibility(View.GONE);
                    }
                }
            });

            rb3.setText("Yes");
            rb3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    linearLayoutMain.setVisibility(View.VISIBLE);
                    linearLayoutEnd.setVisibility(View.VISIBLE);
                    addQuestionWithCommentBox("Which medicines were expired?", linearLayoutEnd, commentBox);
                }
            });

            rb4.setText("No");
            rb4.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (linearLayoutEnd.getChildCount() > 2) {

                        linearLayoutEnd.removeViews(2, 2);
                        commentBox.setText("");

                    }
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void addQuestion(String text, LinearLayout layout, RadioButton radioButton1, RadioButton radioButton2) {

        TextView question = new TextView(getActivity());
        question.setText(text);
        layout.addView(question);

        RadioGroup radioGroup = new RadioGroup(getActivity());
        radioGroup.addView(radioButton1);
        radioGroup.addView(radioButton2);
        radioGroup.setOrientation(LinearLayout.HORIZONTAL);

        layout.addView(radioGroup);

    }

    private void addQuestionWithCommentBox(String text, LinearLayout layout, EditText commentBox) {

        TextView question = new TextView(getActivity());
        question.setText(text);
        layout.addView(question);
        layout.addView(commentBox);

    }

    private void generateBody() {
        try {
            this.sharedPreferencesEditor = new SharedPreferencesEditor(MainContainer.mContext);
            linearLayoutMain = (LinearLayout) parentView.findViewById(R.id.linearlayout_main);
            linearLayoutStart = (LinearLayout) parentView.findViewById(R.id.linearlayout_start);
            linearLayoutEnd = (LinearLayout) parentView.findViewById(R.id.linearlayout_end);
            textViewsMedicines = new TextView[arrayListAllMedicines.size()];
            tableLayoutsMedicines = new TableLayout[arrayListAllMedicines.size()];

            addQuestion("Is focal person for medicine availablility present?", linearLayoutStart, rb1, rb2);

            UIHelper uiContainer = new UIHelper();
            TableRow.LayoutParams[] paramsArrayTableRowColumns = null;
            if (sharedPreferencesEditor.getIsPMO().equals(Constants.DB_TYPE_PMO))
                paramsArrayTableRowColumns = new TableRow.LayoutParams[6];
            else
                paramsArrayTableRowColumns = new TableRow.LayoutParams[5];

            if (sharedPreferencesEditor.getIsPMO().equals(Constants.DB_TYPE_PMO)) {
                paramsArrayTableRowColumns[0] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.20), TableRow.LayoutParams.MATCH_PARENT);
                paramsArrayTableRowColumns[1] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.15), TableRow.LayoutParams.MATCH_PARENT);
                paramsArrayTableRowColumns[2] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.15), TableRow.LayoutParams.WRAP_CONTENT);
                paramsArrayTableRowColumns[3] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.15), TableRow.LayoutParams.MATCH_PARENT);
                paramsArrayTableRowColumns[4] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.15), TableRow.LayoutParams.MATCH_PARENT);
                paramsArrayTableRowColumns[5] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.15), TableRow.LayoutParams.MATCH_PARENT);
            } else {
                paramsArrayTableRowColumns[0] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.3), TableRow.LayoutParams.MATCH_PARENT);
                paramsArrayTableRowColumns[1] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.16), TableRow.LayoutParams.WRAP_CONTENT);
                paramsArrayTableRowColumns[2] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.17), TableRow.LayoutParams.MATCH_PARENT);
                paramsArrayTableRowColumns[3] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.16), TableRow.LayoutParams.MATCH_PARENT);
                paramsArrayTableRowColumns[4] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.17), TableRow.LayoutParams.MATCH_PARENT);

            }
            for (int i = 1; i < paramsArrayTableRowColumns.length; i++) {
                paramsArrayTableRowColumns[i].setMargins(2, 0, 0, 0);
            }

            indexMedicine = 0;
            for (int i = 0; i < arrayListAllMedicines.size(); i++) {
                textViewsMedicines[i] = new TextView(MainContainer.mContext);
                tableLayoutsMedicines[i] = new TableLayout(MainContainer.mContext);
                if (arrayListAllMedicines.get(i).arrayListFields.size() > 0) {
                    linearLayoutMain.addView(textViewsMedicines[i]);
                    linearLayoutMain.addView(tableLayoutsMedicines[i]);
                    uiContainer.generateTableLabel(textViewsMedicines[i], arrayListAllMedicines.get(i).label);
                    if (sharedPreferencesEditor.getIsPMO().equals(Constants.DB_TYPE_PMO)) {
                        uiContainer.generateTableHeader(tableLayoutsMedicines[i], paramsArrayTableRowColumns, new String[]{"Medicines", "Consumption During Previous Month",
                                "Balance according to register at the time of visit", "Physical Stock Count from Store", "Recording medicine used since last MEA visit", "Expired Medicines"});
                    } else {
                        uiContainer.generateTableHeader(tableLayoutsMedicines[i], paramsArrayTableRowColumns, new String[]{"Medicines", "Consumption During Previous Month",
                                "Balance according to register at the time of visit", "Physical Stock Count from Store", "Expired Medicines"});
                    }
                    generateTableMedicines(paramsArrayTableRowColumns, tableLayoutsMedicines[i], arrayListAllMedicines.get(i).arrayListFields);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void generateTableMedicines(TableRow.LayoutParams[] paramsArrayTableRowColumns, TableLayout tableLayout, ArrayList<ClassScreenItem> arrayListItems) {
        int colorBgTableRow = MainContainer.mContext.getResources().getColor(R.color.row_background);
        for (int i = 0; i < arrayListItems.size(); i++, indexMedicine++) {
            boolean bIsOdd = i % 2 == 0;

            TableRow tableRow = new TableRow(MainContainer.mContext);
            CustomEditText editTextMedicine = new CustomEditText(bIsOdd, (indexMedicine + 1) + ". " + arrayListItems.get(i).item_name);
            CustomEditText editTextConsumption = new CustomEditText(bIsOdd);
            CustomEditText editTextBalance = new CustomEditText(bIsOdd);
            CustomEditText editTextStock = new CustomEditText(bIsOdd);
            CustomEditText editTextRecording = new CustomEditText(bIsOdd);
            CustomEditText editTextExpiredMedicines = new CustomEditText(bIsOdd);

            editTextMedicine.customiseEditText(paramsArrayTableRowColumns[0]);
            editTextConsumption.customiseEditText(paramsArrayTableRowColumns[1]);
            editTextBalance.customiseEditText(paramsArrayTableRowColumns[2]);
            editTextStock.customiseEditText(paramsArrayTableRowColumns[3]);
            if (sharedPreferencesEditor.getIsPMO().equals(Constants.DB_TYPE_PMO)) {
                editTextRecording.customiseEditText(paramsArrayTableRowColumns[4]);
                editTextExpiredMedicines.customiseEditText(paramsArrayTableRowColumns[5]);
            } else {
                editTextExpiredMedicines.customiseEditText(paramsArrayTableRowColumns[4]);
            }

            editTextMedicine.setOnClickListener(onClickShowName);
            tableRow.setBackgroundColor(colorBgTableRow);
            tableLayout.addView(tableRow);

            editTextConsumption.setInputTypeNumberEditable();
            editTextBalance.setInputTypeNumberEditable();
            if (arrayListRandomIndeces.contains(indexMedicine)) {
                editTextStock.setInputTypeNumberEditable();
                editTextExpiredMedicines.setInputTypeNumberEditable();
            } else {
                editTextStock.setText("N/A");
                editTextStock.setInputTypeTextNonEditable();
                editTextStock.setGravity(Gravity.CENTER);
                editTextExpiredMedicines.setText("0");
                editTextExpiredMedicines.setInputTypeTextNonEditable();
                editTextExpiredMedicines.setGravity(Gravity.CENTER);
            }
            if (sharedPreferencesEditor.getIsPMO().equals(Constants.DB_TYPE_PMO))
                editTextRecording.setInputTypeNumberEditable();

            //editTextExpiredMedicines.setInputTypeNumberEditable();
            // editTextSanctioned.addTextChangedListener(new TextWatcherCountSingleChild(editTextSanctioned, editTextVacant, null, null));
            // editTextVacant.addTextChangedListener(new TextWatcherCountSingleChild(editTextSanctioned, editTextVacant, "Balance Count", "Consumption Count"));

            editTextExpiredMedicines.addTextChangedListener(new TextWatcherCountSingleChild(editTextBalance, editTextExpiredMedicines, "Expired Medicines count", "Balance at the time of visit Count"));

            tableRow.addView(editTextMedicine);
            tableRow.addView(editTextConsumption);
            tableRow.addView(editTextBalance);
            tableRow.addView(editTextStock);
            if (sharedPreferencesEditor.getIsPMO().equals(Constants.DB_TYPE_PMO))
                tableRow.addView(editTextRecording);

            tableRow.addView(editTextExpiredMedicines);
        }
    }

    private View.OnClickListener onClickShowName = new View.OnClickListener() {

        @SuppressLint("InflateParams")
        @Override
        public void onClick(View view) {
            try {
                LayoutInflater layoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View layout = layoutInflater.inflate(R.layout.layout_pop_up_message, null);
                ((TextView) layout).setText(((EditText) view).getText().toString().trim());

                PopupWindow popupWindow = new PopupWindow(getActivity());
                popupWindow.setContentView(layout);
                popupWindow.setWidth(LinearLayout.LayoutParams.WRAP_CONTENT);
                popupWindow.setHeight(LinearLayout.LayoutParams.WRAP_CONTENT);
                popupWindow.setBackgroundDrawable(new BitmapDrawable());
                popupWindow.setOutsideTouchable(true);
                popupWindow.showAsDropDown(view);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    @Override
    public void onFragmentShown() {
    }

    private String errorMessage;

    @Override
    public String onFragmentChanged(int previousPosition) {
        errorMessage = null;
        isFormValid();
        return errorMessage;
    }

    @Override
    public boolean isFormValid() {
        try {
            boolean bIsValid = true;
            if (linearLayoutMain.getVisibility() == View.VISIBLE) {
                for (int j = 0; j < tableLayoutsMedicines.length; j++) {
                    for (int i = 1; i < tableLayoutsMedicines[j].getChildCount(); i++) {
                        View child = tableLayoutsMedicines[j].getChildAt(i);
                        TableRow tableRow = (TableRow) child;
                        EditText consumption = (EditText) tableRow.getChildAt(1);
                        EditText balance = (EditText) tableRow.getChildAt(2);
                        EditText stock = (EditText) tableRow.getChildAt(3);

                        if (consumption.getText().toString().trim().length() == 0 || balance.getText().toString().trim().length() == 0 || stock.getText().toString().trim().length() == 0) {
                            bIsValid = false;
                        }
                        if (sharedPreferencesEditor.getIsPMO().equals(Constants.DB_TYPE_PMO)) {
                            EditText recording = (EditText) tableRow.getChildAt(4);
                            EditText expired = (EditText) tableRow.getChildAt(5);
                            if (recording.getText().toString().trim().length() == 0 || expired.getText().toString().trim().length() == 0)
                                bIsValid = false;
                        } else {
                            EditText expired = (EditText) tableRow.getChildAt(4);
                            if (expired.getText().toString().trim().length() == 0)
                                bIsValid = false;
                        }
                    }
                }
            }
            if (rb3.isChecked()) {
                if (commentBox.length() == 0) {
                    errorMessage = "Please enter names of medicines";
                    return false;
                }
            }
            if (!bIsValid) {
                errorMessage = "Please fill all Medicines values to continue";
            }
            return bIsValid;
        } catch (Exception e) {
            e.printStackTrace();
            MainContainer.showMessage("Error Processing Your Request");
            return false;
        }
    }
}