package pk.gov.pitb.mea.msh.fragmentactivities;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import pk.gov.pitb.mea.msh.R;
import pk.gov.pitb.mea.msh.database.SharedPreferencesEditor;
import pk.gov.pitb.mea.msh.handlers.InterfaceFragmentCallBack;
import pk.gov.pitb.mea.msh.helpers.Constants;
import pk.gov.pitb.mea.msh.helpers.MainContainer;
import pk.gov.pitb.mea.msh.helpers.UIHelper;
import pk.gov.pitb.mea.msh.models.ClassScreenData;
import pk.gov.pitb.mea.msh.models.ClassScreenItem;
import pk.gov.pitb.mea.msh.views.CustomEditText;

public class FragmentSupplies extends Fragment implements InterfaceFragmentCallBack {

	private ArrayList<ClassScreenItem> arrayListSupplies;
	private ArrayList<Integer> arrayListRandomIndeces;

	private int indexSupplies;
	private View parentView;
	private LinearLayout linearLayoutMain, linearLayoutStart;
	RadioGroup rg;
	RadioButton rb1, rb2;
	TextView question;
	private TextView textViewSupplies;
	private TableLayout tableLayoutSupplies;
	private SharedPreferencesEditor sharedPreferencesEditor;
	@Override
	public void parseObject() {
		try {
			JSONArray jsonArraySupplies = new JSONArray();
			if(linearLayoutMain.getVisibility() == View.VISIBLE) {
				for (int i = 1; i < tableLayoutSupplies.getChildCount(); i++) {
					View child = tableLayoutSupplies.getChildAt(i);
					ClassScreenItem object = arrayListSupplies.get(i - 1);
					JSONObject jsonObjectChild = new JSONObject();

					TableRow tableRow = (TableRow) child;
					EditText consumption = (EditText) tableRow.getChildAt(1);
					EditText balance = (EditText) tableRow.getChildAt(2);
					EditText stock = (EditText) tableRow.getChildAt(3);

					jsonObjectChild.put("supply_id", object.item_id);
					jsonObjectChild.put("supply_name", object.item_name);
					jsonObjectChild.put("consumption_during_previous_month", consumption.getText().toString().trim());
					jsonObjectChild.put("balance_at_visit_time", balance.getText().toString().trim());
					jsonObjectChild.put("physical_stock", stock.getText().toString().trim());

					if (sharedPreferencesEditor.getIsPMO().equals(Constants.DB_TYPE_PMO)) {
						EditText recording = (EditText) tableRow.getChildAt(4);
						jsonObjectChild.put("recording", recording.getText().toString().trim());
					} else {
						jsonObjectChild.put("recording", "");
					}

					jsonArraySupplies.put(jsonObjectChild);
				}
			}

			JSONObject jsonObject = new JSONObject();
			String focalPerson = "";

			if(rb1.isChecked())
				focalPerson = "yes";
			else if(rb2.isChecked())
				focalPerson = "no";

			jsonObject.put("supplies", jsonArraySupplies);
			jsonObject.put("focal_person", focalPerson);

			MainContainer.mJsonObjectFormData.remove(Constants.DB_SCREEN_NAME_SUPPLIES);
			MainContainer.mJsonObjectFormData.put(Constants.DB_SCREEN_NAME_SUPPLIES, jsonObject);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void loadSavedData(){
		try {
			if (MainContainer.mJsonObjectFormData.has(Constants.DB_SCREEN_NAME_SUPPLIES)){
				JSONObject jsonObject = MainContainer.mJsonObjectFormData.getJSONObject(Constants.DB_SCREEN_NAME_SUPPLIES);
				JSONArray jsonArray = jsonObject.getJSONArray("supplies");
				String focalPerson = jsonObject.getString("focal_person");

				for (int i = 1; i < tableLayoutSupplies.getChildCount(); i++) {
					View child = tableLayoutSupplies.getChildAt(i);
					JSONObject jsonObjectChild = jsonArray.getJSONObject(i-1);

					TableRow tableRow = (TableRow) child;
					EditText consumption = (EditText) tableRow.getChildAt(1);
					EditText balance = (EditText) tableRow.getChildAt(2);
					EditText stock = (EditText) tableRow.getChildAt(3);

					consumption.setText(jsonObjectChild.getString("consumption_during_previous_month"));
					balance.setText(jsonObjectChild.getString("balance_at_visit_time"));
					stock.setText(jsonObjectChild.getString("physical_stock"));

					if(sharedPreferencesEditor.getIsPMO().equals(Constants.DB_TYPE_PMO)){
						EditText recording = (EditText) tableRow.getChildAt(4);
						recording.setText(jsonObjectChild.getString("recording"));
					}
				}

				if(focalPerson.equals("yes"))
					rb1.setChecked(true);
				else
					rb2.setChecked(true);

			}
		}
		catch (Exception e){

		}
	}


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		if (parentView == null) {
			parentView = inflater.inflate(R.layout.fragment_default, container, false);
			initializeData();
			generateBody();

			linearLayoutMain.setVisibility(View.GONE);

			loadSavedData();
		}
		return parentView;
	}

	private void initializeData() {
		try {
			ClassScreenData objectScreenData = new ClassScreenData();
			objectScreenData.setDefaultValues();
			objectScreenData.screen_id = Constants.DB_SCREEN_ID_SUPPLIES;
			objectScreenData.screen_name = Constants.DB_SCREEN_NAME_SUPPLIES;

			objectScreenData.section_id = "1";
			objectScreenData.section_name = "section1";
			objectScreenData.section_value = "fields";
			objectScreenData = MainContainer.mDbAdapter.selectScreenData(objectScreenData);
			arrayListSupplies = MainContainer.mDbAdapter.selectScreenItems(objectScreenData);

			arrayListRandomIndeces = new ArrayList<Integer>();
			int max = 0;
			indexSupplies=0;

			max=arrayListSupplies.size();
			int min = 0;
			int size = max > 10 ? 10 : max;
			for (int i = 0; i < size;) {
				int randomNum = min + (int) (Math.random() * ((max - min) + 1));
				if (!arrayListRandomIndeces.contains(randomNum) && randomNum < max) {
					arrayListRandomIndeces.add(randomNum);
					i++;
				}
			}

			rb1 = new RadioButton(getActivity());
			rb2 = new RadioButton(getActivity());

			rb1.setText("Yes");
			rb1.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					linearLayoutMain.setVisibility(View.VISIBLE);
				}
			});

			rb2.setText("No");
			rb2.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					linearLayoutMain.setVisibility(View.GONE);
				}
			});

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void addQuestion(TextView question, String text, LinearLayout layout, RadioGroup radioGroup, RadioButton radioButton1, RadioButton radioButton2){

		question = new TextView(getActivity());
		question.setText(text);
		layout.addView(question);

		radioGroup = new RadioGroup(getActivity());
		radioGroup.addView(radioButton1);
		radioGroup.addView(radioButton2);
		radioGroup.setOrientation(LinearLayout.HORIZONTAL);

		layout.addView(radioGroup);

	}

	private void generateBody() {
		try {
			this.sharedPreferencesEditor = new SharedPreferencesEditor(MainContainer.mContext);
			linearLayoutMain = (LinearLayout) parentView.findViewById(R.id.linearlayout_main);
			linearLayoutStart = (LinearLayout) parentView.findViewById(R.id.linearlayout_start);
			textViewSupplies = new TextView(MainContainer.mContext);
			tableLayoutSupplies = new TableLayout(MainContainer.mContext);

			addQuestion(question, "Is focal person for checking supplies present?", linearLayoutStart, rg, rb1, rb2);

			linearLayoutMain.addView(textViewSupplies);
			linearLayoutMain.addView(tableLayoutSupplies);

			UIHelper uiContainer = new UIHelper();
			uiContainer.generateTableLabel(textViewSupplies, "Supplies");
			TableRow.LayoutParams[] paramsArrayTableRowColumns =null;
			if(sharedPreferencesEditor.getIsPMO().equals(Constants.DB_TYPE_PMO))
				paramsArrayTableRowColumns = new TableRow.LayoutParams[5];
			else
				paramsArrayTableRowColumns = new TableRow.LayoutParams[4];
			if(sharedPreferencesEditor.getIsPMO().equals(Constants.DB_TYPE_PMO)) {
				paramsArrayTableRowColumns[0] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.3), TableRow.LayoutParams.MATCH_PARENT);
				paramsArrayTableRowColumns[1] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.16), TableRow.LayoutParams.MATCH_PARENT);
				paramsArrayTableRowColumns[2] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.16), TableRow.LayoutParams.MATCH_PARENT);
				paramsArrayTableRowColumns[3] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.16), TableRow.LayoutParams.MATCH_PARENT);
				paramsArrayTableRowColumns[4] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.17), TableRow.LayoutParams.WRAP_CONTENT);
			} else {
				paramsArrayTableRowColumns[0] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.3), TableRow.LayoutParams.MATCH_PARENT);
				paramsArrayTableRowColumns[1] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.23), TableRow.LayoutParams.WRAP_CONTENT);
				paramsArrayTableRowColumns[2] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.23), TableRow.LayoutParams.MATCH_PARENT);
				paramsArrayTableRowColumns[3] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.2), TableRow.LayoutParams.MATCH_PARENT);
			}
			for (int i = 1; i < paramsArrayTableRowColumns.length; i++) {
				paramsArrayTableRowColumns[i].setMargins(2, 0, 0, 0);
			}
			if(sharedPreferencesEditor.getIsPMO().equals(Constants.DB_TYPE_PMO)){
				uiContainer.generateTableHeader(tableLayoutSupplies, paramsArrayTableRowColumns, "Supplies", "Consumption During Previous Month", "Balance at Visit Time", "Physical Stock Count","Recording supplies used since last MEA visit");
			} else {
				uiContainer.generateTableHeader(tableLayoutSupplies, paramsArrayTableRowColumns, "Supplies", "Consumption During Previous Month", "Balance at Visit Time", "Physical Stock Count");
			}
			generateTableMedicalEquipmentsType0(paramsArrayTableRowColumns);
			if (tableLayoutSupplies.getChildCount() > 1) {
				textViewSupplies.setVisibility(View.VISIBLE);
				tableLayoutSupplies.setVisibility(View.VISIBLE);
			} else {
				textViewSupplies.setVisibility(View.GONE);
				tableLayoutSupplies.setVisibility(View.GONE);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void generateTableMedicalEquipmentsType0(TableRow.LayoutParams[] paramsArrayTableRowColumns) {
		int colorBgTableRow = MainContainer.mContext.getResources().getColor(R.color.row_background);
		for (int index = 0; index < arrayListSupplies.size(); index++, indexSupplies++) {
			boolean bIsOdd = index % 2 == 0;

			TableRow tableRow = new TableRow(MainContainer.mContext);
			CustomEditText editTextSupplies = new CustomEditText(bIsOdd, (index + 1) + ". " + arrayListSupplies.get(index).item_name);
			CustomEditText editTextConsumption = new CustomEditText(bIsOdd);
			CustomEditText editTextBalance = new CustomEditText(bIsOdd);
			CustomEditText editTextStock = new CustomEditText(bIsOdd);
			CustomEditText editTextRecording = new CustomEditText(bIsOdd);

			editTextSupplies.customiseEditText(paramsArrayTableRowColumns[0]);
			editTextConsumption.customiseEditText(paramsArrayTableRowColumns[1]);
			editTextBalance.customiseEditText(paramsArrayTableRowColumns[2]);
			editTextStock.customiseEditText(paramsArrayTableRowColumns[3]);
			if(sharedPreferencesEditor.getIsPMO().equals(Constants.DB_TYPE_PMO))
				editTextRecording.customiseEditText(paramsArrayTableRowColumns[4]);

			tableRow.setBackgroundColor(colorBgTableRow);
			tableLayoutSupplies.addView(tableRow);

			editTextSupplies.setOnClickListener(onClickShowName);

			editTextConsumption.setInputTypeNumberEditable();
			editTextBalance.setInputTypeNumberEditable();
			//editTextStock.setInputTypeNumberEditable();
			if (arrayListRandomIndeces.contains(indexSupplies)) {
				editTextStock.setInputTypeNumberEditable();
			} else {
				editTextStock.setText("N/A");
				editTextStock.setInputTypeTextNonEditable();
				editTextStock.setGravity(Gravity.CENTER);
			}
			if(sharedPreferencesEditor.getIsPMO().equals(Constants.DB_TYPE_PMO))
				editTextRecording.setInputTypeNumberEditable();
			// editTextConsumption.addTextChangedListener(new TextWatcherCountSingleChild(editTextConsumption, editTextBalance, null, null));
			// editTextBalance.addTextChangedListener(new TextWatcherCountSingleChild(editTextConsumption, editTextBalance, "Balance Count", "Consumption Count"));

			tableRow.addView(editTextSupplies);
			tableRow.addView(editTextConsumption);
			tableRow.addView(editTextBalance);
			tableRow.addView(editTextStock);
			if(sharedPreferencesEditor.getIsPMO().equals(Constants.DB_TYPE_PMO))
				tableRow.addView(editTextRecording);
		}
	}
	private View.OnClickListener onClickShowName = new View.OnClickListener() {

		@SuppressLint("InflateParams")
		@Override
		public void onClick(View view) {
			try {
				LayoutInflater layoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				View layout = layoutInflater.inflate(R.layout.layout_pop_up_message, null);
				((TextView) layout).setText(((EditText) view).getText().toString().trim());

				PopupWindow popupWindow = new PopupWindow(getActivity());
				popupWindow.setContentView(layout);
				popupWindow.setWidth(LinearLayout.LayoutParams.WRAP_CONTENT);
				popupWindow.setHeight(LinearLayout.LayoutParams.WRAP_CONTENT);
				popupWindow.setBackgroundDrawable(new BitmapDrawable());
				popupWindow.setOutsideTouchable(true);
				popupWindow.showAsDropDown(view);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	};

	@Override
	public void onFragmentShown() {
	}

	private String errorMessage;

	@Override
	public String onFragmentChanged(int previousPosition) {
		errorMessage = null;
		isFormValid();
		return errorMessage;
	}

	@Override
	public boolean isFormValid() {
		try {
			boolean bIsValid = true;
			if(linearLayoutMain.getVisibility() == View.VISIBLE) {
				for (int i = 1; i < tableLayoutSupplies.getChildCount(); i++) {
					View child = tableLayoutSupplies.getChildAt(i);
					TableRow tableRow = (TableRow) child;
					EditText consumption = (EditText) tableRow.getChildAt(1);
					EditText balance = (EditText) tableRow.getChildAt(2);
					EditText stock = (EditText) tableRow.getChildAt(3);

					if (consumption.getText().toString().trim().length() == 0 || balance.getText().toString().trim().length() == 0 || stock.getText().toString().trim().length() == 0) {
						bIsValid = false;
					}
					if (sharedPreferencesEditor.getIsPMO().equals(Constants.DB_TYPE_PMO)) {
						EditText recording = (EditText) tableRow.getChildAt(4);
						if (recording.getText().toString().trim().length() == 0)
							bIsValid = false;
					}
				}
			}
			if (!bIsValid) {
				errorMessage = "Please fill all Supplies value to continue";
			}
			return bIsValid;
		} catch (Exception e) {
			e.printStackTrace();
			MainContainer.showMessage("Error Processing Your Request");
			return false;
		}
	}
}