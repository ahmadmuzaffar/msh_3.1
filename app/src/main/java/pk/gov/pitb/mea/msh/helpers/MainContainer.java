package pk.gov.pitb.mea.msh.helpers;

import android.app.Activity;
import android.content.Context;
import android.graphics.Point;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.view.Display;
import android.widget.Toast;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Locale;

import pk.gov.pitb.mea.msh.database.DbAdapter;
import pk.gov.pitb.mea.msh.database.SharedPreferencesEditor;
import pk.gov.pitb.mea.msh.handlers.HandlerActivityCallHealthCouncil;
import pk.gov.pitb.mea.msh.handlers.HandlerActivityCallHepatitis;
import pk.gov.pitb.mea.msh.handlers.HandlerActivityCallNutrients;
import pk.gov.pitb.mea.msh.handlers.HandlerActivityCallPatientInterviews;
import pk.gov.pitb.mea.msh.handlers.InterfaceActivityCallBack;
import pk.gov.pitb.mea.msh.models.ClassDataActivity;
import pk.gov.pitb.mea.msh.models.ClassDialogFacility;
import pk.gov.pitb.mea.msh.models.ClassFacilityData;
import pk.gov.pitb.mea.msh.models.ClassFacilityItem;
import pk.gov.pitb.mea.msh.models.ClassScreenItem;
import pk.gov.pitb.mea.msh.views.CustomEditText;

public class MainContainer {

    public static Locale mLocale = null;
    public static Activity mActivity = null;
    public static Context mContext = null;
    public static DbAdapter mDbAdapter = null;
    public static SharedPreferencesEditor mSPEditor = null;
    public static String mPackageName = null;
    public static String mImeiNumber = null;
    public static int mScreenWidth = 0;
    public static int mScreenHeight = 0;

    public static boolean bIsOpen = true;
    public static ArrayList<String> arrayListOpenedActivities;
    public static InterfaceActivityCallBack activityCallBack = null;
    public static HandlerActivityCallHealthCouncil activityCallBackHealthCouncil;
    public static boolean bIsActivityHealthCouncil;
    public static HandlerActivityCallNutrients activityCallBackNutrients;
    public static boolean bIsActivityNutrients;
    public static HandlerActivityCallHepatitis activityCallBackHepatitis;
    public static boolean bIsActivityHepatitis;
    public static HandlerActivityCallPatientInterviews activityCallBackPatientInterviews;
    public static boolean bIsActivityPatientInterviews;
    public static ClassDataActivity mObjectActivity = null;
    public static ClassDialogFacility mObjectFacility = null;
    public static ClassFacilityData mObjectFacilityData = null;
    public static JSONObject mJsonObjectFormData = null;
    public static JSONObject mJsonObjectFormDataEquipments = null;
    public static JSONObject mJsonObjectSavedData = null;

    public static void setContextOnResume(Activity activity) {
        MainContainer.mActivity = activity;
        MainContainer.mContext = activity;
    }

    public static void initializeOnStartup(Activity activity) {
        MainContainer.mActivity = activity;
        MainContainer.mContext = activity;
        TelephonyManager telephonyManager = (TelephonyManager) MainContainer.mContext.getSystemService(Context.TELEPHONY_SERVICE);
       mImeiNumber = telephonyManager.getDeviceId();
        // mImeiNumber = "357581040008268"; // Huawei Tab (Personal)
        // mImeiNumber = "864390020062790"; // Huawei Tab (Salman Saleem)
        // mImeiNumber = "864390020063855"; // Huawei Tab (Adnan Sb.)
        // mImeiNumber = "864390020196861"; // (Salik Farooq)
//		 mImeiNumber = "359521063612834"; // (Zunair Ahmed)
//         mImeiNumber = "864390027021138"; // (wasim)
        mPackageName = mContext.getPackageName();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            Point size = new Point();
            mActivity.getWindowManager().getDefaultDisplay().getSize(size);
            MainContainer.mScreenWidth = size.x;
            MainContainer.mScreenHeight = size.y;
        } else {
            Display display = mActivity.getWindowManager().getDefaultDisplay();
            MainContainer.mScreenWidth = display.getWidth();
            MainContainer.mScreenHeight = display.getHeight();
        }
        MainContainer.mLocale = Locale.ENGLISH;
        MainContainer.mDbAdapter = new DbAdapter(mContext);
        MainContainer.mSPEditor = new SharedPreferencesEditor(mContext);
        MainContainer.mObjectActivity = new ClassDataActivity(MainContainer.mContext);
        MainContainer.arrayListOpenedActivities = new ArrayList<String>();
        CustomEditText.initializeAttributeSet(mActivity, mContext);
    }

    public static String getScreenTag(String screenName) {
        return screenName.replaceAll(" -", "_").toLowerCase(MainContainer.mLocale);
    }

    public static void resetMainContainer() {
        MainContainer.mDbAdapter.clearOldData();
        bIsOpen = true;
        arrayListOpenedActivities = null;
        mObjectActivity.setEmptyValues();
        if (mObjectActivity.arrayListPictures != null) {
            mObjectActivity.arrayListPictures.clear();
        }
        mObjectFacility = null;
        mObjectFacilityData = null;
        bIsActivityHealthCouncil = false;
        bIsActivityHepatitis = false;
        bIsActivityNutrients = false;
        bIsActivityPatientInterviews = false;
        mJsonObjectFormData = null;
        activityCallBackHealthCouncil = null;
        activityCallBackNutrients = null;
        activityCallBackHepatitis = null;
        mJsonObjectFormDataEquipments = null;
    }

    public static void exitMainContainer() {
        resetMainContainer();
        // activityCallBack = null;
        // mObjectActivity = null;
    }

    public static boolean isCommentsRequired(ClassScreenItem selectedItem) {
        return (selectedItem != null && selectedItem.item_name.toLowerCase(Locale.ENGLISH).equals("official duty other"));
    }

    public static String searchForValueItem(String id, ArrayList<ClassScreenItem> listItems) {
        try {
            for (ClassScreenItem object : listItems) {
                if (object.item_id.equals(id)) {
                    return object.item_name;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String searchForValue(String id, ArrayList<ClassFacilityItem> listItems) {
        try {
            for (ClassFacilityItem object : listItems) {
                if (object.fitem_id.equals(id)) {
                    return object.fitem_name;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public static void showMessage(String message) {
        Toast.makeText(mContext, message, Toast.LENGTH_LONG).show();
    }

    public static void showErrorMessage() {
        MainContainer.showMessage("Error Processing Your Request");
    }

    public static boolean isInternetAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}