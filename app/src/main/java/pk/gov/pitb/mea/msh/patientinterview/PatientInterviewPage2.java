package pk.gov.pitb.mea.msh.patientinterview;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.ToggleButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import pk.gov.pitb.mea.msh.R;
import pk.gov.pitb.mea.msh.adapters.ArrayAdapterSimpleSpinner;
import pk.gov.pitb.mea.msh.adapters.ArrayAdapterSpinner;
import pk.gov.pitb.mea.msh.adapters.ArrayAdapterSpinnerItem;
import pk.gov.pitb.mea.msh.adapters.ArrayAdapterSpinnerSimpleScreenItem;
import pk.gov.pitb.mea.msh.bo.ClassScreenItemMultiSelect;
import pk.gov.pitb.mea.msh.dialogs.DialogStaffDetails;
import pk.gov.pitb.mea.msh.fragmentactivities.FragmentDataonAbsences;
import pk.gov.pitb.mea.msh.handlers.HandlerFragmentCallBackPatientInterviews;
import pk.gov.pitb.mea.msh.helpers.AlertDialogs;
import pk.gov.pitb.mea.msh.helpers.Constants;
import pk.gov.pitb.mea.msh.helpers.MainContainer;
import pk.gov.pitb.mea.msh.helpers.UIHelper;
import pk.gov.pitb.mea.msh.models.ClassScreenData;
import pk.gov.pitb.mea.msh.models.ClassScreenItem;
import pk.gov.pitb.mea.msh.models.ClassStaff;
import pk.gov.pitb.mea.msh.textwatchers.TextWatcherCountSingleChild;
import pk.gov.pitb.mea.msh.textwatchers.TextWatcherTwentyCharacters;
import pk.gov.pitb.mea.msh.views.CustomEditText;
import pk.gov.pitb.mea.msh.views.CustomSpinnerMultiSelect;
import pk.gov.pitb.mea.msh.views.CustomSpinnerSingleSelect;
import pk.gov.pitb.mea.msh.views.CustomToggleButtonSingleState;
import pk.gov.pitb.mea.msh.views.CustomToggleButtonTwoState;
import pk.gov.pitb.mea.msh.views.CustomToggleButtonTwoStateChild;
import pk.gov.pitb.mea.msh.views.CustomToggleButtonTwoStateParent;
import pk.gov.pitb.mea.msh.views.MultiSelectDialog;


public class PatientInterviewPage2 extends Fragment implements HandlerFragmentCallBackPatientInterviews {
    private View parentView = null;
    private RadioGroup radioGroupAttentionRespect, radioGroupTreatment, radioGroupPerson, radioGroupMedicalProblems, radioGroupMoney, /*radioGroupTimeWait, radioGroupTimeSpent,
    radioGroupIllness,*/
            radioGroupInstructions, radioGroupInstructionsTakeMedicines, radioGroupCheckPulses,
            radioGroupCheckBloodPressure, radioGroupCheckTemperature, radioGroupCheckWeight, radioGroupCheckHeight, radioGroupSatisfaction,
            radioGroupReferredFacility, radioGroupReferredHospital, radioGroupReferredFacilityPrivatePublic, radioGroupReferredHospitalPrivatePublic,
            radioGroupTestDone, radioGroupTestDonePrivatePublic, radioGroupPersonAttitude, radioGroupFacilityVisitedPreviously, radioGroupHosipitalChangePositiveNegative;
    private Spinner spinnerStaff, spinnerIllness, spinnerTimeWait, spinnerTimeSpent, spinnerSpendMoney, spinnerPatientSeatedWaitingArea, spinnerQueueAvaible, spinnerWaitingAreaClean, spinnerWardClean, spinnerWashroomClean, spinnerWashroomSoapClean, spinnerObservedChange, spinnerOpinionWhatChangedAtHospital, spinnerRecommendation;
    private LinearLayout linearLayoutIllnessOther, layoutMoney, linearLayoutTestDone, linearLayoutChangeObserved, linearLayoutChangedPostiveNegative, linearLayoutOpnion, linearLayoutEditexOption, linearLayoutMain;
    private EditText editTextTimeWait, editTextTimeSpent, editTextComments,
            editTextMedicineQuantity, editTextFacilityDispensary, editTextOtherIllness,
            editTextAmountPaid, etChangeObserved;
    private MultiSelectDialog multiSelectDialogTests, multiSelectDialogOpinion;
    private MainContainer mGlobals;
    private Button buttonProceed;
    private LinearLayout.LayoutParams lpSpinner;
    private String errorMessage = null;
    private String[] arrayListRecommendation = new String[]{"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10"};
    private String[] arrayListSpinnerList = new String[]{"Public", "Private"};
    private ArrayList<ClassScreenItemMultiSelect> arrayListMoneySpent = new ArrayList<ClassScreenItemMultiSelect>();
    //private String[] staffArray = {"a","b","c","other"};
    private ArrayList<ClassScreenItem> arrayListItems = new ArrayList<ClassScreenItem>();
    private ArrayList<ClassScreenItem> staffArray = new ArrayList<ClassScreenItem>();
    private ArrayList<ClassScreenItem> timeSpentArray = new ArrayList<ClassScreenItem>();
    private ArrayList<ClassScreenItem> timeWaitArray = new ArrayList<ClassScreenItem>();
    //changes
    private ArrayList<ClassScreenItem> spendMoneyArray = new ArrayList<ClassScreenItem>();
    private ArrayList<ClassScreenItem> patientSeatedWaitingAreaArray = new ArrayList<ClassScreenItem>();
    private ArrayList<ClassScreenItem> queueAvaibleWaitingAreaArray = new ArrayList<ClassScreenItem>();
    private ArrayList<ClassScreenItem> waitingAreaCleanArray = new ArrayList<ClassScreenItem>();
    private ArrayList<ClassScreenItem> wardCleanArray = new ArrayList<ClassScreenItem>();
    private ArrayList<ClassScreenItem> washroomCleanArray = new ArrayList<ClassScreenItem>();
    private ArrayList<ClassScreenItem> washroomCleanSoapAvaibleArray = new ArrayList<ClassScreenItem>();
    private ArrayList<ClassScreenItem> changeHospitalArray = new ArrayList<ClassScreenItem>();
    private ArrayList<ClassScreenItem> opnionHostialChangeArray = new ArrayList<ClassScreenItem>();
    private ArrayList<ClassScreenItem> recommendHostialChangeArray = new ArrayList<ClassScreenItem>();
    private TextView textViewAbsentStaff;
    private TableLayout tableLayoutAbsentStaff;
    private TableRow.LayoutParams[] paramsArrayTableRowColumns;
    private ArrayList<ClassScreenItem> arrayListTestDone = new ArrayList<ClassScreenItem>();
    ArrayList<ClassScreenItem> arrayListSpinnerData = new ArrayList<ClassScreenItem>();
    //
    private ArrayList<ClassScreenItem> illnessArray = new ArrayList<ClassScreenItem>();


    @SuppressLint("ResourceType")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (parentView == null) {
            parentView = inflater.inflate(R.layout.fragment_patient_interview_page2, container, false);
            /*mGlobals = Globals.getInstance();*/
            initData();
            generateBody();

            if (mGlobals.mJsonObjectFormData.has(Constants.DB_SCREEN_NAME_PATIENT_INTERVIEW_PAGE2)) {
                JSONObject jsonObject = null;
                try {
                    jsonObject = mGlobals.mJsonObjectFormData.getJSONObject(Constants.DB_SCREEN_NAME_PATIENT_INTERVIEW_PAGE2);
                    String stringValue = jsonObject.has("person_listen") ? jsonObject.getString("person_listen") : "";
                    if (stringValue.equals("yes")) {
                        ((RadioButton) radioGroupPerson.getChildAt(0)).setChecked(true);
                    } else if (stringValue.equals("no")) {
                        ((RadioButton) radioGroupPerson.getChildAt(1)).setChecked(true);
                    }

                    stringValue = jsonObject.has("attention_respect") ? jsonObject.getString("attention_respect") : "";
                    if (stringValue.equals("yes")) {
                        ((RadioButton) radioGroupAttentionRespect.getChildAt(0)).setChecked(true);
                    } else if (stringValue.equals("no")) {
                        ((RadioButton) radioGroupAttentionRespect.getChildAt(1)).setChecked(true);
                    }

                    stringValue = jsonObject.has("treatment") ? jsonObject.getString("treatment") : "";
                    if (stringValue.equals("yes")) {
                        ((RadioButton) radioGroupTreatment.getChildAt(0)).setChecked(true);
                    } else if (stringValue.equals("no")) {
                        ((RadioButton) radioGroupTreatment.getChildAt(1)).setChecked(true);
                    }

                    stringValue = jsonObject.has("medical_problems") ? jsonObject.getString("medical_problems") : "";
                    if (stringValue.equals("yes")) {
                        ((RadioButton) radioGroupMedicalProblems.getChildAt(0)).setChecked(true);
                    } else if (stringValue.equals("no")) {
                        ((RadioButton) radioGroupMedicalProblems.getChildAt(1)).setChecked(true);
                    }


//                    JSONArray jsonArray = jsonObject.getJSONArray("test_information");
//                    int arrayCount = 0;
//                    for (int i = 1; i < tableLayoutAbsentStaff.getChildCount(); i++) {
//                        View child = tableLayoutAbsentStaff.getChildAt(i);
//                        boolean bIsOdd = i % 2 == 0;
//                        if (child instanceof TableRow) {
//                            JSONObject jsonObjectChild = jsonArray.getJSONObject(arrayCount++);
//
//                            TableRow tableRow = (TableRow) child;
//                            EditText editTextName = (EditText) tableRow.getChildAt(0);
//                            CustomSpinnerSingleSelect spinnerSingleSelect = (CustomSpinnerSingleSelect) tableRow.getChildAt(1);
//                            CustomToggleButtonTwoStateParent testDone1 = (CustomToggleButtonTwoStateParent) tableRow.getChildAt(2);
//                            ToggleButton testDone = (ToggleButton) tableRow.getChildAt(3);
//                            EditText editTextAmount = (EditText) tableRow.getChildAt(4);
//
//                            editTextName.setText(jsonObjectChild.getString("tests_name"));
//                            editTextAmount.setText(jsonObjectChild.getString("amount_paid"));
//                            String typesOfAbsenceId = jsonObjectChild.getString("test_done_private_public");
//                            String condition = jsonObjectChild.getString("test_done_yes_no");
//                            String condition1 = jsonObjectChild.getString("test_prescribed");
//
//                            if (condition.equals("yes")) {
//                                testDone.setChecked(true);
//                            }
//
//                            if (condition1.equals("yes")) {
//                                testDone1.setChecked(true);
//
//                            }
//
//
//                            if (typesOfAbsenceId.equals("")) {
//                                spinnerSingleSelect.setSelection(0);
//                            }
//
//                            for (int j = 0; j < arrayListSpinnerData.size(); j++) {
//                                if (arrayListSpinnerData.get(j).item_name.equals(typesOfAbsenceId)) {
//                                    spinnerSingleSelect.setSelection(j);
//                                    break;
//                                }
//                            }
//
//                        }
//                    }


                    stringValue = jsonObject.has("money") ? jsonObject.getString("money") : "";
                    if (stringValue.equals("yes")) {
                        ((RadioButton) radioGroupMoney.getChildAt(0)).setChecked(true);
                    } else if (stringValue.equals("no")) {
                        ((RadioButton) radioGroupMoney.getChildAt(1)).setChecked(true);
                    }

                    stringValue = jsonObject.has("illness") ? jsonObject.getString("illness") : "";
                    /*if (!stringValue.equals("")) {
                        for (int i = 0; i < radioGroupIllness.getChildCount(); i++) {
                            if (radioGroupIllness.getChildAt(i).getId() == Integer.parseInt(stringValue)) {
                                ((RadioButton) radioGroupIllness.getChildAt(i)).setChecked(true);
                                break;
                            }
                        }
                    }*/
                    if (!stringValue.equals("")) {
                        if (!stringValue.equals("")) {
                            //spinnerTransport.setSelection(transportArray.indexOf() /*Integer.parseInt(stringValue)*/);
                            int position = 0;
                            for (int k = 0; k < illnessArray.size(); k++) {
                                if (illnessArray.get(k).item_id.equals(stringValue)) {
                                    position = k;
                                    break;

                                }
                            }
                            spinnerIllness.setSelection(position);
                        }
                    }


                    stringValue = jsonObject.has("other_illness") ? jsonObject.getString("other_illness") : "";
                    editTextOtherIllness.setText(stringValue);

                    stringValue = jsonObject.has("change_observed") ? jsonObject.getString("change_observed") : "";

                    if (!stringValue.isEmpty()) {
                        etChangeObserved.setVisibility(View.VISIBLE);
                        etChangeObserved.setText(stringValue);
                    }

                    stringValue = jsonObject.has("instructions_to_visit") ? jsonObject.getString("instructions_to_visit") : "";
                    if (stringValue.equals("yes")) {
                        ((RadioButton) radioGroupInstructions.getChildAt(0)).setChecked(true);
                    } else if (stringValue.equals("no")) {
                        ((RadioButton) radioGroupInstructions.getChildAt(1)).setChecked(true);
                    }

                    stringValue = jsonObject.has("instructions_take_medicines") ? jsonObject.getString("instructions_take_medicines") : "";
                    if (stringValue.equals("yes")) {
                        ((RadioButton) radioGroupInstructionsTakeMedicines.getChildAt(0)).setChecked(true);
                    } else if (stringValue.equals("no")) {
                        ((RadioButton) radioGroupInstructionsTakeMedicines.getChildAt(1)).setChecked(true);
                    }

                    stringValue = jsonObject.has("check_pulse") ? jsonObject.getString("check_pulse") : "";
                    if (stringValue.equals("yes")) {
                        ((RadioButton) radioGroupCheckPulses.getChildAt(0)).setChecked(true);
                    } else if (stringValue.equals("no")) {
                        ((RadioButton) radioGroupCheckPulses.getChildAt(1)).setChecked(true);
                    }


                    stringValue = jsonObject.has("check_temperature") ? jsonObject.getString("check_temperature") : "";
                    if (stringValue.equals("yes")) {
                        ((RadioButton) radioGroupCheckTemperature.getChildAt(0)).setChecked(true);
                    } else if (stringValue.equals("no")) {
                        ((RadioButton) radioGroupCheckTemperature.getChildAt(1)).setChecked(true);
                    }
                    stringValue = jsonObject.has("check_weight") ? jsonObject.getString("check_weight") : "";
                    if (stringValue.equals("yes")) {
                        ((RadioButton) radioGroupCheckWeight.getChildAt(0)).setChecked(true);
                    } else if (stringValue.equals("no")) {
                        ((RadioButton) radioGroupCheckWeight.getChildAt(1)).setChecked(true);
                    }
                    stringValue = jsonObject.has("check_height") ? jsonObject.getString("check_height") : "";
                    if (stringValue.equals("yes")) {
                        ((RadioButton) radioGroupCheckHeight.getChildAt(0)).setChecked(true);
                    } else if (stringValue.equals("no")) {
                        ((RadioButton) radioGroupCheckHeight.getChildAt(1)).setChecked(true);
                    }

                    stringValue = jsonObject.has("check_blood_pressure") ? jsonObject.getString("check_blood_pressure") : "";
                    if (stringValue.equals("yes")) {
                        ((RadioButton) radioGroupCheckBloodPressure.getChildAt(0)).setChecked(true);
                    } else if (stringValue.equals("no")) {
                        ((RadioButton) radioGroupCheckBloodPressure.getChildAt(1)).setChecked(true);
                    }

                    stringValue = jsonObject.has("satisfied_attention") ? jsonObject.getString("satisfied_attention") : "";
                    if (stringValue.equals("yes")) {
                        ((RadioButton) radioGroupSatisfaction.getChildAt(0)).setChecked(true);
                    } else if (stringValue.equals("no")) {
                        ((RadioButton) radioGroupSatisfaction.getChildAt(1)).setChecked(true);
                    }

                    stringValue = jsonObject.has("satisfied_attention") ? jsonObject.getString("satisfied_attention") : "";
                    if (stringValue.equals("yes")) {
                        ((RadioButton) radioGroupSatisfaction.getChildAt(0)).setChecked(true);
                    } else if (stringValue.equals("no")) {
                        ((RadioButton) radioGroupSatisfaction.getChildAt(1)).setChecked(true);
                    }


//                    stringValue = jsonObject.has("recommend_facility") ? jsonObject.getString("recommend_facility") : "";
//                    if (stringValue.equals("yes")) {
//                        ((RadioButton) radioGroupRecommendFacility.getChildAt(0)).setChecked(true);
//                    } else if (stringValue.equals("no")) {
//                        ((RadioButton) radioGroupRecommendFacility.getChildAt(1)).setChecked(true);
//                    }


                    stringValue = jsonObject.has("time_spent") ? jsonObject.getString("time_spent") : "";
                    /*if (!stringValue.equals("")) {
                        for (int i = 0; i < radioGroupTimeSpent.getChildCount(); i++) {
                            if (radioGroupTimeSpent.getChildAt(i).getId() == Integer.parseInt(stringValue)) {
                                ((RadioButton) radioGroupTimeSpent.getChildAt(i)).setChecked(true);
                                break;
                            }
                        }
                    }*/
                    if (!stringValue.equals("")) {
                        if (!stringValue.equals("")) {
                            //spinnerTransport.setSelection(transportArray.indexOf() /*Integer.parseInt(stringValue)*/);
                            int position = 0;
                            for (int k = 0; k < timeSpentArray.size(); k++) {
                                if (timeSpentArray.get(k).item_id.equals(stringValue)) {
                                    position = k;
                                    break;

                                }
                            }
                            spinnerTimeSpent.setSelection(position);
                        }
                    }


                    stringValue = jsonObject.has("time_wait") ? jsonObject.getString("time_wait") : "";
                   /* if (!stringValue.equals("")) {
                        for (int i = 0; i < radioGroupTimeWait.getChildCount(); i++) {
                            if (radioGroupTimeWait.getChildAt(i).getId() == Integer.parseInt(stringValue)) {
                                ((RadioButton) radioGroupTimeWait.getChildAt(i)).setChecked(true);
                                break;
                            }
                        }
                    }*/
                    if (!stringValue.equals("")) {
                        if (!stringValue.equals("")) {
                            //spinnerTransport.setSelection(transportArray.indexOf() /*Integer.parseInt(stringValue)*/);
                            int position = 0;
                            for (int k = 0; k < timeWaitArray.size(); k++) {
                                if (timeWaitArray.get(k).item_id.equals(stringValue)) {
                                    position = k;
                                    break;

                                }
                            }
                            spinnerTimeWait.setSelection(position);
                        }
                    }
//changes

                    stringValue = jsonObject.has("what_money_spend_on") ? jsonObject.getString("what_money_spend_on") : "";
                    if (!stringValue.equals("")) {
                        if (!stringValue.equals("")) {
                            //spinnerTransport.setSelection(transportArray.indexOf() /*Integer.parseInt(stringValue)*/);
                            int position = 0;
                            for (int k = 0; k < spendMoneyArray.size(); k++) {
                                if (spendMoneyArray.get(k).item_id.equals(stringValue)) {
                                    position = k;
                                    break;

                                }
                            }
                            spinnerSpendMoney.setSelection(position);
                        }
                    }


                    stringValue = jsonObject.has("seated_in_waiting_area") ? jsonObject.getString("seated_in_waiting_area") : "";
                    if (!stringValue.equals("")) {
                        if (!stringValue.equals("")) {
                            //spinnerTransport.setSelection(transportArray.indexOf() /*Integer.parseInt(stringValue)*/);
                            int position = 0;
                            for (int k = 0; k < patientSeatedWaitingAreaArray.size(); k++) {
                                if (patientSeatedWaitingAreaArray.get(k).item_id.equals(stringValue)) {
                                    position = k;
                                    break;

                                }
                            }
                            spinnerPatientSeatedWaitingArea.setSelection(position);
                        }
                    }


                    stringValue = jsonObject.has("systematic_way_in_waiting_area_for_queue") ? jsonObject.getString("systematic_way_in_waiting_area_for_queue") : "";
                    if (!stringValue.equals("")) {
                        if (!stringValue.equals("")) {
                            //spinnerTransport.setSelection(transportArray.indexOf() /*Integer.parseInt(stringValue)*/);
                            int position = 0;
                            for (int k = 0; k < queueAvaibleWaitingAreaArray.size(); k++) {
                                if (queueAvaibleWaitingAreaArray.get(k).item_id.equals(stringValue)) {
                                    position = k;
                                    break;

                                }
                            }
                            spinnerQueueAvaible.setSelection(position);
                        }
                    }


                    stringValue = jsonObject.has("waiting_area_floor_clean_litter_free") ? jsonObject.getString("waiting_area_floor_clean_litter_free") : "";
                    if (!stringValue.equals("")) {
                        if (!stringValue.equals("")) {
                            //spinnerTransport.setSelection(transportArray.indexOf() /*Integer.parseInt(stringValue)*/);
                            int position = 0;
                            for (int k = 0; k < waitingAreaCleanArray.size(); k++) {
                                if (waitingAreaCleanArray.get(k).item_id.equals(stringValue)) {
                                    position = k;
                                    break;

                                }
                            }
                            spinnerWaitingAreaClean.setSelection(position);
                        }
                    }


                    stringValue = jsonObject.has("ward_floor_clean_litter_free") ? jsonObject.getString("ward_floor_clean_litter_free") : "";
                    if (!stringValue.equals("")) {
                        if (!stringValue.equals("")) {
                            //spinnerTransport.setSelection(transportArray.indexOf() /*Integer.parseInt(stringValue)*/);
                            int position = 0;
                            for (int k = 0; k < wardCleanArray.size(); k++) {
                                if (wardCleanArray.get(k).item_id.equals(stringValue)) {
                                    position = k;
                                    break;

                                }
                            }
                            spinnerWardClean.setSelection(position);
                        }
                    }


                    stringValue = jsonObject.has("washroom_floor_clean_litter_free") ? jsonObject.getString("washroom_floor_clean_litter_free") : "";
                    if (!stringValue.equals("")) {
                        if (!stringValue.equals("")) {
                            //spinnerTransport.setSelection(transportArray.indexOf() /*Integer.parseInt(stringValue)*/);
                            int position = 0;
                            for (int k = 0; k < washroomCleanArray.size(); k++) {
                                if (washroomCleanArray.get(k).item_id.equals(stringValue)) {
                                    position = k;
                                    break;

                                }
                            }
                            spinnerWashroomClean.setSelection(position);
                        }
                    }


                    stringValue = jsonObject.has("sink_in_washroom_clean_soap_available") ? jsonObject.getString("sink_in_washroom_clean_soap_available") : "";
                    if (!stringValue.equals("")) {
                        if (!stringValue.equals("")) {
                            //spinnerTransport.setSelection(transportArray.indexOf() /*Integer.parseInt(stringValue)*/);
                            int position = 0;
                            for (int k = 0; k < washroomCleanSoapAvaibleArray.size(); k++) {
                                if (washroomCleanSoapAvaibleArray.get(k).item_id.equals(stringValue)) {
                                    position = k;
                                    break;

                                }
                            }
                            spinnerWashroomSoapClean.setSelection(position);
                        }
                    }


                    stringValue = jsonObject.has("observed_any_change_in_hospital") ? jsonObject.getString("observed_any_change_in_hospital") : "";
                    if (!stringValue.equals("")) {
                        if (!stringValue.equals("")) {
                            linearLayoutChangeObserved.setVisibility(View.VISIBLE);
                            //spinnerTransport.setSelection(transportArray.indexOf() /*Integer.parseInt(stringValue)*/);
                            int position = 0;
                            for (int k = 0; k < changeHospitalArray.size(); k++) {
                                if (changeHospitalArray.get(k).item_id.equals(stringValue)) {
                                    position = k;
                                    break;

                                }
                            }
                            spinnerObservedChange.setSelection(position);
                        }
                    }

                    String stringValue1 = "";
                    stringValue1 = jsonObject.has("opinion_what_changed_at_hospital") ? jsonObject.getString("opinion_what_changed_at_hospital") : "";
                    if (!stringValue1.equals("")) {
                        if (!stringValue1.equals("")) {
                            linearLayoutOpnion.setVisibility(View.VISIBLE);
                            //spinnerTransport.setSelection(transportArray.indexOf() /*Integer.parseInt(stringValue)*/);
                            int position = 0;
                            for (int k = 0; k < opnionHostialChangeArray.size(); k++) {
                                if (opnionHostialChangeArray.get(k).item_id.equals(stringValue1)) {
                                    position = k;
                                    break;

                                }
                            }
                            spinnerOpinionWhatChangedAtHospital.setSelection(position);
                        }
                    }


                    spinnerRecommendation.setSelection(Arrays.asList(arrayListRecommendation).indexOf(jsonObject.getString("recommend_this_hospital")));


//                    stringValue = jsonObject.has("recommend_this_hospital ") ? jsonObject.getString("recommend_this_hospital ") : "";
//                    if (!stringValue.equals("")) {
//                        if (!stringValue.equals("")) {
//                            //spinnerTransport.setSelection(transportArray.indexOf() /*Integer.parseInt(stringValue)*/);
//                            int position = 0;
//                            for (int k = 0; k < recommendHostialChangeArray.size(); k++) {
//                                if (recommendHostialChangeArray.get(k).item_id.equals(stringValue)) {
//                                    position = k;
//                                    break;
//
//                                }
//                            }
//                            spinnerRecommendation.setSelection(position);
//                        }
//                    }

                    stringValue = jsonObject.has("see_today") ? jsonObject.getString("see_today") : "";
                    if (!stringValue.equals("")) {
                        if (!stringValue.equals("")) {
                            //spinnerTransport.setSelection(transportArray.indexOf() /*Integer.parseInt(stringValue)*/);
                            int position = 0;
                            for (int k = 0; k < staffArray.size(); k++) {
                                if (staffArray.get(k).item_id.equals(stringValue)) {
                                    position = k;
                                    break;

                                }
                            }
                            spinnerStaff.setSelection(position);
                        }
                    }
                    //spinnerStaff.setSelection(Integer.parseInt(stringValue));

                    /*stringValue = jsonObject.has("other_staff")? jsonObject.getString("other_staff"): "";
                    editTextRemarks.setText(stringValue);*/

                    /*stringValue = jsonObject.has("money_spent") ? jsonObject.getString("money_spent"):"";
                    if (!stringValue.equals("")) {
                        List<String> list = Arrays.asList(stringValue.split(","));
                        for (int i = 0; i < arrayListMoneySpent.size(); i++) {
                            if (list.contains(arrayListMoneySpent.get(i).item.item_id)) {
                                arrayListMoneySpent.get(i).bIsSelected = true;
                            }
                        }
                        setSpinnerAdapter(spinnerMoneySpent, "Please select the money spent on", arrayListMoneySpent, list.size());
                    }*/

                    /*stringValue = jsonObject.has("other_money_spent") ? jsonObject.getString("other_money_spent"):"";
                    editTextMoneySpent.setText(stringValue);*/

                   /* stringValue = jsonObject.has("seek_treatment_remarks")? jsonObject.getString("seek_treatment_remarks"):"";
                    editTextSeekTreatment.setText(stringValue);*/

                    stringValue = jsonObject.has("medicine_quantity") ? jsonObject.getString("medicine_quantity") : "";
                    editTextMedicineQuantity.setText(stringValue);


                    stringValue = jsonObject.has("facility_dispensary") ? jsonObject.getString("facility_dispensary") : "";
                    editTextFacilityDispensary.setText(stringValue);

                    stringValue = jsonObject.has("comment") ? jsonObject.getString("comment") : "";
                    editTextComments.setText(stringValue);

                    stringValue = jsonObject.has("referred_hospital") ? jsonObject.getString("referred_hospital") : "";
                    if (stringValue.equals("yes")) {
                        ((RadioButton) radioGroupReferredHospital.getChildAt(0)).setChecked(true);
                    } else if (stringValue.equals("no")) {
                        ((RadioButton) radioGroupReferredHospital.getChildAt(1)).setChecked(true);
                    }

                    stringValue = jsonObject.has("referred_hospital_private_public") ? jsonObject.getString("referred_hospital_private_public") : "";
                    if (stringValue.equals("public")) {
                        ((RadioButton) radioGroupReferredHospitalPrivatePublic.getChildAt(0)).setChecked(true);
                    } else if (stringValue.equals("private")) {
                        ((RadioButton) radioGroupReferredHospitalPrivatePublic.getChildAt(1)).setChecked(true);
                    }

                    stringValue = jsonObject.has("referred_facility") ? jsonObject.getString("referred_facility") : "";
                    if (stringValue.equals("yes")) {
                        ((RadioButton) radioGroupReferredFacility.getChildAt(0)).setChecked(true);
                    } else if (stringValue.equals("no")) {
                        ((RadioButton) radioGroupReferredFacility.getChildAt(1)).setChecked(true);
                    }
                    stringValue = jsonObject.has("referred_facility_private_public") ? jsonObject.getString("referred_facility_private_public") : "";
                    if (stringValue.equals("public")) {
                        ((RadioButton) radioGroupReferredFacilityPrivatePublic.getChildAt(0)).setChecked(true);
                    } else if (stringValue.equals("private")) {
                        ((RadioButton) radioGroupReferredFacilityPrivatePublic.getChildAt(1)).setChecked(true);
                    }

                    stringValue = jsonObject.has("test_done") ? jsonObject.getString("test_done") : "";
                    if (stringValue.equals("yes")) {
                        ((RadioButton) radioGroupTestDone.getChildAt(0)).setChecked(true);
                    } else if (stringValue.equals("no")) {
                        ((RadioButton) radioGroupTestDone.getChildAt(1)).setChecked(true);
                    }

//                    stringValue = jsonObject.has("test_done_private_public") ? jsonObject.getString("test_done_private_public") : "";
//                    if (stringValue.equals("public")) {
//                        ((RadioButton) radioGroupTestDonePrivatePublic.getChildAt(0)).setChecked(true);
//                    } else if (stringValue.equals("private")) {
//                        ((RadioButton) radioGroupTestDonePrivatePublic.getChildAt(1)).setChecked(true);
//                    }

//changes
                    stringValue = jsonObject.has("person_attitude") ? jsonObject.getString("person_attitude") : "";
                    if (stringValue.equals("yes")) {
                        ((RadioButton) radioGroupPersonAttitude.getChildAt(0)).setChecked(true);
                    } else if (stringValue.equals("no")) {
                        ((RadioButton) radioGroupPersonAttitude.getChildAt(1)).setChecked(true);
                    }

                    stringValue = jsonObject.has("hospital_change_positive_negative") ? jsonObject.getString("hospital_change_positive_negative") : "";
                    if (stringValue != null && !stringValue.equals("")) {
                        linearLayoutChangedPostiveNegative.setVisibility(View.VISIBLE);
                        if (stringValue.equals("positive")) {
                            ((RadioButton) radioGroupHosipitalChangePositiveNegative.getChildAt(0)).setChecked(true);
                            linearLayoutOpnion.setVisibility(View.VISIBLE);
                            linearLayoutEditexOption.setVisibility(View.GONE);
                        } else if (stringValue.equals("negative")) {

                            ((RadioButton) radioGroupHosipitalChangePositiveNegative.getChildAt(1)).setChecked(true);
                            linearLayoutEditexOption.setVisibility(View.VISIBLE);
                            linearLayoutOpnion.setVisibility(View.GONE);
                        }
                    }


                    stringValue = jsonObject.has("facility_visited_in_one_year") ? jsonObject.getString("facility_visited_in_one_year") : "";
                    if (stringValue.equals("yes")) {
                        ((RadioButton) radioGroupFacilityVisitedPreviously.getChildAt(0)).setChecked(true);
                    } else if (stringValue.equals("no")) {
                        ((RadioButton) radioGroupFacilityVisitedPreviously.getChildAt(1)).setChecked(true);
                    }


                    stringValue = jsonObject.has("tests_name") ? jsonObject.getString("tests_name") : "";
                    if (!stringValue.equals("")) {
                        String[] stringArray = stringValue.split(",");
                        ArrayList<MultiSelectDialog.ItemMultiSelect> totalList = multiSelectDialogTests.getArrayListOriginalItems();
                        for (int i = 0; i < totalList.size(); i++) {
                            for (int j = 0; j < stringArray.length; j++) {
                                if (totalList.get(i).getObject().equals(stringArray[j])) {
                                    totalList.get(i).setSelected(true);
                                    break;
                                }
                            }
                        }

                        multiSelectDialogTests.initData(totalList);
                    }

//                    stringValue = jsonObject.has("aware") ? jsonObject.getString("aware") : "";
//                    if (stringValue.equals("yes")) {
//                        ((RadioButton) radioGroupAware.getChildAt(0)).setChecked(true);
//                    } else if (stringValue.equals("no")) {
//                        ((RadioButton) radioGroupAware.getChildAt(1)).setChecked(true);
//                    }

//                    stringValue = jsonObject.has("opinions") ? jsonObject.getString("opinions") : "";
//                    if (!stringValue.equals("")) {
//                        String[] stringArray = stringValue.split(",");
//                        ArrayList<MultiSelectDialog.ItemMultiSelect> totalList = multiSelectDialogOpinion.getArrayListOriginalItems();
//                        for (int i = 0; i < totalList.size(); i++) {
//                            for (int j = 0; j < stringArray.length; j++) {
//                                if (totalList.get(i).getObject().equals(stringArray[j])) {
//                                    totalList.get(i).setSelected(true);
//                                    break;
//                                }
//                            }
//                        }
//                        multiSelectDialogOpinion.initData(totalList);
//                    }


//                    stringValue = jsonObject.has("amount_paid") ? jsonObject.getString("amount_paid") : "";
//                    editTextAmountPaid.setText(stringValue);

//change
                    stringValue = jsonObject.has("change_observed") ? jsonObject.getString("change_observed") : "";
                    etChangeObserved.setText(stringValue);


                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }


        }
        return parentView;
    }

    private void initData() {
        try {
            ClassScreenData objectScreenData = new ClassScreenData();
            objectScreenData.setDefaultValues();
            objectScreenData.type_id = "";
            objectScreenData.type_name = "";

            objectScreenData.screen_id = Constants.DB_SCREEN_ID_PATIENT_INTERVIEWS;
            objectScreenData.screen_name = Constants.DB_TYPE_NAME_PATIENT_INTERVIEWS;

            objectScreenData.section_id = "1";
            objectScreenData.section_name = "section1";
            objectScreenData.section_value = "money_spend_on";
        /*objectScreenData = mGlobals.mDbAdapter.selectScreenData(objectScreenData);
        arrayListItems = mGlobals.mDbAdapter.selectScreenItems(objectScreenData);
        for (int i = 0 ; i <arrayListItems.size(); i++){
            ClassScreenItemMultiSelect objectSelect = new ClassScreenItemMultiSelect(arrayListItems.get(i), false);
            arrayListMoneySpent.add(objectSelect);
        }*/

            objectScreenData.section_value = "see_today";
            objectScreenData = mGlobals.mDbAdapter.selectScreenData(objectScreenData);
            staffArray = mGlobals.mDbAdapter.selectScreenItems(objectScreenData);
            staffArray.add(0, new ClassScreenItem("0", "Select the relevant option"));

            objectScreenData.section_value = "time_spend";
            objectScreenData = mGlobals.mDbAdapter.selectScreenData(objectScreenData);
            timeSpentArray = mGlobals.mDbAdapter.selectScreenItems(objectScreenData);
            timeSpentArray.add(0, new ClassScreenItem("0", "Select the relevant option"));

            objectScreenData.section_value = "wait_for_person";
            objectScreenData = mGlobals.mDbAdapter.selectScreenData(objectScreenData);
            timeWaitArray = mGlobals.mDbAdapter.selectScreenItems(objectScreenData);
            timeWaitArray.add(0, new ClassScreenItem("0", "Select the relevant option"));


            objectScreenData.section_value = "illness";
            objectScreenData = mGlobals.mDbAdapter.selectScreenData(objectScreenData);
            illnessArray = mGlobals.mDbAdapter.selectScreenItems(objectScreenData);
            illnessArray.add(0, new ClassScreenItem("0", "Select the relevant option"));

            //changes

            objectScreenData.section_value = "what_money_spend_on";
            objectScreenData = mGlobals.mDbAdapter.selectScreenData(objectScreenData);
            spendMoneyArray = mGlobals.mDbAdapter.selectScreenItems(objectScreenData);
            spendMoneyArray.add(0, new ClassScreenItem("0", "Select the relevant option"));


            objectScreenData.section_value = "seated_in_waiting_area";
            objectScreenData = mGlobals.mDbAdapter.selectScreenData(objectScreenData);
            patientSeatedWaitingAreaArray = mGlobals.mDbAdapter.selectScreenItems(objectScreenData);
            patientSeatedWaitingAreaArray.add(0, new ClassScreenItem("0", "Select the relevant option"));


            objectScreenData.section_value = "systematic_way_in_waiting_area_for_queue";
            objectScreenData = mGlobals.mDbAdapter.selectScreenData(objectScreenData);
            queueAvaibleWaitingAreaArray = mGlobals.mDbAdapter.selectScreenItems(objectScreenData);
            queueAvaibleWaitingAreaArray.add(0, new ClassScreenItem("0", "Select the relevant option"));


            objectScreenData.section_value = "waiting_area_floor_clean_litter_free";
            objectScreenData = mGlobals.mDbAdapter.selectScreenData(objectScreenData);
            waitingAreaCleanArray = mGlobals.mDbAdapter.selectScreenItems(objectScreenData);
            waitingAreaCleanArray.add(0, new ClassScreenItem("0", "Select the relevant option"));


            objectScreenData.section_value = "ward_floor_clean_litter_free";
            objectScreenData = mGlobals.mDbAdapter.selectScreenData(objectScreenData);
            wardCleanArray = mGlobals.mDbAdapter.selectScreenItems(objectScreenData);
            wardCleanArray.add(0, new ClassScreenItem("0", "Select the relevant option"));


            objectScreenData.section_value = "washroom_floor_clean_litter_free";
            objectScreenData = mGlobals.mDbAdapter.selectScreenData(objectScreenData);
            washroomCleanArray = mGlobals.mDbAdapter.selectScreenItems(objectScreenData);
            washroomCleanArray.add(0, new ClassScreenItem("0", "Select the relevant option"));


            objectScreenData.section_value = "sink_in_washroom_clean_soap_available";
            objectScreenData = mGlobals.mDbAdapter.selectScreenData(objectScreenData);
            washroomCleanSoapAvaibleArray = mGlobals.mDbAdapter.selectScreenItems(objectScreenData);
            washroomCleanSoapAvaibleArray.add(0, new ClassScreenItem("0", "Select the relevant option"));

            objectScreenData.section_value = "observed_any_change_in_hospital";
            objectScreenData = mGlobals.mDbAdapter.selectScreenData(objectScreenData);
            changeHospitalArray = mGlobals.mDbAdapter.selectScreenItems(objectScreenData);
            changeHospitalArray.add(0, new ClassScreenItem("0", "Select the relevant option"));

            objectScreenData.section_value = "opinion_what_changed_at_hospital";
            objectScreenData = mGlobals.mDbAdapter.selectScreenData(objectScreenData);
            opnionHostialChangeArray = mGlobals.mDbAdapter.selectScreenItems(objectScreenData);
            opnionHostialChangeArray.add(0, new ClassScreenItem("0", "Select the relevant option"));

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    private void generateBody() {
        layoutMoney = (LinearLayout) parentView.findViewById(R.id.layout_money_spent);
        linearLayoutIllnessOther = (LinearLayout) parentView.findViewById(R.id.layout_illness_other);
        linearLayoutMain = (LinearLayout) parentView.findViewById(R.id.linearlayout_main);

        arrayListSpinnerData.add(new ClassScreenItem("0", "Select Sector"));
        arrayListSpinnerData.add(new ClassScreenItem("1", "Public"));
        arrayListSpinnerData.add(new ClassScreenItem("2", "Private"));
        UIHelper uiContainer = new UIHelper();

        radioGroupTreatment = (RadioGroup) parentView.findViewById(R.id.radio_group_treatment);
        radioGroupAttentionRespect = (RadioGroup) parentView.findViewById(R.id.radio_group_attention_respect);
        radioGroupPerson = (RadioGroup) parentView.findViewById(R.id.radio_group_person);
//        radioGroupAware = (RadioGroup) parentView.findViewById(R.id.radio_group_aware);
        radioGroupMedicalProblems = (RadioGroup) parentView.findViewById(R.id.radio_group_medical_problems);
        /*radioGroupSeekTreatment = (RadioGroup) parentView.findViewById(R.id.radio_group_seek_treatment);*/
        radioGroupMoney = (RadioGroup) parentView.findViewById(R.id.radio_group_money);
        /*radioGroupLabTest = (RadioGroup) parentView.findViewById(R.id.radio_group_lab_test);*/
        /*radioGroupTimeSpent = (RadioGroup) parentView.findViewById(R.id.radio_group_time_spent);
        radioGroupTimeWait = (RadioGroup) parentView.findViewById(R.id.radio_group_time_wait);


        radioGroupIllness = (RadioGroup) parentView.findViewById(R.id.radio_group_illness);*/
        radioGroupInstructions = (RadioGroup) parentView.findViewById(R.id.radio_group_instructions);
        radioGroupInstructionsTakeMedicines = (RadioGroup) parentView.findViewById(R.id.radio_group_instructions_take_medicines);
        radioGroupCheckPulses = (RadioGroup) parentView.findViewById(R.id.radio_group_check_pulse);
        radioGroupCheckBloodPressure = (RadioGroup) parentView.findViewById(R.id.radio_group_check_blood_pressure);
        radioGroupCheckTemperature = (RadioGroup) parentView.findViewById(R.id.radio_group_check_temperature);
        radioGroupCheckWeight = (RadioGroup) parentView.findViewById(R.id.radio_group_check_weight);
        radioGroupCheckHeight = (RadioGroup) parentView.findViewById(R.id.radio_group_check_height);
        radioGroupSatisfaction = (RadioGroup) parentView.findViewById(R.id.radio_group_satisfaction);
        //radioGroupRecommendFacility = (RadioGroup) parentView.findViewById(R.id.radio_group_recommend_facility);
        radioGroupReferredHospital = (RadioGroup) parentView.findViewById(R.id.radio_group_referred_hospital);
        radioGroupReferredHospitalPrivatePublic = (RadioGroup) parentView.findViewById(R.id.radio_group_hospital_public_private);
        radioGroupReferredFacility = (RadioGroup) parentView.findViewById(R.id.radio_group_referred_facility);
        radioGroupReferredFacilityPrivatePublic = (RadioGroup) parentView.findViewById(R.id.radio_group_facility_public_private);
        radioGroupTestDone = (RadioGroup) parentView.findViewById(R.id.radio_group_test_done);
        //  radioGroupTestDonePrivatePublic = (RadioGroup) parentView.findViewById(R.id.radio_group_test_public_private);
        //changes
        radioGroupPersonAttitude = (RadioGroup) parentView.findViewById(R.id.radio_group_person_attitude);
        radioGroupFacilityVisitedPreviously = (RadioGroup) parentView.findViewById(R.id.radio_group_facility_visited_previously);
        radioGroupHosipitalChangePositiveNegative = (RadioGroup) parentView.findViewById(R.id.radio_group_change_positive_negative);

        //  multiSelectDialogTests = (MultiSelectDialog) parentView.findViewById(R.id.multiselect_test_done);
//        multiSelectDialogOpinion = (MultiSelectDialog) parentView.findViewById(R.id.multiselect_opinion);
        //editTextAmountPaid = (EditText) parentView.findViewById(R.id.et_amount_piad);
        //change

        etChangeObserved = (EditText) parentView.findViewById(R.id.et_change_observed);
        // linearLayoutTestDone = (LinearLayout) parentView.findViewById(R.id.ll_test_done);

        //  changes

        linearLayoutChangeObserved = (LinearLayout) parentView.findViewById(R.id.ll_change_observed);
        linearLayoutChangedPostiveNegative = (LinearLayout) parentView.findViewById(R.id.ll_changed_postive_negative);
        linearLayoutOpnion = (LinearLayout) parentView.findViewById(R.id.ll_opnion);
        linearLayoutEditexOption = (LinearLayout) parentView.findViewById(R.id.ll_editexOption);


        spinnerStaff = (Spinner) parentView.findViewById(R.id.spinner_staff);
        spinnerIllness = (Spinner) parentView.findViewById(R.id.spinner_illness);
        spinnerTimeWait = (Spinner) parentView.findViewById(R.id.spinner_time_wait);
        spinnerTimeSpent = (Spinner) parentView.findViewById(R.id.spinner_time_spent);
        //changes
        spinnerSpendMoney = (Spinner) parentView.findViewById(R.id.spinner_spend_money);
        spinnerPatientSeatedWaitingArea = (Spinner) parentView.findViewById(R.id.spinner_patient_seated_waiting_area);
        spinnerQueueAvaible = (Spinner) parentView.findViewById(R.id.spinner_waiting_area_queue);
        spinnerWaitingAreaClean = (Spinner) parentView.findViewById(R.id.spinner_waiting_area_clean);
        spinnerWardClean = (Spinner) parentView.findViewById(R.id.spinner_ward_clean);
        spinnerWashroomClean = (Spinner) parentView.findViewById(R.id.spinner_washroom_clean);
        spinnerWashroomSoapClean = (Spinner) parentView.findViewById(R.id.spinner_washroom_soap);
        spinnerObservedChange = (Spinner) parentView.findViewById(R.id.spinner_observed_change);
        spinnerOpinionWhatChangedAtHospital = (Spinner) parentView.findViewById(R.id.spinner_opinion_what_changed_at_hospital);
        spinnerRecommendation = (Spinner) parentView.findViewById(R.id.spinner_recommendation);





        /*spinnerMoneySpent = (CustomSpinnerMultiSelect) parentView.findViewById(R.id.spinner_money_spent);*/

        /*editTextRemarks = (EditText) parentView.findViewById(R.id.et_remarks);*/
        editTextTimeSpent = (EditText) parentView.findViewById(R.id.et_time_spent);
        editTextTimeWait = (EditText) parentView.findViewById(R.id.et_time_wait);
        editTextComments = (EditText) parentView.findViewById(R.id.et_comments);


        /*editTextMoneySpent = (EditText) parentView.findViewById(R.id.et_money_spent);*/
        /*editTextSeekTreatment = (EditText) parentView.findViewById(R.id.et_seek_treatment);*/

        editTextMedicineQuantity = (EditText) parentView.findViewById(R.id.et_medicine_quantity);
        editTextFacilityDispensary = (EditText) parentView.findViewById(R.id.et_facility_dispensary);
        editTextOtherIllness = (EditText) parentView.findViewById(R.id.et_illness_other);


        editTextMedicineQuantity.addTextChangedListener(new TextWatcherTwentyCharacters(editTextMedicineQuantity));
        editTextFacilityDispensary.addTextChangedListener(new TextWatcherTwentyCharacters(editTextFacilityDispensary));


        editTextMedicineQuantity.addTextChangedListener(new TextWatcherCountSingleChild(editTextMedicineQuantity,
                editTextFacilityDispensary, "Prescribed medicines ", "the medicines got from the facility dispensary"));
        editTextFacilityDispensary.addTextChangedListener(new TextWatcherCountSingleChild(editTextMedicineQuantity,
                editTextFacilityDispensary, "Prescribed medicines ", "the medicines got from the facility dispensary"));


        buttonProceed = (Button) parentView.findViewById(R.id.button_patient_experience);

        LinearLayout.LayoutParams lpButtonProceed = new LinearLayout.LayoutParams((int) (mGlobals.mScreenWidth * 0.5),
                (int) (mGlobals.mScreenWidth * 0.1));
        lpButtonProceed.setMargins(0, (int) (mGlobals.mScreenHeight * 0.05), 0, 0);
        buttonProceed.setLayoutParams(lpButtonProceed);

        lpSpinner = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, (int) (mGlobals.mScreenWidth * 0.1));
        lpSpinner.setMargins(0, 10, 0, 0);
        spinnerStaff.setLayoutParams(lpSpinner);
        spinnerIllness.setLayoutParams(lpSpinner);
        spinnerTimeWait.setLayoutParams(lpSpinner);
        spinnerTimeSpent.setLayoutParams(lpSpinner);
        //changes
        spinnerSpendMoney.setLayoutParams(lpSpinner);
        spinnerPatientSeatedWaitingArea.setLayoutParams(lpSpinner);
        spinnerQueueAvaible.setLayoutParams(lpSpinner);
        spinnerWaitingAreaClean.setLayoutParams(lpSpinner);
        spinnerWardClean.setLayoutParams(lpSpinner);
        spinnerWashroomClean.setLayoutParams(lpSpinner);
        spinnerWashroomSoapClean.setLayoutParams(lpSpinner);
        spinnerObservedChange.setLayoutParams(lpSpinner);
        spinnerOpinionWhatChangedAtHospital.setLayoutParams(lpSpinner);
        //   spinnerRecommendation.setLayoutParams(lpSpinner);

        /*spinnerMoneySpent.setLayoutParams(lpSpinner);*/


        buttonProceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isFormValid()) {

                    mGlobals.activityCallBackPatientInterviews.submitData();
                } else {
                    AlertDialogs.getInstance().showDialogOK(null, errorMessage, null, true);
                }
            }
        });

        setSpinnerAdapter(spinnerStaff, staffArray);
        setSpinnerAdapter(spinnerIllness, illnessArray);
        setSpinnerAdapter(spinnerTimeWait, timeWaitArray);
        setSpinnerAdapter(spinnerTimeSpent, timeSpentArray);
        //update
        setSpinnerAdapter(spinnerSpendMoney, spendMoneyArray);
        setSpinnerAdapter(spinnerPatientSeatedWaitingArea, patientSeatedWaitingAreaArray);
        setSpinnerAdapter(spinnerQueueAvaible, queueAvaibleWaitingAreaArray);
        setSpinnerAdapter(spinnerWaitingAreaClean, waitingAreaCleanArray);
        setSpinnerAdapter(spinnerWardClean, wardCleanArray);
        setSpinnerAdapter(spinnerWashroomClean, washroomCleanArray);
        setSpinnerAdapter(spinnerWashroomSoapClean, washroomCleanSoapAvaibleArray);
        setSpinnerAdapter(spinnerObservedChange, changeHospitalArray);
        setSpinnerAdapter(spinnerOpinionWhatChangedAtHospital, opnionHostialChangeArray);

        ArrayAdapterSimpleSpinner adapterCallDeposition = new ArrayAdapterSimpleSpinner(mGlobals.mContext, android.R.layout.simple_spinner_item, arrayListRecommendation);
        adapterCallDeposition.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerRecommendation.setAdapter(adapterCallDeposition);

        //setSpinnerAdapter(spinnerRecommendation,recommendHostialChangeArray );


        spinnerIllness.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i == illnessArray.size() - 1) {
                    linearLayoutIllnessOther.setVisibility(View.VISIBLE);
                } else {
                    linearLayoutIllnessOther.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        radioGroupMoney.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (radioGroupMoney.getCheckedRadioButtonId() == radioGroupMoney.getChildAt(0).getId()) {
                    layoutMoney.setVisibility(View.VISIBLE);
                } else {
                    layoutMoney.setVisibility(View.GONE);
                }
            }
        });

        radioGroupReferredFacility.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (i == R.id.rb_yes_facility) {
                    radioGroupReferredFacilityPrivatePublic.setVisibility(View.VISIBLE);
                } else {
                    radioGroupReferredFacilityPrivatePublic.setVisibility(View.GONE);
                }
            }
        });


        radioGroupFacilityVisitedPreviously.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {


                if (i == R.id.rb_facility_visited_yes) {
                    linearLayoutChangeObserved.setVisibility(View.VISIBLE);
                    linearLayoutChangedPostiveNegative.setVisibility(View.GONE);
                    linearLayoutOpnion.setVisibility(View.GONE);
                    linearLayoutEditexOption.setVisibility(View.GONE);
                    // spinnerObservedChange.setSelection(0);
                    spinnerOpinionWhatChangedAtHospital.setSelection(0);


                } else {
                    linearLayoutChangeObserved.setVisibility(View.GONE);
                    linearLayoutChangedPostiveNegative.setVisibility(View.GONE);
                    linearLayoutOpnion.setVisibility(View.GONE);
                    linearLayoutEditexOption.setVisibility(View.GONE);
                    spinnerObservedChange.setSelection(0);
                    spinnerOpinionWhatChangedAtHospital.setSelection(0);

                }

            }
        });

        spinnerObservedChange.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i == 1) {
                    linearLayoutChangedPostiveNegative.setVisibility(View.VISIBLE);
                    radioGroupHosipitalChangePositiveNegative.clearCheck();
                } else {
                    linearLayoutChangedPostiveNegative.setVisibility(View.GONE);
                    radioGroupHosipitalChangePositiveNegative.clearCheck();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        radioGroupHosipitalChangePositiveNegative.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {


                if (i == R.id.rb_change_yes) {
                    linearLayoutOpnion.setVisibility(View.VISIBLE);
                    linearLayoutEditexOption.setVisibility(View.GONE);
                } else if (i == R.id.rb_change_no) {
                    linearLayoutOpnion.setVisibility(View.GONE);
                    linearLayoutEditexOption.setVisibility(View.VISIBLE);
                } else {
                    linearLayoutOpnion.setVisibility(View.GONE);
                    linearLayoutEditexOption.setVisibility(View.GONE);
                }

            }
        });

        radioGroupTestDone.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (i == R.id.rb_yes_test) {
                    linearLayoutMain.setVisibility(View.VISIBLE);
                } else {
                    linearLayoutMain.setVisibility(View.GONE);
                }
            }
        });

    }

    @Override
    public void onFragmentShown() {

    }

    @Override
    public boolean isFormValid() {
        if (spinnerIllness.getSelectedItemPosition() == 0 && spinnerIllness.getVisibility() == View.VISIBLE) {
            errorMessage = "Please select the option for the question \"What was your illness?\" .";
            return false;
        }
        if (radioGroupTreatment.getCheckedRadioButtonId() == -1 && radioGroupTreatment.getVisibility() == View.VISIBLE) {
            errorMessage = "Please select yes or no for the question of help/treatment of the patient of the day of his/her visit to the facility.";
            return false;
        }
        if (spinnerTimeWait.getSelectedItemPosition() == 0 && spinnerTimeWait.getVisibility() == View.VISIBLE) {
            errorMessage = "Please select the option for the question \"How long did you have to wait to see clinical personnel/doctor?\" .";
            return false;
        }
        if (spinnerTimeSpent.getSelectedItemPosition() == 0 && spinnerTimeSpent.getVisibility() == View.VISIBLE) {
            errorMessage = "Please select the option for the question \"How much time did the person spend with you?\" .";
            return false;
        }
        if (radioGroupMedicalProblems.getCheckedRadioButtonId() == -1 && radioGroupMedicalProblems.getVisibility() == View.VISIBLE) {
            errorMessage = "Please select yes or no for the question of past medical problems.";
            return false;
        }
        if (editTextMedicineQuantity.getVisibility() == View.VISIBLE && editTextMedicineQuantity.getText().toString().equals("")) {
            errorMessage = "Please enter the prescribed medicine quantity";
            return false;
        }
        if (editTextFacilityDispensary.getVisibility() == View.VISIBLE && editTextFacilityDispensary.getText().toString().equals("")) {
            errorMessage = "Please enter the quantity of medicines available from facility dispensary.";
            return false;
        }
        if (radioGroupInstructionsTakeMedicines.getCheckedRadioButtonId() == -1 && radioGroupInstructionsTakeMedicines.getVisibility() == View.VISIBLE) {
            errorMessage = "Please select yes or no for the question of instructions on how and when to take the prescribed medicines";
            return false;
        }
        if (radioGroupCheckPulses.getCheckedRadioButtonId() == -1 && radioGroupCheckPulses.getVisibility() == View.VISIBLE) {
            errorMessage = "Please select yes or no for the question of pulse checked by the clinical person";
            return false;
        }
        if (radioGroupCheckBloodPressure.getCheckedRadioButtonId() == -1 && radioGroupCheckBloodPressure.getVisibility() == View.VISIBLE) {
            errorMessage = "Please select yes or no for the question of blood pressure checked by the clinical person";
            return false;
        }
        if (radioGroupCheckTemperature.getCheckedRadioButtonId() == -1 && radioGroupCheckTemperature.getVisibility() == View.VISIBLE) {
            errorMessage = "Please select yes or no for the question of temperature checked by the clinical person";
            return false;
        }
        if (radioGroupCheckWeight.getCheckedRadioButtonId() == -1 && radioGroupCheckWeight.getVisibility() == View.VISIBLE) {
            errorMessage = "Please select yes or no for the question of weight checked by the clinical person";
            return false;
        }
        if (radioGroupCheckHeight.getCheckedRadioButtonId() == -1 && radioGroupCheckHeight.getVisibility() == View.VISIBLE) {
            errorMessage = "Please select yes or no for the question of height checked by the clinical person";
            return false;
        }
        if (spinnerStaff.getSelectedItemPosition() == 0 && spinnerStaff.getVisibility() == View.VISIBLE) {
            errorMessage = "Please select the option for the question \"Who did you see today?\" .";
            return false;
        }
        if (radioGroupReferredHospital.getCheckedRadioButtonId() == -1 && radioGroupReferredHospital.getVisibility() == View.VISIBLE) {
            errorMessage = "Please select yes or no for the question of referred from another health facility to this hospital";
            return false;
        }
        if (radioGroupTestDone.getCheckedRadioButtonId() == -1 && radioGroupTestDone.getVisibility() == View.VISIBLE) {
            errorMessage = "Please select yes or no for the question of tests done";
            return false;
        }
        if (spinnerPatientSeatedWaitingArea.getSelectedItemPosition() == 0 && spinnerPatientSeatedWaitingArea.getVisibility() == View.VISIBLE) {
            errorMessage = "Please select the option for the question \"Were you seated in a waiting area before you saw the doctor \" .";
            return false;
        }
        return true;
    }

    @Override
    public String onFragmentChanged(int previousPosition) {
        errorMessage = null;
        isFormValid();
        return errorMessage;

    }

    @Override
    public void parseObject() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("other_illness", "");
            jsonObject.put("change_observed", "");
            if (linearLayoutIllnessOther.getVisibility() == View.VISIBLE) {
                jsonObject.put("other_illness", editTextOtherIllness.getText().toString());
            }

            if (radioGroupInstructions.getCheckedRadioButtonId() == radioGroupInstructions.getChildAt(0).getId()) {
                jsonObject.put("instructions_to_visit", "yes");
            } else if (radioGroupInstructions.getCheckedRadioButtonId() == radioGroupInstructions.getChildAt(1).getId()) {
                jsonObject.put("instructions_to_visit", "no");

            } else {
                jsonObject.put("instructions_to_visit", "");

            }

            if (radioGroupInstructionsTakeMedicines.getCheckedRadioButtonId() == radioGroupInstructionsTakeMedicines.getChildAt(0).getId()) {
                jsonObject.put("instructions_take_medicines", "yes");
            } else if (radioGroupInstructionsTakeMedicines.getCheckedRadioButtonId() == radioGroupInstructionsTakeMedicines.getChildAt(1).getId()) {
                jsonObject.put("instructions_take_medicines", "no");

            } else {
                jsonObject.put("instructions_take_medicines", "");

            }


            if (radioGroupCheckPulses.getCheckedRadioButtonId() == radioGroupCheckPulses.getChildAt(0).getId()) {
                jsonObject.put("check_pulse", "yes");
            } else if (radioGroupCheckPulses.getCheckedRadioButtonId() == radioGroupCheckPulses.getChildAt(1).getId()) {
                jsonObject.put("check_pulse", "no");

            } else {
                jsonObject.put("check_pulse", "");

            }


            if (radioGroupCheckTemperature.getCheckedRadioButtonId() == radioGroupCheckTemperature.getChildAt(0).getId()) {
                jsonObject.put("check_temperature", "yes");
            } else if (radioGroupCheckTemperature.getCheckedRadioButtonId() == radioGroupCheckTemperature.getChildAt(1).getId()) {
                jsonObject.put("check_temperature", "no");

            } else {
                jsonObject.put("check_temperature", "");

            }

            if (radioGroupCheckWeight.getCheckedRadioButtonId() == radioGroupCheckWeight.getChildAt(0).getId()) {
                jsonObject.put("check_weight", "yes");
            } else if (radioGroupCheckWeight.getCheckedRadioButtonId() == radioGroupCheckWeight.getChildAt(1).getId()) {
                jsonObject.put("check_weight", "no");

            } else {
                jsonObject.put("check_weight", "");

            }
            if (radioGroupCheckHeight.getCheckedRadioButtonId() == radioGroupCheckHeight.getChildAt(0).getId()) {
                jsonObject.put("check_height", "yes");
            } else if (radioGroupCheckHeight.getCheckedRadioButtonId() == radioGroupCheckHeight.getChildAt(1).getId()) {
                jsonObject.put("check_height", "no");

            } else {
                jsonObject.put("check_height", "");

            }
            if (radioGroupCheckBloodPressure.getCheckedRadioButtonId() == radioGroupCheckBloodPressure.getChildAt(0).getId()) {
                jsonObject.put("check_blood_pressure", "yes");
            } else if (radioGroupCheckBloodPressure.getCheckedRadioButtonId() == radioGroupCheckBloodPressure.getChildAt(1).getId()) {
                jsonObject.put("check_blood_pressure", "no");

            } else {
                jsonObject.put("check_blood_pressure", "");

            }

            if (radioGroupPersonAttitude.getCheckedRadioButtonId() == radioGroupPersonAttitude.getChildAt(0).getId()) {
                jsonObject.put("person_attitude", "yes");
            } else if (radioGroupPersonAttitude.getCheckedRadioButtonId() == radioGroupPersonAttitude.getChildAt(1).getId()) {
                jsonObject.put("person_attitude", "no");

            } else {
                jsonObject.put("person_attitude", "");

            }


            if (radioGroupFacilityVisitedPreviously.getCheckedRadioButtonId() == radioGroupFacilityVisitedPreviously.getChildAt(0).getId()) {
                jsonObject.put("facility_visited_in_one_year", "yes");
            } else if (radioGroupFacilityVisitedPreviously.getCheckedRadioButtonId() == radioGroupPersonAttitude.getChildAt(1).getId()) {
                jsonObject.put("facility_visited_in_one_year", "no");

            } else {
                jsonObject.put("facility_visited_in_one_year", "");

            }


            if (radioGroupHosipitalChangePositiveNegative.getCheckedRadioButtonId() == radioGroupHosipitalChangePositiveNegative.getChildAt(0).getId() && linearLayoutChangedPostiveNegative.getVisibility() == View.VISIBLE) {
                jsonObject.put("hospital_change_positive_negative", "positive");
                //jsonObject.put("hospital_change_positive", "positive");
                jsonObject.put("opinion_what_changed_at_hospital", ((ClassScreenItem) spinnerOpinionWhatChangedAtHospital.getSelectedItem()).item_id);
                jsonObject.put("change_observed", "");
            } else if (radioGroupHosipitalChangePositiveNegative.getCheckedRadioButtonId() == radioGroupHosipitalChangePositiveNegative.getChildAt(1).getId() && linearLayoutChangedPostiveNegative.getVisibility() == View.VISIBLE) {
                jsonObject.put("hospital_change_positive_negative", "negative");
                jsonObject.put("change_observed", etChangeObserved.getText().toString().trim());
                jsonObject.put("opinion_what_changed_at_hospital", "");

            } else {
                jsonObject.put("hospital_change_positive_negative", "");
                jsonObject.put("opinion_what_changed_at_hospital", "");
                jsonObject.put("change_observed", "");
            }


            if (radioGroupSatisfaction.getCheckedRadioButtonId() == radioGroupSatisfaction.getChildAt(0).getId()) {
                jsonObject.put("satisfied_attention", "yes");
            } else if (radioGroupSatisfaction.getCheckedRadioButtonId() == radioGroupSatisfaction.getChildAt(1).getId()) {
                jsonObject.put("satisfied_attention", "no");

            } else {
                jsonObject.put("satisfied_attention", "");

            }

            if (radioGroupTreatment.getCheckedRadioButtonId() == radioGroupTreatment.getChildAt(0).getId()) {
                jsonObject.put("treatment", "yes");
            } else if (radioGroupTreatment.getCheckedRadioButtonId() == radioGroupTreatment.getChildAt(1).getId()) {
                jsonObject.put("treatment", "no");

            } else {
                jsonObject.put("treatment", "");

            }

            if (radioGroupMoney.getCheckedRadioButtonId() == radioGroupMoney.getChildAt(0).getId()) {
                jsonObject.put("money", "yes");
                jsonObject.put("what_money_spend_on", ((ClassScreenItem) spinnerSpendMoney.getSelectedItem()).item_id);
            } else if (radioGroupMoney.getCheckedRadioButtonId() == radioGroupMoney.getChildAt(1).getId()) {
                jsonObject.put("money", "no");
                jsonObject.put("what_money_spend_on", "");

            } else {
                jsonObject.put("money", "");
                jsonObject.put("what_money_spend_on", "");

            }

            if (radioGroupMedicalProblems.getCheckedRadioButtonId() == radioGroupMedicalProblems.getChildAt(0).getId()) {
                jsonObject.put("medical_problems", "yes");
            } else if (radioGroupMedicalProblems.getCheckedRadioButtonId() == radioGroupMedicalProblems.getChildAt(1).getId()) {
                jsonObject.put("medical_problems", "no");

            } else {
                jsonObject.put("medical_problems", "");

            }


            if (radioGroupPerson.getCheckedRadioButtonId() == radioGroupPerson.getChildAt(0).getId()) {
                jsonObject.put("person_listen", "yes");
            } else if (radioGroupPerson.getCheckedRadioButtonId() == radioGroupPerson.getChildAt(1).getId()) {
                jsonObject.put("person_listen", "no");
            } else {
                jsonObject.put("person_listen", "");

            }

            if (radioGroupAttentionRespect.getCheckedRadioButtonId() == radioGroupAttentionRespect.getChildAt(0).getId()) {
                jsonObject.put("attention_respect", "yes");
            } else if (radioGroupAttentionRespect.getCheckedRadioButtonId() == radioGroupAttentionRespect.getChildAt(1).getId()) {
                jsonObject.put("attention_respect", "no");
            } else {
                jsonObject.put("attention_respect", "");

            }

            jsonObject.put("see_today", ((ClassScreenItem) spinnerStaff.getSelectedItem()).item_id);
            jsonObject.put("illness", ((ClassScreenItem) spinnerIllness.getSelectedItem()).item_id);
            jsonObject.put("time_wait", ((ClassScreenItem) spinnerTimeWait.getSelectedItem()).item_id);
            jsonObject.put("time_spent", ((ClassScreenItem) spinnerTimeSpent.getSelectedItem()).item_id);
            //changes
//            jsonObject.put("what_money_spend_on", ((ClassScreenItem) spinnerSpendMoney.getSelectedItem()).item_id);
            jsonObject.put("seated_in_waiting_area", ((ClassScreenItem) spinnerPatientSeatedWaitingArea.getSelectedItem()).item_id);
            jsonObject.put("systematic_way_in_waiting_area_for_queue", ((ClassScreenItem) spinnerQueueAvaible.getSelectedItem()).item_id);
            jsonObject.put("waiting_area_floor_clean_litter_free", ((ClassScreenItem) spinnerWaitingAreaClean.getSelectedItem()).item_id);
            jsonObject.put("ward_floor_clean_litter_free", ((ClassScreenItem) spinnerWardClean.getSelectedItem()).item_id);
            jsonObject.put("washroom_floor_clean_litter_free", ((ClassScreenItem) spinnerWashroomClean.getSelectedItem()).item_id);
            jsonObject.put("sink_in_washroom_clean_soap_available", ((ClassScreenItem) spinnerWashroomSoapClean.getSelectedItem()).item_id);

            jsonObject.put("observed_any_change_in_hospital", ((ClassScreenItem) spinnerObservedChange.getSelectedItem()).item_id);

            jsonObject.put("recommend_this_hospital", spinnerRecommendation.getSelectedItem().toString());

            jsonObject.put("facility_dispensary", editTextFacilityDispensary.getText().toString());
            jsonObject.put("medicine_quantity", editTextMedicineQuantity.getText().toString());
            jsonObject.put("comment", editTextComments.getText().toString());

            if (radioGroupReferredHospital.getCheckedRadioButtonId() == radioGroupReferredHospital.getChildAt(0).getId()) {
                jsonObject.put("referred_hospital", "yes");
                jsonObject.put("referred_hospital_private_public", "");
            } else if (radioGroupReferredHospital.getCheckedRadioButtonId() == radioGroupReferredHospital.getChildAt(1).getId()) {
                jsonObject.put("referred_hospital", "no");
                jsonObject.put("referred_hospital_private_public", "");
            } else {
                jsonObject.put("referred_hospital", "");
                jsonObject.put("referred_hospital_private_public", "");
            }


            if (radioGroupReferredFacility.getCheckedRadioButtonId() == radioGroupReferredFacility.getChildAt(0).getId()) {
                jsonObject.put("referred_facility", "yes");
                if (radioGroupReferredFacilityPrivatePublic.getCheckedRadioButtonId() == radioGroupReferredFacilityPrivatePublic.getChildAt(0).getId()) {
                    jsonObject.put("referred_facility_private_public", "public");
                } else if (radioGroupReferredFacilityPrivatePublic.getCheckedRadioButtonId() == radioGroupReferredFacilityPrivatePublic.getChildAt(1).getId()) {
                    jsonObject.put("referred_facility_private_public", "private");
                } else {
                    jsonObject.put("referred_facility_private_public", "");
                }
            } else if (radioGroupReferredFacility.getCheckedRadioButtonId() == radioGroupReferredFacility.getChildAt(1).getId()) {
                jsonObject.put("referred_facility", "no");
                jsonObject.put("referred_facility_private_public", "");
            } else {
                jsonObject.put("referred_facility", "");
                jsonObject.put("referred_facility_private_public", "");
            }


            if (radioGroupTestDone.getCheckedRadioButtonId() == radioGroupTestDone.getChildAt(0).getId()) {
                jsonObject.put("test_done", "yes");
                jsonObject.put("tests_name", "");
                jsonObject.put("test_done_private_public", "");
                jsonObject.put("amount_paid", "");
            } else if (radioGroupTestDone.getCheckedRadioButtonId() == radioGroupTestDone.getChildAt(1).getId()) {
                JSONArray jsonArray = new JSONArray();
                jsonObject.put("test_done", "no");
                jsonObject.put("tests_name", "");
                jsonObject.put("test_done_private_public", "");
                jsonObject.put("amount_paid", "");
            } else {
                JSONArray jsonArray = new JSONArray();
                jsonObject.put("test_done", "");
                jsonObject.put("tests_name", "");
                jsonObject.put("test_done_private_public", "");
                jsonObject.put("amount_paid", "");
            }

            JSONArray jsonArray = new JSONArray();
            jsonObject.put("test_information", jsonArray);

            mGlobals.mJsonObjectFormData.remove(Constants.DB_SCREEN_NAME_PATIENT_INTERVIEW_PAGE2);
            mGlobals.mJsonObjectFormData.put(Constants.DB_SCREEN_NAME_PATIENT_INTERVIEW_PAGE2, jsonObject);

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    private void setSpinnerAdapter(Spinner spinner, ArrayList<ClassScreenItem> arrayValues) {
        if (arrayValues == null) {
            arrayValues = new ArrayList<ClassScreenItem>();
        }
        ArrayAdapterSpinnerSimpleScreenItem arrayAdapter = new ArrayAdapterSpinnerSimpleScreenItem(getActivity(), arrayValues, Color.BLACK, -1);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(arrayAdapter);
    }


}
