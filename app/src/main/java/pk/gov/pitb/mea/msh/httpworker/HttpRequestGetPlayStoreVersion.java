package pk.gov.pitb.mea.msh.httpworker;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import pk.gov.pitb.mea.msh.R;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;

public class HttpRequestGetPlayStoreVersion {

	private String packageName = null;
	private String currentPlayStoreVersion = null;
	private final String applicationURI;
	private boolean bIsUpdateAvailable = false;
	private Context context = null;

	public HttpRequestGetPlayStoreVersion(Context context) {
		this.bIsUpdateAvailable = false;
		this.context = context;
		this.packageName = this.context.getPackageName();
		this.applicationURI = "https://play.google.com/store/apps/details?id=" + packageName;
	}

	public boolean isUpdateAvailable() {
		return bIsUpdateAvailable;
	}

	public boolean checkPlayStoreVersion() {
		try {
			String currentApplicationVersion = context.getResources().getString(R.string.app_version_name);
			currentPlayStoreVersion = getPlayStoreVersion();
			if (currentPlayStoreVersion != null && currentPlayStoreVersion.length() > 0) {
				double currentAppVersion = Double.parseDouble(currentApplicationVersion);
				double currentPSVersion = Double.parseDouble(currentPlayStoreVersion);
				if (currentAppVersion < currentPSVersion) {
					bIsUpdateAvailable = true;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public void showUpdateDialog() {
		AlertDialog.Builder dialogBuilderUpdate = new AlertDialog.Builder(context);
		dialogBuilderUpdate.setTitle("Update Available");
		dialogBuilderUpdate.setMessage("An update is available on PlayStore");
		dialogBuilderUpdate.setPositiveButton("Update Now", new DialogInterface.OnClickListener() {

			public void onClick(DialogInterface dialog, int which) {
				Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + packageName));
				context.startActivity(intent);
			}
		});
		dialogBuilderUpdate.setNegativeButton("Update Later", new DialogInterface.OnClickListener() {

			public void onClick(DialogInterface dialog, int which) {
			}
		});
		dialogBuilderUpdate.setCancelable(false);
		dialogBuilderUpdate.create();
		dialogBuilderUpdate.show();
	}

	public String getPlayStoreVersion() {
		try {
			currentPlayStoreVersion = httpRequestGetPlayStoreVersion();
			return currentPlayStoreVersion == null ? "" : currentPlayStoreVersion.trim();
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}

	private String httpRequestGetPlayStoreVersion() throws Exception {
		HttpClient httpclient = new DefaultHttpClient();
		HttpResponse response;
		String responseString = null;
		try {
			response = httpclient.execute(new HttpGet(applicationURI));
			StatusLine statusLine = response.getStatusLine();
			if (statusLine.getStatusCode() == HttpStatus.SC_OK) {
				ByteArrayOutputStream out = new ByteArrayOutputStream();
				response.getEntity().writeTo(out);
				out.close();
				responseString = out.toString();
			} else {
				response.getEntity().getContent().close();
				throw new IOException(statusLine.getReasonPhrase());
			}
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		if (responseString == null || responseString.length() == 0) {
			return "";
		} else {
			String[] parts = responseString.split("div");
			boolean VersionTagFound = false;
			int counter = 0;
			while (VersionTagFound == false) {
				if (parts[counter] != null) {
					if (parts[counter].contains("Current Version")) {
						break;
					}
				}
				counter++;
			}
			counter++;
			counter++;

			String[] extractVersionNumberPart = parts[counter].split(">")/*[2].split("<")*/;
			String[] VersionNumber = extractVersionNumberPart[2].split("<");
			return VersionNumber[0];
		}
	}
}