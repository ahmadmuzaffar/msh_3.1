package pk.gov.pitb.mea.msh.fragmentactivities;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TableLayout;
import android.widget.TextView;

import org.json.JSONObject;

import pk.gov.pitb.mea.msh.R;
import pk.gov.pitb.mea.msh.handlers.InterfaceFragmentCallBack;
import pk.gov.pitb.mea.msh.helpers.Constants;
import pk.gov.pitb.mea.msh.helpers.MainContainer;


public class FragmentKPI extends Fragment implements InterfaceFragmentCallBack {


	private View parentView;

	private LinearLayout linearLayoutMain;
	private TextView textViewOutlook = null;
	private RadioGroup radioGroupOutlook = null, radioGroupFocalPerson = null;
	private RadioButton radioButtonOutlookYes = null;
	private RadioButton radioButtonOutlookNo = null;
	private EditText editTextReason;
	private RadioGroup rgGeneralMedicalOfficers, rgNursingCadre, rgSpecialists, rgSupportedStaff, rgAlliedHealthProfessionals;
	private TableLayout tableLayoutKPI;
	@Override
	public void parseObject() {
		try {
			
			JSONObject jsonObject = new JSONObject();

			if (radioGroupOutlook.getCheckedRadioButtonId()==radioButtonOutlookYes.getId()){
				jsonObject.put("individual_performance_record", "yes");
				jsonObject.put("reason", "");
			}
			else if (radioGroupOutlook.getCheckedRadioButtonId()==radioButtonOutlookNo.getId()){
				jsonObject.put("individual_performance_record", "no");
				jsonObject.put("reason", editTextReason.getText().toString());
			}
			else {
				jsonObject.put("individual_performance_record", "");
				jsonObject.put("reason", "");
			}


			if (radioGroupFocalPerson.getCheckedRadioButtonId()==radioGroupFocalPerson.getChildAt(0).getId()){
				jsonObject.put("focal_person_available", "yes");
			}
			else if (radioGroupFocalPerson.getCheckedRadioButtonId()==radioGroupFocalPerson.getChildAt(1).getId()){
				jsonObject.put("focal_person_available", "no");
			}
			else {
				jsonObject.put("focal_person_available", "");
			}


			if (tableLayoutKPI.getVisibility()==View.VISIBLE) {
				if (rgGeneralMedicalOfficers.getCheckedRadioButtonId() == rgGeneralMedicalOfficers.getChildAt(0).getId()) {
					jsonObject.put("general_medical_officers_in_facility", "yes");
				} else if (rgGeneralMedicalOfficers.getCheckedRadioButtonId() == rgGeneralMedicalOfficers.getChildAt(1).getId()) {
					jsonObject.put("general_medical_officers_in_facility", "no");
				} else if (rgGeneralMedicalOfficers.getCheckedRadioButtonId() == rgGeneralMedicalOfficers.getChildAt(2).getId()) {
					jsonObject.put("general_medical_officers_in_facility", "na");
				} else {
					jsonObject.put("general_medical_officers_in_facility", "");
				}

				if (rgNursingCadre.getCheckedRadioButtonId() == rgNursingCadre.getChildAt(0).getId()) {
					jsonObject.put("nursing_cadre", "yes");
				} else if (rgNursingCadre.getCheckedRadioButtonId() == rgNursingCadre.getChildAt(1).getId()) {
					jsonObject.put("nursing_cadre", "no");
				} else if (rgNursingCadre.getCheckedRadioButtonId() == rgNursingCadre.getChildAt(2).getId()) {
					jsonObject.put("nursing_cadre", "na");
				} else {
					jsonObject.put("nursing_cadre", "");
				}

				if (rgSpecialists.getCheckedRadioButtonId() == rgSpecialists.getChildAt(0).getId()) {
					jsonObject.put("specialists", "yes");
				} else if (rgSpecialists.getCheckedRadioButtonId() == rgSpecialists.getChildAt(1).getId()) {
					jsonObject.put("specialists", "no");
				} else if (rgSpecialists.getCheckedRadioButtonId() == rgSpecialists.getChildAt(2).getId()) {
					jsonObject.put("specialists", "na");
				} else {
					jsonObject.put("specialists", "");
				}

				if (rgSupportedStaff.getCheckedRadioButtonId() == rgSupportedStaff.getChildAt(0).getId()) {
					jsonObject.put("support_staff", "yes");
				} else if (rgSupportedStaff.getCheckedRadioButtonId() == rgSupportedStaff.getChildAt(1).getId()) {
					jsonObject.put("support_staff", "no");
				} else if (rgSupportedStaff.getCheckedRadioButtonId() == rgSupportedStaff.getChildAt(2).getId()) {
					jsonObject.put("support_staff", "na");
				} else {
					jsonObject.put("support_staff", "");
				}

				if (rgAlliedHealthProfessionals.getCheckedRadioButtonId() == rgAlliedHealthProfessionals.getChildAt(0).getId()) {
					jsonObject.put("allied_health_professionals", "yes");
				} else if (rgAlliedHealthProfessionals.getCheckedRadioButtonId() == rgAlliedHealthProfessionals.getChildAt(1).getId()) {
					jsonObject.put("allied_health_professionals", "no");
				} else if (rgAlliedHealthProfessionals.getCheckedRadioButtonId() == rgAlliedHealthProfessionals.getChildAt(2).getId()) {
					jsonObject.put("allied_health_professionals", "na");
				} else {
					jsonObject.put("allied_health_professionals", "");
				}

			}else{
				jsonObject.put("general_medical_officers_in_facility", "");
				jsonObject.put("nursing_cadre", "");
				jsonObject.put("specialists", "");
				jsonObject.put("support_staff", "");
				jsonObject.put("allied_health_professionals", "");
			}

			MainContainer.mJsonObjectFormData.remove(Constants.DB_SCREEN_NAME_KPI);
			MainContainer.mJsonObjectFormData.put(Constants.DB_SCREEN_NAME_KPI, jsonObject);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void loadSavedData(){
		try {
			if (MainContainer.mJsonObjectFormData.has(Constants.DB_SCREEN_NAME_KPI)){
				JSONObject jsonObject = MainContainer.mJsonObjectFormData.getJSONObject(Constants.DB_SCREEN_NAME_KPI);
				String individual_performance_record = jsonObject.getString("individual_performance_record");
				if (individual_performance_record.equals("yes")){
					radioButtonOutlookYes.setChecked(true);
					tableLayoutKPI.setVisibility(View.VISIBLE);
				}
				else if (individual_performance_record.equals("no")){
					radioButtonOutlookNo.setChecked(true);
					editTextReason.setText(jsonObject.getString("reason"));
				}

				String focal_person = jsonObject.getString("focal_person_available");
				if (focal_person.equals("yes")){
					((RadioButton)radioGroupFocalPerson.getChildAt(0)).setChecked(true);
				}
				else if (focal_person.equals("no")){
					((RadioButton)radioGroupFocalPerson.getChildAt(1)).setChecked(true);
				}

				String general_medical_officers_in_facility = jsonObject.getString("general_medical_officers_in_facility");
				if (general_medical_officers_in_facility.equals("yes")){
					((RadioButton)rgGeneralMedicalOfficers.getChildAt(0)).setChecked(true);
				}
				else if (general_medical_officers_in_facility.equals("no")){
					((RadioButton)rgGeneralMedicalOfficers.getChildAt(1)).setChecked(true);
				}
				else if (general_medical_officers_in_facility.equals("na")){
					((RadioButton)rgGeneralMedicalOfficers.getChildAt(2)).setChecked(true);
				}


				String nursing_cadre = jsonObject.getString("nursing_cadre");
				if (nursing_cadre.equals("yes")){
					((RadioButton)rgNursingCadre.getChildAt(0)).setChecked(true);
				}
				else if (nursing_cadre.equals("no")){
					((RadioButton)rgNursingCadre.getChildAt(1)).setChecked(true);
				}
				else if (nursing_cadre.equals("na")){
					((RadioButton)rgNursingCadre.getChildAt(2)).setChecked(true);
				}

				String specialists = jsonObject.getString("specialists");
				if (specialists.equals("yes")){
					((RadioButton)rgSpecialists.getChildAt(0)).setChecked(true);
				}
				else if (specialists.equals("no")){
					((RadioButton)rgSpecialists.getChildAt(1)).setChecked(true);
				}
				else if (specialists.equals("na")){
					((RadioButton)rgSpecialists.getChildAt(2)).setChecked(true);
				}

				String support_staff = jsonObject.getString("support_staff");
				if (support_staff.equals("yes")){
					((RadioButton)rgSupportedStaff.getChildAt(0)).setChecked(true);
				}
				else if (support_staff.equals("no")){
					((RadioButton)rgSupportedStaff.getChildAt(1)).setChecked(true);
				}
				else if (support_staff.equals("na")){
					((RadioButton)rgSupportedStaff.getChildAt(2)).setChecked(true);
				}


				String allied_health_professionals = jsonObject.getString("allied_health_professionals");
				if (allied_health_professionals.equals("yes")){
					((RadioButton)rgAlliedHealthProfessionals.getChildAt(0)).setChecked(true);
				}
				else if (allied_health_professionals.equals("no")){
					((RadioButton)rgAlliedHealthProfessionals.getChildAt(1)).setChecked(true);
				}
				else if (allied_health_professionals.equals("na")){
					((RadioButton)rgAlliedHealthProfessionals.getChildAt(2)).setChecked(true);
				}
			}
		}
		catch (Exception e){

		}
	}




	private String errorMessage;

	@Override
	public String onFragmentChanged(int previousPosition) {
		errorMessage = null;
		isFormValid();
		return errorMessage;
	}

	@Override
	public boolean isFormValid() {
		try {
			boolean bIsValid = true;
			if (radioGroupOutlook.getCheckedRadioButtonId()==-1){
				errorMessage = "Select Yes or No option for Individual Performance Record System";
				return false;
			}

			if (radioGroupFocalPerson.getCheckedRadioButtonId()==-1){
				errorMessage = "Select Yes or No option for record keep/focal person available";
				return false;
			}

			if (editTextReason.getVisibility()==View.VISIBLE && editTextReason.getText().toString().equals("")){
				errorMessage = "Specify the reason";
				return false;
			}

			if (tableLayoutKPI.getVisibility()==View.VISIBLE) {
				if (rgGeneralMedicalOfficers.getCheckedRadioButtonId() == -1) {
					errorMessage = "Please choose Yes/No for General Medical Officers?"; // Yes/No/NA
					return false;
				}

				if (rgNursingCadre.getCheckedRadioButtonId() == -1) {
					errorMessage = "Please choose Yes/No for Nursing Cadre?"; // Yes/No/NA
					return false;
				}

				if (rgSpecialists.getCheckedRadioButtonId() == -1) {
					errorMessage = "Please choose Yes/No for Specialists?"; // Yes/No/NA
					return false;
				}

				if (rgSupportedStaff.getCheckedRadioButtonId() == -1) {
					errorMessage = "Please choose Yes/No for Supported Staff?"; // Yes/No/NA
					return false;
				}

				if (rgAlliedHealthProfessionals.getCheckedRadioButtonId() == -1) {
					errorMessage = "Please choose Yes/No for Allied Health Professionals?"; // Yes/No/NA
					return false;
				}
			}

			return bIsValid;
		} catch (Exception e) {
			e.printStackTrace();
			MainContainer.showErrorMessage();
			return false;
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		if (parentView == null) {
			parentView = inflater.inflate(R.layout.fragment_kpi, container, false);
			
			generateBody();
			loadSavedData();
		}
		return parentView;
	}
		private void generateBody() {
		try {
			linearLayoutMain = (LinearLayout) parentView.findViewById(R.id.linearlayout_main);
			textViewOutlook = (TextView) parentView.findViewById(R.id.textview_outlook);

			
			radioGroupOutlook = (RadioGroup) parentView.findViewById(R.id.radiogroup_performance_record);
			radioGroupFocalPerson = (RadioGroup) parentView.findViewById(R.id.radio_group_focal_person);

			radioButtonOutlookYes = (RadioButton) parentView.findViewById(R.id.radiobutton_performance_record_yes);
			radioButtonOutlookNo = (RadioButton) parentView.findViewById(R.id.radiobutton_performance_record_no);
			editTextReason = (EditText) parentView.findViewById(R.id.edittext_reason);

			tableLayoutKPI = (TableLayout) parentView.findViewById(R.id.table_layout_kpi);
			rgGeneralMedicalOfficers = (RadioGroup) parentView.findViewById(R.id.kpi_a);
			rgNursingCadre = (RadioGroup) parentView.findViewById(R.id.kpi_b);
			rgSpecialists = (RadioGroup) parentView.findViewById(R.id.kpi_c);
			rgSupportedStaff = (RadioGroup) parentView.findViewById(R.id.kpi_d);
			rgAlliedHealthProfessionals = (RadioGroup) parentView.findViewById(R.id.kpi_e);


			linearLayoutMain.setPadding((int) (MainContainer.mScreenWidth * 0.02), (int) (MainContainer.mScreenHeight * 0.02), (int) (MainContainer.mScreenWidth * 0.02), 0);
			LinearLayout.LayoutParams paramsEditText = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, (int) (MainContainer.mScreenHeight * 0.35));
			paramsEditText.setMargins(0, 0, 0, (int) (MainContainer.mScreenHeight * 0.02));
			editTextReason.setLayoutParams(paramsEditText);
			
			radioButtonOutlookYes.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					editTextReason.setVisibility(View.GONE);
					tableLayoutKPI.setVisibility(View.VISIBLE);
				}
			});
			radioButtonOutlookNo.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					editTextReason.setVisibility(View.VISIBLE);
					tableLayoutKPI.setVisibility(View.GONE);
				}
			});

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	@Override
	public void onFragmentShown() {
	}
}