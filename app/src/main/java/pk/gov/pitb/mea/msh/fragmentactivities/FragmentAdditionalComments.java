package pk.gov.pitb.mea.msh.fragmentactivities;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;

import org.json.JSONObject;

import pk.gov.pitb.mea.msh.R;
import pk.gov.pitb.mea.msh.handlers.InterfaceFragmentCallBack;
import pk.gov.pitb.mea.msh.helpers.Constants;
import pk.gov.pitb.mea.msh.helpers.MainContainer;

public class FragmentAdditionalComments extends Fragment implements InterfaceFragmentCallBack {

	private View parentView = null;
	private EditText editTextCommentsMEA;

	@Override
	public void parseObject() {
		try {
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("mea_comments", editTextCommentsMEA.getText().toString().trim());
			MainContainer.mJsonObjectFormData.remove(Constants.DB_SCREEN_NAME_COMMENTS);
			MainContainer.mJsonObjectFormData.put(Constants.DB_SCREEN_NAME_COMMENTS, jsonObject);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		if (parentView == null) {
			parentView = inflater.inflate(R.layout.fragment_comments, container, false);
			generateBody();
			loadSavedData();
		}
		return parentView;
	}


	private void loadSavedData(){
		try {
			if (MainContainer.mJsonObjectFormData.has(Constants.DB_SCREEN_NAME_COMMENTS)){
				JSONObject jsonObject = MainContainer.mJsonObjectFormData.getJSONObject(Constants.DB_SCREEN_NAME_COMMENTS);
				editTextCommentsMEA.setText(jsonObject.getString("mea_comments"));
			}
		}
		catch (Exception e){

		}
	}


	private void generateBody() {
		try {
			LinearLayout linearLayout = (LinearLayout) parentView.findViewById(R.id.linearlayout_main);
			editTextCommentsMEA = (EditText) parentView.findViewById(R.id.edittext_comments_mea);

			linearLayout.setPadding((int) (MainContainer.mScreenWidth * 0.02), (int) (MainContainer.mScreenHeight * 0.02), (int) (MainContainer.mScreenWidth * 0.02), 0);
			LinearLayout.LayoutParams paramsEditText = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, (int) (MainContainer.mScreenHeight * 0.35));
			paramsEditText.setMargins(0, 0, 0, (int) (MainContainer.mScreenHeight * 0.02));
			editTextCommentsMEA.setLayoutParams(paramsEditText);

			editTextCommentsMEA.setOnTouchListener(onTouchListenerScroll);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private OnTouchListener onTouchListenerScroll = new View.OnTouchListener() {

		@SuppressLint("ClickableViewAccessibility")
		@Override
		public boolean onTouch(View v, final MotionEvent motionEvent) {
			try {
				v.getParent().requestDisallowInterceptTouchEvent(true);
				switch (motionEvent.getAction() & MotionEvent.ACTION_MASK) {
				case MotionEvent.ACTION_UP:
					v.getParent().requestDisallowInterceptTouchEvent(false);
					break;
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			return false;
		}
	};

	@Override
	public void onFragmentShown() {
	}

	private String errorMessage;

	@Override
	public String onFragmentChanged(int previousPosition) {
		errorMessage = null;
		isFormValid();
		return errorMessage;
	}

	@Override
	public boolean isFormValid() {
		return true;
	}
}