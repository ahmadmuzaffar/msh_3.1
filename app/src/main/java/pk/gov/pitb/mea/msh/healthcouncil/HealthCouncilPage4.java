package pk.gov.pitb.mea.msh.healthcouncil;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TableRow;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import pk.gov.pitb.mea.msh.R;
import pk.gov.pitb.mea.msh.handlers.HandlerFragmentCallBackHealthCouncil;
import pk.gov.pitb.mea.msh.helpers.AlertDialogs;
import pk.gov.pitb.mea.msh.helpers.Constants;
import pk.gov.pitb.mea.msh.helpers.MainContainer;
import pk.gov.pitb.mea.msh.helpers.OnClickShowDatePicker;
import pk.gov.pitb.mea.msh.helpers.UIHelper;
import pk.gov.pitb.mea.msh.views.CustomEditText;


/**
 * Created by murtaza on 10/27/2016.
 */
public class HealthCouncilPage4 extends Fragment implements HandlerFragmentCallBackHealthCouncil {
    private View parentView;
    private LinearLayout linearLayout;
    private TableLayout tableLayout;
    private String errorMessage = "Please fill all fields";
    private Button buttonProceed, buttonAddRow;
    private Button buttonPrevious, buttonNext, buttonSubmit;

    private TableRow.LayoutParams[] paramsArrayTableRowColumns = new TableRow.LayoutParams[6];
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (parentView==null){
            parentView = inflater.inflate(R.layout.health_council_page4,container, false);
            /*MainContainer = Globals.getInstance();*/
            initFragmentData();
            generateBody();
            if (MainContainer.mJsonObjectFormData.has(Constants.DB_SCREEN_NAME_HEALTH_COUNCIL_PAGE4)) {
                JSONArray jsonArray = null;
                try {
                    jsonArray = MainContainer.mJsonObjectFormData.getJSONArray(Constants.DB_SCREEN_NAME_HEALTH_COUNCIL_PAGE4);

                    tableLayout.removeView(tableLayout.getChildAt(1));
                    for (int i =0; i < jsonArray.length(); i++){
                        generateTable(paramsArrayTableRowColumns);
                        JSONObject jsonObject = (JSONObject) jsonArray.get(i);
                        View child1 = tableLayout.getChildAt(i+1);
                        TableRow tableRow1 = (TableRow) child1;
                        EditText date1 = (EditText) tableRow1.getChildAt(1);
                        date1.setText(jsonObject.getString("date"));
                        EditText description1 = (EditText) tableRow1.getChildAt(2);
                        description1.setText(jsonObject.getString("description"));
                        EditText quantity = (EditText) tableRow1.getChildAt(3);
                        quantity.setText(jsonObject.getString("quantity"));
                        EditText expenditure1 = (EditText) tableRow1.getChildAt(4);
                        expenditure1.setText(jsonObject.getString("expenditure"));
                        EditText balance1 = (EditText) tableRow1.getChildAt(5);
                        balance1.setText(jsonObject.getString("balance"));

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }
        return parentView;
    }

    private void initFragmentData() {
    }

    private void generateBody(){

        linearLayout = (LinearLayout) parentView.findViewById(R.id.linear_layout_health_council_page4);
        tableLayout = new TableLayout(MainContainer.mContext);
        linearLayout.addView(tableLayout);


        buttonNext = (Button) parentView.findViewById(R.id.button_next);
        buttonSubmit = (Button) parentView.findViewById(R.id.button_submit);
        buttonPrevious = (Button) parentView.findViewById(R.id.button_previous);

        buttonAddRow = (Button) parentView.findViewById(R.id.health_coucil_page4_add_row);
        buttonProceed = (Button) parentView.findViewById(R.id.btn_proceed_health_council_page4);
        LinearLayout.LayoutParams lpButtonProceed = new LinearLayout.LayoutParams((int) (MainContainer.mScreenWidth * 0.5),
                (int) (MainContainer.mScreenWidth * 0.1));
        lpButtonProceed.setMargins(0, (int) (MainContainer.mScreenHeight * 0.05), 0, 0);
        buttonProceed.setLayoutParams(lpButtonProceed);

        buttonProceed.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                if (isFormValid()) {

                    MainContainer.activityCallBackHealthCouncil.showNextFragment();
                } else {
                    AlertDialogs.getInstance().showDialogOK(null, errorMessage, null, true);
                }
            }
        });

        RelativeLayout.LayoutParams lpButton = new RelativeLayout.LayoutParams((int) (MainContainer.mScreenWidth * 0.05), (int) (MainContainer.mScreenWidth * 0.05));
        lpButton.addRule(RelativeLayout.CENTER_VERTICAL);
        lpButton.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
        buttonAddRow.setLayoutParams(lpButton);

        initClickListeners();


        //final TableRow.LayoutParams[] paramsArrayTableRowColumns = new TableRow.LayoutParams[5];
        paramsArrayTableRowColumns[0] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.07), TableRow.LayoutParams.MATCH_PARENT);
        paramsArrayTableRowColumns[1] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.17), /*(int) (MainContainer.mScreenWidth * 0.08)*/TableRow.LayoutParams.MATCH_PARENT);
        paramsArrayTableRowColumns[2] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.18), /*(int) (MainContainer.mScreenWidth * 0.08)*/TableRow.LayoutParams.MATCH_PARENT);
        paramsArrayTableRowColumns[3] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.13), /*(int) (MainContainer.mScreenWidth * 0.08)*/TableRow.LayoutParams.MATCH_PARENT);
        paramsArrayTableRowColumns[4] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.18), /*(int) (MainContainer.mScreenWidth * 0.08)*/TableRow.LayoutParams.WRAP_CONTENT);
        paramsArrayTableRowColumns[5] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.18), /*(int) (MainContainer.mScreenWidth * 0.08)*/TableRow.LayoutParams.MATCH_PARENT);
        for (int i = 1; i < paramsArrayTableRowColumns.length; i++) {
            paramsArrayTableRowColumns[i].setMargins(2, 0, 0, 0);
        }
        UIHelper uiContainer = new UIHelper();
        uiContainer.generateTableHeaderNew(tableLayout,paramsArrayTableRowColumns,new String[]{"No", "Date", "Description","Quantity", "Expenditure (Rs.)", "Balance (Rs.)"});
        generateTable(paramsArrayTableRowColumns);
        buttonAddRow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                generateTable(paramsArrayTableRowColumns);
            }
        });

    }

    int serialNum = 0;
    private void generateTable(TableRow.LayoutParams[] paramsArrayTableRowColumns) {

        int colorBgTableRow = MainContainer.mContext.getResources().getColor(R.color.row_background);
        {
            boolean bIsOdd = serialNum % 2 == 0;
            serialNum++;

            TableRow tableRow = new TableRow(MainContainer.mContext);
            CustomEditText editTextSerialNum = new CustomEditText(bIsOdd, "" + serialNum);
            CustomEditText editTextDate = new CustomEditText(bIsOdd);
            CustomEditText editTextDescription = new CustomEditText(bIsOdd);
            CustomEditText editTextQuantity = new CustomEditText(bIsOdd);
            CustomEditText editTextExpenditure = new CustomEditText(bIsOdd);
            CustomEditText editTextBalance = new CustomEditText(bIsOdd);

            editTextSerialNum.customiseEditText(paramsArrayTableRowColumns[0]);
            editTextDate.customiseEditText(paramsArrayTableRowColumns[1]);
            editTextDescription.customiseEditText(paramsArrayTableRowColumns[2]);
            editTextQuantity.customiseEditText(paramsArrayTableRowColumns[3]);
            editTextExpenditure.customiseEditText(paramsArrayTableRowColumns[4]);
            editTextBalance.customiseEditText(paramsArrayTableRowColumns[5]);


            editTextDate.setOnClickListener(new OnClickShowDatePicker(MainContainer.mContext, editTextDate, "Since"));
            editTextExpenditure.setInputTypeNumberEditableWithLimit(12);
            editTextExpenditure.setHint("Enter Number");
            editTextBalance.setInputTypeNumberEditableWithLimit(12);
            editTextBalance.setHint("Enter Number");
            editTextQuantity.setInputTypeNumberEditableWithLimit(12);
            editTextQuantity.setHint("Enter Number");

            tableRow.setBackgroundColor(colorBgTableRow);
            tableRow.addView(editTextSerialNum);
            tableRow.addView(editTextDate);
            tableRow.addView(editTextDescription);
            tableRow.addView(editTextQuantity);
            tableRow.addView(editTextExpenditure);
            tableRow.addView(editTextBalance);
            tableLayout.addView(tableRow);
        }
    }

    private void initClickListeners() {
        buttonPrevious.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainContainer.activityCallBackHealthCouncil.showPreviousFragment(1);
            }
        });

        buttonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isFormValid()) {

                    MainContainer.activityCallBackHealthCouncil.showNextFragment();
                } else {
                    AlertDialogs.getInstance().showDialogOK(null, errorMessage, null, true);
                }
            }
        });

        buttonSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
    }


    @Override
    public void onFragmentShown() {

    }

    @Override
    public boolean isFormValid() {
        boolean isValid = true;
        for (int i = 1 ; i < tableLayout.getChildCount(); i++){
            View child = tableLayout.getChildAt(i);
            TableRow tableRow = (TableRow) child;
            EditText date = (EditText) tableRow.getChildAt(1);
            EditText description = (EditText) tableRow.getChildAt(2);
            EditText quantity = (EditText) tableRow.getChildAt(3);
            EditText expenditure = (EditText) tableRow.getChildAt(4);
            EditText balance = (EditText) tableRow.getChildAt(5);

            if (date.getText().toString().equals("")){
                errorMessage = "Please enter the date in row "+Integer.toString(i);
                return false;
            }

            if (description.getText().toString().equals("")){
                errorMessage = "Please enter the description in row "+Integer.toString(i);
                return false;
            }
            if (quantity.getText().toString().equals("")){
                errorMessage = "Please enter the quantity in row "+Integer.toString(i);
                return false;
            }
            if (expenditure.getText().toString().equals("")){
                errorMessage = "Please enter the expenditure in row "+Integer.toString(i);
                return false;
            }

            if (balance.getText().toString().equals("")){
                errorMessage = "Please enter the balance in row "+Integer.toString(i);
                return false;
            }



        }


        return isValid;
    }

    @Override
    public String onFragmentChanged(int previousPosition) {
        return null;
    }

    @Override
    public void parseObject() {

        JSONArray jsonArray = new JSONArray();
        try {
            for (int i = 1 ; i < tableLayout.getChildCount(); i++){
                JSONObject jsonObject = new JSONObject();
                View child = tableLayout.getChildAt(i);
                TableRow tableRow = (TableRow) child;
                EditText date = (EditText) tableRow.getChildAt(1);
                EditText description = (EditText) tableRow.getChildAt(2);
                EditText quantity = (EditText) tableRow.getChildAt(3);
                EditText expenditure = (EditText) tableRow.getChildAt(4);
                EditText balance = (EditText) tableRow.getChildAt(5);


                jsonObject.put("no", Integer.toString(i));
                jsonObject.put("date", date.getText().toString());
                jsonObject.put("description", description.getText().toString());
                jsonObject.put("quantity", quantity.getText().toString());
                jsonObject.put("expenditure", expenditure.getText().toString());
                jsonObject.put("balance",balance.getText().toString());
                jsonArray.put(jsonObject);

            }

            MainContainer.mJsonObjectFormData.remove(Constants.DB_SCREEN_NAME_HEALTH_COUNCIL_PAGE4);
            MainContainer.mJsonObjectFormData.put(Constants.DB_SCREEN_NAME_HEALTH_COUNCIL_PAGE4, jsonArray);


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
