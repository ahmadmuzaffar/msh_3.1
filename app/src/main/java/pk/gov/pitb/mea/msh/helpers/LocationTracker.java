package pk.gov.pitb.mea.msh.helpers;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;

public class LocationTracker {

//  @formatter:off
/** 
 	* ---------------------------------------------------------------------------------------- *
	 				TO BE DECLARED IN MANIFEST FILE
	* ---------------------------------------------------------------------------------------- *
	<uses-permission android:name="android.permission.ACCESS_FINE_LOCATION" />
	<uses-permission android:name="android.permission.INTERNET" />
 	* ---------------------------------------------------------------------------------------- *
	 					IN ACTIVITY CLASS
	* ---------------------------------------------------------------------------------------- *
	// DECLARE
	private LocationTracker locationTracker = null;

	// ON CREATE
	initializeApplication();
	private void initializeApplication() {
    		MainContainer.mContext = ActivityMain.this;
    		locationTracker = new LocationTracker(MainContainer.mContext);
	}
	// TO CHECK LOCATION ENABLE OR NOT
	@Override
	protected void onStart() {
		super.onStart();
		locationTracker.onStart();
	}
	// TO RESUME LOCATION MANAGER
	@Override
	protected void onResume() {
		super.onResume();
		locationTracker.onResume();
	}
	// TO STOP THE SERVICE
	@Override
	protected void onDestroy() {
		super.onDestroy();
		locationTracker.onDestroy();
	}

	// TO CHECK IF LOCATION CAPTURED OR NOT
	if(!locationTracker.isLocationSet()){
		Toast.makeText(getApplicationContext(), ("Location Not Available. Please Check Your Location Settings or Try Again Later!!"), Toast.LENGTH_SHORT).show();
	}

	// TO RESET LOCATION
	locationTracker.resetLocation();

	// TO CHECK AND GET LOCATION
	if(!locationTracker.isLocationSet())
		locationTracker.getBetterLocation();
	String location = locationTracker.getLocation();
 **/
//  @formatter:on
	private Context context = null;
	private String networkLocation = null;
	private String gpsLocation = null;
	private String betterLocation = null;
	// private String accurateLocation = null;
	private LocationManager locationManager;
	private Location locationBetter = null;
	private Location locationGPS = null;
	private Location locationNetwork = null;

	public LocationTracker(Context context) {
		this.context = context;
		this.gpsLocation = "";
		this.networkLocation = "";
		this.betterLocation = "";
	}

	public boolean isLocationSet() {
		if (betterLocation == null || betterLocation.equals("") || betterLocation.equals("0.0,0.0"))
			return false;
		else
			return true;
	}

	public String getBetterLocation() {
		return betterLocation;
	}

	public String getGpsLocation() {
		return gpsLocation;
	}

	public String getNetworkLocation() {
		return networkLocation;
	}

	private String setLocation(double latitude, double longitude) {
		if (latitude != 0.0 && longitude != 0.0) {
			return latitude + "," + longitude;
		} else {
			return "";
		}
	}

	public void resetLocation() {
		gpsLocation = "";
		networkLocation = "";
		betterLocation = "";
	}

	public void onResume() {
		locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
		captureLocation();
	}

	public void onStart() {
		LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
		if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || !locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
			createGpsDisabledAlert();
		}
	}

	public void onDestroy() {
		stopGPS();
	}

	public void stopGPS() {
		locationManager.removeUpdates(locationListener);
	}

	public void createGpsDisabledAlert() {
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setMessage("Please enable location services before you proceed.").setCancelable(false).setPositiveButton("Enable", new DialogInterface.OnClickListener() {

			public void onClick(DialogInterface dialog, int id) {
				showGpsOptions();
			}
		});
		AlertDialog alert = builder.create();
		alert.show();
	}

	private void showGpsOptions() {
		Intent gpsOptionsIntent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
		context.startActivity(gpsOptionsIntent);
	}

	public void getAccurateLocation() {
		double latitude = 0.0, longitude = 0.0;
		resetLocation();
		locationGPS = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
		locationNetwork = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
		if (locationNetwork != null && locationGPS != null) {
			locationBetter = getBetterLocation(locationNetwork, locationGPS);
			latitude = locationBetter.getLatitude();
			longitude = locationBetter.getLongitude();
			betterLocation = setLocation(latitude, longitude);
		}
		if (locationGPS != null) {
			latitude = locationGPS.getLatitude();
			longitude = locationGPS.getLongitude();
			gpsLocation = setLocation(latitude, longitude);
		} else {
			gpsLocation = "";
		}
		if (locationNetwork != null) {
			latitude = locationNetwork.getLatitude();
			longitude = locationNetwork.getLongitude();
			networkLocation = setLocation(latitude, longitude);
		} else {
			networkLocation = "";
		}
		if (!isLocationSet()) {
			if (gpsLocation.length() > 0) {
				betterLocation = gpsLocation;
			} else if (networkLocation.length() > 0) {
				betterLocation = networkLocation;
			}
		}
	}

	public void captureLocation() {
		locationManager.removeUpdates(locationListener);
		locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 10000, 10, locationListener);
		locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 10000, 10, locationListener);
	}

	private final LocationListener locationListener = new LocationListener() {

		public void onLocationChanged(Location location) {
		}

		public void onProviderDisabled(String provider) {
		}

		public void onProviderEnabled(String provider) {
		}

		public void onStatusChanged(String provider, int status, Bundle extras) {
		}
	};

	protected Location getBetterLocation(Location newLocation, Location currentBestLocation) {
		if (currentBestLocation == null) {
			// A new location is always better than no location
			return newLocation;
		}
		// Check whether the new location fix is newer or older
		long timeDelta = newLocation.getTime() - currentBestLocation.getTime();
		boolean isSignificantlyNewer = timeDelta > (1000 * 60 * 2);
		boolean isSignificantlyOlder = timeDelta < -(1000 * 60 * 2);
		boolean isNewer = timeDelta > 0;
		// If it's been more than two minutes since the current location, use the new location
		// because the user has likely moved.
		if (isSignificantlyNewer) {
			return newLocation;
			// If the new location is more than two minutes older, it must be worse
		} else if (isSignificantlyOlder) {
			return currentBestLocation;
		}
		// Check whether the new location fix is more or less accurate
		int accuracyDelta = (int) (newLocation.getAccuracy() - currentBestLocation.getAccuracy());
		boolean isLessAccurate = accuracyDelta > 0;
		boolean isMoreAccurate = accuracyDelta < 0;
		boolean isSignificantlyLessAccurate = accuracyDelta > 200;
		// Check if the old and new location are from the same provider
		boolean isFromSameProvider = isSameProvider(newLocation.getProvider(), currentBestLocation.getProvider());
		// Determine location quality using a combination of timeliness and accuracy
		if (isMoreAccurate) {
			return newLocation;
		} else if (isNewer && !isLessAccurate) {
			return newLocation;
		} else if (isNewer && !isSignificantlyLessAccurate && isFromSameProvider) {
			return newLocation;
		}
		return currentBestLocation;
	}

	private boolean isSameProvider(String provider1, String provider2) {
		if (provider1 == null) {
			return provider2 == null;
		}
		return provider1.equals(provider2);
	}
}