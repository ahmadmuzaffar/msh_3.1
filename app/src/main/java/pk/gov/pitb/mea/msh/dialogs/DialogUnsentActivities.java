package pk.gov.pitb.mea.msh.dialogs;

import java.util.ArrayList;

import org.json.JSONObject;

import pk.gov.pitb.mea.msh.R;
import pk.gov.pitb.mea.msh.helpers.Constants;
import pk.gov.pitb.mea.msh.helpers.MainContainer;
import pk.gov.pitb.mea.msh.models.ClassDataActivity;
import pk.gov.pitb.mea.msh.views.CustomEditText;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.graphics.Typeface;
import android.text.InputType;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;

public class DialogUnsentActivities {

	private EditText editTextLabelUnsentActivities;
	private TableLayout tableLayoutUnsentActivities;

	@SuppressLint("InflateParams")
	public void showUnsentDialog(ArrayList<ClassDataActivity> listUnsent) {
		try {
			AlertDialog.Builder builder = new AlertDialog.Builder(MainContainer.mContext);
			View viewLayout = MainContainer.mActivity.getLayoutInflater().inflate(R.layout.dialog_unsent_activities, null);
			editTextLabelUnsentActivities = (EditText) viewLayout.findViewById(R.id.dialog_label_unsent_activities);
			tableLayoutUnsentActivities = (TableLayout) viewLayout.findViewById(R.id.dialog_table_unsent_activities);

			viewLayout.setPadding((int) (MainContainer.mScreenWidth * 0.002), (int) (MainContainer.mScreenWidth * 0.002), (int) (MainContainer.mScreenWidth * 0.002),
					(int) (MainContainer.mScreenWidth * 0.002));
			LinearLayout.LayoutParams paramsEditTextTitle = new LinearLayout.LayoutParams((int) (MainContainer.mScreenWidth * 0.9), LinearLayout.LayoutParams.WRAP_CONTENT);
			paramsEditTextTitle.setMargins(0, 0, 0, 0);
			editTextLabelUnsentActivities.setLayoutParams(paramsEditTextTitle);
			LinearLayout.LayoutParams paramsScrollView;
			if (listUnsent.size() > 2) {
				paramsScrollView = new LinearLayout.LayoutParams((int) (MainContainer.mScreenWidth * 0.9), (int) (MainContainer.mScreenHeight * 0.5));
			} else {
				paramsScrollView = new LinearLayout.LayoutParams((int) (MainContainer.mScreenWidth * 0.9), LinearLayout.LayoutParams.WRAP_CONTENT);
			}
			paramsScrollView.setMargins(0, 0, 0, 0);
			((ScrollView) tableLayoutUnsentActivities.getParent()).setLayoutParams(paramsScrollView);

			editTextLabelUnsentActivities.setPadding((int) (MainContainer.mScreenWidth * .02), (int) (MainContainer.mScreenWidth * .02), (int) (MainContainer.mScreenWidth * .02),
					(int) (MainContainer.mScreenWidth * .02));

			TableRow.LayoutParams paramsLabel = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * .3), TableRow.LayoutParams.MATCH_PARENT);
			TableRow.LayoutParams paramsValue = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * .6), TableRow.LayoutParams.WRAP_CONTENT);
			paramsLabel.setMargins(0, 0, (int) (MainContainer.mScreenWidth * 0.002), 0);
			paramsValue.setMargins(0, 0, 0, 0);

			int index = 0;
			for (ClassDataActivity objectActivity : listUnsent) {
				JSONObject jsonObject = new JSONObject(objectActivity.json_data);
				JSONObject jsonObjectAppDetails = jsonObject.getJSONObject("app_details");
				String facilityType = jsonObjectAppDetails.getString("facility_type_code");
				if (facilityType.equals(Constants.DB_TYPE_ID_DHQ)) {
					facilityType = Constants.DB_TYPE_NAME_DHQ;
				} else if (facilityType.equals(Constants.DB_TYPE_ID_THQ)) {
					facilityType = Constants.DB_TYPE_NAME_THQ;
				} else if (facilityType.equals(Constants.DB_TYPE_ID_DHQTEACHING)) {
					facilityType = Constants.DB_TYPE_NAME_DHQTEACHING;
				} else {
					facilityType = "";
				}
				// addOtherDetail(index, "Structual Version:", jsonObjectAppDetails.getString("structural_version"), paramsLabel, paramsValue);
				// addOtherDetail(index, "Facility Version:", jsonObjectAppDetails.getString("facility_version"), paramsLabel, paramsValue);
				addOtherDetail(index, "Facility Type:", facilityType, paramsLabel, paramsValue);
				addOtherDetail(index, "Facility ID:", jsonObjectAppDetails.getString("facility_id"), paramsLabel, paramsValue);
				addOtherDetail(index, "Facility Name:", MainContainer.mDbAdapter.getFacilityName(jsonObjectAppDetails.getString("facility_id")), paramsLabel, paramsValue);
				addOtherDetail(index, "Sent Time:", objectActivity.sending_date_time, paramsLabel, paramsValue);
				index++;
			}

			builder.setView(viewLayout);
			// builder.setCancelable(false);
			// builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
			//
			// @Override
			// public void onClick(DialogInterface dialog, int id) {
			// }
			// });
			builder.create();
			builder.show();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void addOtherDetail(int index, String label, String value, TableRow.LayoutParams... paramsCol) {
		try {
			TableRow tableRow = new TableRow(MainContainer.mContext);
			// int index = tableLayoutUnsentActivities.getChildCount() - 1;

			CustomEditText editTextLabel = new CustomEditText();
			editTextLabel.setLayoutParams(paramsCol[0]);
			editTextLabel.setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);
			editTextLabel.setInputType(InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);
			editTextLabel.setClickable(false);
			editTextLabel.setFocusable(false);
			editTextLabel.setFocusableInTouchMode(false);
			editTextLabel.setText(label);
			editTextLabel.setTypeface(null, Typeface.BOLD);

			CustomEditText editTextValue = new CustomEditText();
			editTextValue.setLayoutParams(paramsCol[1]);
			editTextValue.setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);
			editTextValue.setInputType(InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);
			editTextValue.setClickable(false);
			editTextValue.setFocusable(false);
			editTextValue.setFocusableInTouchMode(false);
			editTextValue.setText(value);
			editTextValue.setHint("Not Available");

			editTextLabel.setTextColor(MainContainer.mContext.getResources().getColor(R.color.row_body_text));
			editTextValue.setTextColor(MainContainer.mContext.getResources().getColor(R.color.row_body_text));
			if (index % 2 == 0) {
				editTextLabel.setBackgroundColor(MainContainer.mContext.getResources().getColor(R.color.row_body_background_even));
				editTextValue.setBackgroundColor(MainContainer.mContext.getResources().getColor(R.color.row_body_background_even));
			} else {
				editTextLabel.setBackgroundColor(MainContainer.mContext.getResources().getColor(R.color.row_body_background_odd));
				editTextValue.setBackgroundColor(MainContainer.mContext.getResources().getColor(R.color.row_body_background_odd));
			}
			tableRow.addView(editTextLabel);
			tableRow.addView(editTextValue);
			tableLayoutUnsentActivities.addView(tableRow);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}