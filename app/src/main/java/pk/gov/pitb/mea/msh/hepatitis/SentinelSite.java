package pk.gov.pitb.mea.msh.hepatitis;

import android.app.Fragment;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.util.ArrayList;

import pk.gov.pitb.mea.msh.R;
import pk.gov.pitb.mea.msh.handlers.HandlerFragmentCallBackHepatitis;
import pk.gov.pitb.mea.msh.helpers.AlertDialogs;
import pk.gov.pitb.mea.msh.helpers.Constants;
import pk.gov.pitb.mea.msh.helpers.MainContainer;
import pk.gov.pitb.mea.msh.helpers.UIHelper;
import pk.gov.pitb.mea.msh.models.ClassScreenData;
import pk.gov.pitb.mea.msh.models.ClassScreenItem;
import pk.gov.pitb.mea.msh.textwatchers.TextWatcherAutoFill;
import pk.gov.pitb.mea.msh.textwatchers.TextWatcherCountManyChilds;
import pk.gov.pitb.mea.msh.views.CustomEditText;
import pk.gov.pitb.mea.msh.views.CustomToggleButtonTwoState;


/**
 * Created by Muhammad Abdullah Azam Khan.
 */
public class SentinelSite extends Fragment implements HandlerFragmentCallBackHepatitis {

	/*private Globals MainContainer;*/
	private View parentView;
	private ArrayList<ClassScreenItem> arrayListDisplays;
	private ArrayList<ClassScreenItem> arrayListHr;
	private ArrayList<ClassScreenItem> arrayListHepatitis;
	//private ArrayList<ClassScreenItem> arrayListDisplaysDropdown;

	private LinearLayout linearLayoutMain;
	private TextView textViewDisplays;
	private TableLayout tableLayoutDisplays;



	//private boolean bIsEquipmentsTotalAvailableEditable;
	private ArrayList<ClassScreenItem> arrayListEquipments;
	//private ArrayList<ClassFacilityItem> arrayListEquipmentsTotalAvailable;

	private ArrayList<ClassScreenItem> arrayListSupplies;
	private ArrayList<ClassScreenItem> arrayListMedicines;

	private TextView textViewEquipments;
	private TableLayout tableLayoutEquipments;

	private TextView textViewSupplies;
	private TableLayout tableLayoutSupplies;

	private TextView textViewMedicines;
	private TableLayout tableLayoutMedicines;

	private TextView textViewHepatitis;
	private TableLayout tableLayoutHepatitis;


	private TextView textViewHr;
	private TableLayout tableLayoutHr;

	private Button buttonProceed;
	private Button buttonPrevious, buttonNext, buttonSubmit;
	private EditText commentBox;


	@Override
	public void parseObject() {
		try {
			JSONArray jsonArraySection1 = new JSONArray();
			for (int i = 1; i < tableLayoutDisplays.getChildCount(); i++) {
				View child = tableLayoutDisplays.getChildAt(i);
				ClassScreenItem object = arrayListDisplays.get(i - 1);
				if (child instanceof TableRow) {
					JSONObject jsonObjectChild = new JSONObject();

					TableRow tableRow = (TableRow) child;
					CustomToggleButtonTwoState displayed = (CustomToggleButtonTwoState) tableRow.getChildAt(2);

					String displayedID = "";

					if (!displayed.isCheckChanged()) {
						displayedID="";
					}else if (displayed.isChecked()) {
						displayedID = /*arrayListDisplaysDropdown.get(0).item_id*/"yes";
					} else if (displayed.isEnabled()) {
						displayedID = /*arrayListDisplaysDropdown.get(1).item_id*/"no";
					}
					jsonObjectChild.put("sentinel_site_id", object.item_id);
					jsonObjectChild.put("sentinel_site_name", object.item_name);
					jsonObjectChild.put("status", displayedID);

					jsonArraySection1.put(jsonObjectChild);
				}
			}

			JSONArray jsonArraySection2 = new JSONArray();
			for (int i = 1; i < tableLayoutHr.getChildCount(); i++) {
				View child = tableLayoutHr.getChildAt(i);
				ClassScreenItem object = arrayListHr.get(i - 1);
				if (child instanceof TableRow) {
					JSONObject jsonObjectChild = new JSONObject();

					TableRow tableRow = (TableRow) child;
					CustomToggleButtonTwoState assigned = (CustomToggleButtonTwoState) tableRow.getChildAt(2);
					CustomToggleButtonTwoState present = (CustomToggleButtonTwoState) tableRow.getChildAt(3);
					//EditText comments = (EditText) tableRow.getChildAt(4);

					String assignedID = "", presentID="";

					if (!assigned.isCheckChanged()) {
						assignedID="";
					}else if (assigned.isChecked()) {
						assignedID = "yes";
					} else if (assigned.isEnabled()) {
						assignedID = "no";
					}

					if (!present.isCheckChanged()) {
						presentID="";
					}else if (present.isChecked()) {
						presentID = "yes";
					} else if (present.isEnabled()) {
						presentID = "no";
					}
					jsonObjectChild.put("sentinel_site_id", object.item_id);
					jsonObjectChild.put("sentinel_site_name", object.item_name);
					jsonObjectChild.put("notification_available", assignedID);
					jsonObjectChild.put("present",presentID);
					//jsonObjectChild.put("comments", comments.getText().toString());

					jsonArraySection2.put(jsonObjectChild);
				}
			}


			JSONArray jsonArraySection3 = new JSONArray();
			for (int i = 1; i < tableLayoutEquipments.getChildCount(); i++) {
				View child = tableLayoutEquipments.getChildAt(i);
				ClassScreenItem object = arrayListEquipments.get(i - 1);
				JSONObject jsonObjectChild = new JSONObject();

				TableRow tableRow = (TableRow) child;
				EditText available = (EditText) tableRow.getChildAt(2);
				EditText functional = (EditText) tableRow.getChildAt(3);
				EditText repairable = (EditText) tableRow.getChildAt(4);
				//EditText nonRepairable = (EditText) tableRow.getChildAt(5);

				jsonObjectChild.put("sentinel_site_id", object.item_id);
				jsonObjectChild.put("supplies", object.item_name);
				jsonObjectChild.put("total_available", available.getText().toString().trim());
				jsonObjectChild.put("functional", functional.getText().toString().trim());
				jsonObjectChild.put("non_functional", repairable.getText().toString().trim());
				//jsonObjectChild.put("non_repairable", nonRepairable.getText().toString().trim());

				jsonArraySection3.put(jsonObjectChild);
			}

			JSONArray jsonArraySection4 = new JSONArray();
			for (int i = 1; i < tableLayoutSupplies.getChildCount(); i++) {
				View child = tableLayoutSupplies.getChildAt(i);
				ClassScreenItem object = arrayListSupplies.get(i - 1);
				JSONObject jsonObjectChild = new JSONObject();

				TableRow tableRow = (TableRow) child;
				EditText available = (EditText) tableRow.getChildAt(2);
				/*EditText functional = (EditText) tableRow.getChildAt(3);
				EditText repairable = (EditText) tableRow.getChildAt(4);*/
				//EditText nonRepairable = (EditText) tableRow.getChildAt(5);
				//EditText comments = (EditText) tableRow.getChildAt(6);*/

				jsonObjectChild.put("sentinel_site_id", object.item_id);
				jsonObjectChild.put("sentinel_site_name", object.item_name);
				jsonObjectChild.put("total_provided", available.getText().toString().trim());
				/*jsonObjectChild.put("last_month_consumption", functional.getText().toString().trim());
				jsonObjectChild.put("balance_on_visit", repairable.getText().toString().trim());*/
				//jsonObjectChild.put("stabilization", nonRepairable.getText().toString().trim());
				//jsonObjectChild.put("comments", comments.getText().toString());

				jsonArraySection4.put(jsonObjectChild);
			}

			JSONArray jsonArraySection5 = new JSONArray();
			for (int i = 1; i < tableLayoutMedicines.getChildCount(); i++) {
				View child = tableLayoutMedicines.getChildAt(i);
				ClassScreenItem object = arrayListMedicines.get(i - 1);
				JSONObject jsonObjectChild = new JSONObject();

				TableRow tableRow = (TableRow) child;
				EditText available = (EditText) tableRow.getChildAt(2);
				EditText functional = (EditText) tableRow.getChildAt(3);
				EditText repairable = (EditText) tableRow.getChildAt(4);
				//EditText nonRepairable = (EditText) tableRow.getChildAt(5);
				//EditText comments = (EditText) tableRow.getChildAt(6);

				jsonObjectChild.put("sentinel_site_id", object.item_id);
				jsonObjectChild.put("sentinel_site_name", object.item_name);
				jsonObjectChild.put("medicines_no_received", available.getText().toString().trim());
				jsonObjectChild.put("no_patient_receiving", functional.getText().toString().trim());
				jsonObjectChild.put("no_patient_waiting", repairable.getText().toString().trim());
				//jsonObjectChild.put("expired", nonRepairable.getText().toString().trim());
				//jsonObjectChild.put("comments", comments.getText().toString());

				jsonArraySection5.put(jsonObjectChild);
			}

            JSONArray jsonArraySection6 = new JSONArray();
            for (int i = 1; i < tableLayoutHepatitis.getChildCount(); i++) {
                View child = tableLayoutHepatitis.getChildAt(i);
                ClassScreenItem object = arrayListHepatitis.get(i - 1);
                JSONObject jsonObjectChild = new JSONObject();

                TableRow tableRow = (TableRow) child;
                EditText available = (EditText) tableRow.getChildAt(2);
                /*EditText functional = (EditText) tableRow.getChildAt(3);
                EditText repairable = (EditText) tableRow.getChildAt(4);*/
                //EditText nonRepairable = (EditText) tableRow.getChildAt(5);
                //EditText comments = (EditText) tableRow.getChildAt(6);

                jsonObjectChild.put("sentinel_site_id", object.item_id);
                jsonObjectChild.put("sentinel_site_name", object.item_name);
                jsonObjectChild.put("total", available.getText().toString().trim());
               /* jsonObjectChild.put("total", functional.getText().toString().trim());
                jsonObjectChild.put("total", repairable.getText().toString().trim());*/
                //jsonObjectChild.put("stabilization", nonRepairable.getText().toString().trim());
                //jsonObjectChild.put("comments", comments.getText().toString());

                jsonArraySection6.put(jsonObjectChild);
            }

			JSONObject jsonObject = new JSONObject();
			jsonObject.put("section1", jsonArraySection1);
			jsonObject.put("section2", jsonArraySection2);
			jsonObject.put("section3", jsonArraySection3);
			jsonObject.put("section4", jsonArraySection4);
			jsonObject.put("section5", jsonArraySection5);
			jsonObject.put("section6", jsonArraySection6);
			jsonObject.put("comments", commentBox.getText().toString());
			MainContainer.mJsonObjectFormData.remove(Constants.DB_SCREEN_NAME_SENTINEL);
			MainContainer.mJsonObjectFormData.put(Constants.DB_SCREEN_NAME_SENTINEL, jsonObject);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	public void loadSavedData(){
		try{
			if(MainContainer.mJsonObjectFormData.has(Constants.DB_SCREEN_NAME_SENTINEL)){
				JSONObject jsonObjectComment = MainContainer.mJsonObjectFormData.getJSONObject(Constants.DB_SCREEN_NAME_SENTINEL);
				JSONArray JSONArraySections = jsonObjectComment.has("section1") ? jsonObjectComment.getJSONArray("section1") : new JSONArray();

				for (int i = 0; i < JSONArraySections.length(); i++) {
					boolean bIsOdd = i % 2 == 0;

					View child = tableLayoutDisplays.getChildAt(i+1);
					JSONObject jObject =  JSONArraySections.getJSONObject(i);

					if (child instanceof TableRow) {

						TableRow tableRow = (TableRow) child;
						CustomToggleButtonTwoState displayedToggleButton = (CustomToggleButtonTwoState) tableRow.getChildAt(2);
						String displayedID = jObject.getString("sentinel_site_id");
						String dd_ID = jObject.getString("status");
						if(dd_ID.equals("yes")){
							displayedToggleButton.setbIsCheckChanged(true);
							displayedToggleButton.setChecked(true);
							displayedToggleButton.setBackgroundResource(bIsOdd ? R.drawable.toggle_tick_odd : R.drawable.toggle_tick_even);
						}else if(dd_ID.equals("no")){
							displayedToggleButton.setbIsCheckChanged(true);
							displayedToggleButton.setChecked(false);
							displayedToggleButton.setBackgroundResource(bIsOdd ? R.drawable.toggle_cross_odd : R.drawable.toggle_cross_even);
						}

					}
				}
				String stringSection2 = jsonObjectComment.has("section3") ? jsonObjectComment.getString("section3") : "";
				JSONArray jsonArraySection2 = new JSONArray(stringSection2);
				for (int i = 1; i < tableLayoutEquipments.getChildCount(); i++) {
					View child = tableLayoutEquipments.getChildAt(i);
					JSONObject jsonObjectsections = jsonArraySection2.getJSONObject(i-1);

					if (child instanceof TableRow) {

						TableRow tableRow = (TableRow) child;
						EditText available = (EditText) tableRow.getChildAt(2);
						EditText functional = (EditText) tableRow.getChildAt(3);
						EditText repairable = (EditText) tableRow.getChildAt(4);


						String med_equipment_id = jsonObjectsections.has("sentinel_site_id")? jsonObjectsections.getString("sentinel_site_id"):"";
						String supplies = jsonObjectsections.has("supplies")? jsonObjectsections.getString("supplies"):"";
						String sTotal_available = jsonObjectsections.has("total_available")? jsonObjectsections.getString("total_available"):"";
						String sFunctional = jsonObjectsections.has("functional")? jsonObjectsections.getString("functional"):"";
						String sNon_functional_but_repairable = jsonObjectsections.has("non_functional")? jsonObjectsections.getString("non_functional"):"";

						available.setText(sTotal_available);
						functional.setText(sFunctional);
						repairable.setText(sNon_functional_but_repairable);


					}
				}

				String stringSection3 = jsonObjectComment.has("section4") ? jsonObjectComment.getString("section4") : "";
				JSONArray jsonArraySection3 = new JSONArray(stringSection3);
				for (int i = 1; i < tableLayoutSupplies.getChildCount(); i++) {
					View child = tableLayoutSupplies.getChildAt(i);
					JSONObject jsonObjectsections = jsonArraySection3.getJSONObject(i-1);

					if (child instanceof TableRow) {

						TableRow tableRow = (TableRow) child;
						EditText available = (EditText) tableRow.getChildAt(2);
//						EditText functional = (EditText) tableRow.getChildAt(3);
//						EditText repairable = (EditText) tableRow.getChildAt(4);

						String sTotal_available = jsonObjectsections.has("total_provided")? jsonObjectsections.getString("total_provided"):"";
						/*String sFunctional = jsonObjectsections.has("last_month_consumption")? jsonObjectsections.getString("last_month_consumption"):"";
						String sNon_functional_but_repairable = jsonObjectsections.has("balance_on_visit")? jsonObjectsections.getString("balance_on_visit"):"";
*/
						available.setText(sTotal_available);
						/*functional.setText(sFunctional);
						repairable.setText(sNon_functional_but_repairable);*/


					}
				}

				String stringSection4 = jsonObjectComment.has("section5") ? jsonObjectComment.getString("section5") : "";
				JSONArray jsonArraySection4 = new JSONArray(stringSection4);
				for (int i = 1; i < tableLayoutMedicines.getChildCount(); i++) {
					View child = tableLayoutMedicines.getChildAt(i);
					JSONObject jsonObjectsections = jsonArraySection4.getJSONObject(i-1);

					if (child instanceof TableRow) {

						TableRow tableRow = (TableRow) child;
						EditText available = (EditText) tableRow.getChildAt(2);
						EditText functional = (EditText) tableRow.getChildAt(3);
						EditText repairable = (EditText) tableRow.getChildAt(4);

						String sTotal_available = jsonObjectsections.has("medicines_no_received")? jsonObjectsections.getString("medicines_no_received"):"";
						String sFunctional = jsonObjectsections.has("no_patient_receiving")? jsonObjectsections.getString("no_patient_receiving"):"";
						String sNon_functional_but_repairable = jsonObjectsections.has("no_patient_waiting")? jsonObjectsections.getString("no_patient_waiting"):"";


						available.setText(sTotal_available);
						functional.setText(sFunctional);
						repairable.setText(sNon_functional_but_repairable);


					}
				}
				String stringSection5 = jsonObjectComment.has("section2") ? jsonObjectComment.getString("section2") : "";
				JSONArray jsonArraySection5 = new JSONArray(stringSection5);
				for (int i = 1; i < tableLayoutHr.getChildCount(); i++) {
					View child = tableLayoutHr.getChildAt(i);
					JSONObject jsonObjectsections = jsonArraySection5.getJSONObject(i-1);
					boolean bIsOdd = i % 2 == 0;
					bIsOdd = !bIsOdd;
					if (child instanceof TableRow) {
						JSONObject jsonObjectChild = new JSONObject();

						TableRow tableRow = (TableRow) child;
						CustomToggleButtonTwoState assigned = (CustomToggleButtonTwoState) tableRow.getChildAt(2);
						CustomToggleButtonTwoState present = (CustomToggleButtonTwoState) tableRow.getChildAt(3);


						String dd_ID = jsonObjectsections.getString("notification_available");
						if(dd_ID.equals("yes")){
							assigned.setbIsCheckChanged(true);
							assigned.setChecked(true);
							assigned.setBackgroundResource(bIsOdd ? R.drawable.toggle_tick_odd : R.drawable.toggle_tick_even);
						}else if(dd_ID.equals("no")){
							assigned.setbIsCheckChanged(true);
							assigned.setChecked(false);
							assigned.setBackgroundResource(bIsOdd ? R.drawable.toggle_cross_odd : R.drawable.toggle_cross_even);
						}

						dd_ID = jsonObjectsections.getString("present");
						if(dd_ID.equals("yes")){
							present.setbIsCheckChanged(true);
							present.setChecked(true);
							present.setBackgroundResource(bIsOdd ? R.drawable.toggle_tick_odd : R.drawable.toggle_tick_even);
						}else if(dd_ID.equals("no")){
							present.setbIsCheckChanged(true);
							present.setChecked(false);
							present.setBackgroundResource(bIsOdd ? R.drawable.toggle_cross_odd : R.drawable.toggle_cross_even);
						}
					}
				}

                String stringSection6 = jsonObjectComment.has("section6") ? jsonObjectComment.getString("section6") : "";
                JSONArray jsonArraySection6 = new JSONArray(stringSection6);
                for (int i = 1; i < tableLayoutHepatitis.getChildCount(); i++) {
                    View child = tableLayoutHepatitis.getChildAt(i);
                    JSONObject jsonObjectsections = jsonArraySection6.getJSONObject(i-1);

                    if (child instanceof TableRow) {

                        TableRow tableRow = (TableRow) child;
                        EditText available = (EditText) tableRow.getChildAt(2);
                       /* EditText functional = (EditText) tableRow.getChildAt(3);
                        EditText repairable = (EditText) tableRow.getChildAt(4);*/

                        String sTotal_available = jsonObjectsections.has("total")? jsonObjectsections.getString("total"):"";
                        /*String sFunctional = jsonObjectsections.has("total")? jsonObjectsections.getString("total"):"";
                        String sNon_functional_but_repairable = jsonObjectsections.has("total")? jsonObjectsections.getString("total"):"";*/

                        available.setText(sTotal_available);
                        /*functional.setText(sFunctional);
                        repairable.setText(sNon_functional_but_repairable);*/

                    }
                }

                String comments = jsonObjectComment.getString("comments");

                commentBox.setText(comments);

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	private String errorMessage;

	@Override
	public String onFragmentChanged(int previousPosition) {
		errorMessage = null;
		isFormValid();
		return errorMessage;
	}

	@Override
	public boolean isFormValid() {
		try {
			boolean bIsValid = true;
			for (int i = 1; i < tableLayoutDisplays.getChildCount(); i++) {
				View child = tableLayoutDisplays.getChildAt(i);
				if (child instanceof TableRow) {
					TableRow tableRow = (TableRow) child;
					CustomToggleButtonTwoState displayed = (CustomToggleButtonTwoState) tableRow.getChildAt(2);

					if (!displayed.isCheckChanged()) {
						bIsValid = false;
						errorMessage = "Please select all display values to continue";
						return bIsValid;
					}
				}
			}

			for (int i = 1; i < tableLayoutEquipments.getChildCount(); i++) {
				View child = tableLayoutEquipments.getChildAt(i);
				TableRow tableRow = (TableRow) child;
				EditText available = (EditText) tableRow.getChildAt(2);
				EditText functional = (EditText) tableRow.getChildAt(3);
				EditText repairable = (EditText) tableRow.getChildAt(4);
				//EditText nonRepairable = (EditText) tableRow.getChildAt(5);

				if (available.getText().toString().trim().length() == 0 || functional.getText().toString().trim().length() == 0
						|| repairable.getText().toString().trim().length() == 0 /*|| nonRepairable.getText().toString().trim().length() == 0*/) {
					bIsValid = false;
					errorMessage = "Please fill all Equipments details to continue";
					return bIsValid;
				}
			}

			for (int i = 1; i < tableLayoutSupplies.getChildCount(); i++) {
				View child = tableLayoutSupplies.getChildAt(i);
				TableRow tableRow = (TableRow) child;
				EditText available = (EditText) tableRow.getChildAt(2);
				/*EditText functional = (EditText) tableRow.getChildAt(3);
				EditText repairable = (EditText) tableRow.getChildAt(4);*/
				/*EditText nonRepairable = (EditText) tableRow.getChildAt(5);
				EditText comments = (EditText) tableRow.getChildAt(6);
*/
				if (available.getText().toString().trim().length() == 0 /*|| functional.getText().toString().trim().length() == 0
						|| repairable.getText().toString().trim().length() == 0 || nonRepairable.getText().toString().trim().length() == 0
						|| comments.getText().toString().length() == 0*/) {
					bIsValid = false;
					errorMessage = "Please fill all Supplies details to continue";
					return bIsValid;
				}
			}

			for (int i = 1; i < tableLayoutMedicines.getChildCount(); i++) {
				View child = tableLayoutMedicines.getChildAt(i);
				TableRow tableRow = (TableRow) child;
				EditText available = (EditText) tableRow.getChildAt(2);
				EditText functional = (EditText) tableRow.getChildAt(3);
				EditText repairable = (EditText) tableRow.getChildAt(4);/*
				EditText nonRepairable = (EditText) tableRow.getChildAt(5);
				EditText comments = (EditText) tableRow.getChildAt(6);*/

				if (available.getText().toString().trim().length() == 0 || functional.getText().toString().trim().length() == 0
						|| repairable.getText().toString().trim().length() == 0 /*|| nonRepairable.getText().toString().trim().length() == 0
						|| comments.getText().toString().length() == 0*/) {
					bIsValid = false;
					errorMessage = "Please fill all Medicines details to continue";
					return bIsValid;
				}
			}

			for (int i = 1; i < tableLayoutHepatitis.getChildCount(); i++) {
				View child = tableLayoutHepatitis.getChildAt(i);
				TableRow tableRow = (TableRow) child;
				EditText available = (EditText) tableRow.getChildAt(2);
				/*EditText functional = (EditText) tableRow.getChildAt(3);
				EditText repairable = (EditText) tableRow.getChildAt(4);*/
				/*EditText nonRepairable = (EditText) tableRow.getChildAt(5);
				EditText comments = (EditText) tableRow.getChildAt(6);
*/
				if (available.getText().toString().trim().length() == 0 /*|| functional.getText().toString().trim().length() == 0
						|| repairable.getText().toString().trim().length() == 0 || nonRepairable.getText().toString().trim().length() == 0
						|| comments.getText().toString().length() == 0*/) {
					bIsValid = false;
					errorMessage = "Please fill all Hepatitis details to continue";
					return bIsValid;
				}
			}


			for (int i = 1; i < tableLayoutHr.getChildCount(); i++) {
				View child = tableLayoutHr.getChildAt(i);
				if (child instanceof TableRow) {
					TableRow tableRow = (TableRow) child;
					CustomToggleButtonTwoState assigned = (CustomToggleButtonTwoState) tableRow.getChildAt(2);
					CustomToggleButtonTwoState present = (CustomToggleButtonTwoState) tableRow.getChildAt(3);
					//EditText comments = (EditText) tableRow.getChildAt(4);

					if (!assigned.isCheckChanged()) {
						bIsValid = false;
						errorMessage = "Please select all values in assigned column of HR table to continue";
						return bIsValid;
					}
					if (!present.isCheckChanged()) {
						bIsValid = false;
						errorMessage = "Please select all values in present column of HR table to continue";
						return bIsValid;
					}
					/*if (comments.getText().toString().equals("")) {
						bIsValid = false;
						errorMessage = "Please enter all comments in HR table to continue";
						return bIsValid;
					}*/

				}
			}
			if(commentBox.getText().length() == 0){
				bIsValid = false;
				errorMessage = "Please enter comments";
				return bIsValid;
			}

			parseObject();
			return bIsValid;
		} catch (Exception e) {
			e.printStackTrace();
			MainContainer.showMessage("Error Processing Your Request");
			return false;
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		if (parentView == null) {
			parentView = inflater.inflate(R.layout.fragment_hepatitis_sentinial, container, false);
			/*MainContainer = Globals.getInstance();*/
			initFragmentData();
			generateBody();
			loadSavedData();
		}
		return parentView;
	}

	private void initFragmentData() {
		try {
			ClassScreenData objectScreenData = new ClassScreenData();
			objectScreenData.setDefaultValues();
			objectScreenData.screen_id = Constants.DB_SCREEN_ID_SENTINEL;
			objectScreenData.screen_name = Constants.DB_SCREEN_NAME_SENTINEL;


			objectScreenData.section_id = "";
			objectScreenData.type_id = Constants.DB_TYPE_ID_HEPATITIS;
			objectScreenData.type_name = Constants.DB_TYPE_NAME_HEPATITIS;

			objectScreenData.section_name = "section4";
			objectScreenData.section_value = "fields";
			objectScreenData = MainContainer.mDbAdapter.selectScreenData(objectScreenData);
			arrayListSupplies = MainContainer.mDbAdapter.selectScreenItems(objectScreenData);


			objectScreenData.section_name = "section5";
			objectScreenData.section_value = "fields";
			objectScreenData = MainContainer.mDbAdapter.selectScreenData(objectScreenData);
			arrayListMedicines = MainContainer.mDbAdapter.selectScreenItems(objectScreenData);


			objectScreenData.section_name = "section3";
			objectScreenData.section_value = "fields";
			objectScreenData = MainContainer.mDbAdapter.selectScreenData(objectScreenData);
			arrayListEquipments = MainContainer.mDbAdapter.selectScreenItems(objectScreenData);


			objectScreenData.section_name = "section1";
			objectScreenData.section_value = "fields";
			objectScreenData = MainContainer.mDbAdapter.selectScreenData(objectScreenData);
			arrayListDisplays = MainContainer.mDbAdapter.selectScreenItems(objectScreenData);


			objectScreenData.section_name = "section2";
			objectScreenData.section_value = "fields";
			objectScreenData = MainContainer.mDbAdapter.selectScreenData(objectScreenData);
			arrayListHr = MainContainer.mDbAdapter.selectScreenItems(objectScreenData);

			objectScreenData.section_name = "section6";
			objectScreenData.section_value = "fields";
			objectScreenData = MainContainer.mDbAdapter.selectScreenData(objectScreenData);
			arrayListHepatitis = MainContainer.mDbAdapter.selectScreenItems(objectScreenData);

			/*objectScreenData.screen_id = Constants.DB_SCREEN_ID_OUTLOOK_UTILITIES;
			objectScreenData.screen_name = Constants.DB_SCREEN_NAME_OUTLOOK_UTILITIES;

			objectScreenData.section_id = "1";
			objectScreenData.section_name = "section1";
			objectScreenData.section_value = "fields";
			objectScreenData = MainContainer.mDbAdapter.selectScreenData(objectScreenData);
			arrayListDisplays = MainContainer.mDbAdapter.selectScreenItems(objectScreenData);
			arrayListEquipments = MainContainer.mDbAdapter.selectScreenItems(objectScreenData);
			arrayListSupplies = MainContainer.mDbAdapter.selectScreenItems(objectScreenData);
			arrayListMedicines = MainContainer.mDbAdapter.selectScreenItems(objectScreenData);
			arrayListHr = MainContainer.mDbAdapter.selectScreenItems(objectScreenData);*/

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void generateBody() {
		try {

			LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
			params.setMargins(2, 0, 0, 5);

			commentBox = new EditText(getActivity());

			final Field f = TextView.class.getDeclaredField("mCursorDrawableRes");
			f.setAccessible(true);
			f.set(commentBox, R.drawable.xml_color_cursor);
			commentBox.setHint("Enter Comments");
			commentBox.setLayoutParams(params);
			commentBox.setTextColor(getResources().getColor(R.color.row_body_text));
			commentBox.setBackgroundColor(getResources().getColor(R.color.row_body_background_single));

			linearLayoutMain = (LinearLayout) parentView.findViewById(R.id.linearlayout_main);
			buttonNext = (Button) parentView.findViewById(R.id.button_next);
			buttonSubmit = (Button) parentView.findViewById(R.id.button_submit);
			buttonPrevious = (Button) parentView.findViewById(R.id.button_previous);
			buttonNext.setVisibility(View.GONE);
			buttonSubmit.setVisibility(View.VISIBLE);

			textViewDisplays = new TextView(MainContainer.mContext);
			tableLayoutDisplays = new TableLayout(MainContainer.mContext);
			linearLayoutMain.addView(textViewDisplays);
			linearLayoutMain.addView(tableLayoutDisplays);


			textViewHr = new TextView(MainContainer.mContext);
			tableLayoutHr = new TableLayout(MainContainer.mContext);
			linearLayoutMain.addView(textViewHr);
			linearLayoutMain.addView(tableLayoutHr);

			textViewEquipments = new TextView(MainContainer.mContext);
			tableLayoutEquipments = new TableLayout(MainContainer.mContext);
			linearLayoutMain.addView(textViewEquipments);
			linearLayoutMain.addView(tableLayoutEquipments);

			textViewSupplies = new TextView(MainContainer.mContext);
			tableLayoutSupplies = new TableLayout(MainContainer.mContext);
			linearLayoutMain.addView(textViewSupplies);
			linearLayoutMain.addView(tableLayoutSupplies);

			textViewMedicines = new TextView(MainContainer.mContext);
			tableLayoutMedicines = new TableLayout(MainContainer.mContext);
			linearLayoutMain.addView(textViewMedicines);
			linearLayoutMain.addView(tableLayoutMedicines);

			textViewHepatitis = new TextView(MainContainer.mContext);
			tableLayoutHepatitis = new TableLayout(MainContainer.mContext);
			linearLayoutMain.addView(textViewHepatitis);
			linearLayoutMain.addView(tableLayoutHepatitis);

			linearLayoutMain.addView(commentBox);

			LinearLayout.LayoutParams lpButtonProceed = new LinearLayout.LayoutParams((int) (MainContainer.mScreenWidth * 0.5),
					(int) (MainContainer.mScreenWidth * 0.1));
			lpButtonProceed.setMargins(0, (int) (MainContainer.mScreenHeight * 0.05), 0, 0);
			buttonProceed = new Button(MainContainer.mContext);
			buttonProceed.setBackgroundDrawable(MainContainer.mActivity.getResources().getDrawable(R.drawable.background_btn_continue));
			buttonProceed.setGravity(Gravity.CENTER);
			buttonProceed.setLayoutParams(lpButtonProceed);
			//linearLayoutMain.addView(buttonProceed);

			buttonProceed.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					//MainContainer.activityCallBackNutrients.showNextFragment();
					/*if(isFormValid()) {
						MainContainer.activityCallBackHepatitis.submitData();
					}else{
						AlertDialogs.getInstance().showDialogOK(null, errorMessage, null, true);
					}*/
				}
			});

			TableRow.LayoutParams[] paramsArrayTableRowColumns = new TableRow.LayoutParams[3];
			paramsArrayTableRowColumns[0] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.1), TableRow.LayoutParams.MATCH_PARENT);
			paramsArrayTableRowColumns[1] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.66), (int) (MainContainer.mScreenWidth * 0.08));
			paramsArrayTableRowColumns[2] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.2), (int) (MainContainer.mScreenWidth * 0.08));
			for (int i = 1; i < paramsArrayTableRowColumns.length; i++) {
				paramsArrayTableRowColumns[i].setMargins(2, 0, 0, 0);
			}

			UIHelper uiContainer = new UIHelper();
			uiContainer.generateTableLabel(textViewDisplays, "Infrastructure and Protocol Displayed");

			uiContainer.generateTableHeader(tableLayoutDisplays, paramsArrayTableRowColumns, new String[] { "Sr#", "Name", "Status" });
			generateTableDisplays(paramsArrayTableRowColumns);

			if (tableLayoutDisplays.getChildCount() > 1) {
				textViewDisplays.setVisibility(View.VISIBLE);
				tableLayoutDisplays.setVisibility(View.VISIBLE);
			} else {
				textViewDisplays.setVisibility(View.GONE);
				tableLayoutDisplays.setVisibility(View.GONE);
			}



			TableRow.LayoutParams[] paramsArrayTableRowColumnsEquipments = new TableRow.LayoutParams[5];
			paramsArrayTableRowColumnsEquipments[0] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.1),
					TableRow.LayoutParams.MATCH_PARENT);
			paramsArrayTableRowColumnsEquipments[1] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.26),
					TableRow.LayoutParams.MATCH_PARENT);
			paramsArrayTableRowColumnsEquipments[2] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.15),
					TableRow.LayoutParams.MATCH_PARENT);
			paramsArrayTableRowColumnsEquipments[3] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.15),
					TableRow.LayoutParams.MATCH_PARENT);
			paramsArrayTableRowColumnsEquipments[4] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.15),
					TableRow.LayoutParams.WRAP_CONTENT);/*
			paramsArrayTableRowColumnsEquipments[5] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.15),
					TableRow.LayoutParams.MATCH_PARENT);*/
			for (int i = 1; i < paramsArrayTableRowColumnsEquipments.length; i++) {
				paramsArrayTableRowColumnsEquipments[i].setMargins(2, 0, 0, 0);
			}

			uiContainer.generateTableLabel(textViewEquipments, "Equipments");
			uiContainer.generateTableHeader(tableLayoutEquipments, paramsArrayTableRowColumnsEquipments, "No.", "Equipment", "Total Available",
					"Functional", "Non-Functional");
			generateTableEquipmentsDetails(paramsArrayTableRowColumnsEquipments);

			if (tableLayoutEquipments.getChildCount() > 1) {
				textViewEquipments.setVisibility(View.VISIBLE);
				tableLayoutEquipments.setVisibility(View.VISIBLE);
			} else {
				textViewEquipments.setVisibility(View.GONE);
				tableLayoutEquipments.setVisibility(View.GONE);
			}



			TableRow.LayoutParams[] paramsArrayTableRowColumnsSupplies = new TableRow.LayoutParams[3];
			paramsArrayTableRowColumnsSupplies[0] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.1),
					TableRow.LayoutParams.MATCH_PARENT);
			paramsArrayTableRowColumnsSupplies[1] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.64),
					TableRow.LayoutParams.MATCH_PARENT);
			paramsArrayTableRowColumnsSupplies[2] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.16),
					TableRow.LayoutParams.MATCH_PARENT);
/*			paramsArrayTableRowColumnsSupplies[3] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.12),
					TableRow.LayoutParams.MATCH_PARENT);
			paramsArrayTableRowColumnsSupplies[4] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.12),
					TableRow.LayoutParams.WRAP_CONTENT);*//*
			paramsArrayTableRowColumnsSupplies[5] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.12),
					TableRow.LayoutParams.MATCH_PARENT);
			paramsArrayTableRowColumnsSupplies[6] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.12),
					TableRow.LayoutParams.MATCH_PARENT);*/
			for (int i = 1; i < paramsArrayTableRowColumnsSupplies.length; i++) {
				paramsArrayTableRowColumnsSupplies[i].setMargins(2, 0, 0, 0);
			}

			uiContainer.generateTableLabel(textViewSupplies, "Supplies");
			uiContainer.generateTableHeader(tableLayoutSupplies, paramsArrayTableRowColumnsSupplies, "No.", "Name", "Total Provided");
			generateTableSupplies(paramsArrayTableRowColumnsSupplies);

//			if (tableLayoutSupplies.getChildCount() > 1) {
//				textViewSupplies.setVisibility(View.VISIBLE);
//				tableLayoutSupplies.setVisibility(View.VISIBLE);
//			} else {
//				textViewSupplies.setVisibility(View.GONE);
//				tableLayoutSupplies.setVisibility(View.GONE);
//			}

			TableRow.LayoutParams[] paramsArrayTableRowColumnsMedicines = new TableRow.LayoutParams[5];
			paramsArrayTableRowColumnsMedicines[0] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.1),
					TableRow.LayoutParams.MATCH_PARENT);
			paramsArrayTableRowColumnsMedicines[1] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.26),
					TableRow.LayoutParams.MATCH_PARENT);
			paramsArrayTableRowColumnsMedicines[2] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.12),
					TableRow.LayoutParams.MATCH_PARENT);
			paramsArrayTableRowColumnsMedicines[3] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.12),
					TableRow.LayoutParams.MATCH_PARENT);
			paramsArrayTableRowColumnsMedicines[4] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.12),
					TableRow.LayoutParams.WRAP_CONTENT);/*
			paramsArrayTableRowColumnsMedicines[5] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.12),
					TableRow.LayoutParams.MATCH_PARENT);
			paramsArrayTableRowColumnsMedicines[6] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.12),
					TableRow.LayoutParams.MATCH_PARENT);*/
			for (int i = 1; i < paramsArrayTableRowColumnsMedicines.length; i++) {
				paramsArrayTableRowColumnsMedicines[i].setMargins(2, 0, 0, 0);
			}

			uiContainer.generateTableLabel(textViewMedicines, "Medicines");
			uiContainer.generateTableHeader(tableLayoutMedicines, paramsArrayTableRowColumnsMedicines, "No.", "Name", "No.of medicines received",
					"No. of patients registered receiving treatment", "No. of patients registered in waiting list");
			generateTableMedicines(paramsArrayTableRowColumnsMedicines);

			if (tableLayoutMedicines.getChildCount() > 1) {
				textViewMedicines.setVisibility(View.VISIBLE);
				tableLayoutMedicines.setVisibility(View.VISIBLE);
			} else {
				textViewMedicines.setVisibility(View.GONE);
				tableLayoutMedicines.setVisibility(View.GONE);
			}

			TableRow.LayoutParams[] paramsArrayTableRowColumnsHr = new TableRow.LayoutParams[4];
			paramsArrayTableRowColumnsHr[0] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.1),
					(int) (MainContainer.mScreenWidth * 0.08));
			paramsArrayTableRowColumnsHr[1] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.16),
					(int) (MainContainer.mScreenWidth * 0.08));
			paramsArrayTableRowColumnsHr[2] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.40),
					(int) (MainContainer.mScreenWidth * 0.08));
			paramsArrayTableRowColumnsHr[3] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.20),
					(int) (MainContainer.mScreenWidth * 0.08));/*
			paramsArrayTableRowColumnsHr[4] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.24),
					(int) (MainContainer.mScreenWidth * 0.08));*/
			for (int i = 1; i < paramsArrayTableRowColumnsHr.length; i++) {
				paramsArrayTableRowColumnsHr[i].setMargins(2, 0, 0, 0);
			}

			uiContainer.generateTableLabel(textViewHr, "HR");
			uiContainer.generateTableHeader(tableLayoutHr, paramsArrayTableRowColumnsHr, "No.", "Name", "Notification Available",
					"Present");
			generateTableHr(paramsArrayTableRowColumnsHr);

			if (tableLayoutHr.getChildCount() > 1) {
				textViewHr.setVisibility(View.VISIBLE);
				tableLayoutHr.setVisibility(View.VISIBLE);
			} else {
				textViewHr.setVisibility(View.GONE);
				tableLayoutHr.setVisibility(View.GONE);
			}

			TableRow.LayoutParams[] paramsArrayTableRowColumnsHepatitis = new TableRow.LayoutParams[3];
            paramsArrayTableRowColumnsHepatitis[0] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.1),
                    TableRow.LayoutParams.MATCH_PARENT);
            paramsArrayTableRowColumnsHepatitis[1] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.64),
                    TableRow.LayoutParams.MATCH_PARENT);
            paramsArrayTableRowColumnsHepatitis[2] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.16),
                    TableRow.LayoutParams.MATCH_PARENT);
           /* paramsArrayTableRowColumnsHepatitis[3] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.12),
                    TableRow.LayoutParams.MATCH_PARENT);
            paramsArrayTableRowColumnsHepatitis[4] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.12),
                    TableRow.LayoutParams.MATCH_PARENT);
*/			for (int i = 1; i < paramsArrayTableRowColumnsHepatitis.length; i++) {
                paramsArrayTableRowColumnsHepatitis[i].setMargins(2, 0, 0, 0);
			}

			uiContainer.generateTableLabel(textViewHepatitis, "Hepatitis");
			uiContainer.generateTableHeader(tableLayoutHepatitis, paramsArrayTableRowColumnsHepatitis, "No.", "Name", "Total Provided");
			generateTableHepatitis(paramsArrayTableRowColumnsHepatitis);

//			if (tableLayoutHepatitis.getChildCount() > 1) {
//				textViewHepatitis.setVisibility(View.VISIBLE);
//				tableLayoutHepatitis.setVisibility(View.VISIBLE);
//			} else {
//				textViewHepatitis.setVisibility(View.GONE);
//				tableLayoutHepatitis.setVisibility(View.GONE);
//			}


			initClickListeners();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void initClickListeners() {
		buttonPrevious.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				MainContainer.activityCallBackHepatitis.showPreviousFragment();
			}
		});

		buttonNext.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {

			}
		});

		buttonSubmit.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				if(isFormValid()) {
					MainContainer.activityCallBackHepatitis.submitData();
				}else{
					AlertDialogs.getInstance().showDialogOK(null, errorMessage, null, true);
				}
			}
		});
	}


	private void generateTableDisplays(TableRow.LayoutParams[] paramsArrayTableRowColumns) {
		int serialNum = 0;
		int colorBgTableRow = MainContainer.mContext.getResources().getColor(R.color.row_background);
		for (ClassScreenItem display : arrayListDisplays) {
			boolean bIsOdd = serialNum % 2 == 0;
			serialNum++;

			TableRow tableRow = new TableRow(MainContainer.mContext);
			CustomEditText editTextSerialNum = new CustomEditText(bIsOdd, "" + serialNum);
			CustomEditText editTextLocation = new CustomEditText(bIsOdd, display.item_name);
			CustomToggleButtonTwoState toggleButtonDisplayed = new CustomToggleButtonTwoState(bIsOdd);

			editTextSerialNum.customiseEditText(paramsArrayTableRowColumns[0]);
			editTextLocation.customiseEditText(paramsArrayTableRowColumns[1]);
			toggleButtonDisplayed.customiseToggleButton(paramsArrayTableRowColumns[2]);

			tableRow.setBackgroundColor(colorBgTableRow);
			tableRow.addView(editTextSerialNum);
			tableRow.addView(editTextLocation);
			tableRow.addView(toggleButtonDisplayed);
			tableLayoutDisplays.addView(tableRow);
		}
	}


	private void generateTableHr(TableRow.LayoutParams[] paramsArrayTableRowColumns) {
		int serialNum = 0;
		int colorBgTableRow = MainContainer.mContext.getResources().getColor(R.color.row_background);
		for (ClassScreenItem display : arrayListHr) {
			boolean bIsOdd = serialNum % 2 == 0;
			serialNum++;

			TableRow tableRow = new TableRow(MainContainer.mContext);
			CustomEditText editTextSerialNum = new CustomEditText(bIsOdd, "" + serialNum);
			CustomEditText editTextName = new CustomEditText(bIsOdd, display.item_name);
			CustomToggleButtonTwoState toggleButtonAssgined = new CustomToggleButtonTwoState(bIsOdd);
			CustomToggleButtonTwoState toggleButtonPresent = new CustomToggleButtonTwoState(bIsOdd);
			CustomEditText editTextComments = new CustomEditText(bIsOdd);

			editTextSerialNum.customiseEditText(paramsArrayTableRowColumns[0]);
			editTextName.customiseEditText(paramsArrayTableRowColumns[1]);
			toggleButtonAssgined.customiseToggleButton(paramsArrayTableRowColumns[2]);
			toggleButtonPresent.customiseToggleButton(paramsArrayTableRowColumns[3]);
			//editTextComments.customiseEditText(paramsArrayTableRowColumns[4]);

			tableRow.setBackgroundColor(colorBgTableRow);
			tableRow.addView(editTextSerialNum);
			tableRow.addView(editTextName);
			tableRow.addView(toggleButtonAssgined);
			tableRow.addView(toggleButtonPresent);
			//tableRow.addView(editTextComments);
			tableLayoutHr.addView(tableRow);
		}
	}

	private void generateTableEquipmentsDetails(TableRow.LayoutParams[] paramsArrayTableRowColumns) {
		String[] arrayChildsLabels = new String[] { "Functional Count", "Non-Functional Count", "Non-Repairable Count" };
		int colorBgTableRow = MainContainer.mContext.getResources().getColor(R.color.row_background);
		if(arrayListEquipments != null) {
			for (int index = 0; index < arrayListEquipments.size(); index++) {
				boolean bIsOdd = index % 2 == 0;

				TableRow tableRow = new TableRow(MainContainer.mContext);
				CustomEditText editTextSerialNum = new CustomEditText(bIsOdd, "" + (index + 1));
				CustomEditText editTextEquipment = new CustomEditText(bIsOdd, arrayListEquipments.get(index).item_name);
				CustomEditText editTextAvailable = new CustomEditText(bIsOdd);
				CustomEditText editTextFunctional = new CustomEditText(bIsOdd);
				CustomEditText editTextNonFunctional = new CustomEditText(bIsOdd);
				CustomEditText editTextNonRepairable = new CustomEditText(bIsOdd);

				editTextSerialNum.customiseEditText(paramsArrayTableRowColumns[0]);
				editTextEquipment.customiseEditText(paramsArrayTableRowColumns[1]);
				editTextAvailable.customiseEditText(paramsArrayTableRowColumns[2]);
				editTextFunctional.customiseEditText(paramsArrayTableRowColumns[3]);
				editTextNonFunctional.customiseEditText(paramsArrayTableRowColumns[4]);
				//editTextNonRepairable.customiseEditText(paramsArrayTableRowColumns[5]);

				tableRow.setBackgroundColor(colorBgTableRow);
				tableLayoutEquipments.addView(tableRow);

				editTextFunctional.setInputTypeNumberEditable();
				editTextNonFunctional.setInputTypeNumberEditable();
				editTextNonRepairable.setInputTypeNumberEditable();
				editTextAvailable.setInputTypeNumberEditable();

				/*editTextAvailable.setText(MainContainer.searchForValue(arrayListEquipments.get(index).item_id, arrayListEquipmentsTotalAvailable));
				if (bIsEquipmentsTotalAvailableEditable) {
					editTextAvailable.setInputTypeNumberEditable();
				} else {
					editTextAvailable.setInputTypeNumberNonEditable();
				}*/

				EditText[] arrayChildsEditTexts = new EditText[]{editTextFunctional, editTextNonFunctional, editTextNonRepairable};
				TextWatcherCountManyChilds textWatcherCount = new TextWatcherCountManyChilds(editTextAvailable, null, "Available Count",
						arrayChildsEditTexts, arrayChildsLabels, true);
				textWatcherCount.setTextWatcherChilds();
				editTextAvailable.addTextChangedListener(new TextWatcherAutoFill(editTextAvailable, arrayChildsEditTexts));

				tableRow.addView(editTextSerialNum);
				tableRow.addView(editTextEquipment);
				tableRow.addView(editTextAvailable);
				tableRow.addView(editTextFunctional);
				tableRow.addView(editTextNonFunctional);
				//tableRow.addView(editTextNonRepairable);
			}
		}
	}

	private void generateTableSupplies(TableRow.LayoutParams[] paramsArrayTableRowColumns) {
		/*String[] arrayChildsLabels = new String[] { "Admitted Count", "Cured Count", "Stabilization Count" };*/
		int colorBgTableRow = MainContainer.mContext.getResources().getColor(R.color.row_background);
		if(arrayListSupplies != null) {
			for (int index = 0; index < arrayListSupplies.size(); index++) {
				boolean bIsOdd = index % 2 == 0;

				TableRow tableRow = new TableRow(MainContainer.mContext);
				CustomEditText editTextSerialNum = new CustomEditText(bIsOdd, "" + (index + 1));
				CustomEditText editTextEquipment = new CustomEditText(bIsOdd, arrayListSupplies.get(index).item_name);
				CustomEditText editTextAvailable = new CustomEditText(bIsOdd);
				/*CustomEditText editTextFunctional = new CustomEditText(bIsOdd);
				CustomEditText editTextNonFunctional = new CustomEditText(bIsOdd);
				CustomEditText editTextNonRepairable = new CustomEditText(bIsOdd);
				CustomEditText editTextComments = new CustomEditText(bIsOdd);*/

				editTextSerialNum.customiseEditText(paramsArrayTableRowColumns[0]);
				editTextEquipment.customiseEditText(paramsArrayTableRowColumns[1]);
				editTextAvailable.customiseEditText(paramsArrayTableRowColumns[2]);
				/*editTextFunctional.customiseEditText(paramsArrayTableRowColumns[3]);
				editTextNonFunctional.customiseEditText(paramsArrayTableRowColumns[4]);
				editTextNonRepairable.customiseEditText(paramsArrayTableRowColumns[5]);
				editTextComments.customiseEditText(paramsArrayTableRowColumns[6]);*/


				tableRow.setBackgroundColor(colorBgTableRow);
				tableLayoutSupplies.addView(tableRow);

//				editTextFunctional.setInputTypeNumberEditable();
				editTextAvailable.setInputTypeNumberEditable();
//				editTextNonFunctional.setInputTypeNumberEditable();
//				editTextNonRepairable.setInputTypeNumberEditable();

				/*editTextAvailable.setText(MainContainer.searchForValue(arrayListEquipments.get(index).item_id, arrayListEquipmentsTotalAvailable));
				if (bIsEquipmentsTotalAvailableEditable) {
					editTextAvailable.setInputTypeNumberEditable();
				} else {
					editTextAvailable.setInputTypeNumberNonEditable();
				}*/

				/*EditText[] arrayChildsEditTexts = new EditText[]{editTextFunctional, editTextNonFunctional, editTextNonRepairable};
				TextWatcherCountManyChilds textWatcherCount = new TextWatcherCountManyChilds(editTextAvailable, null, "Referred Count",
						arrayChildsEditTexts, arrayChildsLabels);
				textWatcherCount.setTextWatcherChilds();*/

				tableRow.addView(editTextSerialNum);
				tableRow.addView(editTextEquipment);
				tableRow.addView(editTextAvailable);
				/*tableRow.addView(editTextFunctional);
				tableRow.addView(editTextNonFunctional);
				tableRow.addView(editTextNonRepairable);
				tableRow.addView(editTextComments);*/
			}
		}
	}

	private void generateTableMedicines(TableRow.LayoutParams[] paramsArrayTableRowColumns) {
		/*String[] arrayChildsLabels = new String[] { "Admitted Count", "Cured Count", "Stabilization Count" };*/
		int colorBgTableRow = MainContainer.mContext.getResources().getColor(R.color.row_background);
		if(arrayListMedicines != null) {
			for (int index = 0; index < arrayListMedicines.size(); index++) {
				boolean bIsOdd = index % 2 == 0;

				TableRow tableRow = new TableRow(MainContainer.mContext);
				CustomEditText editTextSerialNum = new CustomEditText(bIsOdd, "" + (index + 1));
				CustomEditText editTextEquipment = new CustomEditText(bIsOdd, arrayListMedicines.get(index).item_name);
				CustomEditText editTextAvailable = new CustomEditText(bIsOdd);
				CustomEditText editTextFunctional = new CustomEditText(bIsOdd);
				CustomEditText editTextNonFunctional = new CustomEditText(bIsOdd);
				CustomEditText editTextNonRepairable = new CustomEditText(bIsOdd);
				CustomEditText editTextComments = new CustomEditText(bIsOdd);

				editTextSerialNum.customiseEditText(paramsArrayTableRowColumns[0]);
				editTextEquipment.customiseEditText(paramsArrayTableRowColumns[1]);
				editTextAvailable.customiseEditText(paramsArrayTableRowColumns[2]);
				editTextFunctional.customiseEditText(paramsArrayTableRowColumns[3]);
				editTextNonFunctional.customiseEditText(paramsArrayTableRowColumns[4]);/*
				editTextNonRepairable.customiseEditText(paramsArrayTableRowColumns[5]);
				editTextComments.customiseEditText(paramsArrayTableRowColumns[6]);*/


				tableRow.setBackgroundColor(colorBgTableRow);
				tableLayoutMedicines.addView(tableRow);

				editTextFunctional.setInputTypeNumberEditable();
				editTextAvailable.setInputTypeNumberEditable();
				editTextNonFunctional.setInputTypeNumberEditable();
				editTextNonRepairable.setInputTypeNumberEditable();

				/*editTextAvailable.setText(MainContainer.searchForValue(arrayListEquipments.get(index).item_id, arrayListEquipmentsTotalAvailable));
				if (bIsEquipmentsTotalAvailableEditable) {
					editTextAvailable.setInputTypeNumberEditable();
				} else {
					editTextAvailable.setInputTypeNumberNonEditable();
				}*/

				/*EditText[] arrayChildsEditTexts = new EditText[]{editTextFunctional, editTextNonFunctional, editTextNonRepairable};
				TextWatcherCountManyChilds textWatcherCount = new TextWatcherCountManyChilds(editTextAvailable, null, "Referred Count",
						arrayChildsEditTexts, arrayChildsLabels);
				textWatcherCount.setTextWatcherChilds();*/

				tableRow.addView(editTextSerialNum);
				tableRow.addView(editTextEquipment);
				tableRow.addView(editTextAvailable);
				tableRow.addView(editTextFunctional);
				tableRow.addView(editTextNonFunctional);/*
				tableRow.addView(editTextNonRepairable);
				tableRow.addView(editTextComments);*/
			}
		}
	}

    private void generateTableHepatitis(TableRow.LayoutParams[] paramsArrayTableRowColumns) {
		/*String[] arrayChildsLabels = new String[] { "Admitted Count", "Cured Count", "Stabilization Count" };*/
        int colorBgTableRow = MainContainer.mContext.getResources().getColor(R.color.row_background);
        if(arrayListHepatitis != null) {
            for (int index = 0; index < arrayListHepatitis.size(); index++) {
                boolean bIsOdd = index % 2 == 0;

                TableRow tableRow = new TableRow(MainContainer.mContext);
                CustomEditText editTextSerialNum = new CustomEditText(bIsOdd, "" + (index + 1));
                CustomEditText editTextEquipment = new CustomEditText(bIsOdd, arrayListHepatitis.get(index).item_name);
                CustomEditText editTextAvailable = new CustomEditText(bIsOdd);
/*                CustomEditText editTextFunctional = new CustomEditText(bIsOdd);
                CustomEditText editTextNonFunctional = new CustomEditText(bIsOdd);
                CustomEditText editTextNonRepairable = new CustomEditText(bIsOdd);
                CustomEditText editTextComments = new CustomEditText(bIsOdd);*/

                editTextSerialNum.customiseEditText(paramsArrayTableRowColumns[0]);
                editTextEquipment.customiseEditText(paramsArrayTableRowColumns[1]);
                editTextAvailable.customiseEditText(paramsArrayTableRowColumns[2]);
                /*editTextFunctional.customiseEditText(paramsArrayTableRowColumns[3]);
                editTextNonFunctional.customiseEditText(paramsArrayTableRowColumns[4]);
				editTextNonRepairable.customiseEditText(paramsArrayTableRowColumns[5]);
				editTextComments.customiseEditText(paramsArrayTableRowColumns[6]);*/


                tableRow.setBackgroundColor(colorBgTableRow);
                tableLayoutHepatitis.addView(tableRow);

//                editTextFunctional.setInputTypeNumberEditable();
                editTextAvailable.setInputTypeNumberEditable();
                /*editTextNonFunctional.setInputTypeNumberEditable();
                editTextNonRepairable.setInputTypeNumberEditable();*/

				/*editTextAvailable.setText(MainContainer.searchForValue(arrayListEquipments.get(index).item_id, arrayListEquipmentsTotalAvailable));
				if (bIsEquipmentsTotalAvailableEditable) {
					editTextAvailable.setInputTypeNumberEditable();
				} else {
					editTextAvailable.setInputTypeNumberNonEditable();
				}*/

				/*EditText[] arrayChildsEditTexts = new EditText[]{editTextFunctional, editTextNonFunctional, editTextNonRepairable};
				TextWatcherCountManyChilds textWatcherCount = new TextWatcherCountManyChilds(editTextAvailable, null, "Referred Count",
						arrayChildsEditTexts, arrayChildsLabels);
				textWatcherCount.setTextWatcherChilds();*/

                tableRow.addView(editTextSerialNum);
                tableRow.addView(editTextEquipment);
                tableRow.addView(editTextAvailable);
                /*tableRow.addView(editTextFunctional);
                tableRow.addView(editTextNonFunctional);
				tableRow.addView(editTextNonRepairable);
				tableRow.addView(editTextComments);*/
            }
        }
    }

	@Override
	public void onFragmentShown() {
	}
}